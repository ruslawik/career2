$(document).ready(function () {

    function menu__func(){
        if ($(window).width() < 1200){
            $('.menu__icon').on('click', function () {
                $(this).toggleClass('active');
                if($(this).hasClass('active')){
                    $('.lf-side').addClass('active');
                    $('body').css('overflow', 'hidden');
                    $('.admin__content').addClass('border');

                } else{
                    $('.lf-side').removeClass('active');
                    $('body').css('overflow', 'unset');
                    $('.admin__content').removeClass('border');
                }
            });
            // $('div').removeAttr('data-aos')
        }
    }
    menu__func();

    $(window).resize(function () {
        menu__func()
    });

    $('.last__actions .arrow__down').on('click', function () {
        $(this).siblings('.last__actions__cont').css('max-height', 'unset').parents('.last__actions').addClass('active');
        $(this).remove()
    });

    $('.tab__buttons button').on('click', function () {
        $('.tab__buttons button').removeClass('active');
        $(this).addClass('active').parents('.tab__buttons').siblings('.tab__contents').children().slideUp();

        $('.tab__content[data-tab=' + $(this).data("tab-open") + ']').slideDown()
    });

    $('.decryption__btns button').on('click', function () {
        $('.decryption__btns button').removeClass('active');
        $(this).addClass('active').parents('.test__decryption').siblings('.test__decryption--content').slideUp();

        $('.test__decryption--content[data-tab=' + $(this).data("tab-open") + ']').slideDown()
    });

    $('.goals__btns button').on('click', function () {
        $('.goals__btns button').removeClass('active');
        $(this).addClass('active').parents('.filterBar').siblings('.tab__contents').children().slideUp();

        $('.goals__table[data-tab=' + $(this).data("tab-open") + ']').slideDown()
    });

    $('.test__answers--desc button').on('click', () => {
        $('.test__answers').slideToggle(500);
    });

    $('.admin__sidebar nav li').on('click', function () {
        $('.admin__sidebar nav li').removeClass('active');
        $(this).addClass('active')
    });

    $('input[type=date]').change(function() {
        $(this).addClass('active');
    });

    $('.threePoints').on('click', function() {
        $(this).siblings('.dropdown').toggleClass('show')
    });

    $('.dropdown__list').on('click', '.status', function (event) {
        const current = $(this).parents('.dropdown').siblings('.current');
        const currentStatus = current.children('.status');
        const list = $(this).parents('.dropdown__list');
        list.append(currentStatus.clone()).parents('.dropdown').removeClass('show');
        currentStatus.remove();
        $(this).appendTo(current);

        console.log(list);
    });

    $('body').on('click', function (event) {
        if(!$(event.target).closest('.threePoints').length) {
            $(".dropdown").removeClass('show');
        }
    });


    // AOS.init();

    $('.internship__carousel').flickity({
        cellAlign: 'left',
        contain: true,
        prevNextButtons: false,
        wrapAround: true,
        imagesLoaded: true
    });

    $('select').niceSelect();

    $(".progress-bar").loading();

    // let iframe = document.querySelector("#child-iframe");
    //
    // iframe.addEventListener('load', function() {
    //     iframe.style.height = iframe.contentDocument.body.scrollHeight + 'px';
    // });
});