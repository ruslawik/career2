@extends('user.layout2')

@section('title', $title)

@section('content')

    <div class="content__body" data-aos="zoom-in-left" data-aos-delay="1000">
            <div class="about__profession">
                <a class="goBack__btn">Ваш профиль</a>
                <div class="buy-tests__cont">
                @if($errors->any())
                    <h4 style="color:red;">{{$errors->first()}}</h4>
                    <br>
                @endif
                @if(\Session::has('status'))
                    <h4 style="color:green;">{{\Session::get('status')}}</h4>
                    <br>
                @endif
                    <form action="/user/update-profile" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        @if($img!=NULL)
                            <img style="width:120px;border-radius: 50%;" src="/storage/user-avatars/{{$img}}" alt="">
                            <br>
                        @else
                            <img src="/new/img/user_icon.png" alt="">
                            <br>
                        @endif
                        Аватар (обязательно загрузите квадрат): <input class="buy_input" type="file" name="img" class="form-control" value="{{$name}}"><br>
                        Имя: <input class="buy_input" type="text" name="name" class="form-control" value="{{$name}}"><br>
                        Фамилия: <input class="buy_input" value="{{$surname}}" type="text" name="surname" class="form-control"><br>
                        Email: <input style="background:WhiteSmoke;" disabled value="{{$email}}" class="buy_input" type="text" name="email" class="form-control"><br>
                        Ваш пароль (введите если нужно его сменить): <input class="buy_input" type="password" name="password" class="form-control"><br>
                        <button type="submit" class="go_to_pay">Обновить информацию</button>
                    </form>
                </div>
            </div>
        </div>

@endsection

@section('mapp_pass_javascript')
    <script>
    </script>
@endsection

