<!DOCTYPE html>
<html> 
 <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
      <meta charset="utf-8">
      <title>PDF результаты MAPP</title>

      <style>
         body {
              font-family: "dejavu sans", serif;
              font-size: 12px;
              color: #000;
          }
          table td{
            padding: 5px;
            border:1px solid silver;
          }
          table{
            width:100% !important;
          }
      </style>
  </head>
<!--
<img width=80% src="https://quickchart.io/chart?c={type:'polarArea',data:{labels:['IN1','IN2', 'IN3','IN4', 'IN5'], datasets:[{data:[1,2,3,2,1]}]}}">!-->

<center>
    @include('user.mapp.download-images')
    <br><br>
    Мотивационная оценка личного потенциала
    <br>
    <h3><?php echo Auth::user()->name."&nbsp;"; echo Auth::user()->surname; ?></h3>
    <br><br><br>
    Этот документ представляет собой инструмент для самопознания, используемый при поиске карьеры.<br><br> Это не психологическая оценка. Если есть вопросы, обращайтесь в консалтинговую фирму Career Vision
    <br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br>
    www.careervision.kz
    <br><br>
    <table>
        <tr>
            <td style="border:none !important;width:32%;">MAPP, Copyright 1995-2021</td>
            <td style="border:none !important;width:32%;"></td>
            <td style="border:none !important;">Дата сдачи: {{$date}}</td>
        </tr>
    </table>
</center>
{!!$html!!}



    