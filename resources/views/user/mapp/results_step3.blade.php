@extends('user.layout2')

@section('title', $title)


@section('content')

    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <div class="content__body" data-aos="zoom-in-left" data-aos-delay="1000">
            <div class="test__title">
                <p>Пользователь - {{$username}}</p>
                <p>Дата сдачи - {{$date}}</p>
            </div>
            <div class="test__steps">
                <h3>шаг 3 из 3</h3>
                <div class="step__bar">
                    <div class="step__line" style="width: 100%"></div>
                </div>
            </div>

            <div class="result__cont">
                <div class="chapter">
                    <div class="chapter__header">
                        <h3>{{ __('all.result_3step_mapp_2') }}</h3>
                        <div class="test__btns">
                            <a href="/user/mapp/results/{{$date}}/step2">
                            <button class="prev" type="button">
                                <svg width="24" height="25" viewBox="0 0 24 25" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 18.5L6 12.5L12 6.5" stroke="#40189D" stroke-width="2"
                                          stroke-linecap="round" stroke-linejoin="round"/>
                                    <path d="M19 18.5L13 12.5L19 6.5" stroke="#40189D" stroke-width="2"
                                          stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                                {{ __('all.prev') }}
                            </button>
                            </a>
{{--                            <a href="/user/mapp/results/{{$date}}/step4">--}}
{{--                            <button class="next" type="button">--}}
{{--                                {{ __('all.next') }}--}}
{{--                                <svg width="24" height="25" viewBox="0 0 24 25" fill="none"--}}
{{--                                     xmlns="http://www.w3.org/2000/svg">--}}
{{--                                    <path d="M12 18.5L18 12.5L12 6.5" stroke="#40189D" stroke-width="2"--}}
{{--                                          stroke-linecap="round" stroke-linejoin="round"/>--}}
{{--                                    <path d="M5 18.5L11 12.5L5 6.5" stroke="#40189D" stroke-width="2"--}}
{{--                                          stroke-linecap="round" stroke-linejoin="round"/>--}}
{{--                                </svg>--}}
{{--                            </button>--}}
{{--                            </a>--}}
                        </div>
                    </div>
                    <div class="chapter__body">
                        <?php
                            if(app()->getLocale()=="ru"){
                        ?>
                        <div class="chapter__video">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/DVmiw9itUyg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                        <?php
                            }
                            if(app()->getLocale()=="kz"){
                        ?>
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/_iNZHVbc1Nk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        <?php
                            }
                        ?>
                    </div>
                </div>
            </div>
            <div class="table__cont">
                <table>
                    <thead>
                        <tr>
                            <th>{{ __('all.types_of_work') }}</th>
                            <th>{{ __('all.motivation_perc') }}</th>
                            <th>{{ __('all.motivation_level') }}</th>
                            <th>{{ __('all.potent_kurstar') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $i=0;
                        ?>
                        @foreach($all_jobs[1] as $job)
                        <tr>
                            <td>{{$job}}</td>
                            <td>{{$all_jobs[2][$i]}}</td>
                            <td>{{$all_jobs[3][$i]}}</td>
                            <td>
                                <a target="_blank" href="/user/course/search?q={{$job}}" class="goTo">{{ __('all.go_to_courses') }}</a>
                            </td>
                        </tr>
                        <?php
                            $i++;
                            if($i==15){
                                break;
                            }
                        ?>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

@endsection
