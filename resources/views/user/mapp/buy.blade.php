@extends('user.layout2')

@section('title', $title)


@section('content')

    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <div class="content__body" data-aos="zoom-in-left" data-aos-delay="1000">
            <div class="about__profession">
                <a class="goBack__btn">Покупка теста MAPP</a>
                <input type="hidden" id="mapp_price" value="{{$mapp_price[0]->value}}">
                <div class="buy-tests__cont">
                    <p id="loader" style="display: none;">
                        <img src="/user_res/loader.gif" width=80><br>
                         <b>Готовим форму покупки... Подождите, пожалуйста...</b>
                     </p>
                     <p id="loader2" style="display: none;">
                        <img src="/user_res/loader.gif" width=80><br>
                         <b>Не закрывайте и не перезагружайте страницу пока данное сообщение не исчезнет...</b>
                     </p>
                     <p id="form">
                     </p>
                </div>
            </div>
        </div>

@endsection

@section('mapp_pass_javascript')
    <script src="https://widget.cloudpayments.ru/bundles/cloudpayments"></script>
    <script>

        this.pay = function () {
            var widget = new cp.CloudPayments();
            var mapp_price = parseInt($("#mapp_price").val());
            widget.charge({ // options
                publicId: 'pk_fb30e07a2d7e7748f7a6a5052c38d',  //id из личного кабинета
                description: 'Оплата теста MAPP: '+jQuery("#mapp_price").val()+' тенге', //назначение
                amount: mapp_price, //сумма
                currency: 'KZT', //валюта
                //invoiceId: '1234567', //номер заказа  (необязательно)
                accountId: jQuery("#Email").val(), //идентификатор плательщика (необязательно)
                skin: "classic", //дизайн виджета
                    data: {
                        myProp: 'myProp value' //произвольный набор параметров
                    }
                },
                function (options) { // success
                    jQuery("#loader2").toggle('fast');
                    jQuery("#form").toggle('fast');
                    jQuery("#reg_form").submit();
                },
                function (reason, options) { // fail
                    //alert("FAIL");
                });
        };   

        function go_to_pay(){
            ok = true;
            if (jQuery("#FirstName").val().length < 3) {
                alert("Поле Имя обязательно для заполнения!", "Заполните данные");
                ok = false;
            }
            if (jQuery("#MiddleInitial").val().length < 2) {
                alert("Поле Фамилия обязательно для заполнения!", "Заполните данные");
                ok = false;
            }
            if (jQuery("#Email").val().length < 1) {
                alert("Поле Email обязательно для заполнения!", "Заполните данные");
                ok = false;      
            }
            if (jQuery("#Password").val().length < 2) {
                alert("Поле Ваш пароль обязательно для заполнения!", "Заполните данные");
                ok = false;    
            }
            if(ok){
                pay();
            }
        }

        function load_form(){
            jQuery.ajax({
                type: "POST",
                url: "/user/mapp/get-buy-form",
                beforeSend: function(){
                    jQuery("#loader").toggle('fast');
                },
                data: {_token: '{{csrf_token()}}'},
                success: function(answer) {
                    jQuery("#loader").toggle('fast');
                    jQuery("#form").html(answer);
                },
                error: function(xhr){
                    alert("Что-то пошло не так. Попробуйте еще раз либо обратитесь к техническому администратору. Ошибка: " + xhr.status + " " + xhr.statusText);
                    jQuery("#loader").toggle('fast');
                }
            });
        }
        load_form();

        function open_promo_field(){
            jQuery("#promocode_form").toggle('fast');
        }
    </script>
@endsection

