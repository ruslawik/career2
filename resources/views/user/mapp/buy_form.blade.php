<h3>Для прохождения теста МАПП необходимо заполнить все поля и оплатить 30 000 тенге </h3>
<form action="/auto-register-post" method="POST" id="reg_form">
    {{ csrf_field() }}
    Имя: <input class="buy_input" type="text" name="FirstName" id="FirstName" class="form-control" value="{{$name}}"><br>
    Фамилия: <input class="buy_input" value="{{$surname}}" type="text" id="MiddleInitial" name="MiddleInitial" class="form-control"><br>
    Email: <input value="{{$email}}" class="buy_input" type="text" name="Email" id="Email" class="form-control"><br>
    Ваш пароль: <input class="buy_input" type="password" name="Password" id="Password" class="form-control"><br>
    <input id="AccountNumber" name="AccountNumber" type="hidden" value="" />
    <input id="KeyCode" name="KeyCode" type="hidden" value="{{$KeyCode}}" />
    <input name="cookies_file_id" type="hidden" value="{{$cookies_file_id}}" />
    <input name="request_token" type="hidden" value="{{$request_token}}" />
</form>
<a href="#/" onClick="go_to_pay();" class="go_to_pay">Перейти к оплате</a>
<a href="#/" style="margin-left:5px;font-size:14px !important;" onClick="open_promo_field();">У меня есть промо-код</a>
<form action="/user/mapp/promo" method="POST" id="promocode_form" style="display: none;">
    {{csrf_field()}}
    Введите промо-код: <input class="buy_input" type="text" name="promocode" id="promocode" class="form-control"><br>
     <button type="submit" class="go_to_pay">Отправить</button>
</form>
