@extends('user.layout2')

@section('title', $title)

@section('content')

    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <script src="/js/d3.v3.min.js"></script>
    <script src="/js/d3.tip.v0.6.3.js"></script>
    <script src="/js/drawMatchGraph.js"></script>


    <style>
    .axis path,
    .axis line {
        fill: none;
        stroke: #000;
        shape-rendering: crispEdges;
    }

    .bar {
        fill: orange;
    }

    .solidArc:hover {
        fill: orangered;
    }

    .solidArc {
        -moz-transition: all 0.3s;
        -o-transition: all 0.3s;
        -webkit-transition: all 0.3s;
        transition: all 0.3s;
    }

    .x.axis path {
        display: none;
    }

    .mappMatchingResults-matchLevel {
        line-height: 1;
        font-weight: bold;
        font-size: x-large;
    }

    .d3-tip {
        line-height: 1;
        font-weight: bold;
        padding: 12px;
        background: rgba(0, 0, 0, 0.8);
        color: #fff;
        border-radius: 2px;
    }

        /* Creates a small triangle extender for the tooltip */
        .d3-tip:after {
            box-sizing: border-box;
            display: inline;
            font-size: 10px;
            width: 100%;
            line-height: 1;
            color: rgba(0, 0, 0, 0.8);
            content: "\25BC";
            position: absolute;
            text-align: center;
        }

        /* Style northward tooltips differently */
        .d3-tip.n:after {
            margin: -1px 0 0 0;
            top: 100%;
            left: 0;
        }

    .table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td {
        border: 1px solid #2980B9;
        padding: 10px;
    }
</style>

 <div class="content__body" data-aos="zoom-in-left" data-aos-delay="1000">
        <!--
    <a href="?lang=ru"><button class="btn btn-success">Русский</button></a>
    <a href="?lang=kz"><button class="btn btn-success">Казахский</button></a>
    <a href="?lang=en"><button class="btn btn-success">Английский</button></a><br><br>
    !-->
    <table>
        <tr>
            <td>
        <a href="#"><h4 onClick="window.history.back();">< {{ __('all.back_to_results') }}</h4></a>
    </td>
</tr>
    <tr>
        <td>
        @if($lang == "ru")
           <h4>{{$job[0]['job_name_rus']}}</h4><br>
            {{$job[0]['job_desc_rus']}}<br>
        @elseif($lang == "kz")
            <h4>{{$job[0]['job_name_kaz']}}</h4><br>
            {{$job[0]['job_desc_kaz']}}<br>
        @elseif($lang == "en")
            <h4>{{$job[0]['job_name']}}</h4><br>
            {{$job[0]['job_desc']}}<br>
        @endif
    </td>
    </tr>
</table>


        <br>
        <!--
        <b>Университеты в Казахстане, куда Вы можете поступить по этой профессии:</b>
        <br>
        {!!$job[0]['univers']!!}
        <hr>!-->
        <div class="table__cont" style="padding:20px;">
        <span id="MAPPMatchGraphContainer" name="MAPPMatchGraphContainer">
                </span><br><br>
                            <table class="table table-striped">
                                <tbody>
                                    @foreach($job_details as $detail)
                                        <tr>
                                            <td>
                                               <?php
                                                    if($detail->detail['subcategory']=="IN") $IN = $detail['percent'];
                                                    if($detail->detail['subcategory']=="TE") $TE = $detail['percent'];
                                                    if($detail->detail['subcategory']=="AP") $AP = $detail['percent'];
                                                    if($detail->detail['subcategory']=="PE") $PE = $detail['percent'];
                                                    if($detail->detail['subcategory']=="TH") $TH = $detail['percent'];
                                                    if($detail->detail['subcategory']=="DA") $DA = $detail['percent'];
                                                    if($detail->detail['subcategory']=="RE") $RE = $detail['percent'];
                                                    if($detail->detail['subcategory']=="MA") $MA = $detail['percent'];
                                                    if($detail->detail['subcategory']=="LA") $LA = $detail['percent'];
                                               ?>
                                               @if($detail->detail['subcategory'] != $detail->detail['category'])
                                                    {{$detail->detail['subcategory']}} - 
                                               @endif
                                               @if($detail->detail['subcategory'] == $detail->detail['category'])
                                               <b>
                                               @endif

                                                @if($lang == "ru")
                                                        {{$detail->detail['detail_name_rus']}}
                                                @elseif($lang == "kz")
                                                        {{$detail->detail['detail_name_kaz']}}
                                                @elseif($lang == "en")
                                                        {{$detail->detail['detail_name']}}
                                                @endif

                                               <br>
                                               @if($detail->detail['subcategory'] == $detail->detail['category'])
                                               </b>
                                               @endif
                                            </td>
                                            <td>
                                                <center>{{$detail['percent']}}%</center>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

@endsection

@section('mapp_pass_javascript')
     <script>
        jQuery(document).ready(function () {
            var data = [
                {id:"IN", order:1, score:{{$IN}}, weight:1, color:"#8080FF", label:"INTEREST IN JOB CONTENT"},
                {id:"TE", order:2, score:{{$TE}}, weight:1, color:"#802060", label:"TEMPERAMENT FOR THE JOB "},
                {id:"AP", order:3, score:{{$AP}}, weight:1, color:"#FFFFA0", label:"APTITUDE FOR THE JOB"},
                {id:"PE", order:4, score:{{$PE}}, weight:1, color:"#A0E0E0", label:"PEOPLE "},
                {id:"TH", order:5, score:{{$TH}}, weight:1, color:"#600080", label:"THINGS "},
                {id:"DA", order:6, score:{{$DA}}, weight:1, color:"#FF8080", label:"DATA"},
                {id:"RE", order:7, score:{{$RE}}, weight:1, color:"#008080", label:"REASONING "},
                {id:"MA", order:8, score:{{$MA}}, weight:1, color:"#C0C0FF", label:"MATHEMATICAL CAPACITY "},
                {id:"LA", order:9, score:{{$LA}}, weight:1, color:"#000080", label:"LANGUAGE CAPACITY"}
            ];
            var overallMAPPMatchScore = 1;
            DisplayMAPPMatchGraph(overallMAPPMatchScore, data);
        });
    </script>
@endsection
