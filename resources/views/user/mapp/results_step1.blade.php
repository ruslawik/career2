@extends('user.layout2')

@section('title', $title)

@section('mapp_css')

    <link href="http://recruit.assessment.com/Content/bootstrap.css" rel="stylesheet"/>

    <link rel="stylesheet" href="https://rawgit.com/tpreusse/radar-chart-d3/master/src/radar-chart.css">
            <style>
                .radar-chart .area {
                    fill-opacity: 0.7;
                }

                .radar-chart.focus .area {
                    fill-opacity: 0.3;
                }

                .radar-chart.focus .area.focused {
                    fill-opacity: 0.9;
                }

                .inner_content {
                    max-width: 900px;
                    margin: auto auto;
                }
            </style>

@endsection

@section('content')

    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <div class="content__body" data-aos="zoom-in-left" data-aos-delay="1000">
            <div class="test__title">
                <p>Пользователь - {{ $username}}</p>
                <p>Дата сдачи - {{$date}}</p>
            </div>
            <div class="test__steps">
                <h3>шаг 1 из 3</h3>
                <div class="step__bar">
                    <div class="step__line" style="width: 33%"></div>
                </div>
            </div>

            <div class="result__cont">
                <div class="chapter">
                    <div class="chapter__header">
                        <h3>{{ __('all.result_1step_mapp_2') }}</h3>
                        <div class="test__btns">
                            <button class="prev" type="button" disabled>
                                <svg width="24" height="25" viewBox="0 0 24 25" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 18.5L6 12.5L12 6.5" stroke="#40189D" stroke-width="2"
                                          stroke-linecap="round" stroke-linejoin="round"/>
                                    <path d="M19 18.5L13 12.5L19 6.5" stroke="#40189D" stroke-width="2"
                                          stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                                {{ __('all.prev') }}
                            </button>
                            <a href="/user/mapp/results/{{$date}}/step2">
                            <button class="next" type="button">
                                {{ __('all.next') }}
                                <svg width="24" height="25" viewBox="0 0 24 25" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 18.5L18 12.5L12 6.5" stroke="#40189D" stroke-width="2"
                                          stroke-linecap="round" stroke-linejoin="round"/>
                                    <path d="M5 18.5L11 12.5L5 6.5" stroke="#40189D" stroke-width="2"
                                          stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                            </button>
                            </a>
                        </div>
                    </div>
                    <div class="chapter__body">
                        <?php if(app()->getLocale() == "ru"){ ?>
                        <div class="chapter__desc">
                            <p>Здесь вы увидите свой уровень мотивации к работе по 7 факторам. В каждом из этих факторов
                                есть подфакторы или черты (критерии работы), определяющие наиболее подходящие для
                                вас.</p>

                            <ul>
                                <li>1 означает - самая высокая мотивация, самый подходящий вариант</li>
                                <li>2 означает - выше среднего</li>
                                <li>3 означает - средний уровень мотивации</li>
                                <li>4 означает - ниже среднего</li>
                                <li>5 означает - самый низкий уровень мотивации</li>
                            </ul>

                            <p>
                                Вы можете сосредоточиться на 1 и 2 и указать, что 4 и 5 - это задачи, которые вы
                                предпочитаете избегать, и часто даже являются прямыми противоположностями ваших главных
                                черт. Если у вас нет 1 или 2, вы можете сосредоточиться на 3.
                            </p>

                        </div>
                        <div class="chapter__video">
                            <iframe height="315" src="https://www.youtube.com/embed/kG7umeAvpQM" frameborder="0"
                                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                    allowfullscreen></iframe>
                        </div>
                        <p>
                            Люди с большим количеством единиц обычно имеют много пятерок. Это может облегчить поиск
                            работы для человека, потому что они могут точно знать, что они хотят. Тем не менее, это
                            также может быть сложной задачей, потому что они могут быть не такими гибкими, если им
                            приходится выполнять задачи, которые им не нравятся. Люди с большим количеством 2 и 3
                            немного более гибки в том, что они предпочитают делать. Это может быть сложной задачей,
                            потому что может быть трудно точно определить подходящую работу. Это также может быть
                            полезно, когда индивидуума просят выполнить несколько различных задач, и он не имеет сильных
                            предпочтений в пользу или против задачи.
                        </p>
                        <?php }?>
                        <?php if(app()->getLocale() == "kz"){ ?>
                        <div class="chapter__desc">
                            <p>Бұл жерде сіз жұмысқа деген ынта көрсеткішін 7 факторға байланысты көре аласыз. Бұл жетеуі өз ішінде бірнеше кішігірім факторларды қамтиды. Олар ең әсерлі факторларды анықтауға септігін тигізеді.</p>

                            <ul>
                                <li>1 - ең қатты әсер ететін мотивация, ең сәйкес нұсқа</li>
                                <li>2 - орташадан жоғары</li>
                                <li>3 - орташа ынта</li>
                                <li>4 - орташадан төмен</li>
                                <li>5 - ынтаның ең төменгі дәрежесі дегенді білдіреді</li>
                            </ul>

                            <p>
                                Сіз 1 мен 2-ні таңдап, 4 пен 5-тің қабілеттеріңізге қайшы келетінін айтуыңыз мүмкін. Тіпті олармен бетпе-бет келмес үшін айналып өтесіз. Егер, 1 мен 2 болмаған жағдайда, 3-ке көңіл бөлсеңіз болады. Әдетте, адамдарда 1 мен 5 қатар жүреді.
                            </p>

                        </div>
                        <div class="chapter__video">
                            <iframe height='315' src='https://www.youtube.com/embed/Q9RTnBkcqno' frameborder='0' allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe></center><br>
                        </div>
                        <p>
                            Яғни, олар не қалайтынын жақсы білгендіктен жұмыс іздеу барысы оңайға соғады, бірақ кей жұмыстар оларға ұнамай қалса, олардың ұнамайтын жұмысқа икемделуі қиындық тудырады. Ал, 2 мен 3-тер қай жұмысты көбірек ұнататынын білмесе де, жұмысты бітіру мәселесінде икемдірек келеді. Яғни, бірнеше тапсырма берілгенде, олар тапсырманың ұнап- ұнамағандығынан гөрі тапсырманы бітіруге көбірек көңіл бөледі.
                        </p>
                        <?php }?>
                    </div>
                </div>
                <div class="chapter__results">
                    <h2 class="section__title">{{ __('all.result_1step_mapp') }}</h2>
                    <div class="chapter__results--cont">
                        <div class="tab__buttons">
                            <button type="button" class="active" data-tab-open="chapter__result--1" onClick='getBlock(0);'>
                                <h4>{{ __('all.interes') }} ({{$interes_average}})</h4>
                                <div class="step__bar">
                                    <div class="step__line" style="width: {{$interes_perc}}%; background: {{$interes_color}};"></div>
                                </div>
                            </button>
                            <button type="button" data-tab-open="chapter__result--2" onClick='getBlock(1);'>
                                <h4>{{ __('all.rab_sreda') }} ({{$rabo4aya_sreda_average}})</h4>
                                <div class="step__bar">
                                    <div class="step__line" style="width: {{$rabo4aya_sreda_perc}}%; background: {{$rabo4aya_sreda_color}};"></div>
                                </div>
                            </button>
                            <button type="button" data-tab-open="chapter__result--3" onClick='getBlock(2);'>
                                <h4>{{ __('all.soc_roli') }} ({{$social_roli_average}})</h4>
                                <div class="step__bar">
                                    <div class="step__line" style="width: {{$social_roli_perc}}%; background: {{$social_roli_color}};"></div>
                                </div>
                            </button>
                            <button type="button" data-tab-open="chapter__result--4" onClick='getBlock(3);'>
                                <h4>{{ __('all.materials') }} ({{$materialy_average}})</h4>
                                <div class="step__bar">
                                    <div class="step__line" style="width: {{$materialy_perc}}%; background: {{$materialy_color}};"></div>
                                </div>
                            </button>
                            <button type="button" data-tab-open="chapter__result--5" onClick='getBlock(4);'>
                                <h4>{{ __('all.data') }} ({{$dannye_average}})</h4>
                                <div class="step__bar">
                                    <div class="step__line" style="width: {{$dannye_perc}}%; background: {{$dannye_color}};"></div>
                                </div>
                            </button>
                            <button type="button" data-tab-open="chapter__result--6" onClick='getBlock(5);'>
                                <h4>{{ __('all.mat_pot') }} ({{$mat_pot_average}})</h4>
                                <div class="step__bar">
                                    <div class="step__line" style="width: {{$mat_pot_perc}}%; background: {{$mat_pot_color}};"></div>
                                </div>
                            </button>
                            <button type="button" data-tab-open="chapter__result--7" onClick='getBlock(6);'>
                                <h4>{{ __('all.yaz_pot') }} ({{$yaz_pot_average}})</h4>
                                <div class="step__bar">
                                    <div class="step__line" style="width: {{$yaz_pot_perc}}%; background: {{$mat_pot_color}};"></div>
                                </div>
                            </button>
                        </div>
                        <div class="tab__contents">
                            <div class="tab__content active" data-tab="chapter__result--1">
                                {!!$interes_k_rabote!!}
                            </div>
                            <div class="tab__content" data-tab="chapter__result--2">
                                {!!$rabo4aya_sreda!!}
                            </div>
                            <div class="tab__content" data-tab="chapter__result--3">
                                {!!$social_roli!!}
                            </div>
                            <div class="tab__content" data-tab="chapter__result--4">
                                {!!$materialy!!}
                            </div>
                            <div class="tab__content" data-tab="chapter__result--5">
                                {!!$dannye!!}
                            </div>
                            <div class="tab__content" data-tab="chapter__result--6">
                                {!!$mat_pot!!}
                            </div>
                            <div class="tab__content" data-tab="chapter__result--7">
                                {!!$yaz_pot!!}
                            </div>

                        </div>
                        <div id="loader" style="display: none;">Загружаем данные... Подождите...</div>
                        <div class="tab__contents text__contents">
                            <div class="tab__content active" data-tab="chapter__result--1" id="chapter0">
                            </div>
                            <div class="tab__content" data-tab="chapter__result--2" id="chapter1">

                            </div>
                            <div class="tab__content" data-tab="chapter__result--3" id="chapter2">

                            </div>
                            <div class="tab__content" data-tab="chapter__result--4" id="chapter3">

                            </div>
                            <div class="tab__content" data-tab="chapter__result--5" id="chapter4">

                            </div>
                            <div class="tab__content" data-tab="chapter__result--6" id="chapter5">

                            </div>
                            <div class="tab__content" data-tab="chapter__result--7" id="chapter6">

                            </div>
                        </div>
                    </div>

@endsection

@section('mapp_pass_javascript')
    <script src="//d3js.org/d3.v3.min.js"></script>
    <script src="http://recruit.assessment.com//Scripts/bootstrap.js"></script>
    <script src="http://recruit.assessment.com//Scripts/respond.js"></script>
    <script src="https://recruit.assessment.com//Scripts/RadarChart.js"></script>
    <script src="https://recruit.assessment.com//Scripts/RadarChartCreation.js"></script>
    <script>
        function CreateNewRadarGraph(graphData, graphName) {
                    var data = FormatGraphData(graphData);
                    RadarChart.draw(graphName, data, maxValue = 5, minValue = 0, levels = 5);
        }
        function getBlock(chapter){

            jQuery.ajax({
                type: "POST",
                url: "/user/mapp/results/getRuDescForMappStep1",
                beforeSend: function(){
                    jQuery("#loader").toggle('fast');
                },
                data: {block: chapter, _token: '{{csrf_token()}}'},
                success: function(answer) {
                    jQuery("#chapter"+chapter).html(answer);
                    jQuery("#loader").toggle('fast');
                },
                error: function(xhr){
                    alert("Что-то пошло не так. Попробуйте еще раз либо обратитесь к техническому администратору. Ошибка: " + xhr.status + " " + xhr.statusText);
                    jQuery("#loader").toggle('fast');
                }
            });

        }
        getBlock(0);
    </script>
 {!!$scripts!!}
@endsection
