@extends('user.layout2')

@section('title', $title)

@section('content')

    <div class="content__body" data-aos="zoom-in-left" data-aos-delay="1000">
            <div class="filterBar test__results__filterBar">
                <div class="filter__desc">
                    <h3>Список тестов</h3>
                    <p>Выберите для просмотра результатов</p>
                </div>
                <div class="filter__btns">
                    <button type="button" class="active">MAPP</button>
                    <a href="/user/disc/results"><button type="button">DISC</button></a>
                    <a href="/user/solomin/results"><button type="button">Ориентация им. И.Л. Соломина</button></a>
                </div>
            </div>

            <div class="tab__contents">
                <div class="goals__table active" data-tab="all">
                    <table>
                        <thead>
                        <tr>
                            <th class="goal__date">Дата сдачи</th>
                            <th class="goal">Тест</th>
                            <th class="goal__status">Результат</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($mapp_results as $key => $result)
                        <tr>
                            <td class="goal__date">{{ $key }}</td>
                            <td class="goal">
                                <h3>MAPP</h3>
                                <h4>Профориентационный тест</h4>
                            </td>
                            <td class="goal__status">
                                <a href="/user/mapp/results/{{$key}}/step1">
                                <div class="current" style="cursor:pointer;">
                                    <div class="status yes">Посмотреть</div>
                                </div>
                                </a>
                                <a href="/user/mapp/results/download/{{$key}}" target="_blank">
                                    <div class="current" style="cursor:pointer;">
                                        <div class="status yes">Скачать</div>
                                    </div>
                                </a>
                                @if(app()->getLocale() == 'kz')
                                    <a href="https://certs.careervision.kz/download-mapp-cert/{{\Auth::user()->email}}/kz" target="_blank">
                                        <div class="current" style="cursor:pointer;">
                                            <div class="status yes">Сертификат</div>
                                        </div>
                                    </a>
                                @else
                                    <a href="https://certs.careervision.kz/download-mapp-cert/{{\Auth::user()->email}}" target="_blank">
                                        <div class="current" style="cursor:pointer;">
                                            <div class="status yes">Сертификат</div>
                                        </div>
                                    </a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>

@endsection
