@extends('user.layout2')

@section('title', $title)


@section('content')

    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <div class="content__body" data-aos="zoom-in-left" data-aos-delay="1000">
            <div class="test__title">
                <p>Пользователь - {{$username}}</p>
                <p>Дата сдачи - {{$date}}</p>
            </div>
            <div class="test__steps">
                <h3>шаг 4 из 4</h3>
                <div class="step__bar">
                    <div class="step__line" style="width: 100%"></div>
                </div>
            </div>

            <div class="result__cont">
                <div class="chapter">
                    <div class="chapter__header">
                        <h3>{{ __('all.one_net_tizimi') }}</h3>
                        <div class="test__btns">
                            <a href="/user/mapp/results/{{$date}}/step3">
                            <button class="prev" type="button">
                                <svg width="24" height="25" viewBox="0 0 24 25" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 18.5L6 12.5L12 6.5" stroke="#40189D" stroke-width="2"
                                          stroke-linecap="round" stroke-linejoin="round"/>
                                    <path d="M19 18.5L13 12.5L19 6.5" stroke="#40189D" stroke-width="2"
                                          stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                                {{ __('all.prev') }}
                            </button>
                            </a>
                            <button class="next" disabled type="button">
                                {{ __('all.next') }}
                                <svg width="24" height="25" viewBox="0 0 24 25" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 18.5L18 12.5L12 6.5" stroke="#40189D" stroke-width="2"
                                          stroke-linecap="round" stroke-linejoin="round"/>
                                    <path d="M5 18.5L11 12.5L5 6.5" stroke="#40189D" stroke-width="2"
                                          stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                            </button>
                        </div>
                    </div>
                    <div class="chapter__body">
                        <?php
                            if(app()->getLocale()=="ru"){
                        ?>
                            В этом разделе представлен список подходящих профессий.<br>
                            Данный список может отличаться от 3 части отчета, так как 4 часть показывает ваш потенциал, а 3 часть показывает наивысший уровень мотивации в видах деятельности.
                        <?php
                            }
                            if(app()->getLocale()=="kz"){
                        ?>
                            Бұл бөлімде тиісті мамандықтардың тізімі келтірілген.<br>
                            Бұл тізім есептің 3-бөлімінен өзгеше болуы мүмкін, өйткені 4-бөлім сіздің әлеуетіңізді, ал 3-бөлім іс-әрекеттегі мотивацияның ең жоғары деңгейін көрсетеді.
                        <?php
                            }
                        ?>
                    </div>
                </div>
            </div>
            <div class="table__cont">
                <table>
                    <thead>
                        <tr>
                            <th><center>{{ __('all.profession') }}</center></th>
                            <th>{{ __('all.description') }}</th>
                            <th>{{ __('all.level_of_saykes') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                    <tr id="loader" style="display: none;">
                        <td>
                        <img src="/user_res/loader.gif" width=80><br>
                         <b>Результаты готовятся... <br>Не перезагружайте страницу... (до 3-ёх минут)</b>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    @if($not_loaded != 1)
                        @foreach($jobs as $job)
                            @if($job->job['not_in_kz']!=1)
                            <tr>
                                <td><a class="mapp_job_one_net_link" href="/user/mapp_job/{{$job->job['sys_id']}}">
                                    <?php
                                        if(app()->getLocale()=="ru"){
                                    ?>
                                            {{$job->job['job_name_rus']}}
                                    <?php
                                        }
                                        if(app()->getLocale()=="kz"){
                                    ?>
                                        {{$job->job['job_name_kaz']}}
                                    <?php
                                        }
                                    ?>
                                </a></td>
                                <td class="table__desc">
                                    <?php
                                        if(app()->getLocale()=="ru"){
                                    ?>
                                            {{$job->job['job_desc_rus']}}
                                    <?php
                                        }
                                        if(app()->getLocale()=="kz"){
                                    ?>
                                            {{$job->job['job_desc_kaz']}}
                                    <?php
                                        }
                                    ?>
                                </td>
                                <td><div class="color__violet">{{$job['percent']}}%</div></td>
                            </tr>
                            @endif
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
@endsection

@section('mapp_pass_javascript')
    <script>
        function load_onenet(){
            jQuery.ajax({
                type: "POST",
                url: "/user/mapp/load_onenet",
                beforeSend: function(){
                    jQuery("#loader").toggle('fast');
                },
                data: {_token: '{{csrf_token()}}'},
                success: function(answer) {
                    jQuery("#loader").toggle('fast');
                    location.reload();
                },
                error: function(xhr){
                    alert("Что-то пошло не так. Попробуйте еще раз либо обратитесь к техническому администратору. Ошибка: " + xhr.status + " " + xhr.statusText);
                    jQuery("#loader").toggle('fast');
                }
            });
        }
        @if($not_loaded == 1)
            load_onenet();
        @endif
    </script>
@endsection

