@extends('user.layout2')

@section('title', $title)


@section('content')

    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <div class="content__body" data-aos="zoom-in-left" data-aos-delay="1000">
            <div class="about__profession">
                <a class="goBack__btn">Покупка теста MAPP</a>
                <input type="hidden" id="mapp_price" value="{{$mapp_price[0]->value}}">
                <div class="buy-tests__cont">
                    <a href="#/" style="font-size:14px !important;">У меня есть промо-код</a>
                    <form action="/user/mapp/promo" method="POST" id="promocode_form">
                        {{csrf_field()}}
                        Введите промо-код: <input class="buy_input" type="text" name="promocode" id="promocode" class="form-control"><br>
                         <button type="submit" class="go_to_pay">Отправить промо-код</button>
                    </form>
                    <br>
                    <hr>
                    <br>
                    <a href="#/" style="font-size:14px !important;">Если у Вас нет промокода и Вы хотите оплатить тест картой</a>
                    <a href="/user/mapp/buy">
                        <button type="submit" class="go_to_pay">Оплатить картой онлайн</button>
                    </a>
                </div>
            </div>
        </div>

@endsection

@section('mapp_pass_javascript')
@endsection

