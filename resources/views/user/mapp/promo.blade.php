@extends('user.layout2')

@section('title', $title)


@section('content')

    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <div class="content__body" data-aos="zoom-in-left" data-aos-delay="1000">
            <div class="about__profession">
                <a class="goBack__btn">Покупка теста MAPP</a>
                <div class="buy-tests__cont">
                    <p id="loader" style="display: none;">
                        <img src="/user_res/loader.gif" width=80><br>
                         <b>Отправляем промо-код на сервер... Подождите, пожалуйста...</b>
                     </p>
                     <p id="loader2" style="display: none;">
                        <img src="/user_res/loader.gif" width=80><br>
                         <b>Не закрывайте и не перезагружайте страницу пока данное сообщение не исчезнет...</b>
                     </p>
                     <p id="form">
                     </p>
                </div>
            </div>
        </div>

@endsection

@section('mapp_pass_javascript')
    <script>
        function load_form(){
            jQuery.ajax({
                type: "POST",
                url: "/user/mapp/get-promo-form",
                beforeSend: function(){
                    jQuery("#loader").toggle('fast');
                },
                data: {code: '{{$code}}', _token: '{{csrf_token()}}'},
                success: function(answer) {
                    jQuery("#loader").toggle('fast');
                    jQuery("#form").html(answer);
                },
                error: function(xhr){
                    alert("Что-то пошло не так. Попробуйте еще раз либо обратитесь к техническому администратору. Ошибка: " + xhr.status + " " + xhr.statusText);
                    jQuery("#loader").toggle('fast');
                }
            });
        }
        load_form();
        function send(){
            jQuery("#loader2").toggle('fast');
            jQuery("#form").toggle('fast');
            jQuery("#reg_form").submit();
        }
    </script>
@endsection

