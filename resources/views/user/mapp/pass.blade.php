@extends('user.layout')

@section('title', $title)

@section('content')

     @if (session('status'))
        <div class="alert alert-danger">
            {{ session('status') }}
        </div>
    @endif

        <center><iframe width="560" height="315" src="https://www.youtube.com/embed/Nq-kfwdmk2w" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></center>

        @if($passed == '0' && $available == 1 && $auth == '1')
        <div class="col-lg-12">
        <input type="hidden" id="total_mapp_groups" value="{{ $group_number }}">
        <form action="{{ $action }}" method="POST" id="forma">
            {{ csrf_field() }}
                <div class="card">
                    <div class="card-header">
                            <strong class="card-title">Ответьте на нижестоящие блоки вопросов </strong>
                             
                        </div>
                    <div class="card-body" style="padding:0px !important;">
                        <div id="loader" style="display:none;"><center><img src="/user_res/loader.gif" width=80>Идет отправка блока ответов на сервер... Подождите немного...</center></div>
                        <div id="we_r_finishing" style="display:none;"><center><img src="/user_res/loader.gif" width=80>Завершаем тест...</center></div>
                        
                        <?php
                            $block_id = 1;
                            $quest_num_in_block = 0;
                        ?>
                        <div id="pr_block_1" style="display:none; background:{{$col['1']}}; padding:1%;">
                            <table class="table">
                            <h5>Блок предпочтений 1 из 7</h5>

                        @foreach ($groups as $key => $group)

                        @if($key % 10 === 0 && $key != 70)
                            <tr>
                                <td>
                            <a class="btn btn-warning" style="color:black;" onClick="show_quest({{ $block_id-1 }}, {{ $block_id }});">Предыдущий блок</a>
                            </td>
                            <td>
                        <a class="btn btn-success" style="color:white;" onClick="show_quest({{ $block_id+1 }}, {{ $block_id }});">Следующий блок</a>
                            </td>
                            <td>
                        <a class="btn btn-warning" style="color:black;float:right;" onClick="show_quest(1, {{ $block_id }});">Вернуться в начало</a>
                            </td>
                            </tr>
                            </table>
                            <input type="hidden" id="group_quest_amount_{{$block_id}}" value="{{$quest_num_in_block}}">
                            </div>
                            <?php
                                $block_id++;
                                $quest_num_in_block = 0;
                            ?>
                            <div id="pr_block_{{$block_id}}" style="display:none; background:{{$col[$block_id]}}; padding:1%;">
                                <table class="table">
                                <h5>Блок предпочтений {{ $block_id }} из 7</h5>
                        @endif
                            <tr>
                                <td style="border:none !important;"><b>Предпочтение #{{ $key }}</b></td>
                                <td style="border:none !important;"></td>
                                <td style="border:none !important;"></td>
                            </tr>
                            <tbody style="border: none;" id="to_check_id{{$key}}">
                            <?php
                                $m_pos = 1;
                                $l_pos = 2;
                                $quest_num_in_block +=1;
                            ?>
                            @foreach ($group as $gr)
                            <tr>
                                <td style="border:none !important;">
                                            <input type="radio" name="pr{{$gr['id']}}" id="id_{{$key}}_{{$m_pos}}_1" onClick="check({{ $key }}, {{$gr['id']}});" value="1" class="radio">
                                            Most
                                </td>
                                <td style="border:none !important;">
                                            <input type="radio" name="pr{{$gr['id']}}" id="id_{{$key}}_{{$l_pos}}_0" onClick="check({{ $key }}, {{$gr['id']}});" value="0" class="radio">
                                            Least
                                </td>
                                <td style="border:none !important;">
                                    {{ $gr['text'] }}
                                </td>
                            </tr>
                            <?php
                                $m_pos = $m_pos+2;
                                $l_pos = $l_pos+2;
                            ?>
                            @endforeach
                            </tbody>
                        @endforeach
                        <tr>
                            <td>
                            <a class="btn btn-warning" style="color:black;" onClick="show_quest({{ $block_id-1 }}, {{ $block_id }});">Предыдущий блок</a>
                            </td>
                            <td>
                        <a class="btn btn-success" style="color:white;" onClick="finish_test();">Завершить тест!</a>
                            </td>
                            <td>
                        <a class="btn btn-warning" style="color:black;float:right;" onClick="show_quest(1, {{ $block_id }});">Вернуться в начало</a>
                            </td>
                        </tr>
                         </table>  
                         <input type="hidden" id="group_quest_amount_{{$block_id}}" value="{{$quest_num_in_block}}">
                    </div>
                                           
                    </div>
                    
                </div>
        </form>
        </div>
    <div class="col-lg-12">
    Вариант верно отвеченного вопроса (картинка):
    <img src="/user_res/mapp_sample.png">
    <br><br>
        @elseif($available==1 && $passed != "0")
        <div class="card">
                    <div class="card-header">
                            <strong class="card-title">Результаты </strong>
                             
                        </div>
                    <div class="card-body">
            Вы уже прошли тестирование. Результаты можно <a href="/user/mapp/results">посмотреть здесь</a>
            </div>
        </div>
    </div>
        @endif
        @if($available != 1)
        <div class="col-lg-12">
            <div class="card">
                    <div class="card-header">
                            <strong class="card-title">Тест недоступен </strong>
                             
                    </div>
                    <div class="card-body">
                        Данный тест недоступен Вам. 
                    </div>
            </div>
        </div>
        @endif
        @if($auth=='0')
            <div class="col-lg-12">
            <div class="card">
                    <div class="card-header">
                            <strong class="card-title">Тест недоступен </strong>
                             
                    </div>
                    <div class="card-body">
                        Обратитесь к администратору, Ваш аккаунт не связан с аккаунтом в системе MAPP. 
                    </div>
            </div>
        </div>
        @endif

<!-- Modal -->
<div class="modal fade" id="message_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Внимание</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="modal_message_text">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
      </div>
    </div>
  </div>
</div>

@endsection

@section('mapp_pass_javascript')
<script>
    function show_quest(next_id, now_id){
        var total = jQuery("#total_mapp_groups").val();
        if(next_id <= 7 && next_id >=1 && now_id!=0){
            
            var one_radio;
            var array_to_send = [];
            jQuery("#pr_block_"+now_id+" :radio").each(function(e){    
                one_radio = this.id;
                if(jQuery("#"+one_radio).is(':checked')){
                    // show id
                    array_to_send.push(one_radio);
                }
            });

            var group_quest_amount = jQuery("#group_quest_amount_"+now_id).val();

            jQuery.ajax({
                type: "POST",
                url: "/user/send_to_mapp_server",
                beforeSend: function(){
                    jQuery("#pr_block_"+now_id).toggle('fast');
                    jQuery("#loader").toggle('fast');
                },
                data: {array_to_send: array_to_send, group_quest_amount: group_quest_amount, _token: '{{csrf_token()}}'},
                success: function(answer) {
                    //console.log(answer);
                    if(answer=="err_fill_all_fields"){
                        show_mess("Заполните все ответы верно, как указано на картинке-примере снизу теста!");
                        jQuery("#loader").toggle('fast');
                        jQuery("#pr_block_"+now_id).toggle('fast');
                    }else if(answer.length < 1){
                        jQuery("#loader").toggle('fast');
                        jQuery("#pr_block_"+next_id).toggle('fast');
                        jQuery("#pr_block_"+now_id).css('display', 'none');
                    }else if(answer == "error"){
                        show_mess("Что-то пошло не так. Попробуйте еще раз либо обратитесь к техническому администратору.");
                        jQuery("#pr_block_"+now_id).toggle('fast');
                    }
                },
                error: function(xhr){
                    show_mess("Что-то пошло не так. Попробуйте еще раз либо обратитесь к техническому администратору. Ошибка: " + xhr.status + " " + xhr.statusText);
                    jQuery("#pr_block_"+now_id).toggle('fast');
                    jQuery("#loader").toggle('fast');
                }
            });
        }
        if(now_id==0){
            jQuery("#pr_block_1").toggle('fast');
        }
        jQuery("html, body").animate({ scrollTop: 0 }, "slow");
    }
    show_quest(1,0);

    function finish_test(){

            var one_radio;
            var array_to_send = [];
            jQuery("#pr_block_7 :radio").each(function(e){    
                one_radio = this.id;
                if(jQuery("#"+one_radio).is(':checked')){
                    // show id
                    array_to_send.push(one_radio);
                }
            });

            var group_quest_amount = jQuery("#group_quest_amount_7").val();

            jQuery.ajax({
                type: "POST",
                url: "/user/send_to_mapp_server",
                beforeSend: function(){
                    jQuery("#pr_block_7").toggle('fast');
                    jQuery("#loader").toggle('fast');
                },
                data: {array_to_send: array_to_send, group_quest_amount: group_quest_amount, _token: '{{csrf_token()}}'},
                success: function(answer) {
                    //console.log(answer);
                    if(answer=="err_fill_all_fields"){
                        show_mess("Заполните все ответы верно, как указано на картинке-примере снизу теста!");
                        jQuery("#loader").toggle('fast');
                        jQuery("#pr_block_7").toggle('fast');
                    }else if(answer.length < 1){
                        jQuery("#loader").toggle('fast');
                        jQuery("#pr_block_7").css('display', 'none');
                        jQuery("#we_r_finishing").toggle('fast');
                        jQuery("#forma").submit();
                    }else if(answer=="error"){
                        show_mess("Что-то пошло не так. Попробуйте еще раз либо обратитесь к техническому администратору.");
                        jQuery("#pr_block_7").toggle('fast');
                    }
                },
                error: function(xhr){
                    show_mess("Что-то пошло не так. Попробуйте еще раз либо обратитесь к техническому администратору. Ошибка: " + xhr.status + " " + xhr.statusText);
                    jQuery("#pr_block_7").toggle('fast');
                }
            });

    }

    function check(id, checked_id){
        var least_checked = jQuery("#to_check_id"+id+" input[value=0]:checked").length;
        if(least_checked > 1){
            jQuery("#to_check_id"+id+" tr td input[value=0]").prop('checked', false);
            jQuery("#to_check_id"+id+" tr td input[name=pr"+checked_id+"][value=0]").prop('checked', true);
        }
        var most_checked = jQuery("#to_check_id"+id+" input[value=1]:checked").length;
        if(most_checked > 1){
            jQuery("#to_check_id"+id+" tr td input[value=1]").prop('checked', false);
            jQuery("#to_check_id"+id+" tr td input[name=pr"+checked_id+"][value=1]").prop('checked', true);
        }
        var checked = jQuery("#to_check_id"+id+" tr td .radio:checked").length;
        if(checked > 2){
            jQuery("#to_check_id"+id+" tr td .radio:checked").prop('checked', false);
            alert("Можно выбрать только два варианта ответа. Оставьте одно поле пустым.");
        }
    }

    function show_mess (text){

        jQuery("#modal_message_text").html(text);
        jQuery("#message_modal").modal();
    }

</script>
@endsection