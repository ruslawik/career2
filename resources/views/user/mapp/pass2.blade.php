@extends('user.layout2')

@section('title', $title)

@section('content')


     @if (session('status'))
        <div class="alert alert-danger">
            {{ session('status') }}
        </div>
    @endif
    <div class="content__body">
            <div class="test__title">
                <p>{{__('all.mapp_tapsyru')}}</p>
            </div>
            <div class="test__steps">
                <h3>шаг <span id="now_step_label">2</span> из 8</h3>
                <div class="step__bar">
                    <div class="step__line" style="width: 25%"></div>
                </div>
            </div>

            <h3 class="test__waiting" style="display:none;" id="loader">{{__('all.mapp_sending_to_server')}}</h3>

            <h3 class="test__waiting" style="display:none;" id="we_r_finishing">{{__('all.mapp_finishing_test')}}</h3>

            <h3 class="test__subtitle">{{__('all.mapp_process_instruction')}}</h3>

            <input type="hidden" id="total_mapp_groups" value="{{ $group_number }}">
            <form action="{{ $action }}" method="POST" id="forma" style="width:100% !important;">
            {{ csrf_field() }}
            <?php
                $block_id = 1;
                $quest_num_in_block = 0;
            ?>

            <div class="test__cont" id="pr_block_1" style="display:none;">
                @foreach ($groups as $key => $group)
                @if($key % 10 === 0 && $key != 70)
                    <div class="test__btns">
                    <button class="prev" type="button" onClick="show_quest({{ $block_id-1 }}, {{ $block_id }});">
                        <svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M12 18.5L6 12.5L12 6.5" stroke="#40189D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M19 18.5L13 12.5L19 6.5" stroke="#40189D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                        {{__('all.prev')}}
                    </button>
                    <button class="next" type="button" onClick="show_quest({{ $block_id+1 }}, {{ $block_id }});">
                        {{__('all.next')}}
                        <svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M12 18.5L18 12.5L12 6.5" stroke="#40189D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M5 18.5L11 12.5L5 6.5" stroke="#40189D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                    </button>
                    <input type="hidden" id="group_quest_amount_{{$block_id}}" value="{{$quest_num_in_block}}">
                    </div>
                </div>
                    <?php
                        $block_id++;
                        $quest_num_in_block = 0;
                    ?>
                <div class="test__cont" id="pr_block_{{$block_id}}" style="display:none;">
                @endif
                <div class="test__box">
                    <div class="box__header">
                        <h3>{{__('all.mapp_kalaular')}} #{{ $key }}</h3>
                    </div>
                    <div class="box__body" id="to_check_id{{$key}}">
                        <?php
                            $m_pos = 1;
                            $l_pos = 2;
                            $quest_num_in_block +=1;
                        ?>
                        @foreach ($group as $gr)
                        <div class="test__item">
                            <div class="radio__box">
                                <div class="radio__item">
                                    <input type="radio" name="pr{{$gr['id']}}" id="id_{{$key}}_{{$m_pos}}_1" onClick="check({{ $key }}, {{$gr['id']}});" value="1" class="radio">
                                    <label for="id_{{$key}}_{{$m_pos}}_1">Most</label>
                                </div>
                                <div class="radio__item">
                                    <input type="radio" name="pr{{$gr['id']}}" id="id_{{$key}}_{{$l_pos}}_0" onClick="check({{ $key }}, {{$gr['id']}});" value="0" class="radio">
                                    <label for="id_{{$key}}_{{$l_pos}}_0">Least</label>
                                </div>
                            </div>
                            <div class="box__question">
                                {{ $gr['text'] }}
                            </div>
                        </div>
                        <?php
                                $m_pos = $m_pos+2;
                                $l_pos = $l_pos+2;
                            ?>
                        @endforeach
                    </div>
                </div>
                @endforeach
                <div class="test__btns">
                    <button class="prev" type="button" onClick="show_quest({{ $block_id-1 }}, {{ $block_id }});">
                        <svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M12 18.5L6 12.5L12 6.5" stroke="#40189D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M19 18.5L13 12.5L19 6.5" stroke="#40189D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                        {{__('all.prev')}}
                    </button>
                    <button class="next" type="button" onClick="finish_test();">
                        {{__('all.finish')}}
                        <svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M12 18.5L18 12.5L12 6.5" stroke="#40189D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M5 18.5L11 12.5L5 6.5" stroke="#40189D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                    </button>
                    <input type="hidden" id="group_quest_amount_{{$block_id}}" value="{{$quest_num_in_block}}">
                </div>
        </form>
    </div>

@endsection

@section('mapp_pass_javascript')
<script>
    function show_quest(next_id, now_id){
        var total = $("#total_mapp_groups").val();
        if(next_id == 0){
            window.location.href = '/user/before-mapp';
        }
        if(next_id <= 7 && next_id >=1 && now_id!=0){
            $("#now_step_label").html(next_id+1);
            var step_line_width = ((next_id+1)/8)*100;
            $(".step__line").width(step_line_width+'%');
            var one_radio;
            var array_to_send = [];
            $("#pr_block_"+now_id+" :radio").each(function(e){    
                one_radio = this.id;
                if($("#"+one_radio).is(':checked')){
                    // show id
                    array_to_send.push(one_radio);
                }
            });

            var group_quest_amount = $("#group_quest_amount_"+now_id).val();

            
            $.ajax({
                type: "POST",
                url: "/user/send_to_mapp_server",
                beforeSend: function(){
                    $("#pr_block_"+now_id).toggle('fast');
                    $(".test__subtitle").toggle('fast');
                    $("#loader").toggle('fast');
                },
                data: {array_to_send: array_to_send, group_quest_amount: group_quest_amount, _token: '{{csrf_token()}}'},
                success: function(answer) {
                    //console.log(answer);
                    if(answer=="err_fill_all_fields"){
                        show_mess("Заполните все ответы верно, как указано на картинке-примере снизу теста!");
                        $("#loader").toggle('fast');
                        $("#pr_block_"+now_id).toggle('fast');
                    }else if(answer.length < 1){
                        $("#loader").toggle('fast');
                        $(".test__subtitle").toggle('fast');
                        $("#pr_block_"+now_id).css('display', 'none');
                        $("#pr_block_"+next_id).toggle('fast');
                    }else if(answer == "error"){
                        show_mess("Что-то пошло не так. Попробуйте еще раз либо обратитесь к техническому администратору.");
                        $("#pr_block_"+now_id).toggle('fast');
                    }
                },
                error: function(xhr){
                    show_mess("Что-то пошло не так. Попробуйте еще раз либо обратитесь к техническому администратору. Ошибка: " + xhr.status + " " + xhr.statusText);
                    $("#pr_block_"+now_id).toggle('fast');
                    $("#loader").toggle('fast');
                }
            });
            
        }
        if(now_id==0){
            $("#pr_block_1").toggle('fast');
        }
        $("html, body").animate({ scrollTop: 0 }, "slow");
    }

    show_quest(1,0);

    function finish_test(){

            var one_radio;
            var array_to_send = [];
            $("#pr_block_7 :radio").each(function(e){    
                one_radio = this.id;
                if($("#"+one_radio).is(':checked')){
                    // show id
                    array_to_send.push(one_radio);
                }
            });

            var group_quest_amount = $("#group_quest_amount_7").val();

            $.ajax({
                type: "POST",
                url: "/user/send_to_mapp_server",
                beforeSend: function(){
                    $("#pr_block_7").toggle('fast');
                    $("#loader").toggle('fast');
                    $(".test__subtitle").toggle('fast');
                },
                data: {array_to_send: array_to_send, group_quest_amount: group_quest_amount, _token: '{{csrf_token()}}'},
                success: function(answer) {
                    //console.log(answer);
                    if(answer=="err_fill_all_fields"){
                        show_mess("Заполните все ответы верно, как указано на картинке-примере снизу теста!");
                        $("#loader").toggle('fast');
                        $("#pr_block_7").toggle('fast');
                    }else if(answer.length < 1){
                        $("#loader").toggle('fast');
                        $("#pr_block_7").css('display', 'none');
                        $("#we_r_finishing").toggle('fast');
                        $("#forma").submit();
                    }else if(answer=="error"){
                        show_mess("Что-то пошло не так. Попробуйте еще раз либо обратитесь к техническому администратору.");
                        $("#pr_block_7").toggle('fast');
                    }
                },
                error: function(xhr){
                    show_mess("Что-то пошло не так. Попробуйте еще раз либо обратитесь к техническому администратору. Ошибка: " + xhr.status + " " + xhr.statusText);
                    $("#pr_block_7").toggle('fast');
                }
            });

    }

    function check(id, checked_id){
        
        var least_checked = $("#to_check_id"+id+" input[value=0]:checked").length;
        if(least_checked > 1){
            $("#to_check_id"+id+" input[value=0]").prop('checked', false);
            $("#to_check_id"+id+" input[name=pr"+checked_id+"][value=0]").prop('checked', true);
        }
        var most_checked = $("#to_check_id"+id+" input[value=1]:checked").length;
        if(most_checked > 1){
            $("#to_check_id"+id+" input[value=1]").prop('checked', false);
            $("#to_check_id"+id+" input[name=pr"+checked_id+"][value=1]").prop('checked', true);
        }
        var checked = $("#to_check_id"+id+" .radio:checked").length;
        if(checked > 2){
            $("#to_check_id"+id+" .radio:checked").prop('checked', false);
            alert("Можно выбрать только два варианта ответа. Оставьте одно поле пустым.");
        }
    }

    function show_mess (text){

        //$("#modal_message_text").html(text);
        //$("#message_modal").show();
        alert(text);
    }

</script>
@endsection
