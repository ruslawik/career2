@extends('user.layout')

@section('title', 'Регистрация на тест МАПП')

@section('content')
        
    <form action="/auto-register-post" method="POST">
        {{ csrf_field() }}
        Имя: <br><input type="text" name="FirstName" class="form-control">
        Фамилия: <br><input type="text" name="MiddleInitial" class="form-control">
        Email: <br><input type="text" name="Email" class="form-control">
        Ваш пароль: <br><input type="text" name="Password" class="form-control">
        <input id="AccountNumber" name="AccountNumber" type="hidden" value="" />
        <input id="KeyCode" name="KeyCode" type="hidden" value="{{$keycode}}" />
        <input name="cookies_file_id" type="hidden" value="{{$cookies_file_id}}" />
        <input name="request_token" type="hidden" value="{{$request_token}}" />
        <br>
        <input type="submit" value="Register" class="btn btn-success">
    </form>

@endsection