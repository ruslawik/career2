@extends('user.layout2')

@section('title', $title)


@section('content')

    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <div class="content__body" data-aos="zoom-in-left" data-aos-delay="1000">
                <div class="test__title">
                <p>Пользователь - {{$username}}</p>
                <p>Дата сдачи - {{$date}}</p>
            </div>
            <div class="test__steps">
                <h3>шаг 2 из 3</h3>
                <div class="step__bar">
                    <div class="step__line" style="width: 66%"></div>
                </div>
            </div>

            <div class="result__cont">
                <div class="chapter">
                    <div class="chapter__header">
                        <h3>{{ __('all.result_2step_mapp_2') }}</h3>
                        <div class="test__btns">
                            <a href="/user/mapp/results/{{$date}}/step1">
                            <button class="prev" type="button">
                                <svg width="24" height="25" viewBox="0 0 24 25" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 18.5L6 12.5L12 6.5" stroke="#40189D" stroke-width="2"
                                          stroke-linecap="round" stroke-linejoin="round"/>
                                    <path d="M19 18.5L13 12.5L19 6.5" stroke="#40189D" stroke-width="2"
                                          stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                                {{ __('all.prev') }}
                            </button>
                            </a>
                            <a href="/user/mapp/results/{{$date}}/step3">
                            <button class="next" type="button">
                                {{ __('all.next') }}
                                <svg width="24" height="25" viewBox="0 0 24 25" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 18.5L18 12.5L12 6.5" stroke="#40189D" stroke-width="2"
                                          stroke-linecap="round" stroke-linejoin="round"/>
                                    <path d="M5 18.5L11 12.5L5 6.5" stroke="#40189D" stroke-width="2"
                                          stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                            </button>
                            </a>
                        </div>
                    </div>
                    <div class="chapter__body">
                        <div class="chapter__desc">
                            <p>
                                <?php if(app()->getLocale() == "ru"){?>
                                После того, как вы ознакомились с другими разделами, и у вас есть общее представление о
                                чертах, которые для вас наиболее важны в вашей работе, вы можете посмотреть на
                                предложенные основные области работы. Этот список основан на главных ваших чертах и
                                рабочих местах, которые включают работу с этими чертами. Также как в 1 части вы увидите
                                напротив каждой сферы деятельности нумерацию от 1 до 5. Во втором столбце вы увидите
                                двузначные числа до 100 или 100 (самый высокий уровень). Числовые данные 2 столбца
                                означают процентное соотношение насколько вы сами подходите к той или иной работе.
                                <?php
                                }
                                if(app()->getLocale() == "kz"){
                                ?>
                                Басқа бөлімдермен танысып, әр жұмысқа қажетті қабілеттер жайлы мәлімет алғаннан кейін, сізге керек болатын негізгі жұмыс алаңдары ұсынылады. Бұл тізім мамандық түрі мен жұмыскердің қабілеттерінін қаншалықты сәйкес келетінін көрсетеді.<br>
                                Сонымен қатар 1-ші бағанда 100-ге дейінгі сандар тізілген. Бұл сандар пайыздық өлшемде әрбір жұмысқа қатысты сіздің икемділігіңізді көрсетеді. Одан кейінгі бағанда жұмыс сфераларына қарама-қарсы 1-ден 5-ке дейін нөмірлерді көресіз.
                                <?php
                                }
                                ?>
                            </p>

                        </div>
                        <div class="chapter__video">
                            <?php if(app()->getLocale() == "ru"){?>
                                <iframe width='560' height='315' src='https://www.youtube.com/embed/IHwVqPtrdjY' frameborder='0' allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>
                            <?php
                                }
                            ?>
                            <?php if(app()->getLocale() == "kz"){?>
                                <iframe width="560" height="315" src="https://www.youtube.com/embed/Wr2DMXMDLVs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            <?php
                                }
                            ?>
                        </div>
                    </div>
                </div>
                <div class="chapter__results">
                    <h2 class="section__title">{{ __('all.result_2step_mapp') }}</h2>
                    <div class="chapter__results--cont2">
                        <div class="tab__buttons">
                            <button data-tab-open="sphere-1" class="active" onclick="goToByScroll();">
                                <img src="/new/img/tab_btn_1.svg" alt="">
                                {{ __('all.osnova') }}
                            </button>
                            <button data-tab-open="sphere-2" onclick="goToByScroll();">
                                <img src="/new/img/tab_btn_2.svg" alt="">
                                {{ __('all.agro') }}
                            </button>
                            <button data-tab-open="sphere-3" onclick="goToByScroll();">
                                <img src="/new/img/tab_btn_3.svg" alt="">
                                {{ __('all.psih') }}
                            </button>
                            <button data-tab-open="sphere-4" onclick="goToByScroll();">
                                <img src="/new/img/tab_btn_4.svg" alt="">
                                {{ __('all.iskusstvo') }}
                            </button>
                            <button data-tab-open="sphere-5" onclick="goToByScroll();">
                                <img src="/new/img/tab_btn_5.svg" alt="">
                                {{ __('all.showbiz') }}
                            </button>
                            <button data-tab-open="sphere-6" onclick="goToByScroll();">
                                <img src="/new/img/tab_btn_6.svg" alt="">
                                {{ __('all.educ') }}
                            </button>
                            <button data-tab-open="sphere-7" onclick="goToByScroll();">
                                <img src="/new/img/tab_btn_7.svg" alt="">
                                {{ __('all.jur') }}
                            </button>
                            <button data-tab-open="sphere-8" onclick="goToByScroll();">
                                <img src="/new/img/tab_btn_8.svg" alt="">
                                {{ __('all.rasled') }}
                            </button>
                            <button data-tab-open="sphere-9" onclick="goToByScroll();">
                                <img src="/new/img/tab_btn_9.svg" alt="">
                                {{ __('all.medic') }}
                            </button>
                            <button data-tab-open="sphere-10" onclick="goToByScroll();">
                                <img src="/new/img/tab_btn_10.svg" alt="">
                                {{ __('all.matem') }}
                            </button>
                            <button data-tab-open="sphere-11" onclick="goToByScroll();">
                                <img src="/new/img/tab_btn_11.svg" alt="">
                                {{ __('all.remesla') }}
                            </button>
                            <button data-tab-open="sphere-12" onclick="goToByScroll();">
                                <img src="/new/img/tab_btn_12.svg" alt="">
                                {{ __('all.injener') }}
                            </button>
                            <button data-tab-open="sphere-13" onclick="goToByScroll();">
                                <img src="/new/img/tab_btn_13.svg" alt="">
                                {{ __('all.market') }}
                            </button>
                            <button data-tab-open="sphere-14" onclick="goToByScroll();">
                                <img src="/new/img/tab_btn_14.svg" alt="">
                                {{ __('all.elem') }}
                            </button>
                            <button data-tab-open="sphere-15" onclick="goToByScroll();">
                                <img src="/new/img/tab_btn_15.svg" alt="">
                                {{ __('all.uslugi') }}
                            </button>
                            <button data-tab-open="sphere-16" onclick="goToByScroll();">
                                <img src="/new/img/tab_btn_16.svg" alt="">
                                {{ __('all.ofis') }}
                            </button>
                            <button data-tab-open="sphere-17" onclick="goToByScroll();">
                                <img src="/new/img/tab_btn_17.svg" alt="">
                                {{ __('all.writing') }}
                            </button>
                            <button data-tab-open="sphere-18" onclick="goToByScroll();">
                                <img src="/new/img/tab_btn_18.svg" alt="">
                                {{ __('all.business') }}
                            </button>
                            <button data-tab-open="sphere-19" onclick="goToByScroll();">
                                <img src="/new/img/tab_btn_19.svg" alt="">
                                {{ __('all.mawina') }}
                            </button>
                        </div>
                        <div class="tab__contents" id="data_table">
                            <div class="tab__content active" data-tab="sphere-1">
                                <div class="table__cont">
                                    {!!$osnova!!}
                                </div>
                            </div>
                            <div class="tab__content" data-tab="sphere-2">
                                <div class="table__cont">
                                    {!!$agro!!}
                                </div>
                            </div>
                            <div class="tab__content" data-tab="sphere-3">
                                <div class="table__cont">
                                    {!!$psih!!}
                                </div>
                            </div>
                            <div class="tab__content" data-tab="sphere-4">
                                <div class="table__cont">
                                    {!!$iskusstvo!!}
                                </div>
                            </div>
                            <div class="tab__content" data-tab="sphere-5">
                                <div class="table__cont">
                                    {!!$showbiz!!}
                                </div>
                            </div>
                            <div class="tab__content" data-tab="sphere-6">
                                <div class="table__cont">
                                    {!!$educ!!}
                                </div>
                            </div>
                            <div class="tab__content" data-tab="sphere-7">
                                <div class="table__cont">
                                    {!!$jur!!}
                                </div>
                            </div>
                            <div class="tab__content" data-tab="sphere-8">
                                <div class="table__cont">
                                    {!!$rasled!!}
                                </div>
                            </div>
                            <div class="tab__content" data-tab="sphere-9">
                                <div class="table__cont">
                                    {!!$medic!!}
                                </div>
                            </div>
                            <div class="tab__content" data-tab="sphere-10">
                                <div class="table__cont">
                                    {!!$matem!!}
                                </div>
                            </div>
                            <div class="tab__content" data-tab="sphere-11">
                                <div class="table__cont">
                                    {!!$remesla!!}
                                </div>
                            </div>
                            <div class="tab__content" data-tab="sphere-12">
                                <div class="table__cont">
                                    {!!$injener!!}
                                </div>
                            </div>
                            <div class="tab__content" data-tab="sphere-13">
                                <div class="table__cont">
                                    {!!$market!!}
                                </div>
                            </div>
                            <div class="tab__content" data-tab="sphere-14">
                                <div class="table__cont">
                                    {!!$elem!!}
                                </div>
                            </div>
                            <div class="tab__content" data-tab="sphere-15">
                                <div class="table__cont">
                                    {!!$uslugi!!}
                                </div>
                            </div>
                            <div class="tab__content" data-tab="sphere-16">
                                <div class="table__cont">
                                    {!!$ofis!!}
                                </div>
                            </div>
                            <div class="tab__content" data-tab="sphere-17">
                                <div class="table__cont">
                                    {!!$writing!!}
                                </div>
                            </div>
                            <div class="tab__content" data-tab="sphere-18">
                                <div class="table__cont">
                                    {!!$business!!}
                                </div>
                            </div>
                            <div class="tab__content" data-tab="sphere-19">
                                <div class="table__cont">
                                    {!!$mawina!!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

@section('mapp_pass_javascript')
<script>
    function goToByScroll(id) {
        // Scroll
        $('html,body').animate({
            scrollTop: $("#data_table").offset().top
        }, 'slow');
    }
</script>
@endsection
