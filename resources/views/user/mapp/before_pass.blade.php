@extends('user.layout2')

@section('title', $title)

@section('content')
        
        @if($available==0)
            <script>
                window.location.href="/user/mapp/how-buy";
            </script>
        @endif
        @if($already_passed==1)
            <script>
                window.location.href="/user/mapp/results";
            </script>
        @endif

        <div class="content__body" data-aos="zoom-in-left" data-aos-delay="1000">
            <div class="test__title">
                <p>{{__('all.mapp_tapsyru')}}</p>
            </div>
            <div class="test__steps">
                <h3>шаг 1 из 8</h3>
                <div class="step__bar">
                    <div class="step__line" style="width: 12.5%"></div>
                </div>
            </div>
            <div class="test__instruction">
                <?php if(app()->getLocale() == "ru"){ ?>
                    <h3 class="test__subtitle">Перед прохождением теста MAPP просим Вас ознакомиться с инструкцией</h3>
                    <iframe height="315" src="https://www.youtube.com/embed/Nq-kfwdmk2w" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <?php } ?>
                <?php if(app()->getLocale() == "kz"){ ?>
                    <h3 class="test__subtitle">MAPP тестін бастамас бұрын нұсқауды қарап шығыңыз</h3>
                    <iframe height="315" src="https://www.youtube.com/embed/_G1JdByMHmU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <?php } ?>
            </div>
            <div class="test__cont">
                <div class="test__box">
                    <div class="box__header">
                        <h3>{{__('all.disc_soylemwe')}} #1  <span>(*{{__('all.durys_jauap')}})</span></h3>
                    </div>
                    <div class="box__body">
                        <div class="test__item">
                            <div class="radio__box">
                                <div class="radio__item">
                                    <input type="radio" id="test1" name="test1" disabled>
                                    <label for="test1">Most</label>
                                </div>
                                <div class="radio__item">
                                    <input type="radio" id="test2" checked name="test1" disabled>
                                    <label for="test2">Least</label>
                                </div>
                            </div>
                            <div class="box__question">
                                {{__('all.mapp_soylem1')}} 
                            </div>
                        </div>
                        <div class="test__item">
                            <div class="radio__box">
                                <div class="radio__item">
                                    <input type="radio" id="test3" checked name="test2" disabled>
                                    <label for="test3">Most</label>
                                </div>
                                <div class="radio__item">
                                    <input type="radio" id="test4" name="test2" disabled>
                                    <label for="test4">Least</label>
                                </div>
                            </div>
                            <div class="box__question">
                                {{__('all.mapp_soylem2')}} 
                            </div>
                        </div>
                        <div class="test__item">
                            <div class="radio__box">
                                <div class="radio__item">
                                    <input type="radio" id="test5" name="test3" disabled>
                                    <label for="test5">Most</label>
                                </div>
                                <div class="radio__item">
                                    <input type="radio" id="test6" name="test3" disabled>
                                    <label for="test6">Least</label>
                                </div>
                            </div>
                            <div class="box__question">
                                {{__('all.mapp_soylem3')}} 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="test__btns">
                    <a href="/user/mapp">
                    <button class="next" type="button">
                        {{__('all.next')}}
                        <svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M12 18.5L18 12.5L12 6.5" stroke="#40189D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M5 18.5L11 12.5L5 6.5" stroke="#40189D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                    </button>
                    </a>
                </div>
            </div>
            
            </form>
        </div>
@endsection