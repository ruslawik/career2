@extends('user.layout2')

@section('title', $title)

@section('content')

    <style>
        .desc_page_block{
            background:#F9DF70;
            width:100%;
            padding:20px;
        }
        .desc_page_block2{
            background:white;
            width:100%;
            padding:20px;
        }
        .desc_page_block3{
            background:#5FB3F9;
            width:100%;
            padding:20px;
        }
        .desc_page_block4{
            background:#5FB3F9;
            width:100%;
            padding:20px;
        }
        .recommend{
            color:#77B83A;
            font-size:20px;
        }
        .recommend2{
            color:#F9DF70;
            font-size:20px;
        }
        .info{
            padding:20px;
            font-size: 18px !important;
        }
    </style>
        <?php if(app()->getLocale() == "ru") { ?>
        <div class="content__body" data-aos="zoom-in-left" data-aos-delay="1000">
            <div class="about__profession">
                <a class="goBack__btn">Список тестов</a>
                <div class="buy-tests__cont">
                    <h3>Выберите тест для просмотра информации и его прохождения</h3>
                    <div class="tests__btns">
                        <button type="button" class="active">MAPP</button>
                        <a href="/user/tests/disc-preview"><button type="button">DISC</button></a>
                        <a href="/user/tests/solomin-preview"><button type="button">Ориентация им. И.Л. Соломина</button></a>
                    </div>
                    <div class="tests__desc">
                        <a href="/user/before-mapp" class="go_to_pay ml-auto">Пройти</a>
                        <br>
                        <div class="desc_page_block">
                            <center>
                                <table>
                                    <tr>
                                        <td>
                                            <center><img src="/mapp-images/1.png" width="150"></center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                <span class="info">MAPP (Мотивационная Оценка Личностного Потенциала) – это тест на определение профессии. Тест MAPP идеально подходит для студентов, выпускников и сотрудников. Вы получите огромное количество информации, которая поможет найти правильную профессию, соответствующую вашему уникальному профилю оценки.
                                </span>
                            </td></tr>
                            </table>
                            </center>
                        </div>
                        <div class="desc_page_block2">
                            <table>
                                <tr>
                                    <td>
                            <span style="font-size:25px;color:#5FB3F9;">
                                Почему именно МАРР?
                            </span><br>
                            <span class="recommend">
                                1. Рекомендуем тем, кто хочет раскрыть свой мотивационный потенциал
                            </span>
                            <p class="info">
                                Что движет людьми, чтобы сделать определенное дело или начать что-то новое, тратить силы, время на определенное дело каждый день? Просто интерес или просто удовлетворение определенной потребности?  Или в бытии человека есть что-то, что подталкивает его? Да, есть, - уверенно отвечаем мы. Это человеческая мотивация. То есть, то, что, движет и удовлетворяет потребности человека. Вот почему вы можете уверенно выбрать свою будущую профессию, зная, к чему у вас больше интереса и мотивации. Тест МAPP является первым и единственным тестом мотивационной оценки личностного потенциала.
                            </p>
                                </td>
                                <td>
                                    <img src="/mapp-images/2.png" width=200 style="float:right;">
                                </td>
                            </tr>
                        </table>
                        </div>
                        <div class="desc_page_block3">
                            <table>
                                <tr>
                                    <td>
                                        <span class="recommend2">
                                            2. Рекомендуем тем, у кого много интересов и кто не знает, какой из них выбрать
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="/mapp-images/3.png" width=200 style="float:right;margin-left:40px;margin-top:20px;"> 
                                        <img src="/mapp-images/4.png" width=200 style="float:right;margin-left:40px;margin-top:20px;">
                                        <img src="/mapp-images/5.png" width=200 style="float:right;margin-left:40px;margin-top:20px;">
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="desc_page_block">
                            <p class="info">
                                Не знаете, какую профессию выбрать? У вас есть интерес и к творчеству, и к организационной работе, и к языкам, но вы не можете выбрать из них одну сферу? Конечно, вы можете отучиться, поработать и, если вам не нравится, посмотреть каждую из этих интересных областей. Но сколько времени вы готовы потратить для этого? Пройдите тест МАРР, и  систематизируйте свои интересы в определенной последовательности, то есть увидеть своими глазами, какая сфера больше всего интересует и мотивирует, и только после этого примите решение.
                            </p>
                        </div>
                        <div class="desc_page_block3">
                            <span class="recommend2">
                                3. Рекомендуем для тех у кого разошлись  мнения с близкими
                            </span>
                            <table>
                                <tr>
                                    <td>
                                        <p class="info">
                                            Интересующая вас сфера другая, а сфера, которую предлагают ваши близкие, совсем другая? Поэтому сомневаетесь в своем решении и все еще думаете? В таких ситуациях кажется правильным решением прислушиваться к тому, что говорят наши близкие, подавляя сомнения, страх перед будущим. Но насколько правильно для вас игнорировать свои желания и свой внутренний голос? Пройдите тест МАРР и  твердо встаньте на ноги, чтобы увидеть свои интересы, профессиональные сферы, которые вам нравятся.
                                        </p>
                                    </td>
                                <td>
                                    <img src="/mapp-images/6.png" width=200 style="float:right;"> 
                                </td>
                            </tr>
                            </table>
                        </div>

                        <table style="width:100%;">
                            <tr>
                                <td style="background-color: #5FB3F9;">
                                    <br>
                                    <center><img src="/mapp-images/7.png" width=150> </center>
                                    <p class="info">
                                    <b>Занимает всего 22 минуты</b><br>
                                    Трудно поверить, что 22-минутный тест может быть таким легким и таким показательным! При завершении теста, мы подберем для вас 1000 вакансий в соответствии с вашим уникальным профилем MAPP
                                </p>
                                </td>
                                <td style="background-color: #5FB3F9;">
                                    <br>
                                    <center><img src="/mapp-images/8.png" width=150> </center>
                                    <p class="info">
                                    <b>Надежность с немедленными результатами</b><br>
                                        Тест MAPP™ - это первый и наиболее полный онлайн тест для потребителей. Более 8 миллионов человек почти во всех странах мира прошли тест MAPP с момента его создания в 1995 году.
                                </p>
                                </td>
                            </tr>
                            <tr>
                                <td style="background-color: #F9DF70;">
                                    <br>
                                    <center><img src="/mapp-images/9.png" width=150> </center>
                                    <p class="info">
                                    <b>Незамедлительный результат</b><br>
                                    Результаты доступны сразу после прохождения теста. Нет никакого ожидания бумажных результатов по почте.
                                </p>
                                </td>
                                <td style="background-color: #F9DF70;">
                                    <br>
                                    <center><img src="/mapp-images/10.png" width=150> </center>
                                    <p class="info">
                                    <b>Валидность проверена</b><br>
                                       Тест MAPP прошел тщательную проверку достоверности и надежности со стороны ряда психологов, включая сопоставление результатов с Strong Interest Inventory®.
                                </p>
                                </td>
                            </tr>
                            <tr>
                                <td style="background-color: #5FB3F9;">
                                    <br>
                                    <center><img src="/mapp-images/11.png" width=150> </center>
                                    <p class="info">
                                    <b>Более 3500 партнеров</b><br>
                                    Более 3500 партнеров, в том числе консультанты по вопросам карьеры, тренеры, фирмы, университеты и высшие учебные заведения используют MAPP для получения информации и консультирования своих нынешних и будущих студентов и клиентов.
                                </p>
                                </td>
                                <td style="background-color: #5FB3F9;">
                                    <br>
                                    <center><img src="/mapp-images/12.png" width=200> </center>
                                    <p class="info">
                                    <b>Совершенствуется уже более 20 Лет!</b><br>
                                        Впервые онлайн в 1995 году, тест MAPP является первым и наиболее полным онлайн тестом для профессиональной ориентации.
                                </p>
                                </td>
                            </tr>
                            <tr>
                                <td style="background-color: #F9DF70;">
                                    <br>
                                    <center><img src="/mapp-images/13.png" width=150> </center>
                                    <p class="info">
                                    <b>Более 8 миллионов клиентов</b><br>
                                    Каждые 30 секунд кто-то где-то в мире проходит тест MAPP (Мотивационная оценка личного потенциала).
                                </p>
                                </td>
                                <td style="background-color: #F9DF70;">
                                    <br>
                                    <center><img src="/mapp-images/14.png" width=150> </center>
                                    <p class="info">
                                    <b>Переведен на 3 языка</b><br>
                                       Этот сайт позволяет вам сдать MAPP и получить свои результаты на казахском, русском, английском языках.
                                </p>
                                </td>
                            </tr>
                            <tr>
                                <td style="background-color: #5FB3F9;">
                                    <br>
                                    <center><img src="/mapp-images/15.png" width=150> </center>
                                    <p class="info">
                                    <b>Надежный</b><br>
                                    Исследования надежности, проведенные с 32 взрослыми, работающими полный рабочий день, также указывают на то, что тест MAPP очень стабилен с течением времени. Результаты надежности тестирования-проверки для характеристик работника составляли 95 процентов!
                                </p>
                                </td>
                                <td style="background-color: #5FB3F9;">
                                    <br>
                                    <center><img src="/mapp-images/16.png" width=200> </center>
                                    <p class="info">
                                    <b>Легко сдавать</b><br>
                                   Вы просто выбираете, какой из них вам больше всего подходит или меньше всего, оставляя один пустым.
                                </p>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <br>
                    <a href="/user/before-mapp" class="go_to_pay ml-auto">Пройти</a>
                </div>
            </div>
        </div>
    <?php } else { ?>

        <div class="content__body" data-aos="zoom-in-left" data-aos-delay="1000">
            <div class="about__profession">
                <a class="goBack__btn">Тесттер тiзiмi</a>
                <div class="buy-tests__cont">
                    <h3>Тесттi таңдаңыз</h3>
                    <div class="tests__btns">
                        <button type="button" class="active">MAPP</button>
                        <a href="/user/tests/disc-preview"><button type="button">DISC</button></a>
                        <a href="/user/tests/solomin-preview"><button type="button">Ориентация им. И.Л. Соломина</button></a>
                    </div>
                    <div class="tests__desc">
                        <a href="/user/before-mapp" class="go_to_pay ml-auto">Өту</a>
                        <br>
                        <div class="desc_page_block">
                            <center>
                                <table>
                                    <tr>
                                        <td>
                                            <center><img src="/mapp-images/1.png" width="150"></center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                <span class="info">MAPP (Жеке потенциалды мотивациялық бағалау) - бұл мамандықты анықтауға арналған тест. MAPP тесті - студенттер, түлектер мен қызметкерлер үшін өте қолайлы. Сіздің өзіңіздің қызығушылықтарыңызға сәйкес, мамандығыңызды анықтауға көмектесетін көптеген ақпарат ала аласыз.
                                </span>
                            </td></tr>
                            </table>
                            </center>
                        </div>
                        <div class="desc_page_block2">
                            <table>
                                <tr>
                                    <td>
                            <span style="font-size:25px;color:#5FB3F9;">
                                Неліктен МАРР тесті?
                            </span><br>
                            <span class="recommend">
                                1. МАРР тесті сіздің жеке потенциалды мотивацияңызды ашады. 
                            </span>
                            <p class="info">
                                Белгілі бір істі жасау немесе жаңа бір нәрсе бастау үшін күніге күш-қуат, уақыт жұмсауға адамдарды не итермелейді? Жай ғана қызығушылық па, әлде белгілі бір қажеттілігін өтеу ғана ма?  Немесе адамның болмысында оны итермелейтін бір дүние бар ма? «Иә, бар!», - деп біз сенімді жауап бере аламыз.  Бұл - адамның мотивациясы. Яғни, адамды әрқашан  қанаттандырып,  қозғалысқа  итермелейтін дүние . Сондықтан  да балаңыздың болмысында неге қызығушылығы/мотивациясы жоғары екенін білу арқылы, болашақ мамандығын сенімді түрде таңдай аласыз. 
                            </p>
                                </td>
                                <td>
                                    <img src="/mapp-images/2.png" width=200 style="float:right;">
                                </td>
                            </tr>
                        </table>
                        </div>
                        <div class="desc_page_block3">
                            <table>
                                <tr>
                                    <td>
                                        <span class="recommend2">
                                            2. Көптеген қызығушылықтары бар, бірақ қандай мамандықты таңдауын  білмей жүргендерге ұсынамыз. 
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="/mapp-images/3.png" width=200 style="float:right;margin-left:40px;margin-top:20px;"> 
                                        <img src="/mapp-images/4.png" width=200 style="float:right;margin-left:40px;margin-top:20px;">
                                        <img src="/mapp-images/5.png" width=200 style="float:right;margin-left:40px;margin-top:20px;">
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="desc_page_block">
                            <p class="info">
                                Қандай мамандықты таңдарыңызды білмей жүрсіз бе? Шығармашылық қабілетіңіз бар, ұйымдастырушылық жұмысқа да, тілдерге де қызығушылығыңыз бар, бірақ осылардың ішінен бір саланы таңдауға қиын соғып жүр ме? Әрине, осы әр қызығатын салаңызды оқып, жұмыс жасап көріп, ұнамаса ауыстырып, әрқайсысын  көре аласыз. Алайда сіз оған қаншама жыл кетіруге дайынсыз ба? Немесе МАРР тестін өту арқылы өзіңіздің қызығушылықтарыңызды белгілі бір ретпен жүйелестіріп, яғни қай сала сізді көбірек қызықтыратынын және ынталандыратынын өз көзіңізбен көре отырып, одан кейін ғана шешім қабылдаңыз.
                            </p>
                        </div>
                        <div class="desc_page_block3">
                            <span class="recommend2">
                                3. Сіздің таңдауыңыз бен  жақындарыңың ұсынысы екі түрлі болған жағдайда тестті тапсыруды ұсынамыз. 
                            </span>
                            <table>
                                <tr>
                                    <td>
                                        <p class="info">
                                            Сізді қызықтыратын сала басқа, ал жақындарыңыз ұсынып жатқан сала мүлдем басқа ма? Сондықтан да шешіміңізге күмән келтіріп, әлі де ойланып жүрсіз бе? Осындай жағдайларда көкірегімізді күмән, болашақтан қорқыныш басу арқылы жақындарымыздың айтқанын тыңдай салу дұрыс шешім сияқты көрінеді. Алайда өзіңіздің ішіңіздегі қалауыңызды аттап өту өзіңізге қатысты қаншалықты дұрыс болады? Сондықтан да МАРР тестін тапсырып, қызығушылықтарыңызды, ұнайтын кәсіби салаларды рет ретімен көру арқылы жоспарлап жүрген мақсатыңызда нық тұрып аяғына дейін орындаңыз.
                                        </p>
                                    </td>
                                <td>
                                    <img src="/mapp-images/6.png" width=200 style="float:right;"> 
                                </td>
                            </tr>
                            </table>
                        </div>

                        <table style="width:100%;">
                            <tr>
                                <td style="background-color: #5FB3F9;">
                                    <br>
                                    <center><img src="/mapp-images/7.png" width=150> </center>
                                    <p class="info">
                                    <b>Бар болғаны 22 минут кетеді</b><br>
                                    22 минуттық тест - өте оңай және анық болатынына сену қиын! Тестті аяқтаған кезде, біз сіздің бірегей MАРР профиліңізге сәйкес, сіз үшін 1000  жұмыс орнын ұсынамыз
                                </p>
                                </td>
                                <td style="background-color: #5FB3F9;">
                                    <br>
                                    <center><img src="/mapp-images/8.png" width=150> </center>
                                    <p class="info">
                                    <b>Шұғыл нәтиже</b><br>
                                        MAPP ™ тесті - бұл тұтынушылар арасындағы  бірінші онлайн және тестің ең көп тараған түрі. 1995 жылдан бері MAPP тестінен дүние жүзіндегі 8 миллионнан астам адам өтті.
                                </p>
                                </td>
                            </tr>
                            <tr>
                                <td style="background-color: #F9DF70;">
                                    <br>
                                    <center><img src="/mapp-images/9.png" width=150> </center>
                                    <p class="info">
                                    <b>Жедел нәтиже</b><br>
                                    Нәтижелер тест тапсырғаннан кейін бірден қолжетімді болады. Поштада қағаз нәтижелері күтілмейді.
                                </p>
                                </td>
                                <td style="background-color: #F9DF70;">
                                    <br>
                                    <center><img src="/mapp-images/10.png" width=150> </center>
                                    <p class="info">
                                    <b>Жарамдылығы тексерілген</b><br>
                                       MAPP тесті бірқатар психологтардың дәлемесі мен сенімділгі негізінде тексерілген, соның ішінде Strong Interest Inventory® -мен салыстырылған.
                                </p>
                                </td>
                            </tr>
                            <tr>
                                <td style="background-color: #5FB3F9;">
                                    <br>
                                    <center><img src="/mapp-images/11.png" width=150> </center>
                                    <p class="info">
                                    <b>3500 -ден астам серіктестер</b><br>
                                    3500 -ден астам серіктестер; оның ішінде мансаптық кеңесшілер, тренерлер, фирмалар, университеттер мен жоғары оқу орындары MAPP -ті қолданады және болашақ студенттер мен клиенттерге  кеңес беру үшін пайдаланады.
                                </p>
                                </td>
                                <td style="background-color: #5FB3F9;">
                                    <br>
                                    <center><img src="/mapp-images/12.png" width=200> </center>
                                    <p class="info">
                                    <b>Ол 20 жылдан астам уақыт бойы жақсарды!</b><br>
                                       MAPP тесті 1995 жылы кәсіби бағдар берудегі алғашқы онлайн және ең ауқымды  болып есептелді.
                                </p>
                                </td>
                            </tr>
                            <tr>
                                <td style="background-color: #F9DF70;">
                                    <br>
                                    <center><img src="/mapp-images/13.png" width=150> </center>
                                    <p class="info">
                                    <b>8 миллионнан астам клиенттер</b><br>
                                    Әр 30 секунд сайын  дүние жүзіндегі бір адам  MAPP (жеке потенциалды мотивациялық бағалау) тестінен өтеді.
                                </p>
                                </td>
                                <td style="background-color: #F9DF70;">
                                    <br>
                                    <center><img src="/mapp-images/14.png" width=150> </center>
                                    <p class="info">
                                    <b>3 тілге аударылған</b><br>
                                       MAPP тестінің нәтижесін  қазақ тілінде, орыс тілінде, ағылшын тілінде алуға толығымен мүмкіндік бар.
                                </p>
                                </td>
                            </tr>
                            <tr>
                                <td style="background-color: #5FB3F9;">
                                    <br>
                                    <center><img src="/mapp-images/15.png" width=150> </center>
                                    <p class="info">
                                    <b>Сенімді</b><br>
                                    Толық уақытты жұмыс істейтін 32 ересек адаммен жүргізілген сенімділік зерттеулері уақыт өте келе MAPP сынағы өте тұрақты екенін көрсетті. Қызметкердің сипаттамалары үшін тестілеу-тексерудің сенімділік нәтижелері 95 пайызын құрады!
                                </p>
                                </td>
                                <td style="background-color: #5FB3F9;">
                                    <br>
                                    <center><img src="/mapp-images/16.png" width=200> </center>
                                    <p class="info">
                                    <b>Тапсыру оңай</b><br>
                                   Сіз өзіңізге қандай нұсқа жаныңызға жақын  және қандай нұсқа сәйкес келмейтінін таңдайсыз, біреуін бос қалдырасыз.
                                </p>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <br>
                    <a href="/user/before-mapp" class="go_to_pay ml-auto">Өту</a>
                </div>
            </div>
        </div>

        <?php
        }
        ?>
@endsection