<ul class="nav navbar-nav">
                    <li class="active">
                        <a href="/user/main"> <i class="menu-icon fa fa-dashboard"></i>Главная </a>
                    </li>
                    <h3 class="menu-title">Пройти тесты</h3><!-- /.menu-title -->
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-laptop"></i>MAPP</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-puzzle-piece"></i><a href="/user/mapp">Пройти</a></li>
                            <li><i class="fa fa-puzzle-piece"></i><a href="/user/mappkaz">Өту</a></li>
                           <li><i class="fa fa-id-badge"></i><a href="/user/mapp/results">Результаты</a></li>
                           <!--<li><i class="fa fa-id-badge"></i><a href="/user/mapp/joblist">ПРОФЕССИИ</a></li>!-->
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-laptop"></i>КЕЙРСИ</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-puzzle-piece"></i><a href="/user/keirsi">Пройти</a></li>
                            <li><i class="fa fa-puzzle-piece"></i><a href="/user/keirsikaz">Өту</a></li>
                            <!--<li><i class="fa fa-id-badge"></i><a href="/user/keirsi/results">Результаты</a></li>!-->
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-laptop"></i>DISC</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-puzzle-piece"></i><a href="/user/disc">Пройти</a></li>
                            <li><i class="fa fa-puzzle-piece"></i><a href="/user/disckaz">Өту</a></li>
                            <li><i class="fa fa-puzzle-piece"></i><a href="/user/discen">Pass</a></li>
                            <li><i class="fa fa-id-badge"></i><a href="/user/disc/results">Результаты</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-laptop"></i>Соц. интеллект</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-puzzle-piece"></i><a href="/user/holl/rus">Пройти</a></li>
                            <li><i class="fa fa-puzzle-piece"></i><a href="/user/holl/kaz">Өту</a></li>
                            <li><i class="fa fa-id-badge"></i><a href="/user/holl_res/results">Результаты</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-laptop"></i>Поведение в конфликте</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-puzzle-piece"></i><a href="/user/tomas/rus">Пройти</a></li>
                            <li><i class="fa fa-puzzle-piece"></i><a href="/user/tomas/kaz">Өту</a></li>
                            <li><i class="fa fa-id-badge"></i><a href="/user/tomas_res/results">Результаты</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-laptop"></i>«Ориентация»</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-puzzle-piece"></i><a href="/user/solomin/rus">Пройти</a></li>
                            <li><i class="fa fa-puzzle-piece"></i><a href="/user/solomin/kaz">Өту</a></li>
                            <li><i class="fa fa-id-badge"></i><a href="/user/solomin_res/results">Результаты</a></li>
                        </ul>
                    </li>
                </ul>