@extends('user.layout2')

@section('title', $title)

@section('content')
        <div class="content__body" data-aos="zoom-in-left" data-aos-delay="1000">
            <div class="internship topCourse course__cards">
                <div class="internship__cont courseCards__cont">
                    @foreach($posts as $post)
                        <div class="carousel-cell">
                            <a target="_blank" style="width:100% !important;" href="/user/news/{{$post->id}}">
                                <div class="internship__img">
                                    <img src="/storage/news-images/{{$post->img}}">
                                </div>
                                <div class="internship__desc">
                                    <div class="internship__title">{{$post->name}}</div>
                                    <div class="internship__excerpt">{{$post->small_desc($post->text)}}</div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
                {{$posts->links()}}
            </div>
        </div>
@endsection
