@extends('user.layout2')

@section('title', $title)

@section('content')
        <div class="content__body" data-aos="zoom-in-left" data-aos-delay="1000">
            <div class="professions__cards">
                <a href="/user/news" onClick="window.history.back();" class="goBack__btn">Вернуться назад</a>
                <div class="professions__cont internship__cont mentors schedule__cont">
                    <div class="profession__card" style="width:100% !important;">
                        <div class="carousel-cell">
                            <div class="mentor__top">
                                <div class="internship__desc">
                                    <div class="internship__title">{{$post[0]->name}}</div>
                                </div>
                            </div>
                            <div class="mentor__bottom">
                                <div class="internship__vacans">{{Carbon\Carbon::parse($post[0]->created_at)->format('d/m/Y')}}</div>
                                <p>
                                    {!!$post[0]->text!!}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection