@extends('user.layout2')

@section('title', $title)

@section('content')
        <div class="content__body" data-aos="zoom-in-left" data-aos-delay="1000">
            <div class="test__title">
                <p>Клиент - {{ $username }}</p>
                <p>{{ __('all.tapsyrgan_date') }} - {{ $date }}</p>
            </div>
            <div class="test__decryption">
                <br>
                <h3>{{__('all.formula_anyktau_tanda')}}</h3>
                <div class="decryption__btns tab__buttons">
                    <button type="button" class="active" data-tab-open="first">{{ __('all.disc_res_maska') }}</button>
                    <button type="button" data-tab-open="second">{{ __('all.disc_res_heart') }}</button>
                    <button type="button" data-tab-open="third">{{ __('all.disc_res_mirror') }}</button>
                </div>
            </div>

            <div class="test__decryption--content active" data-tab="first">
                <div class="result__cont">
                    <div class="chapter">
                        <div class="chapter__header">
                            <h3>
                                <div class="light">{{ __('all.formula_opredelena') }} ({{ __('all.disc_res_maska') }}):</div> {{ $most_formula }} 
                                <br>
                                @if(isset($description_mask[0]))
                                    @if($lang=="kz")
                                        {{ $description_mask[0]['kaz_name'] }} ({{ __('all.disc_res_maska') }})
                                    @else
                                        {{ $description_mask[0]['name'] }} ({{ __('all.disc_res_maska') }})
                                    @endif
                                @endif
                            </h3>
                        </div>
                        <div class="chapter__body">
                            <p>
                                @if($lang=="kz")
                                    {!! $description_mask[0]['kaz_description'] !!}
                                @else
                                    {!! $description_mask[0]['desciption'] !!}
                                @endif
                                <br><br>
                                @if($most_formula_images != NULL)
                                @foreach($most_formula_images->additional_images($most_formula_images['id']) as $image)
                                    @if($lang == $image['lang'])
                                        <img src="/storage/disc-additional-images/{{$image['image_url']}}" width="100%">
                                        <br><br>
                                    @endif
                                @endforeach
                                @endif
                            </p>
                        </div>
                    </div>
                </div>

            </div>
            <div class="test__decryption--content" data-tab="second">
                <div class="result__cont">
                    <div class="chapter">
                        <div class="chapter__header">
                            <h3>
                                <div class="light">{{ __('all.formula_opredelena') }} ({{ __('all.disc_res_heart') }}):</div> {{ $least_formula }} 
                                <br>
                                @if(isset($description_least[0]))
                                    @if($lang=="kz")
                                        {{$description_least[0]['kaz_name']}} ({{ __('all.disc_res_heart') }})
                                    @else
                                        {{$description_least[0]['name']}} ({{ __('all.disc_res_heart') }})
                                    @endif
                                @endif
                            </h3>
                        </div>
                        <div class="chapter__body">
                            <p>
                                @if($lang=="kz")
                                    {!! $description_least[0]['kaz_description'] !!}
                                @else
                                    {!! $description_least[0]['desciption'] !!}
                                @endif
                                <br><br>
                                @if($least_formula_images != NULL)
                                @foreach($least_formula_images->additional_images($least_formula_images['id']) as $image)
                                    @if($lang == $image['lang'])
                                        <img src="/storage/disc-additional-images/{{$image['image_url']}}" width="100%">
                                        <br><br>
                                    @endif
                                @endforeach
                                @endif
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="test__decryption--content" data-tab="third">
                <div class="result__cont">
                    <div class="chapter">
                        <div class="chapter__header">
                            <h3>
                                <div class="light">{{ __('all.formula_opredelena') }} ({{ __('all.disc_res_mirror') }}):</div> {{ $change_formula }}
                                <br>
                                @if(isset($description[0]))
                                    @if($lang=="kz")
                                        {{$description[0]['kaz_name']}} ({{ __('all.disc_res_mirror') }})
                                    @else
                                        {{$description[0]['name']}} ({{ __('all.disc_res_mirror') }})
                                    @endif
                                @endif
                            </h3>
                        </div>
                        <div class="chapter__body">
                            <p>
                                @if($lang=="kz")
                                    {!! $description[0]['kaz_description'] !!}
                                @else
                                    {!! $description[0]['desciption'] !!}
                                @endif
                                <br><br>
                                @if($change_formula_images != NULL)
                                @foreach($change_formula_images->additional_images($change_formula_images['id']) as $image)
                                    @if($lang == $image['lang'])
                                        <img src="/storage/disc-additional-images/{{$image['image_url']}}" width="100%">
                                        <br><br>
                                    @endif
                                @endforeach
                                @endif
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="table__cont disc__table__cont">
                <table>
                    <thead>
                    <tr>
                        <th></th>
                        <th>D</th>
                        <th>I</th>
                        <th>S</th>
                        <th>C</th>
                        <th>*</th>
                        <th>Total score</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>MOST</td>
                        <td>{{ $most['D'] }}</td>
                        <td>{{ $most['I'] }}</td>
                        <td>{{ $most['S'] }}</td>
                        <td>{{ $most['C'] }}</td>
                        <td>{{ $most['*'] }}</td>
                        <td>{{ $most['total'] }}</td>
                    </tr>
                    <tr>
                        <td>LEAST</td>
                        <td>{{ $least['D'] }}</td>
                        <td>{{ $least['I'] }}</td>
                        <td>{{ $least['S'] }}</td>
                        <td>{{ $least['C'] }}</td>
                        <td>{{ $least['*'] }}</td>
                        <td>{{ $least['total'] }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <div class="graphs__cont">
                <div class="graph">
                    <h3>Graph 1 Most</h3>
                    <img src="{{ $img_most_graph }}" style="margin-right:20px;">
                </div>
                <div class="graph">
                    <h3>Graph 2 Least</h3>
                    <img src="{{ $img_least_graph }}"  style="margin-right:20px;">
                </div>
                <div class="graph">
                    <h3>Graph 3 Change</h3>
                    <img src="{{ $img_change_graph }}"  style="margin-right:20px;">
                </div>
            </div>
        </div>
@endsection