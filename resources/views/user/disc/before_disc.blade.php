@extends('user.layout2')

@section('title', $title)

@section('content')
    
        @if($available == 0)
            <script>
                window.location.href="/user/disc/buy";
            </script>
        @endif

        <div class="content__body" data-aos="zoom-in-left" data-aos-delay="1000">
            <div class="test__title">
                <p>{{__('all.disc_tapsyru')}}</p>
            </div>
            <div class="test__steps">
                <h3>шаг 1 из 25</h3>
                <div class="step__bar">
                    <div class="step__line" style="width: 5%"></div>
                </div>
            </div>
            <div class="test__instruction">
                <h3 class="test__subtitle">{{__('all.disc_instruction_title')}}</h3>
            </div>
            <div class="test__cont">
                <div class="test__box">
                    <div class="box__header">
                        <h3>{{__('all.disc_instruction')}}</h3>
                    </div>
                    <div class="box__body">
                        @if($lang == "ru")
                        <ol>
                            <li>1. Выберите место, где Вы будете отвечать на вопросы: работа, дом, другое общественное место и т.д.</li>
                            <li>2. Каждый блок, представленный ниже, содержит четыре фразы. Внимательно прочитайте каждую из четырех фраз в каждом блоке. Выберите "Most" рядом с фразой, которая в НАИБОЛЬШЕЙ СТЕПЕНИ описывает Вас именно в указанном месте.</li>
                            <li>3. Выберите "Least" рядом с фразой, которая в НАИМЕНЬШЕЙ СТЕПЕНИ описывает Вас в месте, которое Вы выбрали.</li>
                            <li>4. Для каждого блока выберите только один ответ, описывающий Вас в НАИБОЛЬШЕЙ и НАИМЕНЬШЕЙ СТЕПЕНИ.</li>
                            <li>5. Ответы должны быть даны в течение 7 минут или в сроки, максимально приближенные к указанному времени.</li>
                        </ol>
                        @endif
                        @if($lang == "kz")
                        <ol>
                        <li>1. Сұрақтарға жауап беретін ортамды таңдаңыз: жұмыс, үй, басқа қоғамдық орындар және т.б.</li>
                        <li>2. Төменде келтірілген әр блокта төрт тіркес бар. Әр блоктағы төрт тіркестің әрқайсысын мұқият оқып шығыңыз.  Сізге ең жақын және нақты сипаттайтын сөз тіркесінің жанына «M» тармағын таңдаңыз.</li>
                        <li>3. Сізге ең алыс және нақты сипаттамайтын тіркестің жанына «L» тармағын таңдаңыз.</li>
                        <li>4. Әр блокта сізді ең жақын және ең алыс деңгейде сипаттайтын бір жауапты таңдаңыз.</li>
                        <li>5. Жауаптар 7 минут ішінде немесе көрсетілген уақытқа ең жақын мерзімде берілуі тиіс.</li>
                        </ol>
                        @endif
                    </div>
                </div>
                <div class="test__box">
                    <div class="box__header">
                        <h3>{{__('all.disc_soylemwe')}} #1  <span>(*{{__('all.durys_jauap')}})</span></h3>
                    </div>
                    <div class="box__body">
                        <div class="test__item">
                            <div class="radio__box">
                                <div class="radio__item">
                                    <input type="radio" id="test1" disabled name="test1">
                                    <label for="test1">Most</label>
                                </div>
                                <div class="radio__item">
                                    <input type="radio" id="test2" disabled checked name="test1">
                                    <label for="test2">Least</label>
                                </div>
                            </div>
                            <div class="box__question">
                                {{__('all.disc_soylem1')}}
                            </div>
                        </div>
                        <div class="test__item">
                            <div class="radio__box">
                                <div class="radio__item">
                                    <input type="radio" id="test3" disabled name="test2">
                                    <label for="test3">Most</label>
                                </div>
                                <div class="radio__item">
                                    <input type="radio" id="test4" disabled name="test2">
                                    <label for="test4">Least</label>
                                </div>
                            </div>
                            <div class="box__question">
                                {{__('all.disc_soylem2')}}
                            </div>
                        </div>
                        <div class="test__item">
                            <div class="radio__box">
                                <div class="radio__item">
                                    <input type="radio" id="test5" disabled checked name="test3">
                                    <label for="test5">Most</label>
                                </div>
                                <div class="radio__item">
                                    <input type="radio" id="test6" disabled name="test3">
                                    <label for="test6">Least</label>
                                </div>
                            </div>
                            <div class="box__question">
                                {{__('all.disc_soylem3')}}
                            </div>
                        </div>
                        <div class="test__item">
                            <div class="radio__box">
                                <div class="radio__item">
                                    <input type="radio" id="test7" disabled name="test4">
                                    <label for="test7">Most</label>
                                </div>
                                <div class="radio__item">
                                    <input type="radio" id="test8" disabled name="test4">
                                    <label for="test8">Least</label>
                                </div>
                            </div>
                            <div class="box__question">
                                {{__('all.disc_soylem4')}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="test__btns">
                    <button class="prev" disabled type="button">
                        <svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M12 18.5L6 12.5L12 6.5" stroke="#40189D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M19 18.5L13 12.5L19 6.5" stroke="#40189D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                        {{__('all.prev')}}
                    </button>
                    <a href="/user/tests/disc">
                    <button class="next" type="button">
                        {{__('all.next')}}
                        <svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M12 18.5L18 12.5L12 6.5" stroke="#40189D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M5 18.5L11 12.5L5 6.5" stroke="#40189D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                    </button>
                    </a>
                </div>
            </div>
        </div>
@endsection