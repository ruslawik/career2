@extends('user.layout2')

@section('title', $title)


@section('content')

    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <div class="content__body" data-aos="zoom-in-left" data-aos-delay="1000">
            <div class="about__profession">
                <input type="hidden" id='email' value="{{ auth()->user()->email }}">
                <input type="hidden" id='disc_price' value="{{$disc_price[0]->value}}">
                <a class="goBack__btn">Покупка теста DISC</a>
                <div class="buy-tests__cont">

                    @if($already_passed > 0)
                        <h4>Вы уже проходили этот тест. Результаты можно посмотреть <a href="/user/disc/results">по ссылке</a></h4>
                        <p>В случае необходимости повторного прохождения теста, запросите дополнительный промо-код или оплатите тестирование</p><br>
                    @endif

                    <a href="#/" onClick="go_to_pay();" class="go_to_pay">Перейти к оплате</a>

                    <form action="/user/disc/open" method="POST" id="reg_form">
                        {{csrf_field()}}
                        <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
                    </form>

                    <a href="#/" style="margin-left:5px;font-size:14px !important;" onClick="open_promo_field();">У меня есть промо-код</a>
                    <form action="/user/disc/open-promo" method="POST" id="promocode_form" style="display: none;">
                            {{csrf_field()}}
                            @if($errors->any())
                                <br>
                                <h4>{{$errors->first()}}</h4>
                            @endif
                            Введите промо-код: <input class="buy_input" type="text" name="promocode" id="promocode" class="form-control"><br>
                            <button type="submit" class="go_to_pay">Отправить</button>
                    </form>
                </div>
            </div>
        </div>

@endsection

@section('mapp_pass_javascript')
    <script src="https://widget.cloudpayments.ru/bundles/cloudpayments"></script>
    <script>

        this.pay = function () {
            var widget = new cp.CloudPayments();
            widget.charge({ // options
                publicId: 'pk_fb30e07a2d7e7748f7a6a5052c38d',  //id из личного кабинета
                description: 'Оплата теста DISC: '+jQuery('#disc_price').val()+' тенге', //назначение
                amount: parseInt(jQuery('#disc_price').val()), //сумма
                currency: 'KZT', //валюта
                //invoiceId: '1234567', //номер заказа  (необязательно)
                accountId: jQuery("#email").val(), //идентификатор плательщика (необязательно)
                skin: "classic", //дизайн виджета
                    data: {
                        myProp: 'myProp value' //произвольный набор параметров
                    }
                },
                function (options) { // success
                    jQuery("#reg_form").submit();
                },
                function (reason, options) { // fail
                    //alert("FAIL");
                });
        };   

        function go_to_pay(){
            ok = true;
            if(ok){
                pay();
            }
        }

        function open_promo_field(){
            jQuery("#promocode_form").toggle('fast');
        }

        @if($errors->any())
            open_promo_field();
        @endif
    </script>
@endsection

