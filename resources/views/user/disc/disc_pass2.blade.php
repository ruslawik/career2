@extends('user.layout2')

@section('title', $title)

@section('content')

        @if($available == 0)
            <script>
                window.location.href="/user/disc/buy";
            </script>
        @endif

        <div class="content__body" data-aos="zoom-in-left" data-aos-delay="1000">
            <div class="test__title">
                 <p>{{__('all.disc_tapsyru')}}</p>
            </div>
            <div class="test__steps">
                <h3>шаг <span id="now_step_span">2</span> из 25</h3>
                <div class="step__bar">
                    <div class="step__line" style="width: 8%"></div>
                </div>
            </div>
            <h3 class="test__subtitle">{{__('all.disc_process_instruction')}}</h3>
            <div class="test__cont">
            <form action="{{$action}}" method="POST">
                {{ csrf_field() }}
                <div class="test__box" id="pr_block_test_box_0" style="display: none;">
                    <div class="box__header">
                        <h3>{{__('all.disc_soylemwe')}} #1</h3>
                    </div>
                    <div class="box__body" style="padding-bottom: 15px !important;">
                        @foreach($questions as $question)
                        <div class="test__item">
                            <div class="radio__box">
                                <div class="radio__item">
                                    <input type="radio" id="m{{ $question->id }}" name="most{{$group_ar[$question->id]}}" value="{{ $question->id }}" onClick="check_most({{ $question->id }});">
                                    <label for="m{{ $question->id }}">Most</label>
                                </div>
                                <div class="radio__item">
                                    <input type="radio" id="l{{ $question->id }}" name="least{{$group_ar[$question->id]}}" value="{{ $question->id }}" onClick="check_least({{ $question->id }});">
                                    <label for="l{{ $question->id }}">Least</label>
                                </div>
                            </div>
                            <div class="box__question">
                                {{ $question->question }}
                            </div>
                        </div>
                        @if($question->id % 4 === 0)
                            <div class="test__btns" style="margin:20px;">
                                @if($question->id == 4)
                                <a href="/user/tests/before-disc">
                                    <button class="prev" type="button">
                                    <svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 18.5L6 12.5L12 6.5" stroke="#40189D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                    <path d="M19 18.5L13 12.5L19 6.5" stroke="#40189D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                    </svg>
                                    {{__('all.prev')}}
                                </button>
                                </a>
                                @else
                                    <button class="prev" type="button" onClick="show_quest({{ $question->id-8 }}, {{ $question->id-4 }}, 0);">
                                    <svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 18.5L6 12.5L12 6.5" stroke="#40189D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                    <path d="M19 18.5L13 12.5L19 6.5" stroke="#40189D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                    </svg>
                                    {{__('all.next')}}
                                </button>
                                @endif
                                @if($question->id == 96)
                                    <button class="next" type="submit">
                                        {{__('all.finish')}}
                                        <svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M12 18.5L18 12.5L12 6.5" stroke="#40189D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                        <path d="M5 18.5L11 12.5L5 6.5" stroke="#40189D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                        </svg>
                                    </button>
                                @else
                                    <button class="next" type="button" onClick="show_quest({{ $question->id }}, {{ $question->id-4 }}, 0);">
                                        {{__('all.next')}}
                                        <svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M12 18.5L18 12.5L12 6.5" stroke="#40189D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                        <path d="M5 18.5L11 12.5L5 6.5" stroke="#40189D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                        </svg>
                                    </button>
                                @endif
                            </div>
                            
                            </div><!--END OF box__body div!-->
                            </div><!--END OF test__box div!-->
                            <div class="test__box" id="pr_block_test_box_{{ $question->id }}" style="display: none;">
                            <div class="box__header">
                                <h3>{{__('all.disc_soylemwe')}} #{{ $group_ar[$question->id]+1 }}</h3>
                            </div>
                            <div class="box__body" style="padding-bottom: 15px !important;">
                        @endif
                    @endforeach
                    </div><!--END OF box__body div!-->
                </div><!--END OF test__box div!-->
            </form>
                
            </div>

        </div>
@endsection

@section('mapp_pass_javascript')
<script>
    function show_quest(next_id, now_id, first_time){
        if(next_id < 96 && next_id >=0){
            var numberOfCheckedRadio = jQuery("#pr_block_test_box_"+now_id+' input:radio:checked').length;
            if(numberOfCheckedRadio<2 && first_time!=1){
                alert("Заполните ответы как показано в инструкции! Не пропускайте");
                jQuery("#pr_block_test_box_"+now_id).css('display', 'block');
            }else{
                var width = next_id+8;
                var now_step = Math.floor((next_id+8)/4);
                $("#now_step_span").html(now_step);
                $(".step__line").css('width', width+'%');
                jQuery("#pr_block_test_box_"+next_id).toggle('fast');
                jQuery("#pr_block_test_box_"+now_id).toggle('fast');
            }
        }
    }
    show_quest(1,0,1);
    function check_most(l_id){
        if(jQuery('#l'+l_id).is(':checked')){ 
            jQuery('#l'+l_id).attr('checked', false);
        }
    }
    function check_least(l_id){
        if(jQuery('#m'+l_id).is(':checked')){ 
            jQuery('#m'+l_id).attr('checked', false);
        }
    }
</script>
@endsection