@extends('user.layout2')

@section('title', $title)

@section('content')

    <div class="content__body" data-aos="zoom-in-left" data-aos-delay="1000">
            <div class="filterBar test__results__filterBar">
                <div class="filter__desc">
                    <h3>Список тестов</h3>
                    <p>Выберите для просмотра результатов</p>
                </div>
                <div class="filter__btns">
                    <a href="/user/mapp/results"><button type="button">MAPP</button></a>
                    <button type="button" class="active">DISC</button>
                    <a href="/user/solomin/results"><button type="button">Ориентация им. И.Л. Соломина</button></a>
                </div>
            </div>

            <div class="tab__contents">
                <div class="goals__table active" data-tab="all">
                    <table>
                        <thead>
                        <tr>
                            <th class="goal__date">Дата сдачи</th>
                            <th class="goal">Тест</th>
                            <th class="goal__status">Результат</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($disc_results as $key => $result)
                        <tr>
                            <td class="goal__date">{{ $key }}</td>
                            <td class="goal">
                                <h3>DISC</h3>
                                <h4>Поведенческий тест</h4>
                            </td>
                            <td class="goal__status">
                                <a href="/user/disc/results/{{$key}}">
                                <div class="current" style="cursor:pointer;">
                                    <div class="status yes">Посмотреть</div>
                                </div>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>

@endsection