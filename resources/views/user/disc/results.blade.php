@extends('user.layout')

@section('title', $title)

@section('content')

    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                            <strong class="card-title">Пройденные тесты</strong>
                        </div>
                    <div class="card-body">
                     @foreach ($disc_results as $key => $result)
                        Завершенное тестирование DISC - {{ $key }}
                        <a href="/user/disc/{{ $key }}/ru" class="btn-sm btn-success" style="color:white;cursor:pointer;"> Просмотреть</a>
                        <br><br>
                    @endforeach
                </div>
    </div>

@endsection