@extends('user.layout2')

@section('title', $title)

@section('content')
        <div class="content__body" data-aos="zoom-in-left" data-aos-delay="1000">
            <div class="professions__cards">
                <div class="professions__cont internship__cont mentors schedule__cont">
                    @foreach($posts as $post)
                    <div class="profession__card">
                        <div class="carousel-cell">
                            <div class="mentor__top">
                                <div class="internship__desc">
                                    <div class="internship__title">{{$post->name}}</div>
                                </div>
                            </div>
                            <div class="mentor__bottom">
                                <div class="internship__vacans">{{Carbon\Carbon::parse($post->created_at)->format('d/m/Y')}}</div>
                                @if(strlen($post->img) > 0)
                                    <img src="/storage/schedule-images/{{$post->img}}" width=100% style="margin-bottom: 10px;">
                                @endif
                                <p>
                                    {{$post->small_desc($post->text)}}
                                </p>
                                <a href="/user/schedule/{{$post->id}}">Перейти</a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                {{$posts->links()}}
            </div>
        </div>
@endsection