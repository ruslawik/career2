@extends('user.layout2')

@section('title', $title)

@section('content')
        <div class="content__body" data-aos="zoom-in-left" data-aos-delay="1000">
            <div class="filterBar goals__filterBar">
                <div class="filter__desc">
                    <h3>Поставьте свою цель и срок ее исполнения. Вы обязательно ее достигнете!</h3>
                </div>
            </div>

            @if ($errors->any())
                <ul style="margin-left:25px;margin-bottom: 25px;color:red;">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                </ul>
            @endif

            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <ul style="margin-left:25px;margin-bottom: 25px;color:green;">
                        <li>{!! \Session::get('success') !!}</li>
                    </ul>
                </div>
            @endif

            <form style="width:100% !important;" action="/user/goals/add" method="POST">
                {{csrf_field()}}
                <div class="bigSearchBox goals__form">
                    <div class="myDate">
                        <input value="{{old('date_of_exec')}}" type="date" name="date_of_exec" placeholder="Дата исполнения">
                    </div>
                    <input value="{{old('goal')}}" type="text" name="goal" placeholder="Введите цель кратко">
                    <input value="{{old('description')}}" type="text" name="description" placeholder="Введите описание">
                    <div class="btns">
                        <button type="submit" class="search__btn">
                            Добавить
                        </button>
                    </div>
                </div>
            </form>

            <div class="goals__table">
                <table>
                    <thead>
                        <tr>
                            <th class="goal__num">Номер цели</th>
                            <th class="goal__date">Срок исполнения</th>
                            <th class="goal">Цель</th>
                            <th class="goal__status">Статус</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($goals as $goal)
                        <tr>
                            <td class="goal__num">{{$goal->local_num}}</td>
                            <td class="goal__date">{{Carbon\Carbon::parse($goal->date_of_exec)->format('d/m/Y')}}</td>
                            <td class="goal">
                                <h3>{{$goal->goal}}</h3>
                                <h4>{{$goal->description}}</h4>
                            </td>
                            <td class="goal__status">
                                <div class="current">
                                    <div class="status {{$statuses[$goal->status]}}">{{$status_texts[$goal->status]}}</div>
                                </div>
                                <button type="button" class="threePoints">
                                    <svg width="20" height="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M10.001 12C10.001 13.1046 10.8964 14 12.001 14C13.1055 14 14.001 13.1046 14.001 12C14.001 10.8954 13.1055 10 12.001 10C10.8964 10 10.001 10.8954 10.001 12Z" fill="black"/>
                                        <path d="M10.001 4C10.001 5.10457 10.8964 6 12.001 6C13.1055 6 14.001 5.10457 14.001 4C14.001 2.89543 13.1055 2 12.001 2C10.8964 2 10.001 2.89543 10.001 4Z" fill="black"/>
                                        <path d="M10.001 20C10.001 21.1046 10.8964 22 12.001 22C13.1055 22 14.001 21.1046 14.001 20C14.001 18.8954 13.1055 18 12.001 18C10.8964 18 10.001 18.8954 10.001 20Z" fill="black"/>
                                    </svg>
                                </button>
                                <div class="dropdown">
                                    <div class="dropdown__list">
                                        <a href="/user/goals/{{$goal->id}}/update-status/2"><div class="status yes">Сделано!</div></a>
                                        <a href="/user/goals/{{$goal->id}}/update-status/1"><div class="status proccess">В процессе</div></a>
                                        <a href="/user/goals/{{$goal->id}}/update-status/0"><div class="status no">Не сделано</div></a>
                                    </div>
                                </div>
                            </td>

                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{$goals->links()}}
            </div>
        </div>
@endsection