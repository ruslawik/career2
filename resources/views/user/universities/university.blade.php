@extends('user.layout2')

@section('title', $title)
@php
    if($university->socials){
        $socials = explode(';', $university->socials);
        $fbLink = null;
        $instagramLink = null;
        $youtubeLink = null;
        $twitterLink = null;
        if(count($socials)){
            foreach ($socials as $social){
                if(\Illuminate\Support\Str::contains($social, 'instagram')){
                    $instagramLink = trim($social);
                }else if(\Illuminate\Support\Str::contains($social, 'facebook')){
                    $fbLink = trim($social);
                }else if(\Illuminate\Support\Str::contains($social, 'youtube')){
                    $youtubeLink = trim($social);
                }else if(\Illuminate\Support\Str::contains($social, 'twitter')){
                    $twitterLink = trim($social);
                }
            }
        }
    }
@endphp

@section('content')
    <div class="content__body" data-aos="zoom-in-left" data-aos-delay="1000">
        <div class="about__profession">
            <a href="/user/university/search" onClick="window.history.back();" class="goBack__btn">Вернуться к
                поиску</a>
            <div class="about__profession__box">
                <div class="left">
                    <div class="name">{{$university->category->name}}</div>
                    <div class="faculty">{{$university->name}}</div>
                    @if($university->socials)
                    <small>Соц. сети: </small>
                    <ul style="display: inline;">
                        @if($instagramLink)<li style="font-size: 20px"><a href="{{ $instagramLink }}" target="_blank"><i class="fa fa-instagram"></i></a></li>@endif
                        @if($fbLink)<li style="font-size: 20px"><a href="{{ $fbLink }}" target="_blank"><i class="fa fa-facebook"></i></a></li>@endif
                        @if($youtubeLink)<li style="font-size: 20px"><a href="{{ $youtubeLink }}" target="_blank"><i class="fa fa-youtube"></i></a></li>@endif
                        @if($twitterLink)<li style="font-size: 20px"><a href="{{ $twitterLink }}" target="_blank"><i class="fa fa-twitter"></i></a></li>@endif
                    </ul>
                    @endif
                </div>
                <div class="right">
                    <h4>Описание вуза</h4>
                    <div class="excerpt">
                        {!! $university->description !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
    ul li {
        display: inline;
        padding: 5px;
    }
</style>
