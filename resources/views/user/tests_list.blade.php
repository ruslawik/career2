@extends('user.layout2')

@section('title', $title)

@section('content')

    <style>
        .desc_page_block {
            background: #F9DF70;
            width: 100%;
            padding: 20px;
        }

        .desc_page_block2 {
            background: white;
            width: 100%;
            padding: 20px;
        }

        .desc_page_block3 {
            background: #5FB3F9;
            width: 100%;
            padding: 20px;
        }

        .desc_page_block4 {
            background: #5FB3F9;
            width: 100%;
            padding: 20px;
        }

        .recommend {
            color: #77B83A;
            font-size: 20px;
        }

        .recommend2 {
            color: #F9DF70;
            font-size: 20px;
        }

        .info {
            padding: 20px;
            font-size: 18px !important;
        }
    </style>
    <?php if(app()->getLocale() == "ru") { ?>

    <div class="content__body" data-aos="zoom-in-left" data-aos-delay="1000">
        <a class="goBack__btn">Список тестов</a>
        <div class="internship topCourse course__cards">
            <h3>Выберите тест для просмотра информации и его прохождения</h3>
            <div class="internship__cont courseCards__cont">
                <div class="carousel-cell">
                    <a href="/user/tests/mapp">
                        <div class="internship__img">
                            <img src="{{ asset('img/tests/mapp.png') }}">
                        </div>
                        <div class="internship__desc">
                            <div class="internship__title">MAPP</div>
                            <div class="internship__excerpt">
                                MAPP (Мотивационная Оценка Личностного Потенциала) – это тест на определение профессии.
                                Тест MAPP идеально подходит для студентов, выпускников и сотрудников. Вы получите
                                огромное количество информации, которая поможет найти правильную профессию,
                                соответствующую вашему уникальному профилю оценки.
                            </div>
                        </div>
                    </a>
                </div>
                <div class="carousel-cell">
                    <a href="/user/tests/disc-preview">
                        <div class="internship__img">
                            <img src="{{ asset('img/tests/disc.png') }}">
                        </div>
                        <div class="internship__desc">
                            <div class="internship__title">DISC</div>
                            <div class="internship__excerpt">
                                DISC — группа психологических описаний, развитая Джоном Гайером и основанная на работах
                                психолога Уильяма Марстона (1893—1947). Это четырехсекторная поведенческая модель для
                                исследования поведения людей в окружающей их среде или в определённой ситуации. DISC
                                рассматривает стили поведения и предпочтения в поведении.
                            </div>
                        </div>
                    </a>
                </div>
                <div class="carousel-cell">
                    <a href="/user/tests/solomin-preview">
                        <div class="internship__img">
                            <img src="{{ asset('img/tests/orientation.png') }}">
                        </div>
                        <div class="internship__desc">
                            <div class="internship__title">Ориентация им. И.Л. Соломина</div>
                            <div class="internship__excerpt">
                                Анкета 'Ориентация' определяет профессиональную направленность личности к определенной
                                сфере деятельности. Опросник профориентации был разработан петербургским психологом
                                Игорем Леонидовичем Соломиным.
                                Тест Соломина применяется для самооценки профессиональных интересов и способностей
                                молодых и взрослых людей.
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <?php } else { ?>

    <div class="content__body" data-aos="zoom-in-left" data-aos-delay="1000">
        <div class="about__profession">
            <a class="goBack__btn">Тесттер тiзiмi</a>
            <div class="internship topCourse course__cards">
                <h3>Тесттi таңдаңыз</h3>
                <div class="internship__cont courseCards__cont">
                    <div class="carousel-cell">
                        <a href="/user/tests/mapp">
                            <div class="internship__img">
                                <img src="{{ asset('img/tests/mapp.png') }}">
                            </div>
                            <div class="internship__desc">
                                <div class="internship__title">MAPP</div>
                                <div class="internship__excerpt">
                                    MAPP (Жеке потенциалды мотивациялық бағалау) - бұл мамандықты анықтауға арналған
                                    тест. MAPP тесті - студенттер, түлектер мен қызметкерлер үшін өте қолайлы. Сіздің
                                    өзіңіздің қызығушылықтарыңызға сәйкес, мамандығыңызды анықтауға көмектесетін
                                    көптеген ақпарат ала аласыз.
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="carousel-cell">
                        <a href="/user/tests/disc-preview">
                            <div class="internship__img">
                                <img src="{{ asset('img/tests/disc.png') }}">
                            </div>
                            <div class="internship__desc">
                                <div class="internship__title">DISC</div>
                                <div class="internship__excerpt">
                                    DISC — группа психологических описаний, развитая Джоном Гайером и основанная на
                                    работах
                                    психолога Уильяма Марстона (1893—1947). Это четырехсекторная поведенческая модель
                                    для
                                    исследования поведения людей в окружающей их среде или в определённой ситуации. DISC
                                    рассматривает стили поведения и предпочтения в поведении.
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="carousel-cell">
                        <a href="/user/tests/solomin-preview">
                            <div class="internship__img">
                                <img src="{{ asset('img/tests/orientation.png') }}">
                            </div>
                            <div class="internship__desc">
                                <div class="internship__title">Ориентация им. И.Л. Соломина</div>
                                <div class="internship__excerpt">
                                    Анкета 'Ориентация' определяет профессиональную направленность личности к
                                    определенной
                                    сфере деятельности. Опросник профориентации был разработан петербургским психологом
                                    Игорем Леонидовичем Соломиным.
                                    Тест Соломина применяется для самооценки профессиональных интересов и способностей
                                    молодых и взрослых людей.
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
    }
    ?>
@endsection
