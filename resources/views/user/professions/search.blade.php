@extends('user.layout2')

@section('title', $title)

@section('content')
        <div class="content__body" data-aos="zoom-in-left" data-aos-delay="1000">
            <form style="width:100% !important;" action="/user/profession/search" method="GET">
            <div class="bigSearchBox">
                <div class="mySelect">
                    <select name="category">
                        <option value="-1" data-display="Все категории"></option>
                        @foreach($categories as $category)
                            <option value="{{$category->id}}" @if($selected_cat==$category->id) selected @endif>{{$category->name}}</option>
                        @endforeach
                    </select>
                </div>
                <input type="search" name="q" value="{{$q}}" placeholder="Ищите по названию или описанию профессий">
                <div class="btns">
                    <button type="submit" class="search__btn">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g clip-path="url(#clip012)">
                                <path d="M13.5038 20.9967C7.7069 20.9967 3.00757 16.2974 3.00757 10.5005C3.00757 4.70356 7.70685 0.0043335 13.5038 0.0043335C19.3007 0.0043335 24 4.70361 24 10.5005C24 16.2974 19.3006 20.9967 13.5038 20.9967ZM13.5038 3.00326C9.36312 3.00326 6.0065 6.35988 6.0065 10.5005C6.0065 14.6412 9.36312 17.9978 13.5038 17.9978C17.6444 17.9978 21.001 14.6412 21.001 10.5005C21.001 6.35988 17.6444 3.00326 13.5038 3.00326Z" fill="white"/>
                                <path d="M1.50815 23.9956C1.10958 23.9979 0.726507 23.8414 0.443545 23.5607C-0.144446 22.9775 -0.148323 22.0282 0.434863 21.4402C0.437757 21.4373 0.440651 21.4344 0.443545 21.4315L6.08148 15.7936C6.69015 15.2056 7.66023 15.2224 8.24822 15.8311C8.83621 16.4397 8.81942 17.4098 8.21075 17.9978L2.57276 23.5607C2.2898 23.8414 1.90673 23.9979 1.50815 23.9956Z" fill="white"/>
                            </g>
                            <defs>
                                <clipPath id="clip012">
                                    <rect width="24" height="24" fill="white"/>
                                </clipPath>
                            </defs>
                        </svg>
                        Поиск
                    </button>
                </div>
            </div>
            </form>
            <div class="suggestions">
                <div class="section__title">Предложения категорий: </div>
                <div class="internship topCourse course__cards">
                    <div class="internship__cont courseCards__cont">
                        @foreach($teklifler as $teklif)
{{--                        <a href="?category={{$teklif->id}}"><button @if($selected_cat==$teklif->id) class="active" @endif type="button">{{$teklif->name}}</button></a>--}}
                            <div class="carousel-cell @if($selected_cat==$teklif->id) active @endif">
                                <a style="width:100% !important;" href="?category={{$teklif->id}}">
                                    <div class="internship__img">
                                        @if($teklif->img)
                                            <img src="/storage/profession-cats-images/{{$teklif->img}}">
                                        @else
                                            <img src="{{ asset('img/no-image.png') }}">
                                        @endif
                                    </div>
                                    <div class="internship__desc">
                                        <div class="internship__title">{{$teklif->name}}</div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="filterBar">
                <div class="filter__desc" style="margin-left:10px !important;">
                    <h3>Результатов: {{$professions->total()}} </h3>
                    <p>введите запрос или выберите категорию</p>
                </div>

            </div>
            <div class="professions__cards">
                <div class="professions__cont internship__cont mentors">
                    @foreach($professions as $prof)
                    <div class="profession__card">
                        <div class="carousel-cell">
                            <div class="mentor__top">
                                <div class="internship__desc">
                                    <div class="internship__title">{{$prof->name}}</div>
                                </div>
                                <div class="internship__img">

                                </div>
                            </div>
                            <div class="mentor__bottom">
                                <div class="internship__vacans">{{$prof->salary}}</div>
                                <p>
                                    {{$prof->small_desc($prof->description)}}
                                </p>
                                <a href="/user/profession/{{$prof->id}}">Перейти</a>
                                <!--<h4 class="internship__category">Просмотреть курсы</h4>!-->
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="statistics">Показано {{$professions->count()}} из {{$professions->total()}} записей</div>
                {{$professions->links()}}
            </div>
        </div>
@endsection
<style>
    .admin__content .topCourse .internship__cont .active{
        border: 1px solid #33339B;
    }
</style>
