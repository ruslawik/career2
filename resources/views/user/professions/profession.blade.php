@extends('user.layout2')

@section('title', $title)

@section('content')
        <div class="content__body" data-aos="zoom-in-left" data-aos-delay="1000">
            <div class="about__profession">
                <a href="#/" onClick="window.history.back();" class="goBack__btn">Вернуться к поиску</a>
                <div class="about__profession__box">
                    <div class="left" style="width: 100%">
                        <div class="name">{{$prof->category->name}}</div>
                        <div class="faculty">{{$prof->name}}</div>
                        <ul>
                            <li>Средняя зарплата - <strong>{{$prof->salary}}</strong></li>
                            <li>Навыки - <strong>{{$prof->skills}}</strong></li>
                        </ul>
                        <br>
                        <h4>Описание профессии</h4>
                        <div class="excerpt">
                            {!! $prof->description !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
