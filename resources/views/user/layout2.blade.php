<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="/new/js/html5shiv.js"></script>
    <link rel="stylesheet" href="/new/css/main.css">
    <link rel="stylesheet" href="/new/css/media.css">
    <link rel="stylesheet" href="/new/css/libs.css">
    @yield('mapp_css')
    <title>@yield('title')</title>
</head>
<body>

<div class="admin__panel">
    @include('user.__include.menu2')
    <div class="admin__content" data-aos="fade-left" data-aos-duration="1000">
        <div class="content__header" data-aos="zoom-out-down" data-aos-delay="1000">
            <div class="header__title">
                <div class="menu__icon">
                    <svg width="29" height="18" viewBox="0 0 29 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M22.5 18H1.50001C0.671548 18 0 17.3284 0 16.5C0 15.6716 0.671548 15 1.50001 15H22.5C23.3285 15 24.0001 15.6716 24.0001 16.5C24 17.3284 23.3285 18 22.5 18Z" fill="black"/>
                        <path d="M27.5 10.5H6.50001C5.67155 10.5 5 9.82848 5 9.00007C5 8.17166 5.67155 7.50006 6.50001 7.50006H27.5C28.3285 7.50006 29.0001 8.17161 29.0001 9.00007C29.0001 9.82853 28.3285 10.5 27.5 10.5Z" fill="black"/>
                        <path d="M22.5 3.00002H1.50001C0.671548 3.00002 0 2.32847 0 1.50001C0 0.671548 0.671548 0 1.50001 0H22.5C23.3285 0 24.0001 0.671548 24.0001 1.50001C24.0001 2.32847 23.3285 3.00002 22.5 3.00002Z" fill="black"/>
                    </svg>
                </div>
                {{ $head_title }}
            </div>
            <button class="header__notify__btn" type="button">
            </button>
            <div class="header__account">
                <?php
                    if(app()->getLocale() == "kz"){
                ?>
                    <a href="/local/ru">РУС</a>&nbsp;&nbsp;
                <?php
                    }else{
                ?>
                    <a href="/local/kz">ҚАЗ</a>&nbsp;&nbsp;
                <?php
                    }
                ?>
                <div class="account__img">
                    <?php
                        $user_img_q = \App\User::where('id', Auth::user()->id)->get();
                        $img = $user_img_q[0]->img;
                    ?>
                    @if($img!=NULL)
                        <img src="/storage/user-avatars/{{$img}}" alt="">
                    @else
                        <img src="/new/img/user_icon.png" alt="">
                    @endif
                </div>
                <div class="account__desc">
                    <a href="/user/profile"><h3 class="account__name">{{ $username }}</h3></a>
                    <div class="account__staff"><a href="/logout">Выйти</a></div>
                </div>
            </div>
        </div>
        @yield('content')
    </div>
</div>

<script src="/new/js/jquery.min.js"></script>
<script src="/new/js/script.js"></script>
@yield('mapp_pass_javascript')
</body>
</html>