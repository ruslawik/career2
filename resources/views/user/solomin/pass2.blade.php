@extends('user.layout2')

@section('title', $title)

@section('content')
        <div class="content__body" data-aos="zoom-in-left" data-aos-delay="1000">
            <div class="test__title">
                <p>Прохождение теста</p>
            </div>
            <div class="test__steps">
                <h3>шаг <span id="now_step_label">1</span> из 2</h3>
                <div class="step__bar">
                    <div class="step__line" style="width: 50%"></div>
                </div>
            </div>
            <h3 class="test__subtitle">Напротив каждого высказывания выберите цифру, соответствующую степени вашего желания заниматься этим видом деятельности:
                0 - вовсе нет; 1 - пожалуй, так; 2 - верно; 3 - совершенно верно.</h3>

            @if ($errors->any())
    <div style="color:red; font-weight:bold;margin-bottom: 30px !important;">
        <ul style="margin-left:20px;">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

            <div class="test__cont">
            <form action="{{ $action }}" method="POST" style="width:100% !important;">
                {{ csrf_field() }}
                <div class="test__box">
                    <div class="box__header orientation__box__header">
                        <h3>Блок #<span id="now_step_label2">1</span></h3>
                        <div class="checkbox__examples">
                            <div class="item">0</div>
                            <div class="item">1</div>
                            <div class="item">2</div>
                            <div class="item">3</div>
                        </div>
                    </div>
                    <div class="box__body">
                        <div id="block1">
                        @foreach($questions1 as $question)
                        <div class="test__item">
                            <div class="stepCheck__item">
                                <div class="stepCheck">
                                    <input type="radio" id="stepCheck1-{{$question->id}}" name="check{{$question->id}}" value="0">
                                    <label for="stepCheck1-{{$question->id}}"><span>0</span></label>
                                </div>
                                <div class="stepCheck">
                                    <input type="radio" id="stepCheck2-{{$question->id}}" name="check{{$question->id}}" value="1">
                                    <label for="stepCheck2-{{$question->id}}"><span>1</span></label>
                                </div>
                                <div class="stepCheck">
                                    <input type="radio" id="stepCheck3-{{$question->id}}" name="check{{$question->id}}" value="2">
                                    <label for="stepCheck3-{{$question->id}}"><span>2</span></label>
                                </div>
                                <div class="stepCheck">
                                    <input type="radio" id="stepCheck4-{{$question->id}}" name="check{{$question->id}}" value="3">
                                    <label for="stepCheck4-{{$question->id}}"><span>3</span></label>
                                </div>
                                <h4>
                                    @if($lang=="ru")
                                        {{ $question->text }}
                                    @endif
                                    @if($lang=="kz")
                                        {{ $question->text_kaz }}
                                    @endif
                                </h4>
                            </div>
                        </div>
                        @endforeach
                        <br>
                        <div class="test__btns" style="margin-right:10px;">
                    <button class="prev" disabled type="button">
                        <svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M12 18.5L6 12.5L12 6.5" stroke="#40189D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M19 18.5L13 12.5L19 6.5" stroke="#40189D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                        Назад
                    </button>
                    <button class="next" type="button" onClick="show_quest(2,1);">
                        Далее
                        <svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M12 18.5L18 12.5L12 6.5" stroke="#40189D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M5 18.5L11 12.5L5 6.5" stroke="#40189D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                    </button>
                    </div>
                    <br>

                        </div>
                        <div id="block2" style="display: none;">
                        @foreach($questions2 as $question)
                        <div class="test__item">
                            <div class="stepCheck__item">
                                <div class="stepCheck">
                                    <input type="radio" id="stepCheck1-{{$question->id}}-2" name="two_check{{$question->id}}" value="0">
                                    <label for="stepCheck1-{{$question->id}}-2"><span>0</span></label>
                                </div>
                                <div class="stepCheck">
                                    <input type="radio" id="stepCheck2-{{$question->id}}-2" name="two_check{{$question->id}}" value="1">
                                    <label for="stepCheck2-{{$question->id}}-2"><span>1</span></label>
                                </div>
                                <div class="stepCheck">
                                    <input type="radio" id="stepCheck3-{{$question->id}}-2" name="two_check{{$question->id}}" value="2">
                                    <label for="stepCheck3-{{$question->id}}-2"><span>2</span></label>
                                </div>
                                <div class="stepCheck">
                                    <input type="radio" id="stepCheck4-{{$question->id}}-2" name="two_check{{$question->id}}" value="3">
                                    <label for="stepCheck4-{{$question->id}}-2"><span>3</span></label>
                                </div>
                                <h4>
                                    @if($lang=="ru")
                                        {{ $question->text }}
                                    @endif
                                    @if($lang=="kz")
                                        {{ $question->text_kaz }}
                                    @endif
                                </h4>
                            </div>
                        </div>
                        @endforeach
                        <br>
                        <div class="test__btns" style="margin-right:10px;">
                    <button class="prev" type="button" onClick="show_quest(1,2);">
                        <svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M12 18.5L6 12.5L12 6.5" stroke="#40189D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M19 18.5L13 12.5L19 6.5" stroke="#40189D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                        Назад
                    </button>
                    <button class="next" type="submit">
                        Завершить
                        <svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M12 18.5L18 12.5L12 6.5" stroke="#40189D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M5 18.5L11 12.5L5 6.5" stroke="#40189D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                    </button>
                    </div>
                    <br>
                        </div>
                    </div>
                </div>
            </form>
            </div>
        </div>
@endsection

@section('mapp_pass_javascript')
<script>
    function show_quest(next_id, now_id){
        if(next_id <= 2 && next_id >=1){
            $("#now_step_label").html(next_id);
            $("#now_step_label2").html(next_id);
            var step_line_width = ((next_id)/2)*100;
            $(".step__line").width(step_line_width+'%');
            jQuery("#block"+next_id).toggle('fast');
            jQuery("#block"+now_id).toggle('fast');
        }
        jQuery("html, body").animate({ scrollTop: 0 }, "slow");
    }
    //show_quest(1,0);

</script>
@endsection