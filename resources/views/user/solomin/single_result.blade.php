@extends('user.layout2')

@section('title', $title)

@section('content')

        <div class="content__body" data-aos="zoom-in-left" data-aos-delay="1000">
            <div class="test__title">
                <p>Пользователь - {{$username}}</p>
                <p>Дата сдачи - {{$date}}</p>
                <br>
            </div>

                <div class="orientation__tables">
                    <div class="orientation__table">
                        <div class="table__header">
                            <h3>Таблица «Я хочу»</h3>
                        </div>
                        <div class="table__body">
                            <p>1) человек - человек - {{$elovek_4elovek_1}}</p>
                            <p>2) человек - техника - {{$elovek_texnika_1}}</p>
                            <p>3) человек - знаковая система - {{$elovek_znakov_1}}</p>
                            <p>4) человек - художественный образ - {{$elovek_hud_1}}</p>
                            <p>5) человек - природа - {{$elovek_priroda_1}}</p>
                            <p>А) исполнительские - {{$harakter_a_ispolnit_1}}</p>
                            <p>Б) творческие - {{$harakter_b_tvor4_1}}</p>
                        </div>
                    </div>
                    <div class="orientation__table">
                        <div class="table__header">
                            <h3>Таблица «Я могу»</h3>
                        </div>
                        <div class="table__body">
                            <p>1) человек - человек - {{$elovek_4elovek_2}}</p>
                            <p>2) человек - техника - {{$elovek_texnika_2}}</p>
                            <p>3) человек - знаковая система - {{$elovek_znakov_2}}</p>
                            <p>4) человек - художественный образ - {{$elovek_hud_2}}</p>
                            <p>5) человек - природа - {{$elovek_priroda_2}}</p>
                            <p>А) исполнительские - {{$harakter_a_ispolnit_2}}</p>
                            <p>Б) творческие - {{$harakter_b_tvor4_2}}</p>
                        </div>
                    </div>
                </div>

            <div class="test__answers--desc">
                <h3>Ниже Вы можете просмотреть детализацию ответов</h3>
                <button>Показать/скрыть детали</button>
            </div>

            <div class="test__cont test__answers">
                <div class="orientation__tables">
                    <div class="orientation__table">
                        <div class="table__header">
                            <h3>Таблица «Я хочу»</h3>
                        </div>
                        <div class="table__body">
                            <div class="question__cont">
                                @foreach ($results1 as $result)
                                    <div class="question">
                                        <h4>Вопрос {{ $result->quest_id }}</h4> 
                                        <p>{{ $result->quest_text->text }} </p>
                                        <p>Ответ: {{ $result->answer }}</p>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="orientation__table">
                        <div class="table__header">
                            <h3>Таблица «Я могу»</h3>
                        </div>
                        <div class="table__body">
                            <div class="question__cont">
                                @foreach ($results2 as $result)
                                    <div class="question">
                                        <h4>Вопрос {{ $result->quest_id }}</h4> 
                                        <p>{{ $result->quest_text->text }} </p>
                                        <p>Ответ: {{ $result->answer }}</p>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection