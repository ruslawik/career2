@extends('user.layout2')

@section('title', $title)

@section('content')
        <div class="content__body" data-aos="zoom-in-left" data-aos-delay="1000">
            <div class="account__datas">
                <div class="account__img progress--circle progress--88">
                    <a href="/user/profile">
                        @if($img!=NULL)
                            <img src="/storage/user-avatars/{{$img}}" alt="">
                        @else
                            <img src="/new/img/user_icon.png" alt="">
                        @endif
                    </a>
                </div>
                <div class="account__desc">
                    <a href="/user/profile"><h3 class="account__name">{{ $username }}</h3></a>
                    <div class="account__staff">Клиент</div>
                </div>
{{--                <div class="skills">--}}
{{--                    <div class="skills__item progress-bar" data-percent="76" data-duration="1000" data-color="#F0F0F0,#F09340">--}}
{{--                        <div class="skill__title">Доктор?</div>--}}
{{--                    </div>--}}
{{--                    <div class="skills__item progress-bar" data-percent="31" data-duration="1000" data-color="#F0F0F0,#62A034">--}}
{{--                        <div class="skill__title">Учитель?</div>--}}
{{--                    </div>--}}
{{--                    <div class="skills__item progress-bar" data-percent="7" data-duration="1000" data-color="#F0F0F0,#56A791">--}}
{{--                        <div class="skill__title">Юрист?</div>--}}
{{--                    </div>--}}
{{--                </div>--}}
                <a href="/user/tests"><button type="button" class="test__btn">Сдать тест</button></a><br>
                @if($isPassedTest)
                <a href="/user/mapp/results"><button type="button" class="test__btn">Полный результат</button></a>
                @endif
            </div>
            <main>
                <h2 class="recommendedJobs">
                    @if($isPassedTest)
                    <h2 class="mb-4">Топ-15 сфер деятельности</h2>
                    <div class="text__box">
                        <div class="table__cont">
                            <table>
                                <thead>
                                <tr>
                                    <th>{{ __('all.types_of_work') }}</th>
                                    <th>{{ __('all.motivation_perc') }}</th>
                                    <th>{{ __('all.motivation_level') }}</th>
                                    <th>{{ __('all.potent_kurstar') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $i=0;
                                ?>
                                @foreach($all_jobs[1] as $job)
                                    <tr>
                                        <td>{{$job}}</td>
                                        <td>{{$all_jobs[2][$i]}}</td>
                                        <td>{{$all_jobs[3][$i]}}</td>
                                        <td>
                                            <a target="_blank" href="/user/course/search?q={{$job}}" class="goTo">{{ __('all.go_to_courses') }}</a>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                    if($i==15){
                                        break;
                                    }
                                    ?>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    @else
                        <div class="text__box">
                            <h6>Карьерный тест MAPP™ — это первый и наиболее полный онлайн-тест для потребителей. Более 8 миллионов человек почти во всех странах мира прошли тест MAPP с момента его создания в 1995 году. МАРР - идеально подходит для студентов, выпускников и работающих взрослых. Вы получите обширную информацию, которая поможет найти правильную карьеру, соответствующую вашему уникальному профилю оценки.</h6>
                            <a href="/user/tests"><button type="button" class="btn-info">Сдать тест</button></a>
                            <br>
                            @if($video)
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12" style="margin: 0 auto">
                                        <div class="embed-responsive embed-responsive-16by9">
                                            <iframe class="embed-responsive-item" width="100%" height="400"
                                                    src="https://www.youtube.com/embed/{{ $video->url }}" title="YouTube video player" frameborder="0"
                                                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                                    allowfullscreen></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>

                    @endif
{{--                <div class="internship">--}}
{{--                    <div class="section__title">--}}
{{--                        <img src="/new/img/growing-knowledge1.svg" alt="">--}}
{{--                        Компании, в которых можно пройти стажировку от <strong>CareerVision</strong>--}}
{{--                    </div>--}}
{{--                    <div class="internship__cont">--}}
{{--                        <div class="internship__carousel">--}}
{{--                            @foreach($internships as $internship)--}}
{{--                            <div class="carousel-cell">--}}
{{--                                <a style="width:100% !important;" href="/user/internship/{{$internship->id}}">--}}
{{--                                    <div class="internship__img" style="margin-bottom: 8px;">--}}
{{--                                        <img src="/storage/internship-images/{{$internship->img}}">--}}
{{--                                    </div>--}}
{{--                                    <div class="internship__desc">--}}
{{--                                        <div class="internship__title">{{$internship->name}}</div>--}}
{{--                                        <div class="internship__vacans">{{$internship->location}}</div>--}}
{{--                                    </div>--}}
{{--                                </a>--}}
{{--                            </div>--}}
{{--                            @endforeach--}}
{{--                        </div>--}}
{{--                        <a href="/user/internship" class="more__btn">Больше--}}
{{--                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">--}}
{{--                                <path d="M23.5607 10.9394L18.2461 5.62482C17.9532 5.3319 17.5693 5.18546 17.1854 5.18546C16.8015 5.18546 16.4176 5.3319 16.1247 5.62482C15.539 6.21062 15.539 7.16035 16.1247 7.74615L18.8787 10.5001L1.5 10.5001C0.671578 10.5001 0 11.1716 0 12.0001C0 12.8285 0.671578 13.5001 1.5 13.5001L18.8787 13.5001L16.1247 16.254C15.539 16.8398 15.539 17.7895 16.1247 18.3753C16.7106 18.9611 17.6602 18.9611 18.2461 18.3753L23.5607 13.0607C24.1464 12.4749 24.1464 11.5252 23.5607 10.9394Z" fill="#40189D"/>--}}
{{--                            </svg>--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--                </div>--}}
                <div class="internship topCourse">
                    <div class="section__title">
                        <img src="/new/img/topcourse.svg" alt="">
                        Топ-курсы от <strong>CareerVision</strong>
                    </div>
                    <div class="internship__cont">
                        <div class="internship__carousel">
                            @foreach($courses as $course)
                            <div class="carousel-cell">
                                <a target="_blank" style="width:100% !important;" href="{{$course->link}}">
                                <div class="internship__img">
                                    <img src="/storage/course-images/{{$course->img}}">
                                </div>
                                <div class="internship__desc">
                                    <div class="internship__title">{{$course->name}}</div>
                                    <div class="internship__excerpt">{{$course->very_small_desc($course->description)}}</div>
                                </div>
                                </a>
                            </div>
                            @endforeach
                        </div>
                        <a href="/user/course/search" class="more__btn">Больше
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M23.5607 10.9394L18.2461 5.62482C17.9532 5.3319 17.5693 5.18546 17.1854 5.18546C16.8015 5.18546 16.4176 5.3319 16.1247 5.62482C15.539 6.21062 15.539 7.16035 16.1247 7.74615L18.8787 10.5001L1.5 10.5001C0.671578 10.5001 0 11.1716 0 12.0001C0 12.8285 0.671578 13.5001 1.5 13.5001L18.8787 13.5001L16.1247 16.254C15.539 16.8398 15.539 17.7895 16.1247 18.3753C16.7106 18.9611 17.6602 18.9611 18.2461 18.3753L23.5607 13.0607C24.1464 12.4749 24.1464 11.5252 23.5607 10.9394Z" fill="#40189D"/>
                            </svg>
                        </a>
                    </div>
                </div>
            </main>
        </div>

@endsection
<style>
    .admin__content .table__cont th, .admin__content .table__cont td {
        padding: 5px !important;
    }
</style>
