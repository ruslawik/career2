@if ($paginator->hasPages())
    <div class="pagination">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <button class="nextPrev__btn">
                <svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M12 18.5L6 12.5L12 6.5" stroke="#40189D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M19 18.5L13 12.5L19 6.5" stroke="#40189D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                        Пред
            </button>
        @else
        <a href="{{ $paginator->previousPageUrl() }}" class="nextPrev__btn">
                <svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M12 18.5L6 12.5L12 6.5" stroke="#40189D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M19 18.5L13 12.5L19 6.5" stroke="#40189D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                        Пред
        </a>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="disabled" aria-disabled="true"><span>{{ $element }}</span></li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
            <div class="pages">
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <a href="#/" class="active">{{ $page }}</a>
                    @else
                        <a href="{{ $url }}">{{ $page }}</a>
                    @endif
                @endforeach
            </div>
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
                    <a class="nextPrev__btn" href="{{ $paginator->nextPageUrl() }}">
                        След
                        <svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M12 18.5L18 12.5L12 6.5" stroke="#40189D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M5 18.5L11 12.5L5 6.5" stroke="#40189D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                    </a>
        @else
            <button class="nextPrev__btn">
                        След
                        <svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M12 18.5L18 12.5L12 6.5" stroke="#40189D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M5 18.5L11 12.5L5 6.5" stroke="#40189D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                    </button>
        @endif
    </div>
@endif
