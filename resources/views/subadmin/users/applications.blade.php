@extends('manage.layout')

@section('title', $title)

@section('content')

      <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-success">Список</span> Нажмите на имя для просмотра полной информации по заявке
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>

    <div class="col-lg-12">
            <table id="table" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>№ заявки</th>
                        <th>Имя</th>
                        <th>Фамилия</th>
                        <th>Дата подачи</th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($apps as $app)
                            <tr>
                                <td>{{ $app->id }}</a></td>
                                <td><a href="/manage/app/{{ $app->id }}">{{ $app->name }}</td>
                                <td>{{ $app->surname }}</td>
                                <td>{{ $app->created_at }}</td>
                            </tr>
                        @endforeach
                    </tbody>
            </table>
    </div>



@endsection

@section('datatable_js')
<script src="/manage_res/assets/js/lib/data-table/datatables.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/dataTables.bootstrap.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/dataTables.buttons.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.bootstrap.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/jszip.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/pdfmake.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/vfs_fonts.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.html5.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.print.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.colVis.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/datatables-init.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
          	jQuery('#table').dataTable({
        			"order": [[0, "desc"]],
        			"dom": 'lBfrtip',
        			"buttons": [
            			'copy', 'csv', 'excel', 'pdf', 'print'
        			]
    			});
        	});
    </script>

@endsection