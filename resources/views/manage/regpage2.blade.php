@extends('frontend.layouts.app')
@php
    $title = 'Регистрация';
@endphp
@section('title', $title)

@section('content')
    <section class="hero">
        <div class="container">
            <div class="row align-items-center justify-content-between">
                <div class="col-12 col-lg-7 col-xl-5 text-center text-lg-start content mt-5 align-center"><h1 class="h1">
                        CAREER VISION KAZAKHSTAN</h1>
                    <p>Программа профессиональной ориентации</p></div>
                <div class="col-12 col-lg-5 col-xl-5 form">
                    <form action="{{$action}}" method="POST" class="g-3 needs-validation form-registration" novalidate>
                        {{csrf_field()}}
                        <h2 class="text-center">Регистрация</h2>
                        <div class="col-12 d-flex justify-content-center">
                            <a href="/" class="back-registration text-center my-1">Вернуться ко входу</a>
                        </div>
                        <p>Зарегистрируйтесь и получите доступ к атласу профессий, курсам, тестам и множеству других
                            материалов для профоориентации!</p>
                        @if ($errors->any())
                            @foreach ($errors->all() as $error)
                                <u style="color: red;" class="text-center">{{ $error }}</u><br>
                            @endforeach
                            <br>
                        @endif
                        <div class="row">
                            <div class="col-12 col-lg-6">
                                <input type="text" class="form-control" name="login" value="{{old('login')}}" placeholder="Придумайте логин" style="color: #000" required>
                            </div>
                            <div class="col-12 col-lg-6">
                                <input type="email" class="form-control" name="email" value="{{old('email')}}" placeholder="Ваш email" style="color: #000" required>
                            </div>
                            <div class="col-12 col-lg-6">
                                <input type="text" class="form-control" name="name" value="{{old('name')}}" placeholder="Ваше имя" style="color: #000" required></div>
                            <div class="col-12 col-lg-6">
                                <input type="text" class="form-control" name="surname" value="{{old('surname')}}" placeholder="Ваша фамилия" style="color: #000" required>
                            </div>
                            <div class="col-12">
                                <input type="password" class="form-control" name="password" placeholder="Придумайте пароль" style="color: #000" required>
                            </div>
                            <div class="col-12">
                                <input type="password" class="form-control" name="repassword" placeholder="Повторите пароль" style="color: #000" required>
                            </div>
                        </div>
                        <div class="btn-container d-flex justify-content-center">
                            <button type="submit" class="btn btn-info enter">Зарегистрироваться</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
