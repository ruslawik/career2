@extends('manage.layout')
@php
    $title = 'Изменить видео';
@endphp
@section('title', $title)

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <!-- Default box -->
                <div class="card">
                    <div class="card-header">
                        <a href="{{ route('videos.create') }}" class="btn btn-info">+ Добавить</a>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        @include('manage.partials.errors')
                        <form action="{{ route('videos.update', $video) }}" method="post" enctype="multipart/form-data">
                            @csrf
                            @method('put')
                            @include('manage.videos._form')

                        </form>
                    </div>
                </div>
            </div>
            <!-- /.card -->
        </div>
    </section>
@endsection
