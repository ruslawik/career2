<div class="card-body">
    <div class="form-group">
        <label for="link">Ссылка</label>
        <input name="link" id="link" type="text"
               class="form-control @error('link') is-invalid @enderror"
               value="{{ old('link', $video->link ?? null) }}">

        @error('link')
        <span class="invalid-feedback">{{ $message }}</span>
        @enderror
    </div>
    <div class="form-group">
        <label for="type_id">{{ __('Тип') }}</label>
        <select name="type_id" id="type_id" class="form-control @error('type_id') is-invalid @enderror">
            <option value>{{ __('Выберите тип') }}</option>

            @foreach ($types as $key => $value)
                    <option
                        value="{{ $key }}"
                        @if(old('type_id') == $key || isset($video) && $video->type_id == $key) selected="selected" @endif
                    >
                        {{ $value }}
                    </option>
            @endforeach
        </select>

        @error('type_id')
        <span class="invalid-feedback">{{ $message }}</span>
        @enderror
    </div>

</div>
    <button type="submit" class="btn btn-success float-right">Сохранить</button>
