<table class="table table-bordered">
    <thead>
    <tr>
        <th style="width: 10px">#</th>
        <th>Ссылка</th>
        <th>Тип</th>
        <th style="width: 15%;">Действия</th>
    </tr>
    </thead>
    <tbody>
    @foreach($videos as $video)
        <tr id="{{ $video->id }}" data-index="{{ $loop->index }}">
            <td>{{ $video->id }}</td>
            <td>{{ $video->link }}</td>
            <td>{{ $video->getType() }}</td>
            <td>
                <div class='btn-group'>
                    <form action="{{ route('videos.destroy', $video) }}" class="d-inline"
                          method="post">
                        @csrf
                        @method('delete')
                        <a href="{{ route('videos.edit', $video) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                        <button type="submit" class="btn btn-danger"  onclick="return confirm('Вы действительно хотите удалить?')">
                            <i class="fa fa-trash"></i>
                        </button>
                    </form>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
