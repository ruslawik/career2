@extends('manage.layout')
@php
    $title = 'Видео';
@endphp
@section('title', $title)

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ $title }}</h1>
                </div>

            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-12">
                <!-- Default box -->
                <div class="card">
                    <div class="card-header">
                        <a href="{{ route('videos.create') }}" class="btn btn-info">+ Добавить</a>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        @include('manage.partials.errors')
                        @include('manage.videos._table')
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer clearfix">
                        @if ($videos->hasPages())
                            {{ $videos->links() }}
                        @endif
                    </div>
                </div>
                <!-- /.card -->
            </div>
        </div>

    </section>

@endsection

