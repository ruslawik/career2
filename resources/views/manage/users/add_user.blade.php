@extends('manage.layout')

@section('title', $title)

@section('content')

  <div class="col-sm-12">
                <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-warning">Внимание!</span> Вы можете добавлять новых пользователей и устанавливать их роли в системе.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-success">Описание ролей</span> <br><b>Subadmin</b> - может назначать тесты, добавлять новых пользователей (своих subadmin и клиентов), смотреть результаты тестов<br><b>Клиент</b> - имеет доступ к тестам, может проходить их, смотреть свои результаты
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>

    <div class="col-lg-6">
            <form method="POST" action="{{ $action }}">
                {{ csrf_field() }}
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Добавить пользователя</strong>
                        </div>
                        <div class="card-body">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            @if (session('status'))
                                <div class="alert alert-success">
                                    {!! session('status') !!}
                                </div>
                            @endif
                            <div class="input-group">
                                            <div class="input-group-addon">Выберите роль</div>
                                            <select name="role" class="form-control col-sm-12" id="role_selected">
                                                <option value="2">Клиент</option>
                                                <!--<option value="1">Администратор</option>!-->
                                                <option value="3">Sub-admin</option>
                                            </select>
                            </div><br>
                            <div class="input-group">
                                            <div class="input-group-addon">Придумайте логин</div>
                                            <input type="text" class="form-control col-sm-12" name="login">
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Введите имя</div>
                                            <input type="text" class="form-control col-sm-12" name="name">
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Введите фамилию</div>
                                            <input type="text" class="form-control col-sm-12" name="surname">
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Придумайте пароль</div>
                                            <input type="text" class="form-control col-sm-12" name="password">
                            </div>
                            <br>
                            <hr>
                                Введите логин и пароль тестируемого от <a href="https://recruit.assessment.com/CareerCenter/" target="_blank">https://recruit.assessment.com/CareerCenter/</a> (если требуется)
                                <div class="input-group">
                                            <div class="input-group-addon">Логин mapp</div>
                                            <input type="text" class="form-control col-sm-12" name="mapp_login">
                                </div>
                                <br>
                                <div class="input-group">
                                            <div class="input-group-addon">Пароль mapp</div>
                                            <input type="text" class="form-control col-sm-12" name="mapp_pass">
                                </div>
                            <hr><br>
                        </div>
                    </div>
            
    </div>

    <div class="col-lg-6" id="user_additionals">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Тесты к прохождению</strong>
                        </div>
                        <div class="card-body">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <td>Тест</td>
                                        <td>Выбрать</td>
                                        <td>Цена за тест</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>MAPP</td>
                                        <td>
                                            <input type="checkbox" name="mapp_test" value="1" class="form-control">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="mapp_test_price" value=25000>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>DISC</td>
                                        <td>
                                            <div class="form-check">
                                                <input type="radio" name="disc_test" id="disc_test" value="1" class="form-check-input"> 
                                                <label class="form-check-label" for="disc_test">
                                                   1 тест
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input type="radio" name="disc_test" id="disc_test" value="2" class="form-check-input"> 
                                                <label class="form-check-label" for="disc_test">
                                                   2 теста
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input type="radio" name="disc_test" id="disc_test" value="3" class="form-check-input"> 
                                                <label class="form-check-label" for="disc_test">
                                                   3 теста
                                                </label>
                                            </div>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="disc_test_price" value=10000>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Кейрси</td>
                                        <td>
                                            <input type="checkbox" name="keirsi_test" value="1" class="form-control">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="keirsi_test_price" value=0>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Холл</td>
                                        <td>
                                            <input type="checkbox" name="holl_test" value="1" class="form-control">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="holl_test_price" value=0>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Томас</td>
                                        <td>
                                            <input type="checkbox" name="tomas_test" value="1" class="form-control">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="tomas_test_price" value=0>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Соломин</td>
                                        <td>
                                            <input type="checkbox" name="solomin_test" value="1" class="form-control">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="solomin_test_price" value=0>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <button type="submit" class="btn btn-success" style="float:right;"><i class="fa fa-magic"></i>&nbsp; Добавить!</button>
                    </div>
                    </form>
    </div>


@endsection

@section('datatable_js')
    <script>
        jQuery("#role_selected").change(function() {
            var a = jQuery("#role_selected").val();
            if(a == 3){
                window.location.href = "/manage/add_subadmin";
            }
            if(a == 2){
                window.location.href = "/manage/add_user";
            }
        });
    </script>
@endsection