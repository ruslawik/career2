@extends('manage.layout')

@section('title', $title)

@section('content')

      <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-success">Инфо</span> Информация о пользователе {{ $username }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>

    <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">Пройденные тесты</strong>
                </div>
                <div class="card-body">
                    <h6>Тестирование DISC</h6>
                    <hr>
                     @foreach ($disc_results as $key => $result)
                        Завершенное тестирование DISC - {{ $key }}
                        <a target="_blank" href="/manage/user/{{ $user_id }}/disc/{{ $key }}/ru" class="btn-sm btn-success" style="color:white;cursor:pointer;"> Просмотреть</a>
                        <br><br>
                    @endforeach
                    <hr>
                    <h6>Тестирование MAPP</h6>
                    <hr>
                    @foreach ($mapp_results as $key => $result)
                        <a target="_blank" href="/manage/user/{{ $user_id }}/mapp/{{ $key }}">Завершенное тестирование MAPP- {{ $key }}</a>
                        @if($auth=="1")
                            <a href="/manage/download_mapp_results/{{$user_id}}/ru" target="_blank" class="btn-sm btn-success" style="color:white;cursor:pointer;"> Показать результаты</a>
                        @else
                            - <span style="font-size:12px;">Аккаунт не связан с MAPP системой, скачать результаты здесь невозможно, только просмотреть</span>
                        @endif
                        <br><br>
                    @endforeach
                    <hr>
                    <h6>Тестирование Кейрси</h6>
                    <hr>
                     @foreach ($keirsi_results as $key => $result)
                        Завершенное тестирование КЕЙРСИ - {{ $key }}
                        <a target="_blank" href="/manage/user/{{ $user_id }}/keirsi/{{ $key }}" class="btn-sm btn-success" style="color:white;cursor:pointer;"> Просмотреть</a>
                        <br><br>
                    @endforeach
                    <hr>
                    <h6>Тестирование "Социальный интеллект"</h6>
                    <hr>
                     @foreach ($holl_results as $key => $result)
                        Завершенное тестирование Холла - {{ $key }}
                        <a target="_blank" href="/manage/user/{{ $user_id }}/holl/{{ $key }}" class="btn-sm btn-success" style="color:white;cursor:pointer;"> Просмотреть</a>
                        <br><br>
                    @endforeach
                    <hr>
                    <h6>Тестирование "Поведение в конфликте"</h6>
                    <hr>
                     @foreach ($tomas_results as $key => $result)
                        Завершенное тестирование Томаса - {{ $key }}
                        <a target="_blank" href="/manage/user/{{ $user_id }}/tomas/{{ $key }}" class="btn-sm btn-success" style="color:white;cursor:pointer;"> Просмотреть</a>
                        <br><br>
                    @endforeach

                    <hr>
                    <h6>Тестирование Соломин</h6>
                    <hr>
                     @foreach ($solomin_results as $key => $result)
                        Завершенное тестирование Соломина - {{ $key }}
                        <a target="_blank" href="/manage/user/{{ $user_id }}/solomin/{{ $key }}" class="btn-sm btn-success" style="color:white;cursor:pointer;"> Просмотреть</a>
                        <br><br>
                    @endforeach

                </div>
    </div>



@endsection