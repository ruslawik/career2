@extends('manage.layout')

@section('title', $title)

@section('content')

      <div class="col-sm-12">
                <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-warning">Внимание!</span> Вы можете добавлять группы предпочтений по три в каждой. На данный момент редактировать введенные данные невозможно, эта функция появится немного позже. Результаты теста можно просмотреть <a href="/manage/mapp_results">здесь</a>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>

    <div class="col-lg-12">
            <form method="POST" action="{{ $action }}">
                {{ csrf_field() }}
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Предпочтения (сейчас в базе - {{ $group_number }})</strong>
                        </div>
                        <div class="card-body">
                            <font color="green"><b>Группа предпочтений № {{ $group_number+1 }}</b></font>
                            <input type="hidden" name="group_number" value="{{ $group_number+1 }}">
                            <div class="input-group">
                                    <div class="input-group-addon">Предпочтение 1</div>
                                    <input type="text" class="form-control" name="predp_1">
                            </div>
                            <div class="input-group">
                                    <div class="input-group-addon">Предпочтение 2</div>
                                    <input type="text" class="form-control" name="predp_2">
                            </div>
                            <div class="input-group">
                                    <div class="input-group-addon">Предпочтение 3</div>
                                    <input type="text" class="form-control" name="predp_3">
                            </div>
                            <br>
                            <button type="submit" class="btn btn-success"><i class="fa fa-magic"></i>&nbsp; Добавить группу</button>
                            </form>
                            <br><br>
                            <form action="/manage/mapp_edit/save_edited" method="POST">
                                {{ csrf_field() }}
                                <button type="submit" style="float:right;" class="btn btn-success"><i class="fa fa-magic"></i>&nbsp; Сохранить</button>
                            <br><br>
                            <?php $k = 1; ?>
                            @foreach ($groups as $key => $group)
                                <b>Предпочтение номер {{ $key }} </b>
                                @foreach ($group as $gr)
                                    <input type="hidden" name="tid{{$k}}" value="{{$gr['id']}}">
                                    <input type="text" name="text_of{{$k}}" class="form-control" value="{{ $gr['text'] }}">
                                    <?php $k++; ?>
                                @endforeach
                                <br>
                            @endforeach
                            <input type="hidden" name="total_preds" value="{{$k-1}}">
                            <button type="submit" class="btn btn-success"><i class="fa fa-magic"></i>&nbsp; Сохранить</button>
                            </form>
                        </div>
                    </div>
            
    </div>



@endsection