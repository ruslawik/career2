@extends('frontend.layouts.app')
@php
    $title = 'Вход';
@endphp
@section('title', $title)


@section('content')
    <section class="hero">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div class="col-12 col-lg-7 col-xl-5 text-center text-lg-start content mt-5 align-center"><h1
                        class="h1">
                        CAREER VISION KAZAKHSTAN</h1>
                    <p class="hero__subtitle">Программа профессиональной ориентации</p></div>
                <div class="col-12 col-lg-5 col-xl-4 form">
                    <form class="row g-3 needs-validation login-form" method="POST" action="{{ $action }}">
                        {{ csrf_field() }}

                        <h2 class="text-center">Вход</h2>
                        <p>Для входа в систему введите ниже свой логин и пароль...</p>
                        @if (session('error'))
                            <u style="color: red;" class="text-center">{{ session('error') }}</u>
                        @endif

                        <div class="col-12"><label for="validationCustom01" class="form-label">Логин</label>
                            <input type="text" class="form-control" id="login" name="login" placeholder="Логин" style="color: #000" required>
                            <div class="invalid-feedback">Введен неправильный email</div>
                        </div>
                        <div class="col-12"><label for="validationCustom02" class="form-label">Пароль</label>
                            <input type="password" class="form-control" id="password" name="password"
                                placeholder="&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;" style="color: #000" required>
                            <div class="invalid-feedback">Введите пароль</div>
                        </div>
                        <div class="new-account row d-flex justify-content-between">
                            <div class="form-check col-6">
                                <input class="form-check-input" type="checkbox" id="remember" name="remember">
                                <label class="form-check-label" for="remember">Запомнить меня</label>
                            </div>
{{--                            <div class="col-6 text-end"><a href="">Забыли пароль?</a></div>--}}
                        </div>
                        <div class="btn-container d-flex justify-content-center">
                            <button type="submit" class="btn btn-info enter">Войти</button>
                        </div>
                        <p class="new-account">Нет аккаунта?
                            <a href="/regpage" class="open-regist">Зарегистрироваться!</a>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
