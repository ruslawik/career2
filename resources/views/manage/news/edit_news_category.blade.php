@extends('manage.layout')

@section('title', $title)

@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
      <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-success">Список</span> Добавьте категорию или отредактируйте
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
        </div>
    <div class="col-lg-12">
        <form action="/manage/edit-news-category" method="POST">
            {{csrf_field()}}
                <input type="hidden" class="form-control col-sm-12" name="cat_id" value="{{$cat[0]->id}}">
                <p class="col-sm-12">Название на русском:</p>
                <input type="text" name="name_ru" value="{{$cat[0]->getTranslation('name', 'ru')}}" class="form-control col-sm-12 ml-3">
                <br>
                <p class="col-sm-12">Название на казахском:</p>
                <input type="text" name="name_kz" value="{{$cat[0]->getTranslation('name', 'kz')}}" class="form-control col-sm-12 ml-3  ">
                <input type="submit" value="Обновить" class="btn btn-success col-sm-2 ml-3 mt-2">
        </form>
    </div>
@endsection
