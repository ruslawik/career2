@extends('manage.layout')

@section('title', $title)

@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
      <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-success">Список</span> Нажмите на блог для его просмотра и редактирования
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
    </div>
    @if(\Session::has('status'))
        <div class="col-sm-12">
            <div class="alert alert-success">
                {{\Session::get('status')}}
            </div>
        </div>
    @endif
    <div class="col-sm-12">
        <a href="/manage/add-news"><button class="btn btn-success">Добавить блог</button></a>
        <hr>
    </div>
    <div class="col-lg-12">
            <table id="all_professions" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>Название</th>
                        <th>Текст блога</th>
                        <th>Дата публикации</th>
                        <th><center>Ред.</center></th>
                        <th><center>Удалить</center></th>
                      </tr>
                    </thead>
            </table>
    </div>
@endsection

@section('datatable_js')
<script src="/manage_res/assets/js/lib/data-table/datatables.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/dataTables.bootstrap.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/dataTables.buttons.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.bootstrap.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/jszip.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/pdfmake.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/vfs_fonts.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.html5.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.print.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.colVis.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/datatables-init.js"></script>

    <script type="text/javascript">
        jQuery(document).ready(function(){
        jQuery('#all_professions').DataTable({
            'processing': false,
            'serverSide': false,
            'serverMethod': 'get',
            'ajax': {
                'url':'/manage/all-news',
            }, 
            'columns': [
                { data: 'name',
                  "render": function(data,type,row,meta) {
                        return row.name;
                    }
                },
                { data: 'text',
                  "render": function(data,type,row,meta) {
                        return row.text;
                    }
                },
                { data: 'created_at',
                  "render": function(data,type,row,meta) {
                        return data;
                    }
                },
                { data: "id", // can be null or undefined
                  "render": function(data,type,row,meta) {
                        return '<center><a href="/manage/edit-news/'+data+'"><i class="fa fa-edit" style="font-size:24px;"></i></a></center>';
                    }
                },
                { data: "id", // can be null or undefined
                  "render": function(data,type,row,meta) {
                        return '<center><a href="/manage/delete-news/'+data+'"><i class="fa fa-trash" style="font-size:24px;"></i></a></center>';
                    }
                }
                ]
            });
        });
    </script>

@endsection