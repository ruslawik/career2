@extends('manage.layout')

@section('title', $title)

@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
      <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-success">Редактировать</span> Вы можете обновить блог, представленную ниже
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
        </div>
        @if(\Session::has('status'))
        <div class="col-sm-12">
            <div class="alert alert-success">
                {{\Session::get('status')}}
            </div>
        </div>
        @endif
    <div class="col-lg-12">
        <form action="/manage/edit-news" method="POST" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="news_id" value="{{$news[0]->id}}">
            <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Редактировать блог от {{Carbon\Carbon::parse($news[0]->created_at)->format('d/m/Y')}}</strong>
                        </div>
                        <div class="card-body">
                            <div class="input-group">
                                <div class="input-group-addon">Категория</div>
                                <select name="category_id" class="form-control">
                                    <option value="">Выберите</option>
                                @foreach($categories as $category)
                                        <option value="{{$category->id}}" @if($news[0]->category_id==$category->id) selected @endif>{{$category->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Название</div>
                                            <input type="text" class="form-control col-sm-12" name="name_ru" value="{{$news[0]->getTranslation('name', 'ru')}}">
                            </div><br>
                            <div class="input-group">
                                            <div class="input-group-addon">Название (KZ)</div>
                                            <input type="text" class="form-control col-sm-12" name="name_kz" value="{{$news[0]->getTranslation('name', 'kz')}}">
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Текст блога</div>
                                            <textarea id="summary-ckeditor" class="form-control col-sm-12" name="text_ru" rows=6>{{$news[0]->getTranslation('text', 'ru')}}</textarea>
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Текст новости (KZ)</div>
                                            <textarea id="summary-ckeditor-kz" class="form-control col-sm-12" name="text_kz" rows=6>{{$news[0]->getTranslation('text', 'kz')}}</textarea>
                            </div>
                            <br>
                            @if(strlen($news[0]->img) > 1)
                            <img src="/storage/news-images/{{$news[0]->img}}" width=100><br>
                            <a href="/manage/news/delete-image/{{$news[0]->id}}">Удалить картинку</a>
                            <br><br>
                            @else
                            Картинка отсутствует
                            @endif
                            <div class="input-group">
                                            <div class="input-group-addon">Картинка новости <br>(Загрузите если нужно обновить)</div>
                                            <input type="file" name="img" class="form-control">
                            </div>
                            <br>
                            <input type="submit" value="Обновить" class="btn btn-success">
                        </div>
                    </div>
        </form>
    </div>
@endsection


@section('mapp_pass_javascript')
    <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
    <script>
    CKEDITOR.replace('summary-ckeditor', {
        filebrowserUploadUrl: "{{route('news_upload_image', ['_token' => csrf_token() ])}}",
        filebrowserUploadMethod: 'form',
        width: "85%",
        height: "300px"
    });
    CKEDITOR.replace('summary-ckeditor-kz', {
        filebrowserUploadUrl: "{{route('news_upload_image', ['_token' => csrf_token() ])}}",
        filebrowserUploadMethod: 'form',
        width: "82%",
        height: "300px"
    });
    </script>

@endsection
