@extends('manage.layout')
@php
    $title = 'Добавить Деталь для Тарифного плана';
@endphp
@section('title', $title)

@section('content')
    <section class="content">
            <div class="row">
                <div class="col-12">
                    <!-- Default box -->
                    <div class="card">
                        <div class="card-header">
                            <a href="{{ route('plan-items.create') }}" class="btn btn-info">+ Добавить</a>
                            <a href="{{ route('plans.index') }}" class="btn btn-secondary float-right">Назад</a>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            @include('manage.partials.errors')
                            <form action="{{ route('plan-items.store') }}" method="post"
                                  enctype="multipart/form-data">
                                @csrf
                                @include('manage.plan-items._form')
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /.card -->
            </div>
    </section>
@endsection
