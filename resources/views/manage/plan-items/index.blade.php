@extends('manage.layout')
@php
    $title = 'Детали Тарифного плана';
@endphp
@section('title', $title)

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <!-- Default box -->
                <div class="card">
                    <div class="card-header">
                        <a href="{{ route('plan-items.create') }}" class="btn btn-info">+ Добавить</a>
                        <a href="{{ route('plans.index') }}" class="btn btn-secondary float-right">Назад</a>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        @include('manage.partials.errors')
                        @include('manage.plan-items._table')
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer clearfix">
                        @if ($planItems->hasPages())
                            {{ $planItems->links() }}
                        @endif
                    </div>
                </div>
                <!-- /.card -->
            </div>
        </div>

    </section>

@endsection

