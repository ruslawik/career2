<table class="table table-bordered">
    <thead>
    <tr>
        <th style="width: 10px">#</th>
        <th>Заголовок</th>
        <th>Тарифный план</th>
        <th>Активный</th>
        <th style="width: 15%;">Действия</th>
    </tr>
    </thead>
    <tbody>
    @foreach($planItems as $item)
        <tr id="{{ $item->id }}" data-index="{{ $loop->index }}">
            <td>{{ $item->id }}</td>
            <td>{{ $item->title }}</td>
            <td>{{ $item->plan->title }}</td>
            <td>
                {!! $item->is_active ? '<span class="badge badge-success">Да</span>' : '<span class="badge badge-danger">Нет</span>' !!}
            </td>
            <td>
                <div class='btn-group'>
                    <form action="{{ route('plan-items.destroy', $item) }}" class="d-inline"
                          method="post">
                        @csrf
                        @method('delete')
                        <a href="{{ route('plan-items.edit', $item) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                        <button type="submit" class="btn btn-danger"  onclick="return confirm('Вы действительно хотите удалить?')">
                            <i class="fa fa-trash"></i>
                        </button>
                    </form>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
