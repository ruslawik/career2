@extends('manage.layout')
@php
    $title = 'Изменить Деталь для Тарифного плана';
@endphp
@section('title', $title)

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row">
            <div class="col-sm-12">
                <b class="ml-4">{{ $title }}: {{ $planItem->plan->title }}</b>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-12">
                <!-- Default box -->
                <div class="card">
                    <div class="card-header">
                        <a href="{{ route('plan-items.create') }}" class="btn btn-info">+ Добавить</a>
                        <a href="{{ route('plans.index') }}" class="btn btn-secondary float-right">Назад</a>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        @include('manage.partials.errors')
                        <form action="{{ route('plan-items.update', $planItem) }}" method="post"
                              enctype="multipart/form-data">
                            @csrf
                            @method('put')
                            @include('manage.plan-items._form')

                        </form>
                    </div>
                </div>
            </div>
            <!-- /.card -->
        </div>

    </section>
@endsection
