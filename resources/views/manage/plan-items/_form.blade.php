<div class="card-body">
    <div class="form-group">
        <label for="title">Заголовок</label>
        <input name="title" id="title" type="text"
               class="form-control @error('title') is-invalid @enderror"
               value="{{ old('title', $planItem->title ?? null) }}">

        @error('title')
        <span class="invalid-feedback">{{ $message }}</span>
        @enderror
    </div>
    <div class="form-group">
        <label for="plan_id">{{ __('Тип') }}</label>
        <select name="plan_id" id="plan_id" class="form-control @error('plan_id') is-invalid @enderror">
            <option value>{{ __('Выберите тип') }}</option>

            @foreach ($plans as $plan)
                <option
                    value="{{ $plan->id }}"
                    @if(old('plan_id') == $plan->id || isset($planItem) && $planItem->plan_id == $plan->id) selected="selected" @endif
                >
                    {{ $plan->title }}
                </option>
            @endforeach
        </select>

        @error('plan_id')
        <span class="invalid-feedback">{{ $message }}</span>
        @enderror
    </div>
    <div class="form-group">
        <label for="is_active">{{ __('Активный') }}</label>
        @if (isset($planItem) && $planItem->is_active === 1)
            <input type="checkbox" name="is_active" checked="" value="1">
        @else
            <input type="checkbox" name="is_active" {{ old('is_active') ? 'checked' : '' }} value="1">
        @endif

        @if($errors->has('is_active'))
            <div class="invalid-feedback">
                <strong>{{ $message }}</strong>
            </div>
        @endif
    </div>
</div>
    <button type="submit" class="btn btn-success float-right">Сохранить</button>
