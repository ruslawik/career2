@extends('subadmin.layout')

@section('title', $title)

@section('content')

    <div class="col-lg-12">
        <a href="javascript:history.back()" class="btn btn-success">Назад</a> <br><br>
        <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Статистика продажи пакетов теста {{$test_name}} с {{$from_date}} до {{$to_date}}</strong>
                        </div>
                        <div class="card-body">
                            <table class="table table-striped">
                                <thead class="thead-light">
                                <tr>
                                        <th>
                                            Тест
                                        </th>
                                        <th>
                                            SubAdmin
                                        </th>
                                        <th>
                                            Количество
                                        </th>
                                        <th>
                                            Цена за тест
                                        </th>
                                        <th>
                                            Время выдачи
                                        </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $sum = 0; ?>
                                @foreach($all_pockets as $pocket)
                                    <tr>
                                        <td>
                                            {{$test_name}}
                                        </td>
                                        <td>
                                            {{$pocket->subadmin->name." ".$pocket->subadmin->surname}}
                                        </td>
                                        <td>
                                            {{$pocket->value}}
                                        </td>
                                        <td>
                                            {{$pocket->test_price->price}}
                                            <?php
                                                $sum += ($pocket->test_price->price*$pocket->value);
                                            ?>
                                        </td>
                                        <td>
                                            {{$pocket->created_at}}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            </table>
                            <b>Итог: <?php print($sum); ?> тенге</b>
                        </div>
        </div>
    </div>


@endsection