@extends('manage.layout')

@section('title', $title)

@section('content')

  <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-success">Настройки</span> Вводите цены продаж тестов и сохраните
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>

    <div class="col-lg-12">
            <form method="POST" action="/manage/settings-save">
                {{ csrf_field() }}
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Настройки системы</strong>
                        </div>
                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success">
                                    {!! session('status') !!}
                                </div>
                            @endif
                            <div class="input-group">
                                            <div class="input-group-addon">Цена продажи теста MAPP</div>
                                            <input type="text" class="form-control col-sm-12" name="mapp_price" value="{{$mapp_price[0]->value}}">
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Цена продажи теста DISC</div>
                                            <input type="text" class="form-control col-sm-12" name="disc_price" value="{{$disc_price[0]->value}}">
                            </div>
                            <br>
                            <input type="submit" value="Сохранить" class="btn btn-success">
                        </div>
                    </div>
            </form>
    </div>
@endsection