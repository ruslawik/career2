@extends('manage.layout')

@section('title', $title)

@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
      <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-success">Добавить</span> Для добавления расписания курсов заполните следующие поля
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
        </div>
        @if (count($errors) > 0)
        <div class="col-sm-12">
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
          @endif
    <div class="col-lg-12">
        <form action="/manage/add-schedule" method="POST" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Добавить расписание курсов</strong>
                        </div>
                        <div class="card-body">
                            <div class="input-group">
                                            <div class="input-group-addon">Название</div>
                                            <input value="{{old('name_ru')}}" type="text" class="form-control col-sm-12" name="name_ru">
                            </div><br>
                            <div class="input-group">
                                            <div class="input-group-addon">Название (KZ)</div>
                                            <input value="{{old('name_kz')}}" type="text" class="form-control col-sm-12" name="name_kz">
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Текст поста расписания курсов</div>
                                            <textarea id="summary-ckeditor" class="form-control col-sm-12" name="text_ru" rows=6>{{old('text_ru')}}</textarea>
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Текст поста расписания курсов (KZ)</div>
                                            <textarea id="summary-ckeditor-kz" class="form-control col-sm-12" name="text_kz" rows=6>{{old('text_kz')}}</textarea>
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Картинка превью</div>
                                            <input type="file" name="img" class="form-control">
                            </div>
                            <br>
                            <input type="submit" value="Добавить" class="btn btn-success">
                        </div>
                    </div>
        </form>
    </div>
@endsection

@section('mapp_pass_javascript')
    <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
    <script>
    CKEDITOR.replace('summary-ckeditor', {
        filebrowserUploadUrl: "{{route('schedule_upload_image', ['_token' => csrf_token() ])}}",
        filebrowserUploadMethod: 'form',
        width: "85%",
        height: "300px"
    });
    CKEDITOR.replace('summary-ckeditor-kz', {
        filebrowserUploadUrl: "{{route('schedule_upload_image', ['_token' => csrf_token() ])}}",
        filebrowserUploadMethod: 'form',
        width: "85%",
        height: "300px"
    });
    </script> 
@endsection
