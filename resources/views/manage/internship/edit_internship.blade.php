@extends('manage.layout')

@section('title', $title)

@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
      <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-success">Редактировать</span> Вы можете обновить информацию о стажировке, представленную ниже
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
        </div>
        @if(\Session::has('status'))
        <div class="col-sm-12">
            <div class="alert alert-success">
                {{\Session::get('status')}}
            </div>
        </div>
    @endif
    <div class="col-lg-12">
        <form action="/manage/edit-internship" method="POST" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="internship_id" value="{{$internship[0]->id}}">
            <div class="card">
                        <div class="card-body">
                            <div class="input-group">
                                            <div class="input-group-addon">Название</div>
                                            <input type="text" class="form-control col-sm-12" name="name_ru" value="{{$internship[0]->getTranslation('name', 'ru')}}">
                            </div><br>
                            <div class="input-group">
                                            <div class="input-group-addon">Название (KZ)</div>
                                            <input type="text" class="form-control col-sm-12" name="name_kz" value="{{$internship[0]->getTranslation('name', 'kz')}}">
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Местоположение</div>
                                            <input value="{{$internship[0]->getTranslation('location', 'ru')}}" type="text" class="form-control col-sm-12" name="location_ru">
                            </div><br>
                            <div class="input-group">
                                            <div class="input-group-addon">Местоположение (KZ)</div>
                                            <input value="{{$internship[0]->getTranslation('location', 'kz')}}" type="text" class="form-control col-sm-12" name="location_kz">
                            </div><br>
                            <div class="input-group">
                                            <div class="input-group-addon">Описание</div>
                                            <textarea id="summary-ckeditor" class="form-control col-sm-12" name="descrip_ru" rows=6>{{$internship[0]->getTranslation('descrip', 'ru')}}</textarea>
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Описание (KZ)</div>
                                            <textarea id="summary-ckeditor-kz" class="form-control col-sm-12" name="descrip_kz" rows=6>{{$internship[0]->getTranslation('descrip', 'kz')}}</textarea>
                            </div>
                            <br>
                            @if(strlen($internship[0]->img) > 1)
                            <img src="/storage/internship-images/{{$internship[0]->img}}" width=100><br>
                            <a href="/manage/internship/delete-image/{{$internship[0]->id}}">Удалить картинку</a>
                            <br><br>
                            @else
                            Картинка отсутствует
                            @endif
                            <div class="input-group">
                                            <div class="input-group-addon">Картинка стажировки <br>(Загрузите если нужно обновить)</div>
                                            <input type="file" name="img" class="form-control">
                            </div>
                            <br>
                            <input type="submit" value="Обновить" class="btn btn-success">
                        </div>
                    </div>
        </form>
    </div>
@endsection


@section('mapp_pass_javascript')
    <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
    <script>
    CKEDITOR.replace('summary-ckeditor', {
        filebrowserUploadUrl: "{{route('internship_upload_image', ['_token' => csrf_token() ])}}",
        filebrowserUploadMethod: 'form',
        width: "85%",
        height: "300px"
    });
    CKEDITOR.replace('summary-ckeditor-kz', {
        filebrowserUploadUrl: "{{route('internship_upload_image', ['_token' => csrf_token() ])}}",
        filebrowserUploadMethod: 'form',
        width: "82%",
        height: "300px"
    });
    </script> 
    
@endsection
