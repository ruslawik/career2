@extends('manage.layout')

@section('title', $title)

@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
      <div class="col-sm-12">

                <div id="info_mess" class="alert  alert-success alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-success">Список</span> <span id="info_mess_p">Вы можете сгенерировать и выдать новые промо-коды для прохождения тестов DISC</span>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
    </div>
    @if(\Session::has('status'))
        <div class="col-sm-12">
            <div class="alert alert-warning">
                {{\Session::get('status')}}
            </div>
        </div>
    @endif
    <div class="col-lg-12">
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Выдано промокодов</th>
                    <th>Использовано промокодов</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th>
                        <center>{{$given}}</center>
                        <hr>
                        <form action="/manage/give-disc-keycodes" method="POST" style="margin-left:20px;">
                            {{csrf_field()}}
                            <div class="row">
                                    <input name="amount" placeholder="Количество" type="text" class="form-control col-sm-4">
                                    <button type="submit" class="btn btn-success col-sm-4">Выдать</button>
                            </div>
                        </form>
                    </th>
                    <th>
                        {{$used}}
                    </th>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="col-lg-12">
            <table id="all_professions" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>Промо-код</th>
                        <th>Выдан как промокод</th>
                        <th>Использован</th>
                        <th>Имя</th>
                        <th>Фамилия</th>
                        <th>Дата использования</th>
                        <th>Email клиента</th>
                      </tr>
                    </thead>
            </table>
    </div>
@endsection

@section('datatable_js')
<script src="/manage_res/assets/js/lib/data-table/datatables.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/dataTables.bootstrap.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/dataTables.buttons.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.bootstrap.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/jszip.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/pdfmake.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/vfs_fonts.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.html5.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.print.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.colVis.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/datatables-init.js"></script>

    <script type="text/javascript">
        jQuery(document).ready(function(){
        jQuery('#all_professions').DataTable({
            dom: 'Bfrtip',
            buttons: [
            'csv', 'excel', 'pdf', 'print'
            ],
            'processing': false,
            'serverSide': false,
            'serverMethod': 'get',
            'ajax': {
                'url':'/manage/disc-promocodes-ajax',
            }, 
            'columns': [
                { data: 'code',
                  "render": function(data,type,row,meta) {
                        return row.code;
                    }
                },
                { data: 'given',
                  "render": function(data,type,row,meta) {
                        if(row.given == 1){
                            return "<font style='background-color:pink;padding:5px;'>Да</font>";
                        }else{
                            return "<font style='background-color:lightgreen;padding:5px;'>Нет</font>";
                        }
                    }
                },
                { data: 'used',
                  "render": function(data,type,row,meta) {
                        if(row.used == 1){
                            return "<font style='background-color:pink;padding:5px;'>Да</font>";
                        }else{
                            return "<font style='background-color:lightgreen;padding:5px;'>Нет</font>";
                        }
                    }
                },
                { data: 'user_name',
                  "render": function(data,type,row,meta) {
                        return data;
                    }
                },
                { data: 'user_surname',
                  "render": function(data,type,row,meta) {
                        return data;
                    }
                },
                { data: 'used_datetime',
                  "render": function(data,type,row,meta) {
                        return data;
                    }
                },
                { data: 'user_email',
                  "render": function(data,type,row,meta) {
                        return data;
                    }
                },
                ]
            });
        });
    </script>

@endsection