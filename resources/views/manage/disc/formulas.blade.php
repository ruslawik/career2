@extends('manage.layout')

@section('title', $title)

@section('content')

      <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-success">Редактировать</span> Добавляйте и редактируйте (удаление невозможно)
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>

    <div class="col-lg-12">
            <div class="card">
                <form method="POST" action="{{ $action_add }}">
                <div class="card-header">
                    <strong class="card-title">Соответствие формул типам моделей</strong>
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-success" style="float:right;"><i class="fa fa-magic"></i>&nbsp; Добавить формулу</button>
                        <br><br>
                </div>
            </form>
                <div class="card-body">
                     <form method="POST" action="{{ $action }}">
                        {{ csrf_field() }}
                    <table class="table table-striped">
                        <thead><td>Формула</td><td>Модель</td></thead>
                   @foreach($all_formulas as $formula)
                        <tr>
                            <td>
                            <input type="text" name="f{{$formula['id']}}" value="{{ $formula['formula'] }}" class="form-control">
                            </td>
                            <td>
                            <select class="form-control" name="t{{$formula['id']}}">
                                @foreach($all_types as $type)
                                    <option @if($type['id']==$formula['type_id']) selected @endif value="{{$type['id']}}">{{$type['name']}}</option>
                                @endforeach
                            </select>
                            </td>
                            <td>
                                @foreach($formula->additional_images($formula['id']) as $image)
                                    <img src="/storage/disc-additional-images/{{$image['image_url']}}" width="150">
                                    {{$image['lang']}} | 
                                    <a href="/manage/disc-additional-images/delete/{{$image['id']}}">Удалить</a>
                                    <br><br>
                                @endforeach
                                <button onClick="load_formula_id({{$formula['id']}}, '{{$formula['formula']}}');" type="button" class="btn btn-sm btn-success mt-2" value="Загрузить картинку" data-toggle="modal" data-target="#exampleModal">Загрузить картинку</button>
                            </td>
                        </tr>
                   @endforeach
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <button type="submit" class="btn btn-success" style="float:right;"><i class="fa fa-save"></i>&nbsp; Сохранить</button>
                        </td>
                    </tr>
                    </table>
                </form>
                </div>
                </form>
    </div>
@endsection

@section('datatable_js') 
<!-- Modal -->
<div class="modal fade" id="exampleModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Добавить картинку в формулу <span id="formula_name"></span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <form id="img_form" action="/manage/upload-disc-additional-img" method="POST" enctype="multipart/form-data">
                {{csrf_field()}}
                <input type="file" name="img"><br>
                <input type="hidden" id="disc_formula_id" name="disc_formula_id" value="">
                <br>
                <select class="form-control" name="lang">
                    <option value="ru">Русский</option>
                    <option value="kz">Казахский</option>
                </select>
                <br>
                <input type="submit" value="Загрузить" class="btn btn-success" style="float:right;">
            </form>
      </div>
    </div>
  </div>
  <script>
    function load_formula_id(formula_id, formula_name){
        jQuery("#formula_name").html('"'+formula_name+'"');
        jQuery("#disc_formula_id").val(formula_id);
    }
  </script>
@endsection