@extends('manage.layout')

@section('title', $title)

@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
      <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-success">Редактировать</span> Вы можете обновить информацию о вузе, представленную ниже
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
        </div>
        @if(\Session::has('status'))
        <div class="col-sm-12">
            <div class="alert alert-success">
                {{\Session::get('status')}}
            </div>
        </div>
    @endif
    <div class="col-lg-12">
        <form action="/manage/edit-university" method="POST" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="university_id" value="{{$university[0]->id}}">
            <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Редактировать вуз</strong>
                        </div>
                        <div class="card-body">
                            <div class="input-group">
                                            <div class="input-group-addon">Категория</div>
                                            <select name="category_id" class="form-control">
                                                @foreach($categories as $category)
                                                    <option value="{{$category->id}}" @if($university[0]->category_id==$category->id) selected @endif>{{$category->name}}</option>
                                                @endforeach
                                            </select>
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Название</div>
                                            <input type="text" class="form-control col-sm-12" name="name_ru" value="{{$university[0]->getTranslation('name', 'ru')}}">
                            </div><br>
                            <div class="input-group">
                                            <div class="input-group-addon">Название (KZ)</div>
                                            <input type="text" class="form-control col-sm-12" name="name_kz" value="{{$university[0]->getTranslation('name', 'kz')}}">
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Описание профессии</div>
                                            <textarea id="description_ru" class="form-control col-sm-12" name="description_ru" rows=6>{{$university[0]->getTranslation('description', 'ru')}}</textarea>
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Описание профессии (KZ)</div>
                                            <textarea id="description_kz" class="form-control col-sm-12" name="description_kz" rows=6>{{$university[0]->getTranslation('description', 'kz')}}</textarea>
                            </div>
                            <br>
                            <div class="input-group">
                                <div class="input-group-addon">Социальные сети</div>
                                <textarea id="socials" class="form-control col-sm-12" name="socials" rows=6>{{ $university[0]->socials }}</textarea>
                            </div>
                            <small>Социальные сети(instagram, fb, youtube, twitter) вводятся через знак <b>;</b></small>
                            <hr>
                            <br>
                            @if(strlen($university[0]->img) > 1)
                                <img src="/storage/universities-images/{{$university[0]->img}}" width=100><br>
                                <a href="/manage/universities/delete-image/{{$university[0]->id}}">Удалить картинку</a>
                                <br><br>
                            @else
                                Картинка отсутствует
                            @endif
                            <div class="input-group">
                                <div class="input-group-addon">Картинка превью <br>(Загрузите если нужно обновить)</div>
                                <input type="file" name="img" class="form-control">
                            </div>
                            <br>
                            <input type="submit" value="Обновить" class="btn btn-success">
                        </div>
                    </div>
        </form>
    </div>
@endsection
@section('mapp_pass_javascript')
    <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
    <script>
        CKEDITOR.replace('description_ru', {
            filebrowserUploadUrl: "{{route('news_upload_image', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form',
            width: "85%",
            height: "300px"
        });
        CKEDITOR.replace('description_kz', {
            filebrowserUploadUrl: "{{route('news_upload_image', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form',
            width: "82%",
            height: "300px"
        });
    </script>

@endsection
