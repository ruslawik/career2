@extends('manage.layout')

@section('title', $title)

@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
      <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-success">Добавить</span> Для добавления вуза заполните следующие поля
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
        </div>
        @if (count($errors) > 0)
        <div class="col-sm-12">
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
          @endif
    <div class="col-lg-12">
        <form action="/manage/add-university" method="POST" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Добавить вуз</strong>
                        </div>
                        <div class="card-body">
                            <div class="input-group">
                                            <div class="input-group-addon">Категория</div>
                                            <select name="category_id" class="form-control">
                                                @foreach($categories as $category)
                                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                                @endforeach
                                            </select>
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Название</div>
                                            <input type="text" class="form-control col-sm-12" name="name_ru">
                            </div><br>
                            <div class="input-group">
                                            <div class="input-group-addon">Название (KZ)</div>
                                            <input type="text" class="form-control col-sm-12" name="name_kz">
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Описание профессии</div>
                                            <textarea id="description_ru" class="form-control col-sm-12" name="description_ru" rows=6></textarea>
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Описание профессии (KZ)</div>
                                            <textarea id="description_kz" class="form-control col-sm-12" name="description_kz" rows=6></textarea>
                            </div>
                            <br>
                            <div class="input-group">
                                <div class="input-group-addon">Социальные сети</div>
                                <textarea id="socials" class="form-control col-sm-12" name="socials" rows=6></textarea>
                            </div>
                            <small>Социальные сети(instagram, fb, youtube, twitter) вводятся через знак <b>;</b></small>
                            <hr>
                            <br>
                            <div class="input-group">
                                <div class="input-group-addon">Картинка превью</div>
                                <input type="file" name="img" class="form-control">
                            </div>
                            <br>
                            <input type="submit" value="Добавить" class="btn btn-success">
                        </div>
                    </div>
        </form>
    </div>
@endsection
@section('mapp_pass_javascript')
    <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
    <script>
        CKEDITOR.replace('description_ru', {
            filebrowserUploadUrl: "{{route('news_upload_image', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form',
            width: "85%",
            height: "300px"
        });
        CKEDITOR.replace('description_kz', {
            filebrowserUploadUrl: "{{route('news_upload_image', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form',
            width: "82%",
            height: "300px"
        });
    </script>

@endsection
