@extends('manage.layout')

@section('title', $title)

@section('content')
      <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-success">Приветствуем!</span> Добро пожаловать, {{ $username }}!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            @if(\Auth::user()->login != "admin")
            <div class="col-lg-12">
                 <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Период</strong>
                        </div>
                        <div class="card-body">
                            <form action="{{$action}}" method="POST">
                                {{csrf_field()}}
                            <div class="row">
                                <div class="col">Начиная с
                                    <input type="date" class="form-control" name="start_date" value="{{$firstDay}}" />
                                </div>
                                <div class="col">
                                    До
                                    <input type="date" class="form-control" name="end_date" value="{{$lastDay}}" />
                                </div>
                                <div class="col"><br>
                                    <input type="submit" value="Применить фильтр" class="btn btn-success">
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>

            </div>

    <div class="col-lg-6">

        <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Компания CareerVision (индивидуальные тесты)</strong>
                        </div>
                        <div class="card-body">
                            <table class="table table-striped">
                                <thead class="thead-light">
                                <tr>
                                        <th>
                                            Тест
                                        </th>
                                        <th>
                                            Пройдено кол.
                                        </th>
                                        <th>
                                            Общая сумма
                                        </th>
                                </tr>
                            </thead>
                            <tbody>
                                    <tr>
                                        <td>
                                            MAPP
                                        </td>
                                        <td>
                                            <a href="/manage/show_stat/mapp/{{$firstDay}}/{{$lastDay}}">{{$mapp_tests}}</a>
                                        </td>
                                        <td>
                                            {{$mapp_total_sum}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            DISC
                                        </td>
                                        <td>
                                            <a href="/manage/show_stat/disc/{{$firstDay}}/{{$lastDay}}">{{$disc_tests}}</a>
                                        </td>
                                        <td>
                                            {{$disc_total_sum}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Кейрси
                                        </td>
                                        <td>
                                            <a href="/manage/show_stat/keirsi/{{$firstDay}}/{{$lastDay}}">{{$keirsi_tests}}</a>
                                        </td>
                                        <td>
                                            {{$keirsi_total_sum}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Холл
                                        </td>
                                        <td>
                                            <a href="/manage/show_stat/holl/{{$firstDay}}/{{$lastDay}}">{{$holl_tests}}</a>
                                        </td>
                                        <td>
                                            {{$holl_total_sum}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Томас
                                        </td>
                                        <td>
                                            <a href="/manage/show_stat/tomas/{{$firstDay}}/{{$lastDay}}">{{$tomas_tests}}</a>
                                        </td>
                                        <td>
                                            {{$tomas_total_sum}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Соломин
                                        </td>
                                        <td>
                                            <a href="/manage/show_stat/solomin/{{$firstDay}}/{{$lastDay}}">{{$solomin_tests}}</a>
                                        </td>
                                        <td>
                                            {{$solomin_total_sum}}
                                        </td>
                                    </tr>
                            </tbody>
                            </table>
                            <b>Итог:</b>
                            {{($mapp_total_sum)+($disc_total_sum)+($keirsi_total_sum)+($holl_total_sum)+($tomas_total_sum)+($solomin_total_sum)}} тенге
                        </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Компания CareerVision (выданные пакеты тестов)</strong>
                        </div>
                        <div class="card-body">

                            <table class="table table-striped">
                                <thead class="thead-light">
                                <tr>
                                        <th>
                                            Тест
                                        </th>
                                        <th>
                                            Количество
                                        </th>
                                        <th>
                                            Общая сумма
                                        </th>
                                </tr>
                            </thead>
                            <tbody>
                                    <tr>
                                        <td>
                                            MAPP
                                        </td>
                                        <td>
                                            <a href="/manage/show_stat_by_pockets/mapp/{{$firstDay}}/{{$lastDay}}">{{$mapp_pockets_num}}</a>
                                        </td>
                                        <td>
                                            {{$mapp_pockets_sum}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            DISC
                                        </td>
                                        <td>
                                            <a href="/manage/show_stat_by_pockets/disc/{{$firstDay}}/{{$lastDay}}">{{$disc_pockets_num}}</a>
                                        </td>
                                        <td>
                                            {{$disc_pockets_sum}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Кейрси
                                        </td>
                                        <td>
                                            <a href="/manage/show_stat_by_pockets/keirsi/{{$firstDay}}/{{$lastDay}}">{{$keirsi_pockets_num}}</a>
                                        </td>
                                        <td>
                                            {{$keirsi_pockets_sum}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Холл
                                        </td>
                                        <td>
                                            <a href="/manage/show_stat_by_pockets/holl/{{$firstDay}}/{{$lastDay}}">{{$holl_pockets_num}}</a>
                                        </td>
                                        <td>
                                            {{$holl_pockets_sum}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Томас
                                        </td>
                                        <td>
                                            <a href="/manage/show_stat_by_pockets/tomas/{{$firstDay}}/{{$lastDay}}">{{$tomas_pockets_num}}</a>
                                        </td>
                                        <td>
                                            {{$tomas_pockets_sum}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Соломин
                                        </td>
                                        <td>
                                            <a href="/manage/show_stat_by_pockets/solomin/{{$firstDay}}/{{$lastDay}}">{{$solomin_pockets_num}}</a>
                                        </td>
                                        <td>
                                            {{$solomin_pockets_sum}}
                                        </td>
                                    </tr>
                            </tbody>
                            </table>
                            <b>Итог:</b> {{$mapp_pockets_sum+$disc_pockets_sum+$keirsi_pockets_sum+$holl_pockets_sum+$tomas_pockets_sum+$solomin_pockets_sum}} 
            </div>
        </div>
    </div>
     <div class="col-lg-6">
        <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Список выданных SubAdmin (всех)</strong>
                        </div>
                        <div class="card-body">

                            <table class="table table-striped">
                                <thead class="thead-light">
                                <tr>
                                        <th>
                                            Данные об аккаунте
                                        </th>
                                        <th>
                                            Дата выдачи
                                        </th>
                                        <th>
                                            Ред.
                                        </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($subadmins as $subadmin)
                                    <tr>
                                        <td><a href="/manage/subadmin_info/{{$subadmin->id}}">{{$subadmin->name}} {{$subadmin->surname}} | {{$subadmin->login}}<a/></td>
                                        <td>{{$subadmin->created_at}}</td>
                                        <td><a href="/manage/edit_subadmin/{{$subadmin->id}}"><i class="fa fa-edit" style="font-size:24px;"></i></a></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
    </div>

    <div class="col-lg-6">
        <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Компания DigitalPax</strong>
                        </div>
                        <div class="card-body">

                            <table class="table table-striped">
                                <thead class="thead-light">
                                <tr>
                                        <th>
                                            Тест
                                        </th>
                                        <th>
                                            30% от общей суммы
                                        </th>
                                </tr>
                            </thead>
                            <tbody>
                                    <tr>
                                        <td>
                                            MAPP
                                        </td>
                                        <td>
                                            {{($mapp_total_sum+$mapp_pockets_sum)*0.3}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            DISC
                                        </td>
                                        <td>
                                            {{($disc_total_sum+$disc_pockets_sum)*0.3}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Кейрси
                                        </td>
                                        <td>
                                            {{($keirsi_total_sum+$keirsi_pockets_sum)*0.3}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Холл
                                        </td>
                                        <td>
                                            {{($holl_total_sum+$holl_pockets_sum)*0.3}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Томас
                                        </td>
                                        <td>
                                            {{($tomas_total_sum+$tomas_pockets_sum)*0.3}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Соломин
                                        </td>
                                        <td>
                                            {{($solomin_total_sum+$solomin_pockets_sum)*0.3}}
                                        </td>
                                    </tr>
                            </tbody>
                            </table>
                            <b>Итог:</b>
                            {{($mapp_total_sum+$mapp_pockets_sum)*0.3+($disc_total_sum+$disc_pockets_sum)*0.3+($keirsi_total_sum+$keirsi_pockets_sum)*0.3+($holl_total_sum+$holl_pockets_sum)*0.3+($tomas_total_sum+$tomas_pockets_sum)*0.3+($solomin_total_sum+$solomin_pockets_sum)*0.3}} тенге
                        </div>
        </div>
    </div>
    @else
         <div class="col-lg-12">
                 <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Панель администрирования</strong>
                        </div>
                        <div class="card-body">
                            Вы можете редактировать тесты, переводы, добавлять и назначать тесты новым пользователям для их автоматического прохождения и вычисления результата. 
                            <br><br>В разделе все пользователи Вам доступны готовые результаты пройденных тестов. 
                        </div>
                    </div>

            </div>
    @endif
@endsection