@if(\Auth::user()->id == 1)
<ul class="nav navbar-nav">
                    <li class="active">
                        <a href="/manage/admin"> <i class="menu-icon fa fa-dashboard"></i>Главная </a>
                    </li>
                    <h3 class="menu-title">Базы информации</h3><!-- /.menu-title -->
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-laptop"></i>Профессии</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-briefcase"></i><a href="/manage/professions">Все профессии</a></li>
                            <li><i class="fa fa-list"></i><a href="/manage/profession-cats">Категории</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-laptop"></i>Курсы</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-briefcase"></i><a href="/manage/courses">Все курсы</a></li>
                            <li><i class="fa fa-list"></i><a href="/manage/courses-cats">Категории курсов</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-list"></i>Блоги</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-briefcase"></i><a href="/manage/news">Все Блоги</a></li>
                            <li><i class="fa fa-list"></i><a href="/manage/news-cats">Категории блогов</a></li>
                        </ul>
                    </li>
                    <li><a href="/manage/schedule"><i class="menu-icon fa fa-clock-o" style="font-size:16px"></i>Расписание курсов</a></li>
                    <li><a href="/manage/internship"><i style="font-size:16px" class="menu-icon fa fa-graduation-cap"></i>Стажировки</a></li>
                    <!--<li><a href="/manage/univers"><i style="font-size:16px" class="menu-icon fa fa-university"></i>ВУЗы</a></li>!-->
                    <li><a href="/manage/mapp-keycodes"><i style="font-size:16px" class="menu-icon fa fa-key"></i>MAPP KEYCODES</a></li>
                    <li><a href="/manage/disc-promocodes"><i style="font-size:16px" class="menu-icon fa fa-key"></i>DISC PROMOCODES</a></li>
                    <li><a href="/manage/settings"><i style="font-size:16px" class="menu-icon fa fa-cogs"></i>Настройки</a></li>
                    <li><a href="/manage/videos"><i style="font-size:16px" class="menu-icon fa fa-video-camera"></i>Видео</a></li>
                    <li><a href="/manage/plans"><i style="font-size:16px" class="menu-icon fa fa-money"></i>Тарифные планы</a></li>

                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-laptop"></i>Вузы</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-briefcase"></i><a href="/manage/universities">Все Вузы</a></li>
                            <li><i class="fa fa-list"></i><a href="/manage/university-cats">Категории вузов</a></li>
                        </ul>
                    </li>

                    <h3 class="menu-title">Пользователи</h3><!-- /.menu-title -->
                        <!--<li><a href="/manage/applications"><i class="menu-icon fa fa-list"></i>Заявки</a></li>!-->
                        <li><a href="/manage/all_users"><i class="menu-icon fa fa-list"></i>Все пользователи</a></li>
                        <li><a href="/manage/add_user"><i class="menu-icon fa fa-puzzle-piece"></i>Добавить</a></li>

                    <h3 class="menu-title">Редактор тестов</h3><!-- /.menu-title -->
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-laptop"></i>Кейрси</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-puzzle-piece"></i><a href="/manage/keirsi_edit">Редактировать</a></li>
                            <li><i class="fa fa-puzzle-piece"></i><a href="/manage/keirsi_edit_kaz">Редактировать КАЗ</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-laptop"></i>MAPP</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-puzzle-piece"></i><a href="/manage/mapp_edit">Редактировать</a></li>
                            <li><i class="fa fa-puzzle-piece"></i><a href="/manage/mapp_edit_kaz">Редактировать КАЗ</a></li>
                            <li><i class="fa fa-language"></i><a href="/manage/mapp_tercume">ПЕРЕВОДЫ</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-laptop"></i>DISC</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-puzzle-piece"></i><a href="/manage/disc_formulas">Формулы</a></li>
                            <li><i class="fa fa-puzzle-piece"></i><a href="/manage/disc_edit">Редактировать</a></li>
                            <li><i class="fa fa-puzzle-piece"></i><a href="/manage/disc_edit_kaz">Редактировать КАЗ</a></li>
                            <li><i class="fa fa-puzzle-piece"></i><a href="/manage/disc_edit_en">Редактировать ENG</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-laptop"></i>Холл</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-puzzle-piece"></i><a href="/manage/holl_edit">Редактировать</a></li>
                            <li><i class="fa fa-puzzle-piece"></i><a href="/manage/holl_edit_kaz">Редактировать КАЗ</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-laptop"></i>Томас</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-puzzle-piece"></i><a href="/manage/tomas_edit">Редактировать</a></li>
                            <li><i class="fa fa-puzzle-piece"></i><a href="/manage/tomas_edit_kaz">Редактировать КАЗ</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-laptop"></i>Соломин</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-puzzle-piece"></i><a href="/manage/solomin_edit">Редактировать 1</a></li>
                            <li><i class="fa fa-puzzle-piece"></i><a href="/manage/solomin_editkaz">Редактировать 1 КАЗ</a></li>
                            <li><i class="fa fa-puzzle-piece"></i><a href="/manage/solomin2_edit">Редактировать 2</a></li>
                            <li><i class="fa fa-puzzle-piece"></i><a href="/manage/solomin2_editkaz">Редактировать 2 КАЗ</a></li>
                        </ul>
                    </li>
                </ul>
@else
    <h3 class="menu-title"><center>Статистика продаж</center></h3>
@endif
