@extends('manage.layout')

@section('title', $title)

@section('content')

  <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-success">Настройки</span> Выберите нужную информацию и сохраните
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>

    <div class="col-lg-12">
            <form method="POST" action="/manage/mappkeycode-save">
                {{ csrf_field() }}
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Keycode - {{$keycode[0]->keycode}}</strong>
                        </div>
                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success">
                                    {!! session('status') !!}
                                </div>
                            @endif
                            <input type="hidden" name="keycode_id" value="{{$keycode[0]->id}}">
                            <div class="input-group">
                                    <div class="input-group-addon">Использован</div>
                                    <select class="form-control" name="used">
                                        <option value="1" @if($keycode[0]->used==1) selected @endif>Да</option>
                                        <option value="0" @if($keycode[0]->used==0) selected @endif>Нет</option>
                                    </select>
                            </div>
                            <br>
                            <div class="input-group">
                                    <div class="input-group-addon">Выдан как промо-код</div>
                                    <select class="form-control" name="given">
                                        <option value="1" @if($keycode[0]->given==1) selected @endif>Да</option>
                                        <option value="0" @if($keycode[0]->given==0) selected @endif>Нет</option>
                                    </select>
                            </div>
                            <br>
                            <div class="input-group">
                                    <div class="input-group-addon">Email клиента (кто использовал)</div>
                                    <input type="text" class="form-control" name="user_email" value="{{$keycode[0]->user_email($keycode[0]->id)}}">
                            </div>
                            <br>
                            <div class="input-group">
                                    <div class="input-group-addon">Дата и время использования</div>
                                    <?php
                                        if($keycode[0]->used_datetime != NULL){
                                            $datetime = new DateTime($keycode[0]->used_datetime);
                                        }
                                    ?>
                                    <input type="datetime-local" class="form-control" name="used_datetime" value="<?php if($keycode[0]->used_datetime != NULL){ echo $datetime->format('Y-m-d\TH:i:s'); } ?>">
                            </div>
                            <br>
                            <input type="submit" value="Сохранить" class="btn btn-success">
                        </div>
                    </div>
            </form>
    </div>
@endsection