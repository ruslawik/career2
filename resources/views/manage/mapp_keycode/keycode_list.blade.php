@extends('manage.layout')

@section('title', $title)

@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
      <div class="col-sm-12">
                <div id="loader" class="alert  alert-warning" style="display: none;" role="alert">
                  <span class="badge badge-pill badge-warning">Обновление</span> Обновляем данные... Не закрывайте эту страницу, пока данное сообщение не пропадет...
                </div>

                <div id="info_mess" class="alert  alert-success alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-success">Список</span> <span id="info_mess_p">Нажмите "Загрузить новые" если Вы приобретали новые keycodes и еще не обновляли данный список</span>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
    </div>
    @if(\Session::has('status'))
        <div class="col-sm-12">
            <div class="alert alert-warning">
                {{\Session::get('status')}}
            </div>
        </div>
    @endif
    <div class="col-sm-12">
        <button id="update_button" class="btn btn-success" onClick="load_keycodes();"><span id="loader"></span>Загрузить новые из assessment.com</button>
        <hr>
    </div>
    <div class="col-lg-12">
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Всего keycodes</th>
                    <th>Использовано <br>(с онлайн-оплатой)</th>
                    <th>Доступных</th>
                    <th>Выдано как промокоды/<br>Из них использовано</th>
                    <th>В процессе регистрации</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th>{{$keycodes_total}}</th>
                    <th>{{$keycodes_used}}</th>
                    <th>{{$keycodes_available}}</th>
                    <th>
                        <center>{{$keycodes_given}}/{{$keycodes_used_from_given}}</center><hr>
                        <form action="/manage/give-keycodes" method="POST">
                            {{csrf_field()}}
                            <div class="row">
                                    <input name="amount" placeholder="Количество" type="text" class="form-control col-sm-6">
                                    <button type="submit" class="btn btn-success col-sm-6">Выдать</button>
                            </div>
                        </form>
                    </th>
                    <th>{{$keycodes_in_usage}}</th>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="col-lg-12">
            <table id="all_professions" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>Keycode &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                        <th>Использован</th>
                        <th>Выдан как промокод</th>
                        <th>Тест пройден</th>
                        <th>Имя</th>
                        <th>Фамилия</th>
                        <th>Дата использования</th>
                        <th>Email клиента</th>
                        <th>Ред.</th>
                      </tr>
                    </thead>
            </table>
    </div>
@endsection

@section('datatable_js')
<script src="/manage_res/assets/js/lib/data-table/datatables.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/dataTables.bootstrap.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/dataTables.buttons.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.bootstrap.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/jszip.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/pdfmake.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/vfs_fonts.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.html5.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.print.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.colVis.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/datatables-init.js"></script>

    <script>
        function load_keycodes (){

            jQuery.ajax({
                type: "GET",
                url: "/manage/load-keycodes",
                beforeSend: function(){
                    jQuery("#info_mess").toggle('fast');
                    jQuery("#loader").toggle('fast');
                    jQuery("#update_button").toggle('fast');
                },
                data: { _token: '{{csrf_token()}}' },
                success: function(answer) {
                    jQuery("#loader").toggle('fast');
                    jQuery("#info_mess").toggle('fast');
                    jQuery("#update_button").toggle('fast');
                    jQuery("#info_mess_p").html("Keycodes успешно загружены с сервера assessment.com! ("+answer+")");
                },
                error: function(xhr){
                    jQuery("#loader").toggle('fast');
                    jQuery("#info_mess").toggle('fast');
                    alert("Что-то пошло не так. Попробуйте еще раз либо обратитесь к техническому администратору. Ошибка: " + xhr.status + " " + xhr.statusText);
                }
            });
        }
    </script>
    <script type="text/javascript">
        jQuery(document).ready(function(){
        jQuery('#all_professions').DataTable({
            dom: 'Bfrtip',
            buttons: [
            'csv', 'excel', 'pdf', 'print'
            ],
            'processing': false,
            'serverSide': false,
            'serverMethod': 'get',
            'ajax': {
                'url':'/manage/all-keycodes',
            }, 
            'columns': [
                { data: 'keycode',
                  "render": function(data,type,row,meta) {
                        return row.keycode;
                    }
                },
                { data: 'used',
                  "render": function(data,type,row,meta) {
                        if(row.used == 1){
                            return "<font style='background-color:pink;padding:5px;'>Да</font>";
                        }else{
                            return "<font style='background-color:lightgreen;padding:5px;'>Нет</font>";
                        }
                    }
                },
                { data: 'given',
                  "render": function(data,type,row,meta) {
                        if(row.given == 1){
                            return "<font style='background-color:pink;padding:5px;'>Да</font>";
                        }else{
                            return "<font style='background-color:lightgreen;padding:5px;'>Нет</font>";
                        }
                    }
                },
                { data: 'mapp_passed',
                  "render": function(data,type,row,meta) {
                        if(row.mapp_passed == 0){
                            return "<font style='background-color:pink;padding:5px;'>Нет</font>";
                        }else{
                            return "<font style='background-color:lightgreen;padding:5px;'>Да</font>";
                        }
                    }
                },
                { data: 'user_name',
                  "render": function(data,type,row,meta) {
                        return data;
                    }
                },
                { data: 'user_surname',
                  "render": function(data,type,row,meta) {
                        return data;
                    }
                },
                { data: 'used_datetime',
                  "render": function(data,type,row,meta) {
                        return data;
                    }
                },
                { data: 'user_email',
                  "render": function(data,type,row,meta) {
                        return data;
                    }
                },
                { data: 'id',
                  "render": function(data,type,row,meta) {
                        return '<center><a href="/manage/edit-mapp-keycode/'+data+'"><i class="fa fa-edit" style="font-size:24px;"></i></a></center>';
                    }
                },
                ]
            });
        });
    </script>

@endsection