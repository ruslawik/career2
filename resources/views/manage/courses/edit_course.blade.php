@extends('manage.layout')

@section('title', $title)

@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
      <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-success">Редактировать</span> Вы можете обновить информацию о курсе, представленную ниже
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
        </div>
         @if (count($errors) > 0)
        <div class="col-sm-12">
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
          @endif
          @if(\Session::has('status'))
        <div class="col-sm-12">
            <div class="alert alert-success">
                {{\Session::get('status')}}
            </div>
        </div>
    @endif
    <div class="col-lg-12">
        <form action="/manage/edit-course" method="POST" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="course_id" value="{{$course[0]->id}}">
            <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Редактировать курс</strong>
                        </div>
                        <div class="card-body">
                            <div class="input-group">
                                            <div class="input-group-addon">Категория</div>
                                            <select name="category_id" class="form-control">
                                                @foreach($categories as $category)
                                                    <option value="{{$category->id}}" @if($course[0]->cat_id==$category->id) selected @endif>{{$category->name}}</option>
                                                @endforeach
                                            </select>
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Опубликовать для всех</div>
                                            <select name="public" class="form-control">
                                                <option value="1">Опубликовано</option>
                                                <option value="0" @if($course[0]->public==0) selected @endif>Черновик</option>
                                            </select>
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Название</div>
                                            <input type="text" class="form-control col-sm-12" name="name_ru" value="{{$course[0]->getTranslation('name', 'ru')}}">
                            </div><br>
                            <div class="input-group">
                                            <div class="input-group-addon">Название (KZ)</div>
                                            <input type="text" class="form-control col-sm-12" name="name_kz" value="{{$course[0]->getTranslation('name', 'kz')}}">
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Теги</div>
                                            <input type="text" class="form-control col-sm-12" name="tags_ru" value="{{$course[0]->getTranslation('tags', 'ru')}}">
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Теги (KZ)</div>
                                            <input type="text" class="form-control col-sm-12" name="tags_kz" value="{{$course[0]->getTranslation('tags', 'kz')}}">
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Описание курса</div>
                                            <textarea class="form-control col-sm-12" name="description_ru" rows=6>{{$course[0]->getTranslation('description', 'ru')}}</textarea>
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Описание курса (KZ)</div>
                                            <textarea class="form-control col-sm-12" name="description_kz" rows=6>{{$course[0]->getTranslation('description', 'kz')}}</textarea>
                            </div>
                            <br>
                            <img src="/storage/course-images/{{$course[0]->img}}" width=100>
                            <br><br>
                            <div class="input-group">
                                            <div class="input-group-addon">Картинка курса <br>(Загрузите если нужно обновить)</div>
                                            <input type="file" name="img" class="form-control">
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Ссылка на курс</div>
                                            <input value="{{$course[0]->link}}" type="text" class="form-control col-sm-12" name="link">
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">ТОП-курс?</div>
                                            <select name="top_course" class="form-control">
                                                <option value="0">Нет</option>
                                                <option value="1" @if($course[0]->top_course==1) selected @endif>Да</option>
                                            </select>
                            </div>
                            <br>
                            <input type="submit" value="Обновить" class="btn btn-success">
                        </div>
                    </div>
        </form>
    </div>
@endsection
