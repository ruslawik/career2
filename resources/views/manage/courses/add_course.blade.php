@extends('manage.layout')

@section('title', $title)

@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
      <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-success">Добавить</span> Для добавления курса заполните следующие поля
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
        </div>
        @if (count($errors) > 0)
        <div class="col-sm-12">
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
          @endif
    <div class="col-lg-12">
        <form action="/manage/add-course" method="POST" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Добавить курс</strong>
                        </div>
                        <div class="card-body">
                            <div class="input-group">
                                            <div class="input-group-addon">Категория</div>
                                            <select name="category_id" class="form-control">
                                                @foreach($categories as $category)
                                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                                @endforeach
                                            </select>
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Опубликовать для всех</div>
                                            <select name="public" class="form-control">
                                                <option value="1">Опубликовано</option>
                                                <option value="0">Черновик</option>
                                            </select>
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Название</div>
                                            <input type="text" class="form-control col-sm-12" name="name_ru" value="{{old('name_ru')}}">
                            </div><br>
                            <div class="input-group">
                                            <div class="input-group-addon">Название (KZ)</div>
                                            <input type="text" class="form-control col-sm-12" name="name_kz" value="{{old('name_kz')}}">
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Теги</div>
                                            <input type="text" class="form-control col-sm-12" name="tags_ru" placeholder="#гимнастика, #спорт, #физкультура" value="{{old('tags_ru')}}">
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Теги (KZ)</div>
                                            <input type="text" class="form-control col-sm-12" name="tags_kz" placeholder="#гимнастика, #спорт, #физкультура" value="{{old('tags_kz')}}">
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Описание курса</div>
                                            <textarea class="form-control col-sm-12" name="description_ru" rows=6>{{old('description_ru')}}</textarea>
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Описание курса (KZ)</div>
                                            <textarea class="form-control col-sm-12" name="description_kz" rows=6>{{old('description_kz')}}</textarea>
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Картинка курса</div>
                                            <input type="file" name="img" class="form-control">
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Ссылка на курс</div>
                                            <input value="{{old('link')}}" type="text" class="form-control col-sm-12" name="link" placeholder="https://billionaire.kz/course/1">
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">ТОП-курс?</div>
                                            <select name="top_course" class="form-control">
                                                <option value="0" selected>Нет</option>
                                                <option value="1">Да</option>
                                            </select>
                            </div>
                            <br>
                            <input type="submit" value="Добавить" class="btn btn-success">
                        </div>
                    </div>
        </form>
    </div>
@endsection
