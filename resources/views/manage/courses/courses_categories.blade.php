@extends('manage.layout')

@section('title', $title)

@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
      <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-success">Список</span> Добавьте категорию или отредактируйте
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
        </div>
        @if(\Session::has('status'))
        <div class="col-sm-12">
            <div class="alert alert-success">
                {{\Session::get('status')}}
            </div>
        </div>
        @endif
    <div class="col-lg-12">
        <form action="/manage/add-course-category" method="POST" class="mb-4">
            {{csrf_field()}}
            <div class="row">
                <input type="text" name="name_ru" class="form-control col-sm-3 ml-3 mt-2" placeholder="Название на русском">
                <input type="text" name="name_kz" class="form-control col-sm-3 ml-3 mt-2" placeholder="Название на казахском">
                <select name="on_promo" class="form-control col-sm-3 ml-3 mt-2">
                    <option value="0">
                        Не в предложениях
                    </option>
                    <option value="1">
                        В предложениях
                    </option>
                </select>
                <input type="submit" value="Добавить" class="btn btn-success col-sm-2 ml-3 mt-2">
            </div>
        </form>
            <table id="all_categories" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>Категория</th>
                        <th>Вывести в предложениях</th>
                        <th><center>Ред.</center></th>
                      </tr>
                    </thead>

                    @foreach($categories as $category)
                        <tr>
                            <td>{{$category->id}}</td>
                            <td>{{$category->name}}</td>
                            <td>{{$category->on_promo}}</td>
                            <td><center><a href="/manage/edit-course-category/{{$category->id}}"><i class="fa fa-edit" style="font-size:24px;"></i></a></center></td>
                        </tr>
                    @endforeach

            </table>
    </div>
@endsection

@section('datatable_js')
<script src="/manage_res/assets/js/lib/data-table/datatables.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/dataTables.bootstrap.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/dataTables.buttons.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.bootstrap.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/jszip.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/pdfmake.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/vfs_fonts.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.html5.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.print.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.colVis.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/datatables-init.js"></script>

    <script type="text/javascript">
        jQuery(document).ready(function(){
            jQuery('#all_categories').DataTable();
        });
    </script>

@endsection