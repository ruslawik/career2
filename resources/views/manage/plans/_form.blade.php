<div class="card-body">
    <div class="form-group">
        <label for="title">Заголовок</label>
        <input name="title" id="title" type="text"
               class="form-control @error('title') is-invalid @enderror"
               value="{{ old('title', $plan->title ?? null) }}">

        @error('title')
        <span class="invalid-feedback">{{ $message }}</span>
        @enderror
    </div>
    <div class="form-group">
        <label for="price">Стоимость</label>
        <input name="price" id="price" type="text"
               class="form-control @error('price') is-invalid @enderror"
               value="{{ old('price', $plan->price ?? null) }}">

        @error('price')
        <span class="invalid-feedback">{{ $message }}</span>
        @enderror
    </div>
    <div class="form-group">
        <label for="link">Ссылка на WhatsApp</label>
        <input name="link" id="link" type="text"
               class="form-control @error('link') is-invalid @enderror"
               value="{{ old('link', $plan->link ?? null) }}">

        @error('link')
        <span class="invalid-feedback">{{ $message }}</span>
        @enderror
    </div>
</div>
    <button type="submit" class="btn btn-success float-right">Сохранить</button>
