<table class="table table-bordered">
    <thead>
    <tr>
        <th style="width: 10px">#</th>
        <th>Заголовок</th>
        <th>Стоимость</th>
        <th>Количество элементов</th>
        <th style="width: 15%;">Действия</th>
    </tr>
    </thead>
    <tbody>
    @foreach($plans as $plan)
        <tr id="{{ $plan->id }}" data-index="{{ $loop->index }}">
            <td>{{ $plan->id }}</td>
            <td>{{ $plan->title }}</td>
            <td>{{ $plan->price }}</td>
            <td>{{ $plan->items ? $plan->items->count() : 0  }}</td>
            <td>
                <div class='btn-group'>
                    <form action="{{ route('plans.destroy', $plan) }}" class="d-inline"
                          method="post">
                        @csrf
                        @method('delete')
                        <a href="{{ route('plan-items.index', ['plan_id' => $plan->id]) }}" class="btn btn-success"><i class="fa fa-list"></i></a>
                        <a href="{{ route('plans.edit', $plan) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                        <button type="submit" class="btn btn-danger"  onclick="return confirm('Вы действительно хотите удалить?')">
                            <i class="fa fa-trash"></i>
                        </button>
                    </form>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
