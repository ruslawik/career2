@extends('manage.layout')
@php
    $title = 'Тарифный план';
@endphp
@section('title', $title)

@section('content')

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <!-- Default box -->
                <div class="card">
                    <div class="card-header">
                        <a href="{{ route('plans.create') }}" class="btn btn-info">+ Добавить</a>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        @include('manage.partials.errors')
                        @include('manage.plans._table')
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer clearfix">
                        @if ($plans->hasPages())
                            {{ $plans->links() }}
                        @endif
                    </div>
                </div>
                <!-- /.card -->
            </div>
        </div>

    </section>

@endsection

