@extends('manage.layout')
@php
    $title = 'Добавить Тарифный план';
@endphp
@section('title', $title)

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-12">
                <!-- Default box -->
                <div class="card">
                    <div class="card-header">
                        <a href="{{ route('plans.create') }}" class="btn btn-info">+ Добавить</a>
                        <a href="{{ route('plans.index') }}" class="btn btn-secondary float-right">Назад</a>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        @include('manage.partials.errors')
                        <form action="{{ route('plans.store') }}" method="post"
                              enctype="multipart/form-data">
                            @csrf
                            @include('manage.plans._form')
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.card -->
        </div>
    </section>
@endsection
