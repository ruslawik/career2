@extends('manage.layout')

@section('title', $title)

@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
      <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-success">Список</span> Добавьте категорию или отредактируйте
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
        </div>
    <div class="col-lg-12">
        <form action="/manage/edit-profession-category" method="POST" enctype="multipart/form-data">
            {{csrf_field()}}
                <input type="hidden" class="form-control col-sm-12" name="cat_id" value="{{$cat[0]->id}}">
                <p class="col-sm-12">Название на русском:</p>
                <input type="text" name="name_ru" value="{{$cat[0]->getTranslation('name', 'ru')}}" class="form-control col-sm-12 ml-3">
                <br>
                <p class="col-sm-12">Название на казахском:</p>
                <input type="text" name="name_kz" value="{{$cat[0]->getTranslation('name', 'kz')}}" class="form-control col-sm-12 ml-3  ">
                <select name="on_promo" class="form-control col-sm-2 ml-3 mt-2">
                    <option value="0" @if($cat[0]->on_promo == 0) selected @endif>
                        Не в предложениях
                    </option>
                    <option value="1" @if($cat[0]->on_promo == 1) selected @endif>
                        В предложениях
                    </option>
                </select>
            <br>
            @if(strlen($cat[0]->img) > 1)
                <img src="/storage/profession-cats-images/{{$cat[0]->img}}" width=100><br>
                <a href="/manage/professions/delete-image/{{$cat[0]->id}}">Удалить картинку</a>
                <br><br>
            @else
                Картинка отсутствует
            @endif
            <div class="input-group">
                <div class="input-group-addon">Картинка превью<br>(Загрузите если нужно обновить)</div>
                <input type="file" name="img" class="form-control">
            </div>
            <br>
                <input type="submit" value="Обновить" class="btn btn-success col-sm-2 ml-3 mt-2">
        </form>
    </div>
@endsection
