@extends('manage.layout')

@section('title', $title)

@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
      <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-success">Список</span> Нажмите на профессию для ее просмотра и редактирования
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
    </div>
    @if(\Session::has('status'))
        <div class="col-sm-12">
            <div class="alert alert-success">
                {{\Session::get('status')}}
            </div>
        </div>
    @endif
    <div class="col-sm-12">
        <a href="/manage/add-profession"><button class="btn btn-success">Добавить профессию</button></a>
        <hr>
    </div>
    <div class="col-lg-12">
            <table id="all_professions" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>Категория</th>
                        <th>Название</th>
                        <th>Зарплата</th>
                        <th>Описание</th>
                        <th>Стажировка</th>
                        <th>Навыки</th>
                        <th><center>Ред.</center></th>
                        <th><center>Удалить</center></th>
                      </tr>
                    </thead>
            </table>
    </div>
@endsection

@section('datatable_js')
<script src="/manage_res/assets/js/lib/data-table/datatables.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/dataTables.bootstrap.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/dataTables.buttons.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.bootstrap.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/jszip.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/pdfmake.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/vfs_fonts.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.html5.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.print.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.colVis.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/datatables-init.js"></script>

    <script type="text/javascript">
        jQuery(document).ready(function(){
        jQuery('#all_professions').DataTable({
            'processing': false,
            'serverSide': false,
            'serverMethod': 'get',
            'ajax': {
                'url':'/manage/all-professions',
            }, 
            'columns': [
                { data: 'category',
                  "render": function(data,type,row,meta) {
                        return row.category.name.ru;
                    }
                },
                { data: 'name' },
                { data: 'salary' },
                { data: 'description',
                    "render": function(data,type,row,meta) {
                        return data.substring(0,50)+"...";
                    }
                },
                { data: 'internship',
                    "render": function(data,type,row,meta) {
                        if(data==1){
                            var a = 'Возможна';
                        }
                        if(data==0){
                            var a = 'Отсутствует';
                        }
                        return a;
                    }
                },
                { data: 'skills' },
                { data: "id", // can be null or undefined
                  "render": function(data,type,row,meta) {
                        return '<center><a href="/manage/edit-profession/'+data+'"><i class="fa fa-edit" style="font-size:24px;"></i></a></center>';
                    }
                },
                { data: "id", // can be null or undefined
                  "render": function(data,type,row,meta) {
                        return '<center><a href="/manage/delete-profession/'+data+'"><i class="fa fa-trash" style="font-size:24px;"></i></a></center>';
                    }
                }
                ]
            });
        });
    </script>

@endsection