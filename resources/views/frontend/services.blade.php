@extends('frontend.layouts.app')
@php
    $title = 'Наши Услуги';
@endphp
@section('title', $title)


@section('content')
    <section class="services blue-bg">
        <div class="container"><h2>Что входит в наши услуги?</h2>
            <div class="row">
                <div class="col-md-6 col-lg-4 px-4 d-flex flex-direction-column">
                    <div class="card"><img src="{{ asset('img/services/analysis.svg') }}" class="card-img-top"
                                           alt="Анализ и консультирование">
                        <div class="card-body"><h5 class="card-title">Анализ и консультирование</h5>
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">Консультации на основе результатов теста с учеником и
                                    родителями
                                </li>
                                <li class="list-group-item">Поведенческий анализ с помощью тренингов</li>
                                <li class="list-group-item">Консультации на основе тренингов с учеником и родителями</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 px-4 d-flex flex-direction-column">
                    <div class="card card--two"><img src="{{ asset('img/services/training.svg') }}" class="card-img-top"
                                                     alt="Тренинги личностного роста">
                        <div class="card-body"><h5 class="card-title">Тренинги личностного роста</h5>
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">Тематические тренинги по саморазвитию, по обучению и развитию
                                    софт-навыков
                                </li>
                                <li class="list-group-item">Семинары</li>
                                <li class="list-group-item">Игры</li>
                                <li class="list-group-item">Деловые кейсы</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 px-4 d-flex flex-direction-column">
                    <div class="card card--three"><img src="{{ asset('img/services/study.svg') }}" class="card-img-top"
                                                       alt="Обучение профориентологов">
                        <div class="card-body"><h5 class="card-title">Обучение профориентологов</h5>
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">Модульное обучение</li>
                                <li class="list-group-item">Обучение диагностике по тестам DISC и MAPP</li>
                                <li class="list-group-item">Техники и инструменты</li>
                                <li class="list-group-item">Изучение и познание профессий</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="peculiarities blue-bg">
        <div class="container"><h2>Особенности нашей программы</h2>
            <div class="row">
                <div class="col col-md-6 col-xl-4">
                    <div class="card"><img src="{{ asset('img/peculiarities/couching.svg') }}" class="card-img-top" alt="Коучинг">
                        <div class="card-body"><h5 class="card-title">Коучинг</h5>
                            <p class="card-text">Коуч помогает преодолеть разрыв между тем, где человек находится сейчас и
                                где он хочет быть.</p></div>
                    </div>
                </div>
                <div class="col col-md-6 col-xl-4">
                    <div class="card"><img src="{{ asset('img/peculiarities/mentoring.svg') }}" class="card-img-top" alt="Менторство">
                        <div class="card-body"><h5 class="card-title">Менторство</h5>
                            <p class="card-text">Ментор вдохновляет, помогает развиваться в личной и профессиональной
                                жизни.</p></div>
                    </div>
                </div>
                <div class="col col-md-6 col-xl-4">
                    <div class="card"><img src="{{ asset('img/peculiarities/individual.svg') }}" class="card-img-top"
                                           alt="Индивидуальный подход">
                        <div class="card-body"><h5 class="card-title">Индивидуальный подход</h5>
                            <p class="card-text">Учитывая потребности клиента, мы поможем разрешить запрос каждого клиента
                                индивидуально.</p></div>
                    </div>
                </div>
                <div class="col col-m-left col-md-6 col-xl-4">
                    <div class="card"><img src="{{ asset('img/peculiarities/online.svg') }}" class="card-img-top" alt="Онлайн обучение">
                        <div class="card-body"><h5 class="card-title">Онлайн обучение</h5>
                            <p class="card-text">У вас есть возможность учиться практически в любое время, в своем
                                собственном темпе и в любом месте.</p></div>
                    </div>
                </div>
                <div class="col col-m-right col-md-6 col-xl-4">
                    <div class="card"><img src="{{ asset('img/peculiarities/project.svg') }}" class="card-img-top"
                                           alt="Проектное обучение">
                        <div class="card-body"><h5 class="card-title">Проектное обучение</h5>
                            <p class="card-text">Проектное обучение ценен тем, что дети учатся приобретать знания, достигать
                                целей и получать опыт самостоятельно.</p></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="errors blue-bg">
        <div class="container"><h2>Какие ошибки может допустить ребенок?</h2>
            <div class="row">
                <div class="col col-md-6 col-lg-4">
                    <div class="card">
                        <div class="card-body"><p class="card-text">Выбирать профессию, не имея о ней достоверной
                                информации</p></div>
                    </div>
                </div>
                <div class="col col-md-6 col-lg-4">
                    <div class="card">
                        <div class="card-body"><p class="card-text">Ориентироваться только на такие признаки, как
                                престижность и доходность</p></div>
                    </div>
                </div>
                <div class="col col-md-6 col-lg-4">
                    <div class="card">
                        <div class="card-body"><p class="card-text">Выбор профессии под влиянием близких и товарищей («за
                                компанию», чтобы не отстать)</p></div>
                    </div>
                </div>
                <div class="col col-md-6 col-lg-4">
                    <div class="card">
                        <div class="card-body"><p class="card-text">Прислушиваться к мнению людей, некомпетентных в вопросах
                                выбора профессии</p></div>
                    </div>
                </div>
                <div class="col col-md-6 col-lg-4">
                    <div class="card">
                        <div class="card-body"><p class="card-text">Отождествлять будущюю профессию с любимым школьным
                                предметом</p></div>
                    </div>
                </div>
                <div class="col col-img col-md-6 col-lg-4">
                    <div class="card">
                        <div class="card-body"><p class="card-text">Игнорировать собственные способности и интересы.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="what-learn blue-bg">
        <div class="container"><h2>Чему учится ребенок?</h2>
            <div class="row">
                <div class="col col-lg-6">
                    <div class="card">
                        <div class="card-body"><h5 class="card-title">Познание и приянтие себя</h5>
                            <p class="card-text">Узнает особенности своего внутреннего и внешнего я, определит свой
                                темперамент, жизненные ценности и научится самоанализу</p></div>
                    </div>
                    <div class="card">
                        <div class="card-body"><h5 class="card-title">Исследование</h5>
                            <p class="card-text">Пройдет тестирование на определение архетипа. Научится правильному
                                исследованию профессии. Узнает основные характеристики исследования.</p></div>
                    </div>
                    <div class="card">
                        <div class="card-body"><h5 class="card-title">Принятие решения</h5>
                            <p class="card-text">Вы научитесь эффективным методам принятия решений и сможет осознанно делать
                                выбор.</p></div>
                    </div>
                </div>
                <div class="col col-lg-6">
                    <div class="card">
                        <div class="card-body"><h5 class="card-title">Как правильно выбрать ВУЗ?</h5>
                            <p class="card-text">Научится по каким критериям и особенностям выбирать ВУЗ.</p></div>
                    </div>
                    <div class="card">
                        <div class="card-body"><h5 class="card-title">Навыки концентрации</h5>
                            <p class="card-text">Научится осознанно или неосознанно исключать из сознания всё лишнее,
                                мешающее для достижения цели.</p></div>
                    </div>
                    <div class="card">
                        <div class="card-body"><h5 class="card-title">Навыки личной эффективности</h5>
                            <p class="card-text">Приобретёт навыки личной эффективности способные максимально быстро и
                                качественно выполнять поставленные задачи.</p></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="test blue-bg">
        <div class="container">
            <div class="row">
                <div class="col col-12 col-lg-6"><h2>Карьерный тест MAPP™</h2>
                    <p>Сдав MAPP тест, вы получите обширную информацию, которая поможет найти правильную карьеру,
                        соответствующую вашему уникальному профилю оценки.</p>
                    <div class="btns d-flex">
                        <a href="#price-list" class="btn btn-info">ПРОЙТИ ТЕСТ </a>
                        <a href="#mapp">УЗНАТЬ БОЛЬШЕ</a>
                    </div>
                </div>
                <div class="col col-12 col-lg-6"><img src="{{ asset('img/test-img.png') }}" alt="Карьерный тест MAPP™"></div>
            </div>
        </div>
    </section>
    <section id="mapp" class="mapp blue-bg"><h2>MAPP Тест это...</h2>
        <div class="container"><h2></h2>
            <div class="row">
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card"><img src="{{ asset('img/mapp/1.svg') }}" class="card-img-top" alt="20 лет на рынке">
                        <div class="card-body"><h5 class="card-title">20 лет на рынке</h5>
                            <p class="card-text">Тест был разработан в 1995 году.</p></div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card"><img src="{{ asset('img/mapp/2.svg') }}" class="card-img-top" alt="Более 8 миллионов сдавших тест">
                        <div class="card-body"><h5 class="card-title">Более 8 миллионов сдавших тест</h5>
                            <p class="card-text">Каждые 30 секунд, кто-то в мире сдает MAPP тест.</p></div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card"><img src="{{ asset('img/mapp/3.svg') }}" class="card-img-top" alt="Надежность ">
                        <div class="card-body"><h5 class="card-title">Надежность</h5>
                            <p class="card-text">МАРР прошел проверку достоверности со стороны ряда психологов, включая
                                Strong Interest Inventory.</p></div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card"><img src="{{ asset('img/mapp/4.svg') }}" class="card-img-top" alt="Незамедлительный результат">
                        <div class="card-body"><h5 class="card-title">Незамедлительный результат</h5>
                            <p class="card-text">Результаты доступны сразу после прохождения теста.</p></div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card"><img src="{{ asset('img/mapp/5.svg') }}" class="card-img-top" alt="Занимает 22 минуты">
                        <div class="card-body"><h5 class="card-title">Занимает 22 минуты</h5>
                            <p class="card-text">Трудно поверить, что 22 – минутный тест может быть таким показательным!</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="price-list" class="price-list blue-bg">
        <div class="container"><h2>ПРАЙС-ЛИСТ</h2>
            <p class="subtitle">Выберите тарифный план, подходящий именно вам!</p>
            <div class="row">
                @php
                $count = 0;
                @endphp
                @if($plans->count())
                    @foreach($plans as $plan)
                        @php
                            $count++;
                        @endphp
                        <div class="col-12 col-lg-4">
                            <div class="card">
                                <div class="card-body">
                                    <div class="top d-flex justify-content-between">
                                        <h5 class="card-title">{{ $plan->title }}</h5>
                                        <span>{{ $plan->price }} KZT</span>
                                    </div>
                                    @if($plan->items->count())
                                        <ul class="list-group">
                                            @foreach($plan->items as $item)
                                                <li class="list-group-item @if(!$item->is_active) text-muted @endif">{{ $item->title }}</li>
                                            @endforeach
                                        </ul>
                                    @endif
                                    @if($count == 1)
                                        <a href="{{ $plan->link }}" class="btn btn-info">Выбрать план</a>
                                    @else
                                        <a href="{{ $plan->link }}" class="btn btn-outline-info">Выбрать план</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif

            </div>
        </div>
    </section>
    <section class="reviews blue-bg">
        <div class="container"><h2>Что о нас говорят наши клиенты?</h2>
            <div class="row">
                <div class="col-12 col-md-6 col-xl-3">
                    <div class="card"><img src="{{ asset('img/icons/quotes.svg') }}" class="card-img-top">
                        <div class="card-body"><p class="card-text">Мне больше всего понравилось открытое общение и веселая
                                атмосфера. Результаты тестирования MAPP довольно правдивые. “Career Vision” это профессия,
                                будущее, качество.</p></div>
                        <div class="card-footer">Биржан Бисултан</div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-xl-3">
                    <div class="card"><img src="{{ asset('img/icons/quotes.svg') }}" class="card-img-top">
                        <div class="card-body"><p class="card-text">Семинар для меня лично стал стимулом для нового этапа в
                                моей жизни. Прежде всего порадовала тёплая, дружная обстановка, высокий уровень организации и
                                профессионализма.</p></div>
                        <div class="card-footer">Абдижаппар Ахмет</div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-xl-3">
                    <div class="card"><img src="{{ asset('img/icons/quotes.svg') }}" class="card-img-top">
                        <div class="card-body"><p class="card-text">Мне больше всего понравилось как психологи занимались с
                                нами. Особенно могу выделить технику “икигай”. Результаты теста MAPP совпали с моим выбором.
                                Career Vision лучшие в своей сфере, отличные собеседники и дают точные результаты.</p></div>
                        <div class="card-footer">Мерей Сейіт</div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-xl-3">
                    <div class="card"><img src="{{ asset('img/icons/quotes.svg') }}" class="card-img-top">
                        <div class="card-body"><p class="card-text">Бұл шара туралы класс жетекшісінен білдім. Баламның
                                мамандығын білейін деп, яғни келешекте қандай маман бала алады екенин. Іс-шара жақсы өтті, маған
                                ұнады, балама көп пайдасы болды. Басқаларға да қатысуға кеңес берер едім.</p></div>
                        <div class="card-footer">Құмақан Алмастың анасы Жұмабекова Алтынай</div>
                    </div>
                </div>
            </div>
            <div class="reviews__videos row">
                @if($videos->count())
                    @foreach($videos as $video)
                        <div class="col-12 col-lg-6">
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" width="100%" height="400"
                                        src="https://www.youtube.com/embed/{{ $video->url }}" title="YouTube video player" frameborder="0"
                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                        allowfullscreen></iframe>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </section>
    <section class="partners blue-bg">
        <div class="container">
            <div class="partners__inner d-flex"><h2>Наши партнёры</h2>
                <div class="partners__mozaic"><img class="partners__img first" src="{{ asset('img/partners/1.png') }}" alt=""> <img
                        class="partners__img second" src="{{ asset('img/partners/2.png') }}" alt=""> <img class="partners__img third"
                                                                                           src="{{ asset('img/partners/3.png') }}" alt="">
                    <img class="partners__img fourth" src="{{ asset('img/partners/4.png') }}" alt=""> <img class="partners__img fifth"
                                                                                            src="{{ asset('img/partners/5.png') }}" alt="">
                    <img class="partners__img sixth" src="{{ asset('img/partners/6.png') }}" alt=""> <img class="partners__img seventh"
                                                                                           src="{{ asset('img/partners/7.png') }}" alt="">
                    <img class="partners__img eighth" src="{{ asset('img/partners/8.png') }}" alt=""> <img class="partners__img nineth"
                                                                                            src="{{ asset('img/partners/9.png') }}" alt="">
                    <img class="partners__img tenth" src="{{ asset('img/partners/10.png') }}" alt="">
                    <div class="partners__row"></div>
                    <div class="partners__row"></div>
                    <div class="partners__row"></div>
                </div>
            </div>
        </div>
    </section>
@endsection
