<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@600&family=Jost:ital,wght@0,500;1,400&family=Raleway&family=Ubuntu:ital,wght@0,300;0,500;1,300;1,400&display=swap"
          rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <title>@yield('title') - Career Vision</title></head>
<body>
<header class="header">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-lg"><a class="navbar-brand" href="/"><img src="{{ asset('img/logo.png') }}" alt=""> </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                    data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="ms-auto navbar-nav mb-2 mb-lg-0">
                    <li class="nav-item"><a class="nav-link" href="/about">О НАС</a></li>
                    <li class="nav-item"><a class="nav-link" href="/services">НАШИ УСЛУГИ</a></li>
                    <li class="nav-item"><a class="nav-link" href="/contacts">КОНТАКТЫ</a></li>
                    <li class="nav-item"><a href="/" aria-current="page" class="nav-link btn btn-outline-info px-3 active">Вход</a></li>
                </ul>
            </div>
        </div>
    </nav>
</header>
@yield('content')
<script src="{{ asset('js/all.js') }}"></script>
</body>
</html>
