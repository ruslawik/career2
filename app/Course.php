<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations as BaseHasTranslations;

trait HasTranslations
{
    use BaseHasTranslations;
    /**
     * Convert the model instance to an array.
     *
     * @return array
     */
    public function toArray()
    {
        $attributes = parent::toArray();
        foreach ($this->getTranslatableAttributes() as $field) {
            $attributes[$field] = $this->getTranslation($field, \App::getLocale());
        }
        return $attributes;
    }
}

class Course extends Model
{
	use HasTranslations;
    protected $table = 'courses';

    public $translatable = ['name', 'description', 'tags'];

    public function category(){

        return $this->belongsTo('App\CourseCats', 'cat_id', 'id');
    }

    public function small_desc($desc){
        return mb_substr($desc, 0, 120)."...";
    }

    public function very_small_desc($desc){
        return mb_substr($desc, 0, 40)."...";
    }
}
