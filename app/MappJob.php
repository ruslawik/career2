<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MappJob extends Model
{
    protected $table = "mapp_jobs";
}
