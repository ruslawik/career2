<?php

namespace App;

use App\User;
use App\SubadminTestPrice;
use Illuminate\Database\Eloquent\Model;

class SubadminSetting extends Model
{
    protected $table = "subadmin_settings";

    public function test_price(){
    	return $this->hasOne("App\SubadminTestPrice", 'setting_id', 'id');
    }

    public function user(){
    	return $this->hasOne("App\User", 'id', 'who_set_id');
    }

    public function subadmin(){
    	return $this->hasOne("App\User", 'id', 'user_id');
    }

}
