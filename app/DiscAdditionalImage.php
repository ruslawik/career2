<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiscAdditionalImage extends Model
{
    protected $table = "disc_additional_images";
}
