<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Questions_Keirsi;
use App\Answers_Keirsi;

class Results_Keirsi extends Model
{
    protected $table = 'results_keirsi';
    
    function quest_text (){

    	return $this->hasOne("App\Questions_Keirsi", 'id', 'quest_id');
    }
    
    function answer_text ($type, $quest_id){

    	$res = Answers_Keirsi::where("type", $type)
    						  ->where("question_id", $quest_id)
    						  ->first();
    	return $res;
    }
}