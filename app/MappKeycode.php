<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class MappKeycode extends Model
{
    protected $table = "keycodes";

    public function user_email($keycode_id){

    	$keycode = MappKeycode::where('id', $keycode_id)->get();
    	if($keycode[0]->used_by_user_id > 0){
    		$user_data = User::where('id', $keycode[0]->used_by_user_id)->get();
    		return $user_data[0]->email;
    	}else{
    		return "";
    	}
    }
 
}
