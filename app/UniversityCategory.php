<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class UniversityCategory extends Model
{
    use HasTranslations;

    protected $table = "university_categories";

    public $translatable = ['name'];
}
