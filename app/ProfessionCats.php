<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class ProfessionCats extends Model
{

	use HasTranslations;

    protected $table = "profession_cats";

    public $translatable = ['name'];
}
