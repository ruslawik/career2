<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class CourseCats extends Model
{
	use HasTranslations;

    protected $table = 'course_cats';
    public $translatable = ['name'];
}
