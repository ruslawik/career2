<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Results_Mapp;

class Questions_Mapp_Kaz extends Model
{
    protected $table = 'questions_mapp_kaz';

    public function answer($user_id, $date, $question_id)
    {
        $result = Results_Mapp::where('question_id', $question_id)
        						->where('user_id', $user_id)
        						->where('created_at', $date)
        						->first();
        return $result;
    }

}