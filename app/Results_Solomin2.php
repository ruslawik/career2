<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Results_Solomin2 extends Model
{
    protected $table = "results_solomin2";

    function quest_text (){

    	return $this->hasOne("App\Solomin2", 'id', 'quest_id');
    }

}
