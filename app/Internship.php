<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;


class Internship extends Model
{
    use HasTranslations;
    protected $table = 'internships';

    public $translatable = ['name', 'location', 'descrip'];

    public function small_desc($desc){
        $desc = strip_tags(html_entity_decode($desc));
        return mb_substr($desc, 0, 120)."...";
    }
}
