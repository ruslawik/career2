<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MappJobDetails extends Model
{
    protected $table = "mapp_jobs_details";
}
