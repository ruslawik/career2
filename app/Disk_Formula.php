<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\DiscAdditionalImage;

class Disk_Formula extends Model
{
    protected $table = "disc_formulas";

    public function additional_images($formula_id){
    	$res = DiscAdditionalImage::where('disc_formula_id', $formula_id)->get();
    	return $res;
    }
}
