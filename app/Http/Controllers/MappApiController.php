<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\MappAuto;
use App\MappHtmlResRu;
use Auth;
use Storage;
use App\Keycode;
use Carbon\Carbon;
use DateTimeZone;
use App\User;
use PDF;

class MappApiController extends Controller
{

    public function isMappPassed ($email){

        $user_data = User::where("email", $email)->whereNotNull("mapp_finished_date")->get();
        $user_count = User::where("email", $email)->whereNotNull("mapp_finished_date")->count();

        if($user_count > 0){

            return response()->json(["status" => "yes", "mapp_finished_date" => $user_data[0]->mapp_finished_date, "user_id" => $user_data[0]->id, "name" => $user_data[0]->name, "surname" => $user_data[0]->surname]);

        }else{
            return response()->json(["status" => "no"]);
        }
    }

    public function autoreg_show_form($cookies_file_id, $keycode){

    	//запрашиваем страницу регистрации чтобы вырвать оттуда request_token 
     	$req_token = $this->getRequestToken($cookies_file_id, "https://recruit.assessment.com/CareerCenter/Account/Register");
     	
     	//ОТПРАВЛЯЕМ ЗАПРОС НА проверку keycode и получение куков страницы регистрации
     	$url = 'https://recruit.assessment.com/CareerCenter/Account/RegisterWithKeyCode';

     	$ch = curl_init();
     	curl_setopt($ch, CURLOPT_URL, $url);
     	curl_setopt($ch, CURLOPT_POST, 1);
     	curl_setopt($ch, CURLOPT_POSTFIELDS,
    					 "KeyCode=".$keycode."&__RequestVerificationToken=".$req_token);
     	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
     	curl_setopt ($ch, CURLOPT_COOKIEFILE, public_path().'/mapp_cookies/'.$cookies_file_id);
     	curl_setopt($ch, CURLOPT_COOKIEJAR, public_path().'/mapp_cookies/'.$cookies_file_id);

     	$response = curl_exec($ch);
     	$err = curl_error($ch);  //if you need
     	curl_close($ch);

     	preg_match('<input name="__RequestVerificationToken" type="hidden" value="(.*?)" /> ', $response, $out);
     	$request_token = $out[1];

     	$ar['title'] = "Регистрация на тест MAPP";
     	$ar['keycode'] = $keycode;
     	$ar['cookies_file_id'] = $cookies_file_id;
     	$ar['request_token'] = $request_token;

     	return view('user.mapp.autoreg_form', $ar);
    }

    public function reg_in_mapp_system (Request $r){

    	$url = 'https://recruit.assessment.com/CareerCenter/Account/RegisterNewMember';

    	$FirstName = $r->input('FirstName');
    	$MiddleInitial = $r->input('MiddleInitial');
    	$Email = $r->input('Email');
    	$Password = $r->input('Password');
    	$KeyCode = $r->input('KeyCode');
    	$cookies_file_id = $r->input('cookies_file_id');
    	$request_token = $r->input('request_token');

    	$ch = curl_init();
     	curl_setopt($ch, CURLOPT_URL, $url);
     	curl_setopt($ch, CURLOPT_POST, 1);
     	curl_setopt($ch, CURLOPT_POSTFIELDS,
    					"KeyCode=".$KeyCode.
    					"&__RequestVerificationToken=".$request_token.
    					"&FirstName=".$FirstName.
    					"&MiddleInitial=".
    					"&LastName=".$MiddleInitial.
    					"&Email=".$Email.
    					"&Password=".$Password.
    					"&ConfirmPassword=".$Password.
    					"&AccountNumber="
    					);
     	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
     	curl_setopt ($ch, CURLOPT_COOKIEFILE, public_path().'/mapp_cookies/'.$cookies_file_id);
     	curl_setopt($ch, CURLOPT_COOKIEJAR, public_path().'/mapp_cookies/'.$cookies_file_id);

     	$response = curl_exec($ch);
     	$err = curl_error($ch);  //if you need
     	curl_close($ch);

        //Storage::put('reged_response.txt', $response);

        Keycode::where('keycode', '=', $KeyCode)->update(["used"=>1, "used_by_user_id"=>Auth::user()->id, "used_datetime"=>Carbon::now(new DateTimeZone('Asia/Almaty'))]);

        User::where('id', Auth::user()->id)->update(["mapp"=>1, "mapp_login"=>$Email, "mapp_pass"=>$Password]);

     	return redirect('/user/before-mapp');
    }

    public function getRequestToken ($cookie_file_name, $url){

     	$ch = curl_init();
     	curl_setopt($ch, CURLOPT_URL, $url);
     	curl_setopt($ch, CURLOPT_POST, 0);
     	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
     	curl_setopt ($ch, CURLOPT_COOKIEFILE, public_path().'/mapp_cookies/'.$cookie_file_name);
     	curl_setopt($ch, CURLOPT_COOKIEJAR, public_path().'/mapp_cookies/'.$cookie_file_name);

     	$response = curl_exec($ch);
     	$err = curl_error($ch);  //if you need
     	curl_close($ch);
     	preg_match('<input name="__RequestVerificationToken" type="hidden" value="(.*?)" /> ', $response, $out);
     	$request_token = $out[1];
     	return $request_token;
    }

    public function getResultsRus (){
    		$user_id = Auth::user()->id;
    		download:
    		$result = MappHtmlResRu::where('user_id', $user_id)->count();
    		if($result > 0){
    			return 0;
    		}
    		if(file_exists( public_path().'/mapp_cookies/'.$user_id)){
            
                $url = 'http://recruit.assessment.com/CareerCenter/YourMAPPResults';
            	
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS,
    					"selectedLanguage=russian"
    					);
                curl_setopt($ch, CURLOPT_COOKIEFILE, public_path().'/mapp_cookies/'.$user_id);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                $response = curl_exec($ch);
                $err = curl_error($ch);
                curl_close($ch);
                
                MappHtmlResRu::insert(["html" => $response, "user_id" => $user_id]);
                print("<script>location.reload();</script>");
            }else{
                $mapp = new MappAuto();
                $mapp->mapp_auth($mapp_login, $mapp_pass, $user_id);
                goto download;
        	}
    }

    public function extractBlock ($block_integer, $html){
    	$block_integer2 = $block_integer+1;
    	preg_match('/<a name="NARR_'.$block_integer.'"><\/a>[\t\n\r\s\w\W\S\d\D]+<a name="NARR_'.$block_integer2.'"><\/a>/', $html, $out);
    	return $out[0];
    }

    public function getRuDescForMappStep1 (Request $r){
    	$user_id = Auth::user()->id;
    	//Метод для выдирания русских описаний к графикам на первом шаге результатов теста MAPP 
    	$block = $r->input('block');
    	show:
    	$result = MappHtmlResRu::where('user_id', $user_id)->count();
    	if($result > 0){

    		$text = "";
    		$html = MappHtmlResRu::where('user_id', $user_id)->get();

    		if($block>=0 && $block<6){
    			if($block>=1 && $block!=5){
    				$text = $this->extractBlock($block+1, $html[0]->html);
    			}else{
    				$text = $this->extractBlock($block, $html[0]->html);
    			}
    			if($block==5){
    				$text = $this->extractBlock($block+2, $html[0]->html);
    			}
    		}
    		if($block==6){
    				preg_match('/(<a name="NARR_8"><\/a>[\t\n\r\s\w\W\S\d\D]+<\/div>)[\t\n\r\s]+<div class="row">[\t\n\r\s\w\W\S\d\D]+<h2>СИСТЕМА КОДОВ ПРОФЕССИОНАЛЬНО ВАЖНЫХ/', $html[0]->html, $out);
    				$text = $out[1];
    		}

    		print($text);
    	}else{
    		$this->getResultsRus();
    		goto show;
    	}
    }

    public function download ($date){

        
        $user_id = Auth::user()->id;
            download2:
            $result = MappHtmlResRu::where('user_id', $user_id)->count();
            if($result > 0){
                $result2 = MappHtmlResRu::where('user_id', $user_id)->get();
                preg_match('/(<div class="row">[\t\n\r\s]+<div class="col-md-12">[\t\n\r\s]+<h2>TABLE OF CONTENTS<\/h2>[\t\n\r\s\w\W\S\d\D]+)<div class="modal fade" id="myModal">/', $result2[0]->html, $out_res);

                $res_to_export = $out_res[1];

                //Выбираем скрипты, чтобы вставить потом на странице
                preg_match('/<script>[\t\n\r\s]+\$\(document\)[\t\n\r\s\w\W\S\d\D]+var graphName = "\#aBIN";[\t\n\r\s\w\W\S\d\D]+<!-- Google Code for Remarketing Tag -->/', $result2[0]->html, $scripts);

                $res_to_export = $this->draw_charts($res_to_export, $scripts[0]);

                //return view("user.mapp.download", ["html"=>$res_to_export]);

                $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('user.mapp.download', ["html"=>$res_to_export, "date"=>$date]);
                return $pdf->stream();

            }elseif(file_exists( public_path().'/mapp_cookies/'.$user_id)){
            
                $url = 'http://recruit.assessment.com/CareerCenter/YourMAPPResults';
                
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS,
                        "selectedLanguage=russian"
                        );
                curl_setopt($ch, CURLOPT_COOKIEFILE, public_path().'/mapp_cookies/'.$user_id);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                $response = curl_exec($ch);
                $err = curl_error($ch);
                curl_close($ch);
                
                MappHtmlResRu::insert(["html" => $response, "user_id" => $user_id]);
                print("<script>location.reload();</script>");
            }else{
                $mapp = new MappAuto();
                $mapp->mapp_auth($mapp_login, $mapp_pass, $user_id);
                goto download2;
            }
            
    }

    public function chart_processing ($start_from, $chart_name, $res_to_export, $data){   

        preg_match('/'.$start_from.'(.*?)]/', $data, $out);

        $original_code = $out[0];

        preg_match_all('/value:"(\d)"/', $original_code, $all_digits);
        preg_match_all('/axis:"(.*?)"/', $original_code, $all_axises);

        $code_to_insert = "<img width=100% src=\"https://quickchart.io/chart?c={type:'polarArea', options:{scale:{ticks:{max:-1, min:-5, stepSize:1}}}, data:{labels:[";
        foreach ($all_axises[1] as $axis) {
            $code_to_insert .= "'".$axis."',";
        }
        $code_to_insert .= '], datasets:[{data:[';
        foreach ($all_digits[1] as $digit) {
            $digit *= -1;
            $code_to_insert .= "'".$digit."',";
        }
        $code_to_insert .= ']}]}}">';

        $res_to_export = preg_replace('/<div class="chart-container text-center" id="'.$chart_name.'"><\/div>/', $code_to_insert, $res_to_export);

        return $res_to_export;
    }

    public function draw_charts($res_to_export, $data){

        $res_to_export = $this->chart_processing('\[{axis:"IN1"', 'aBIN', $res_to_export, $data);

        $res_to_export = $this->chart_processing('\[{axis:"TE1"', 'aBTE', $res_to_export, $data);

        $res_to_export = $this->chart_processing('\[{axis:"APG"', 'aBAP', $res_to_export, $data);

        $res_to_export = $this->chart_processing('\[{axis:"PE0"', 'aBPE', $res_to_export, $data);

        $res_to_export = $this->chart_processing('\[{axis:"TH0"', 'aBTH', $res_to_export, $data);

        $res_to_export = $this->chart_processing('\[{axis:"DA0"', 'aBDA', $res_to_export, $data);

        $res_to_export = $this->chart_processing('\[{axis:"RE6"', 'aBRE', $res_to_export, $data);

        $res_to_export = $this->chart_processing('\[{axis:"MA6"', 'aBMA', $res_to_export, $data);

        $res_to_export = $this->chart_processing('\[{axis:"LA6"', 'aBLA', $res_to_export, $data);
        

        return $res_to_export;
    }
}
