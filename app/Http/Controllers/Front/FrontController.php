<?php
namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App;

class FrontController extends Controller{
    function getIndex (){
        $ar = array();
        $ar['title'] = 'Career Vision';

        return view("front.index", $ar);
    }

    public function setLang($locale){
        App::setLocale($locale);
        session()->put('language', $locale);
        return redirect()->back();
    }
    
}
