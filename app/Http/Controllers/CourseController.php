<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\CourseCats;
use App\Course;
use Storage;

class CourseController extends Controller
{
	//COURSES
	//USER methods

    public function search(Request $r){

    	$ar = array();

        $ar['title'] = "Поиск курсов";
        $ar['head_title'] = "Поиск курсов";
        $ar['username'] = Auth::user()->name." ".Auth::user()->surname;
        $ar['categories'] = CourseCats::all();
        $ar['selected_cat'] = $r->input('category');
        $ar['teklifler'] = CourseCats::where('on_promo', 1)->get();
        $ar['q'] = "";
        $ar['courses'] = Course::where('public', 1)->paginate(6);

        if($r->has('category') && $r->input('category')!=-1){
            $ar['courses'] = Course::where('cat_id', $r->input('category'))
                                                ->paginate(6);           
        }

        if($r->has('q')){
            $ar['courses'] = Course::where('name->ru', 'LIKE', "%".$r->input('q')."%")
                                        ->orWhere('name->kz', 'LIKE', "%".$r->input('q')."%")
                                        ->orWhere('description->ru', 'LIKE', "%".$r->input('q')."%")
                                        ->orWhere('description->kz', 'LIKE', "%".$r->input('q')."%")
                                        ->orWhere('tags->kz', 'LIKE', "%".$r->input('q')."%")
                                        ->orWhere('tags->ru', 'LIKE', "%".$r->input('q')."%")
                                        ->where('public', 1)
                                        ->paginate(6);  
            $ar['q'] = $r->input('q');
        }

        if($r->has('q') && $r->has('category')){
            $q = $r->input('q');
            $cat = $r->input('category');
            if($cat==-1){
                return redirect('user/course/search?q='.$q);
            }
            $ar['courses'] = Course::where(function ($query){
                                    global $q;
                                    $query->where('name->ru', 'LIKE', "%".$q."%")
                                        ->orWhere('name->kz', 'LIKE', "%".$q."%")
                                        ->orWhere('description->ru', 'LIKE', "%".$q."%")
                                        ->orWhere('description->kz', 'LIKE', "%".$q."%")
                                        ->orWhere('tags->kz', 'LIKE', "%".$q."%")
                                        ->orWhere('tags->ru', 'LIKE', "%".$q."%");
                                    })
                                    ->where('cat_id', $cat)
                                    ->where('public', 1)
                                    ->paginate(6);  
            $ar['q'] = $r->input('q');
        }

    	return view('user.courses.search', $ar);
    }

    //ADMIN methods

    public function courses_list (){

    	$ar['title'] = "Все курсы";

    	return view('manage.courses.course_list', $ar);
    }

    public function allCoursesAjax (){

    	$courses = Course::select('cat_id', 'name', 'tags', 'description', 'img', 'link', 'id')->with('category')
                        ->get()
                        ->toArray();

        $output = array(
            'data' => $courses
        );
        return response()->json($output);
    }

    public function addCourse (){

    	$ar['title'] = "Добавить курс";
    	$ar['categories'] = CourseCats::all();

    	return view('manage.courses.add_course', $ar);
    }

    public function postAddCourse (Request $r){

    	$r->validate([
        	'img' => 'required',
        	'link' => 'required',
        	'name_ru' => 'required',
        	'name_kz' => 'required',
        	'description_ru' => 'required',
        	'description_kz' => 'required',
        	'tags_ru' => 'required',
        	'tags_kz' => 'required',
        ]);

    	if($r->file()) {
            $fileName = time().'_'.$r->img->getClientOriginalName();
            $filePath = $r->file('img')->storeAs('course-images', $fileName, 'public');
        }

    	$course = new course();
    	$course->public = $r->input('public');
        $course->top_course = $r->input('top_course');
    	$course->link = $r->input('link');
    	$course->cat_id = $r->input('category_id');
    	$course->img = $fileName;

    	$course->setTranslation('name', 'ru', $r->input('name_ru'))
    				->setTranslation('name', 'kz', $r->input('name_kz'))
    				->setTranslation('description', 'ru', $r->input('description_ru'))
    				->setTranslation('description', 'kz', $r->input('description_kz'))
    				->setTranslation('tags', 'ru', $r->input('tags_ru'))
    				->setTranslation('tags', 'kz', $r->input('tags_kz'))
    				->save();

    	return redirect('/manage/courses')->with('status', 'Новый курс успешно добавлен!');
    }

    public function editCourse ($course_id){

    	$ar['title'] = "Редактировать курс";

    	$ar['course'] = Course::where('id', $course_id)->get();
    	$ar['categories'] = CourseCats::all();

    	return view("manage.courses.edit_course", $ar);

    }

    public function postEditCourse (Request $r){

    	$r->validate([
        	'link' => 'required',
        	'name_ru' => 'required',
        	'name_kz' => 'required',
        	'description_ru' => 'required',
        	'description_kz' => 'required',
        	'tags_ru' => 'required',
        	'tags_kz' => 'required',
        ]);

    	if($r->file()) {
            $fileName = time().'_'.$r->img->getClientOriginalName();
            $filePath = $r->file('img')->storeAs('course-images', $fileName, 'public');
            $now_file = Course::where('id', $r->input('course_id'))->get();
            if(strlen($now_file[0]->img) > 1 && $now_file[0]->img != NULL){
                unlink("storage/course-images/".$now_file[0]->img);
            }
            Course::where('id', $r->input('course_id'))
    				->update(["img" => $fileName]);
        }

    	Course::where('id', $r->input('course_id'))
    				->update(["link" => $r->input('link'),
    						  "cat_id" => $r->input('category_id'),
    						  "public" => $r->input('public'),
                              "top_course" => $r->input('top_course')]);

    	$course = Course::where('id', $r->input('course_id'))->get();
    	$course[0]->setTranslation('name', 'ru', $r->input('name_ru'))
    				->setTranslation('name', 'kz', $r->input('name_kz'))
    				->setTranslation('description', 'ru', $r->input('description_ru'))
    				->setTranslation('description', 'kz', $r->input('description_kz'))
    				->setTranslation('tags', 'ru', $r->input('tags_ru'))
    				->setTranslation('tags', 'kz', $r->input('tags_kz'))
    				->save();

    	return back()->with('status', 'Информация успешно обновлена!');
    }

    public function categories_list (){

    	$ar['title'] = "Все категории курсов";

    	$ar['categories'] = CourseCats::all();

    	return view('manage.courses.courses_categories', $ar);
    }

    public function addCourseCat (Request $r){

    	$course_cat = new CourseCats();
    	$course_cat->setTranslation('name', 'ru', $r->input('name_ru'))
    				->setTranslation('name', 'kz', $r->input('name_kz'));
    	$course_cat->on_promo = $r->input('on_promo');
    	$course_cat->save();

    	return back()->with('status', 'Успешно добавлено!');

    }

    public function editCourseCat ($cat_id){

    	$ar['title'] = "Редактировать категорию";

    	$ar['cat'] = CourseCats::where('id', $cat_id)->get();

    	return view('manage.courses.edit_course_category', $ar);
    }

    public function postEditCourseCat (Request $r){

    	CourseCats::where('id', $r->input('cat_id'))->update(["on_promo"=>$r->input('on_promo')]);

    	$prof_cat = CourseCats::where('id', $r->input('cat_id'))->get();
    	$prof_cat[0]->setTranslation('name', 'ru', $r->input('name_ru'))
    				->setTranslation('name', 'kz', $r->input('name_kz'))
    				->save();

    	return redirect('/manage/courses-cats')->with('status', 'Успешно обновлено!');
    }

    public function deleteCourse ($course_id){

    	$now_file = Course::where('id', $course_id)->get();
        if($now_file[0]->img != NULL){
            unlink("storage/course-images/".$now_file[0]->img);
        }

        Course::where('id', $course_id)->delete();

        return back()->with('status', 'Курс успешно удален!');
    }
}
