<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Profession;
use App\ProfessionCats;

class ProfessionController extends Controller
{

	//USER METHODS

    public function profession ($prof_id){

        $ar = array();

        $ar['title'] = "Профессии";
        $ar['head_title'] = "Профессии";
        $ar['username'] = Auth::user()->name." ".Auth::user()->surname;

        $prof = Profession::where('id', $prof_id)->get();
        $ar['prof'] = $prof[0];

        return view("user.professions.profession", $ar);
    }

    public function search(Request $r){

    	$ar = array();

        $ar['title'] = "Профессии";
        $ar['head_title'] = "Профессии";
        $ar['username'] = Auth::user()->name." ".Auth::user()->surname;
        $ar['teklifler'] = ProfessionCats::where('on_promo', 1)->get();
        $ar['categories'] = ProfessionCats::all();
        $ar['professions'] = Profession::paginate(6);
        $ar['selected_cat'] = $r->input('category');
        $ar['q'] = "";

        if($r->has('category') && $r->input('category')!=-1){
            $ar['professions'] = Profession::where('category_id', $r->input('category'))
                                                ->paginate(6);
        }

        if($r->has('q')){
            $word = mb_strtolower($r->input('q'));
            $ar['professions'] = Profession::whereRaw('LOWER(name->"$.ru") like ?', '%'.$word.'%')
                                        ->orWhereRaw('LOWER(name->"$.kz") like ?', '%'.$word.'%')
                                        ->orWhereRaw('LOWER(description->"$.kz") like ?', '%'.$word.'%')
                                        ->orWhereRaw('LOWER(description->"$.ru") like ?', '%'.$word.'%')
                                        ->paginate(6);
            $ar['q'] = $r->input('q');
        }

        if($r->has('q') && $r->has('category')){
            $q = $r->input('q');
            $cat = $r->input('category');
            if($cat==-1){
                return redirect('user/profession/search?q='.$q);
            }
            $ar['professions'] = Profession::where(function ($query){
                                    global $q;
                                    $query->where('name->ru', 'LIKE', "%".$q."%")
                                        ->orWhere('name->kz', 'LIKE', "%".$q."%")
                                        ->orWhere('description->ru', 'LIKE', "%".$q."%")
                                        ->orWhere('description->kz', 'LIKE', "%".$q."%");
                                    })
                                    ->where('category_id', $cat)
                                    ->paginate(6);
            $ar['q'] = $r->input('q');
        }

    	return view('user.professions.search', $ar);
    }



    //ADMIN METHODS

    public function profession_list (){

    	$ar['title'] = "Все профессии";

    	return view('manage.professions.profession_list', $ar);
    }

    public function categories_list (){

    	$ar['title'] = "Все категории профессий";

    	$ar['categories'] = ProfessionCats::all();

    	return view('manage.professions.profession_categories', $ar);
    }

    public function addProfession (){

    	$ar['title'] = "Добавить профессию";
    	$ar['categories'] = ProfessionCats::all();

    	return view('manage.professions.add_profession', $ar);
    }

    public function postAddProfession (Request $r){

        $r->validate([
            'salary' => 'required',
            'name_ru' => 'required',
            'name_kz' => 'required',
            'description_ru' => 'required',
            'description_kz' => 'required',
            'skills_ru' => 'required',
            'skills_kz' => 'required',
        ]);

    	$profession = new Profession();
    	$profession->salary = $r->input('salary');
    	$profession->internship = $r->input('internship');
    	$profession->category_id = $r->input('category_id');
    	$profession->setTranslation('name', 'ru', $r->input('name_ru'))
    				->setTranslation('name', 'kz', $r->input('name_kz'))
    				->setTranslation('description', 'ru', $r->input('description_ru'))
    				->setTranslation('description', 'kz', $r->input('description_kz'))
    				->setTranslation('skills', 'ru', $r->input('skills_ru'))
    				->setTranslation('skills', 'kz', $r->input('skills_kz'))
    				->save();

    	return redirect('/manage/professions')->with('status', 'Новая профессия успешно добавлена!');
    }

    public function editProfession ($prof_id){

    	$ar['title'] = "Редактировать профессию";

    	$ar['prof'] = Profession::where('id', $prof_id)->get();
    	$ar['categories'] = ProfessionCats::all();

    	return view("manage.professions.edit_profession", $ar);

    }

    public function postEditProfession (Request $r){

    	Profession::where('id', $r->input('prof_id'))
    				->update(["salary" => $r->input('salary'),
    						  "internship" => $r->input('internship'),
    						  "category_id" => $r->input('category_id')]);

    	$profession = Profession::where('id', $r->input('prof_id'))->get();
    	$profession[0]->setTranslation('name', 'ru', $r->input('name_ru'))
    				->setTranslation('name', 'kz', $r->input('name_kz'))
    				->setTranslation('description', 'ru', $r->input('description_ru'))
    				->setTranslation('description', 'kz', $r->input('description_kz'))
    				->setTranslation('skills', 'ru', $r->input('skills_ru'))
    				->setTranslation('skills', 'kz', $r->input('skills_kz'))
    				->save();

    	return back()->with('status', 'Информация успешно обновлена!');
    }

    public function addProfessionCat (Request $r){

    	$profession = new ProfessionCats();
    	$profession->setTranslation('name', 'ru', $r->input('name_ru'))
    				->setTranslation('name', 'kz', $r->input('name_kz'));
    	$profession->on_promo = $r->input('on_promo');
    	$profession->save();

    	return back()->with('status', 'Успешно добавлено!');
    }

    public function editProfessionCat ($cat_id){

    	$ar['title'] = "Редактировать категорию";

    	$ar['cat'] = ProfessionCats::where('id', $cat_id)->get();

    	return view('manage.professions.edit_profession_category', $ar);
    }

    public function postEditProfessionCat (Request $r){

    	ProfessionCats::where('id', $r->input('cat_id'))->update(["on_promo"=>$r->input('on_promo')]);

        if($r->file()) {
            $fileName = time().'_'.$r->img->getClientOriginalName();
            $filePath = $r->file('img')->storeAs('profession-cats-images', $fileName, 'public');
            $now_file = ProfessionCats::where('id', $r->input('cat_id'))->get();
            if(strlen($now_file[0]->img) > 0){
                $this->deleteFile("storage/profession-cats-images/".$now_file[0]->img);
            }
            ProfessionCats::where('id', $r->input('cat_id'))
                ->update(["img" => $fileName]);
        }

    	$prof_cat = ProfessionCats::where('id', $r->input('cat_id'))->get();
    	$prof_cat[0]->setTranslation('name', 'ru', $r->input('name_ru'))
    				->setTranslation('name', 'kz', $r->input('name_kz'))
    				->save();

    	return redirect('/manage/profession-cats')->with('status', 'Успешно обновлено!');
    }

    public function allProfessionsAjax (){

    	$professions = Profession::select('category_id', 'name', 'salary', 'description', 'internship', 'skills', 'id')->with('category')
                        ->get()
                        ->toArray();

        $output = array(
            'data' => $professions
        );
        return response()->json($output);
    }

    public function deleteImg ($cat_id){

        $now_file = ProfessionCats::where('id', $cat_id)->get();
        $this->deleteFile("storage/profession-cats-images/".$now_file[0]->img);

        ProfessionCats::where('id', $cat_id)->update(['img' => null]);

        return back()->with('status', 'Картинка успешно удалена!');
    }

    public function deleteFile($fullPath){
        if(file_exists($fullPath)){
            unlink($fullPath);
        }
    }

    public function deleteProfession ($prof_id){

        Profession::where('id', $prof_id)->delete();

        return back()->with('status', 'Профессия успешно удалена!');
    }
}
