<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\MappKeycode;
use App\User;
use Carbon\Carbon;
use DateTimeZone;
use Response;
use Session;
use App\Results_Mapp;

class MappKeycodeManage extends Controller
{

    public function editKeycode ($keycode_id){

        $ar = array();

        $ar['title'] = "Управление MAPP Keycodes";
        $ar['head_title'] = "Управление MAPP Keycodes";
        $ar['username'] = Auth::user()->name." ".Auth::user()->surname;
        $ar['keycode'] = MappKeycode::where('id', $keycode_id)->get();

        return view("manage.mapp_keycode.keycode_edit", $ar);
    }

    public function saveKeycode (Request $r){

        $user = User::where('email', $r->input('user_email'))->whereNotNull('email')->get();
        $user_count = User::where('email', $r->input('user_email'))->whereNotNull('email')->count();

        if($user_count <= 0){
            MappKeycode::where('id', $r->input('keycode_id'))
                    ->update([
                            "used" => $r->input('used'),
                            "given" => $r->input('given'),
                            "used_by_user_id" => NULL,
                            "used_datetime" => $r->input("used_datetime")
            ]);
            return back()->with('status', 'Данные успешно обновлены, но такой пользователь не найден - '.$r->input('user_email'));
        }

        MappKeycode::where('id', $r->input('keycode_id'))
                    ->update([
                            "used" => $r->input('used'),
                            "given" => $r->input('given'),
                            "used_by_user_id" => $user[0]->id,
                            "used_datetime" => $r->input("used_datetime")
                        ]);

        if($r->input('used')==0){
            MappKeycode::where('id', $r->input('keycode_id'))
                    ->update([
                            "used" => $r->input('used'),
                            "given" => $r->input('given'),
                            "used_by_user_id" => NULL,
                            "used_datetime" => NULL
                        ]);
        }

        return back()->with('status', 'Данные успешно обновлены!');
    }

    public function index (){

    	$ar = array();

        $ar['title'] = "Управление MAPP Keycodes";
        $ar['head_title'] = "Управление MAPP Keycodes";
        $ar['username'] = Auth::user()->name." ".Auth::user()->surname;

        MappKeycode::where('usage_started', '<', Carbon::now(new DateTimeZone('Asia/Almaty'))->subMinutes(25)->toDateTimeString())->where('used', 0)->update(["in_usage"=>0, "in_usage_by_user_id"=>NULL, "usage_started"=>NULL]);

        $ar['keycodes_total'] = MappKeycode::count();
        $ar['keycodes_used'] = MappKeycode::where('used', 1)->where('given', 0)->count();
        $ar['keycodes_given'] = MappKeycode::where('given', 1)->count();
        $ar['keycodes_used_from_given'] = MappKeycode::where('given', 1)->where('used', 1)->count();
        $ar['keycodes_in_usage'] = MappKeycode::where('in_usage', 1)->where('used', 0)->count();
        $ar['keycodes_available'] = MappKeycode::where('in_usage', 0)
        										->where('used', 0)
        										->where('given', 0)
        										->count();

    	return view("manage.mapp_keycode.keycode_list", $ar);
    }

    public function giveKeycodes (Request $r){

    	$amount = $r->input('amount');
        if($amount <= 0){
            return back()->with('status', 'Введите число кейкодов для выдачи');
        }
        if(!is_numeric($amount)){
            return back()->with('status', 'Введите число кейкодов для выдачи');
        }

    	MappKeycode::where('usage_started', '<', Carbon::now(new DateTimeZone('Asia/Almaty'))->subMinutes(25)->toDateTimeString())->where('used', 0)->update(["in_usage"=>0, "in_usage_by_user_id"=>NULL, "usage_started"=>NULL]);

    	$keycodes_to_give = MappKeycode::where('in_usage', 0)->where('used', 0)->where('given', 0)->limit($amount)->get();

    	if(count($keycodes_to_give) < $amount){
    		return back()->with('status', 'Недостаточно кодов для выдачи. Купите дополнительные коды.');
    	}else{
    		$now_time = Carbon::now(new DateTimeZone('Asia/Almaty'));
    		$fp = fopen(public_path().'/keycodes_'.$now_time.'.txt', 'w');
    		foreach ($keycodes_to_give as $key => $value) {
    			fwrite($fp, $value->keycode."\r\n");
    			MappKeycode::where('id', $value->id)->update(['given' => 1]);
    		}
			fclose($fp);
			return Response::download(public_path().'/keycodes_'.$now_time.'.txt', "keycodes_".$now_time.".txt")->deleteFileAfterSend(true);
    	}
    }

    public function loadKeycodes (){
    	
    	//ПОЛУЧАЕМ СТРАНИЦУ ВХОДА Админа, БЕРЕМ ОТТУДА crsf_field token и куки, чтобы отправить
    	//пост запрос на авторизацию в системе
  		$url = 'https://recruit.assessment.com/Account/LogIn';

     	$ch = curl_init();
     	curl_setopt($ch, CURLOPT_URL, $url);
     	curl_setopt($ch, CURLOPT_POST, 0);
     	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
     	curl_setopt($ch, CURLOPT_COOKIEJAR, public_path().'/auth/cok.txt');

     	$response = curl_exec($ch);
     	$err = curl_error($ch); 
     	curl_close($ch);
     	//print(htmlspecialchars($response));
     	preg_match('<input name="__RequestVerificationToken" type="hidden" value="(.*?)" /> ', $response, $out);
     	$request_token = $out[1];
     	print($err);
     	//ОТПРАВЛЯЕМ ПОСТ ЗАПРОС НА АВТОРИЗАЦИЮ В СИСТЕМЕ
     	$url = 'https://recruit.assessment.com/Account/Login';

     	$ch = curl_init();
     	curl_setopt($ch, CURLOPT_URL, $url);
     	curl_setopt($ch, CURLOPT_POST, 1);
     	curl_setopt($ch, CURLOPT_POSTFIELDS,
    					 "UserName=Alettin&Password=Irmak&__RequestVerificationToken=".$request_token);
     	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
     	curl_setopt ($ch, CURLOPT_COOKIEFILE, public_path().'/auth/cok.txt');
     	curl_setopt($ch, CURLOPT_COOKIEJAR, public_path().'/auth/cok.txt');

     	$response = curl_exec($ch);
     	$err = curl_error($ch);
     	curl_close($ch);

     	$ch = curl_init();
     	$url = 'https://recruit.assessment.com/Keycodes';

     	$ch = curl_init();
     	curl_setopt($ch, CURLOPT_URL, $url);
     	curl_setopt($ch, CURLOPT_POST, 0);
     	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
     	curl_setopt ($ch, CURLOPT_COOKIEFILE, public_path().'/auth/cok.txt');
     	curl_setopt($ch, CURLOPT_COOKIEJAR, public_path().'/auth/cok.txt');

     	$response = curl_exec($ch);
     	$err = curl_error($ch); 
     	curl_close($ch);
		file_put_contents(public_path().'/auth/keycodes.txt', $response);

		$response2 = file_get_contents(public_path().'/auth/keycodes.txt');
		preg_match_all("/<td>[\t\n\r\s]+(\d{1,7}-\d{1,7}-\d{1,7})[\t\n\r\s]+<\/td>[\t\n\r\s]+<td>[\t\n\r\s]+\d{1,3}\/\d{1,3}\/\d{1,5}[\t\n\r\s]+<\/td>[\t\n\r\s]+<td>[\t\n\r\s]+\d{1,3}\/\d{1,3}\/\d{1,5}[\t\n\r\s]+<\/td>[\t\n\r\s]+<td>[\t\n\r\s]+<\/td>[\t\n\r\s]+<td>[\t\n\r\s]+<\/td>/", $response2, $free_keycodes);

		$total = 0;
		
		foreach ($free_keycodes[1] as $key => $value) {
			if(MappKeycode::where('keycode', $value)->exists()){

			}else{
				$mapp_keycode = MappKeycode::insert(["keycode" => $value]);
				$total += 1;
			}
		}

		unlink(public_path().'/auth/cok.txt');
		unlink(public_path().'/auth/keycodes.txt');

		return "Добавлено новых keycodes: ".$total;
    }

    public function get_user_email ($user_id){

    	$user_data = User::where('id', $user_id)->get();
    	if($user_data->count() > 0){
    		return $user_data[0]->email;
    	}else{
    		return "-";
    	}
    }

    public function get_user_name ($user_id){

        $user_data = User::where('id', $user_id)->get();
        if($user_data->count() > 0){
            return $user_data[0]->name;
        }else{
            return "-";
        }
    }

    public function get_user_surname ($user_id){

        $user_data = User::where('id', $user_id)->get();
        if($user_data->count() > 0){
            return $user_data[0]->surname;
        }else{
            return "-";
        }
    }

    public function is_mapp_passed ($user_id){

        $data = User::where('id', $user_id)->get();
        if($data->count() > 0){
            if($data[0]['mapp_finished_date']){
                return 1;
            }else{
                return 0;
            }
        }else{
            return 0;
        }
    }

    public function allKeycodesAjax (){

    	$keycodes = MappKeycode::select('keycode', 'used', 'used_datetime', 'given', 'used_by_user_id', 'id')->get()->toArray();

    	$i=0;
    	foreach ($keycodes as $value) {
    		$keycodes[$i]['user_email'] = $this->get_user_email($keycodes[$i]['used_by_user_id']);
            $keycodes[$i]['user_name'] = $this->get_user_name($keycodes[$i]['used_by_user_id']);
            $keycodes[$i]['user_surname'] = $this->get_user_surname($keycodes[$i]['used_by_user_id']);
            $keycodes[$i]['mapp_passed'] = $this->is_mapp_passed($keycodes[$i]['used_by_user_id']);
    		$i++;
    	}

        $output = array(
            'data' => $keycodes
        );
        return response()->json($output);
    }
}
