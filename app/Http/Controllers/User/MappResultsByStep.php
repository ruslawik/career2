<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\MappAuto;
use Illuminate\Http\Response;
use App\MappJob;
use App\MappJobDetails;
use App\MappTranses;
use App\MappHtmlRes;
use App\MappHtmlResRu;
use App\MappResPercent;
use App\User;
use Auth;

class MappResultsByStep extends Controller
{

    function select_color($dannye_perc){
        if($dannye_perc>=90 && $dannye_perc<=100){
                $dannye_color = "#4DD535";
            }elseif ($dannye_perc>=60 && $dannye_perc<90) {
                $dannye_color = "#3F9AE0";
            }elseif ($dannye_perc>=40 && $dannye_perc<60) {
                $dannye_color = "#F8DF06";
            }elseif ($dannye_perc>=0 && $dannye_perc<40) {
                $dannye_color = "#FF1D1D";
            }
           return $dannye_color;
    }

    function extract_table ($name1, $name2, $sub){

        preg_match('/(<table class="table table-hover table-bordered table-responsive">[\t\n\r\s]+<thead>[\t\n\r\s]+<tr>[\t\n\r\s]+<td width="80%"><h3>'.$name1.'<\/h3><\/td>[\t\n\r\s\w\W\S\d\D]+<\/table>)[\t\n\r\s\w\W\S\d\D]+<h3>'.$name2.'/', $sub, $out);

        return $out[1] ?? '';
    }

    function trans_it($text){
        $lang = app()->getLocale();

        //Выполняем перевод
            $all_details = MappJobDetails::all();
            $all_mapp_transes = MappTranses::all();

            foreach ($all_details as $detail) {
                $det1 = $detail->detail_name;
                if($lang == "ru"){
                    $det2 = $detail->detail_name_rus;
                }
                if($lang == "kz"){
                    $det2 = $detail->detail_name_kaz;
                }

                $text = preg_replace("~".$det1."~", $det2, $text);
            }

            foreach ($all_mapp_transes as $trans) {
                $trans1 = $trans->eng;
                if($lang == "ru"){
                    $trans2 = $trans->rus;
                }
                if($lang == "kz"){
                    $trans2 = $trans->kaz;
                }
                $trans1 = preg_replace("/\//", "\/", $trans1);
                $trans1 = preg_replace("/\(/", "\(", $trans1);
                $trans1 = preg_replace("/\)/", "\)", $trans1);
                $trans1 = preg_replace("/'/", "\&\#39\;", $trans1);

                if($trans->not_in_kz == 1){
                    $text = preg_replace('/<tr>[\t\n\r\s]+<td>'.$trans1.' <\/td>[\t\n\r\s]+<td>\d{1,3}<\/td>[\t\n\r\s]+<td>\d<\/td>[\t\n\r\s]+<\/tr>/', "", $text);
                }

                $text = preg_replace('~<h3>'.$trans1.'</h3>~', "<h3>".$trans2."</h3>", $text);
                $text = preg_replace('~<h3>'.$trans1.' </h3>~', "<h3>".$trans2."</h3>", $text);

                $text = preg_replace("~<td>".$trans1."[\t\n\r\s]+</td>~", "<td>".$trans2."</td>", $text);
                $text = preg_replace("~<td>".$trans1."</td>~", "<td>".$trans2."</td>", $text);
                $text = preg_replace('~<h3>'.$trans1.'</h3>~', "<h3>".$trans2."</h3>", $text);
                $text = preg_replace('~<h3>'.$trans1.'[\t\n\r\s]+</h3>~', "<h3>".$trans2."</h3>", $text);

            }
            return $text;
    }

    function step1 ($date){

        $lang = app()->getLocale();

        $ar = array();
        $ar['title'] = trans('all.mapp_results_title');
        $ar['head_title'] = trans('all.mapp_results_title');
        $ar['username'] = Auth::user()->name." ".Auth::user()->surname;
        $ar['date'] = $date;

        $user_id = Auth::user()->id;

        $user_data = User::where('id', $user_id)->get();
        $mapp_login = $user_data['0']->mapp_login;
        $mapp_pass = $user_data['0']->mapp_pass;
        $name = $user_data['0']->name;
        $surname = $user_data['0']->surname;
        download:
        $result_in_base = MappHtmlRes::where('user_id', $user_id)->get();

        if($result_in_base->count() > 0){

            $response = $result_in_base[0]->html;

            if (strpos($response, 'if you have a Key Code') !== false) {
                unlink(public_path().'/mapp_cookies/'.$user_id);
                MappHtmlRes::where('user_id', $user_id)->delete();
                MappHtmlResRu::where('user_id', $user_id)->delete();
                goto download;
            }

            //Выбираем из страницы таблицу с результатами
            preg_match_all('/(<table class="table table-striped table-bordered table-responsive">[\t\n\r\s]+<thead>[\t\n\r\s]+<tr>[\t\n\r\s]+<td width="90%"><h3>INTEREST IN JOB CONTENT <\/h3><\/td>[\t\n\r\s\w\W\S\d\D]+)<div class="row">[\t\n\r\s]+<div class="col-md-12">[\t\n\r\s]+<h2>VOCATIONAL ANALYSIS<\/h2>[\t\n\r\s\w\W\S\d\D]+<a name="VOC_0"><\/a>([\t\n\r\s\w\W\S\d\D]+)<a name="TOP_VOC_AREAS"><\/a>/', $response, $out);
            //Выбираем скрипты, чтобы вставить потом на странице
            preg_match('/<script>[\t\n\r\s]+\$\(document\)[\t\n\r\s\w\W\S\d\D]+var graphName = "\#aBIN";[\t\n\r\s\w\W\S\d\D]+<!-- Google Code for Remarketing Tag -->/', $response, $scripts);
            $ar['scripts'] = $scripts[0];


            $out[1][0] = preg_replace('/<a name="WTC_2"><\/a>[\t\n\r\s\w\W\S\d\D]+<a name="WTC_3"><\/a>/', "", $out[1][0]);
            $out[1][0] = preg_replace('/<a name="WTC_6"><\/a>[\t\n\r\s\w\W\S\d\D]+<a name="WTC_7"><\/a>/', "", $out[1][0]);
            $out[2][0] = preg_replace('/<a name="VOC_18"><\/a>[\t\n\r\s\w\W\S\d\D]+<a name="VOC_19"><\/a>/', "", $out[2][0]);

            //Выдираем ИНТЕРЕС К СОДЕРЖАНИЮ РАБОТЫ
            preg_match('/(<table class="table table-striped table-bordered table-responsive">[\t\n\r\s]+<thead>[\t\n\r\s]+<tr>[\t\n\r\s]+<td width="90%"><h3>INTEREST IN JOB CONTENT <\/h3><\/td>[\t\n\r\s\w\W\S\d\D]+)<\/div>[\t\n\r\s\w\W\S\d\D]+<\/div>[\t\n\r\s\w\W\S\d\D]+<a name="WTC_1"><\/a>/', $out[1][0], $interes_k_rabote);
            $ar['interes_k_rabote'] = $this->trans_it($interes_k_rabote[1]);
            preg_match_all('/<td>(\d)<\/td>/', $interes_k_rabote[1], $levels);
            $interes_sum = 0;
            $k = 0;
            foreach ($levels[1] as $value) {
                $interes_sum += $value;
                $k++;
            }
            $ar['interes_average'] = round($interes_sum/$k, 2);

            $ar['interes_perc'] = ((6-$ar['interes_average'])/5)*100;

            $ar['interes_color']=$this->select_color($ar['interes_perc']);

            //Выдираем РАБОЧАЯ СРЕДА
            preg_match('/(<table class="table table-striped table-bordered table-responsive">[\t\n\r\s]+<thead>[\t\n\r\s]+<tr>[\t\n\r\s]+<td width="90%"><h3>TEMPERAMENT FOR THE JOB <\/h3><\/td>[\t\n\r\s\w\W\S\d\D]+<\/table>)[\t\n\r\s\w\W\S\d\D]+<td width="90%"><h3>PEOPLE <\/h3><\/td>/', $out[1][0], $rabo4aya_sreda);
            $ar['rabo4aya_sreda'] = $this->trans_it($rabo4aya_sreda[1]);
            preg_match_all('/<td>(\d)<\/td>/', $rabo4aya_sreda[1], $levels);
            $rabo4aya_sreda_sum = 0;
            $k = 0;
            foreach ($levels[1] as $value) {
                $rabo4aya_sreda_sum += $value;
                $k++;
            }
            $ar['rabo4aya_sreda_average'] = round($rabo4aya_sreda_sum/$k, 2);
            $ar['rabo4aya_sreda_perc'] = ((6-$ar['rabo4aya_sreda_average'])/5)*100;
            $ar['rabo4aya_sreda_color']=$this->select_color($ar['rabo4aya_sreda_perc']);

            //Выдираем СОЦИАЛЬНЫЕ РОЛИ, РОЛЬ В КОМАНДЕ
            preg_match('/(<table class="table table-striped table-bordered table-responsive">[\t\n\r\s]+<thead>[\t\n\r\s]+<tr>[\t\n\r\s]+<td width="90%"><h3>PEOPLE <\/h3><\/td>[\t\n\r\s\w\W\S\d\D]+<\/table>)[\t\n\r\s\w\W\S\d\D]+<td width="90%"><h3>THINGS <\/h3><\/td>/', $out[1][0], $social_roli);
            $ar['social_roli'] = $this->trans_it($social_roli[1]);
            preg_match_all('/<td>(\d)<\/td>/', $social_roli[1], $levels);
            $social_roli_sum = 0;
            $k = 0;
            foreach ($levels[1] as $value) {
                $social_roli_sum += $value;
                $k++;
            }
            $ar['social_roli_average'] = round($social_roli_sum/$k, 2);
            $ar['social_roli_perc'] = ((6-$ar['social_roli_average'])/5)*100;
            $ar['social_roli_color']=$this->select_color($ar['social_roli_perc']);

            //Выдираем МАТЕРИАЛЫ
            preg_match('/(<table class="table table-striped table-bordered table-responsive">[\t\n\r\s]+<thead>[\t\n\r\s]+<tr>[\t\n\r\s]+<td width="90%"><h3>THINGS <\/h3><\/td>[\t\n\r\s\w\W\S\d\D]+<\/table>)[\t\n\r\s\w\W\S\d\D]+<td width="90%"><h3>DATA<\/h3><\/td>/', $out[1][0], $materialy);
            $ar['materialy'] = $this->trans_it($materialy[1]);
            preg_match_all('/<td>(\d)<\/td>/', $materialy[1], $levels);
            $materialy_sum = 0;
            $k = 0;
            foreach ($levels[1] as $value) {
                $materialy_sum += $value;
                $k++;
            }
            $ar['materialy_average'] = round($materialy_sum/$k, 2);
            $ar['materialy_perc'] = ((6-$ar['materialy_average'])/5)*100;
            $ar['materialy_color']=$this->select_color($ar['materialy_perc']);

            //Выдираем ДАННЫЕ
            preg_match('/(<table class="table table-striped table-bordered table-responsive">[\t\n\r\s]+<thead>[\t\n\r\s]+<tr>[\t\n\r\s]+<td width="90%"><h3>DATA<\/h3><\/td>[\t\n\r\s\w\W\S\d\D]+<\/table>)[\t\n\r\s\w\W\S\d\D]+<td width="90%"><h3>MATHEMATICAL CAPACITY <\/h3><\/td>/', $out[1][0], $dannye);
            $ar['dannye'] = $this->trans_it($dannye[1]);
            preg_match_all('/<td>(\d)<\/td>/', $dannye[1], $levels);
            $dannye_sum = 0;
            $k = 0;
            foreach ($levels[1] as $value) {
                $dannye_sum += $value;
                $k++;
            }
            $ar['dannye_average'] = round($dannye_sum/$k, 2);
            $ar['dannye_perc'] = ((6-$ar['dannye_average'])/5)*100;
            $ar['dannye_color']=$this->select_color($ar['dannye_perc']);

            //Выдираем MATЕМАТИЧЕСКИЙ ПОТЕНЦИАЛ
            preg_match('/(<table class="table table-striped table-bordered table-responsive">[\t\n\r\s]+<thead>[\t\n\r\s]+<tr>[\t\n\r\s]+<td width="90%"><h3>MATHEMATICAL CAPACITY <\/h3><\/td>[\t\n\r\s\w\W\S\d\D]+<\/table>)[\t\n\r\s\w\W\S\d\D]+<td width="90%"><h3>LANGUAGE CAPACITY<\/h3><\/td>/', $out[1][0], $mat_pot);
            $ar['mat_pot'] = $this->trans_it($mat_pot[1]);
            preg_match_all('/<td>(\d)<\/td>/', $mat_pot[1], $levels);
            $mat_pot_sum = 0;
            $k = 0;
            foreach ($levels[1] as $value) {
                $mat_pot_sum += $value;
                $k++;
            }
            $ar['mat_pot_average'] = round($mat_pot_sum/$k, 2);
            $ar['mat_pot_perc'] = ((6-$ar['mat_pot_average'])/5)*100;
            $ar['mat_pot_color']=$this->select_color($ar['mat_pot_perc']);

            //Выдираем ЯЗЫКОВОЙ ПОТЕНЦИАЛ
            preg_match('/(<table class="table table-striped table-bordered table-responsive">[\t\n\r\s]+<thead>[\t\n\r\s]+<tr>[\t\n\r\s]+<td width="90%"><h3>LANGUAGE CAPACITY<\/h3><\/td>[\t\n\r\s\w\W\S\d\D]+<\/table>)/', $out[1][0], $yaz_pot);
            $ar['yaz_pot'] = $this->trans_it($yaz_pot[1]);
            preg_match_all('/<td>(\d)<\/td>/', $yaz_pot[1], $levels);
            $yaz_pot_sum = 0;
            $k = 0;
            foreach ($levels[1] as $value) {
                $yaz_pot_sum += $value;
                $k++;
            }
            $ar['yaz_pot_average'] = round($yaz_pot_sum/$k, 2);
            $ar['yaz_pot_perc'] = ((6-$ar['yaz_pot_average'])/5)*100;
            $ar['yaz_pot_color']=$this->select_color($ar['yaz_pot_perc']);

        }else{
            if(file_exists( public_path().'/mapp_cookies/'.$user_id)){

                $url = 'http://recruit.assessment.com/CareerCenter/YourMAPPResults';

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_COOKIEFILE, public_path().'/mapp_cookies/'.$user_id);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                $response = curl_exec($ch);
                $err = curl_error($ch);
                curl_close($ch);

                MappHtmlRes::insert(["html" => $response, "user_id" => $user_id]);
                print("<script>location.reload();</script>");
            }else{
                $mapp = new MappAuto();
                $mapp->mapp_auth($mapp_login, $mapp_pass, $user_id);
                goto download;
            }
        }
        //print("<pre>");
        //print($ar['interes_perc']);
        return view('user.mapp.results_step1', $ar);
    }


    public function step2 ($date){

        $lang = app()->getLocale();

        $ar = array();
        $ar['title'] = trans('all.mapp_results_title');
        $ar['head_title'] = trans('all.mapp_results_title');
        $ar['username'] = Auth::user()->name." ".Auth::user()->surname;
        $ar['date'] = $date;

        $user_id = Auth::user()->id;

        $user_data = User::where('id', $user_id)->get();
        $mapp_login = $user_data['0']->mapp_login;
        $mapp_pass = $user_data['0']->mapp_pass;
        $name = $user_data['0']->name;
        $surname = $user_data['0']->surname;
        $result_in_base = MappHtmlRes::where('user_id', $user_id)->get();

        $response = $result_in_base[0]->html;

            //Выбираем из страницы таблицу с результатами
            preg_match_all('/(<table class="table table-striped table-bordered table-responsive">[\t\n\r\s]+<thead>[\t\n\r\s]+<tr>[\t\n\r\s]+<td width="90%"><h3>INTEREST IN JOB CONTENT <\/h3><\/td>[\t\n\r\s\w\W\S\d\D]+)<div class="row">[\t\n\r\s]+<div class="col-md-12">[\t\n\r\s]+<h2>VOCATIONAL ANALYSIS<\/h2>[\t\n\r\s\w\W\S\d\D]+<a name="VOC_0"><\/a>([\t\n\r\s\w\W\S\d\D]+)<a name="TOP_VOC_AREAS"><\/a>/', $response, $out);
            //Выбираем скрипты, чтобы вставить потом на странице
            preg_match('/<script>[\t\n\r\s]+\$\(document\)[\t\n\r\s\w\W\S\d\D]+var graphName = "\#aBIN";[\t\n\r\s\w\W\S\d\D]+<!-- Google Code for Remarketing Tag -->/', $response, $scripts);
            $ar['scripts'] = $scripts[0];

            //Выполняем перевод
            $all_details = MappJobDetails::all();
            $all_mapp_transes = MappTranses::all();

            foreach ($all_details as $detail) {
                $det1 = $detail->detail_name;
                if($lang == "ru"){
                    $det2 = $detail->detail_name_rus;
                }
                if($lang == "kz"){
                    $det2 = $detail->detail_name_kaz;
                }

                $out[1][0] = preg_replace("~".$det1."~", $det2, $out[1][0]);
            }

            foreach ($all_mapp_transes as $trans) {
                $trans1 = $trans->eng;
                if($lang == "ru"){
                    $trans2 = $trans->rus;
                }
                if($lang == "kz"){
                    $trans2 = $trans->kaz;
                }
                $trans1 = preg_replace("/\//", "\/", $trans1);
                $trans1 = preg_replace("/\(/", "\(", $trans1);
                $trans1 = preg_replace("/\)/", "\)", $trans1);
                $trans1 = preg_replace("/'/", "\&\#39\;", $trans1);

                if($trans->not_in_kz == 1){
                    $out[2][0] = preg_replace('/<tr>[\t\n\r\s]+<td>'.$trans1.' <\/td>[\t\n\r\s]+<td>\d{1,3}<\/td>[\t\n\r\s]+<td>\d<\/td>[\t\n\r\s]+<\/tr>/', "", $out[2][0]);
                }

                $out[1][0] = preg_replace('~<h3>'.$trans1.'</h3>~', "<h3>".$trans2."</h3>", $out[1][0]);
                $out[1][0] = preg_replace('~<h3>'.$trans1.' </h3>~', "<h3>".$trans2."</h3>", $out[1][0]);

                $out[2][0] = preg_replace("~<td>".$trans1."[\t\n\r\s]+</td>~", "<td>".$trans2."</td>", $out[2][0]);
                $out[2][0] = preg_replace("~<td>".$trans1."</td>~", "<td>".$trans2."</td>", $out[2][0]);
                $out[2][0] = preg_replace('~<h3>'.$trans1.'</h3>~', "<h3>".$trans2."</h3>", $out[2][0]);
                $out[2][0] = preg_replace('~<h3>'.$trans1.'[\t\n\r\s]+</h3>~', "<h3>".$trans2."</h3>", $out[2][0]);

            }

            $out[1][0] = preg_replace('/<a name="WTC_2"><\/a>[\t\n\r\s\w\W\S\d\D]+<a name="WTC_3"><\/a>/', "", $out[1][0]);
            $out[1][0] = preg_replace('/<a name="WTC_6"><\/a>[\t\n\r\s\w\W\S\d\D]+<a name="WTC_7"><\/a>/', "", $out[1][0]);
            $out[2][0] = preg_replace('/<a name="VOC_18"><\/a>[\t\n\r\s\w\W\S\d\D]+<a name="VOC_19"><\/a>/', "", $out[2][0]);

        $ar['osnova'] = $this->extract_table(trans('all.osnova'), trans('all.iskusstvo'), $out[2][0]);

        $ar['iskusstvo'] = $this->extract_table(trans('all.iskusstvo'), trans('all.business'), $out[2][0]);

        $ar['business'] = $this->extract_table(trans('all.business'), trans('all.ofis'), $out[2][0]);

        $ar['ofis'] = $this->extract_table(trans('all.ofis'), trans('all.psih'), $out[2][0]);

        $ar['psih'] = $this->extract_table(trans('all.psih'), trans('all.remesla'), $out[2][0]);

         $ar['remesla'] = $this->extract_table(trans('all.remesla'), trans('all.educ'), $out[2][0]);

        $ar['educ'] = $this->extract_table(trans('all.educ'), trans('all.elem'), $out[2][0]);

        $ar['elem'] = $this->extract_table(trans('all.elem'), trans('all.injener'), $out[2][0]);

        $ar['injener'] = $this->extract_table(trans('all.injener'), trans('all.showbiz'), $out[2][0]);

        $ar['showbiz'] = $this->extract_table(trans('all.showbiz'), trans('all.agro'), $out[2][0]);

        $ar['agro'] = $this->extract_table(trans('all.agro'), trans('all.rasled'), $out[2][0]);

        $ar['rasled'] = $this->extract_table(trans('all.rasled'), trans('all.jur'), $out[2][0]);

        $ar['jur'] = $this->extract_table(trans('all.jur'), trans('all.mawina'), $out[2][0]);

        $ar['mawina'] = $this->extract_table(trans('all.mawina'), trans('all.matem'), $out[2][0]);

        $ar['matem'] = $this->extract_table(trans('all.matem'), trans('all.medic'), $out[2][0]);

        $ar['medic'] = $this->extract_table(trans('all.medic'), trans('all.market'), $out[2][0]);

        $ar['market'] = $this->extract_table(trans('all.market'), trans('all.uslugi'), $out[2][0]);

        $ar['uslugi'] = $this->extract_table(trans('all.uslugi'), trans('all.writing'), $out[2][0]);

        preg_match('/(<table class="table table-hover table-bordered table-responsive">[\t\n\r\s]+<thead>[\t\n\r\s]+<tr>[\t\n\r\s]+<td width="80%"><h3>'.trans("all.writing").'<\/h3><\/td>[\t\n\r\s\w\W\S\d\D]+<\/table>)/', $out[2][0], $writing);
        $ar['writing'] = $writing[1];

        //print("<pre>");
        //print_r($ar['psih']);
        return view('user.mapp.results_step2', $ar);
    }

    public function step3($date){

        $lang = app()->getLocale();

        $ar = array();
        $ar['title'] = trans('all.mapp_results_title');
        $ar['head_title'] = trans('all.mapp_results_title');
        $ar['username'] = Auth::user()->name." ".Auth::user()->surname;
        $ar['date'] = $date;

        $user_id = Auth::user()->id;

        $user_data = User::where('id', $user_id)->get();
        $mapp_login = $user_data['0']->mapp_login;
        $mapp_pass = $user_data['0']->mapp_pass;
        $name = $user_data['0']->name;
        $surname = $user_data['0']->surname;
        $result_in_base = MappHtmlRes::where('user_id', $user_id)->get();
        $response = $result_in_base[0]->html;

                    //Выбираем из страницы таблицу с результатами
            preg_match_all('/(<table class="table table-striped table-bordered table-responsive">[\t\n\r\s]+<thead>[\t\n\r\s]+<tr>[\t\n\r\s]+<td width="90%"><h3>INTEREST IN JOB CONTENT <\/h3><\/td>[\t\n\r\s\w\W\S\d\D]+)<div class="row">[\t\n\r\s]+<div class="col-md-12">[\t\n\r\s]+<h2>VOCATIONAL ANALYSIS<\/h2>[\t\n\r\s\w\W\S\d\D]+<a name="VOC_0"><\/a>([\t\n\r\s\w\W\S\d\D]+)<a name="TOP_VOC_AREAS"><\/a>/', $response, $out);
            //Выбираем скрипты, чтобы вставить потом на странице
            preg_match('/<script>[\t\n\r\s]+\$\(document\)[\t\n\r\s\w\W\S\d\D]+var graphName = "\#aBIN";[\t\n\r\s\w\W\S\d\D]+<!-- Google Code for Remarketing Tag -->/', $response, $scripts);
            $ar['scripts'] = $scripts[0];

            //Выполняем перевод
            $all_details = MappJobDetails::all();
            $all_mapp_transes = MappTranses::all();

            foreach ($all_details as $detail) {
                $det1 = $detail->detail_name;
                if($lang == "ru"){
                    $det2 = $detail->detail_name_rus;
                }
                if($lang == "kz"){
                    $det2 = $detail->detail_name_kaz;
                }

                $out[1][0] = preg_replace("~".$det1."~", $det2, $out[1][0]);
            }

            foreach ($all_mapp_transes as $trans) {
                $trans1 = $trans->eng;
                if($lang == "ru"){
                    $trans2 = $trans->rus;
                }
                if($lang == "kz"){
                    $trans2 = $trans->kaz;
                }
                $trans1 = preg_replace("/\//", "\/", $trans1);
                $trans1 = preg_replace("/\(/", "\(", $trans1);
                $trans1 = preg_replace("/\)/", "\)", $trans1);
                $trans1 = preg_replace("/'/", "\&\#39\;", $trans1);

                if($trans->not_in_kz == 1){
                    $out[2][0] = preg_replace('/<tr>[\t\n\r\s]+<td>'.$trans1.' <\/td>[\t\n\r\s]+<td>\d{1,3}<\/td>[\t\n\r\s]+<td>\d<\/td>[\t\n\r\s]+<\/tr>/', "", $out[2][0]);
                }

                $out[1][0] = preg_replace('~<h3>'.$trans1.'</h3>~', "<h3>".$trans2."</h3>", $out[1][0]);
                $out[1][0] = preg_replace('~<h3>'.$trans1.' </h3>~', "<h3>".$trans2."</h3>", $out[1][0]);

                $out[2][0] = preg_replace("~<td>".$trans1."[\t\n\r\s]+</td>~", "<td>".$trans2."</td>", $out[2][0]);
                $out[2][0] = preg_replace("~<td>".$trans1."</td>~", "<td>".$trans2."</td>", $out[2][0]);
                $out[2][0] = preg_replace('~<h3>'.$trans1.'</h3>~', "<h3>".$trans2."</h3>", $out[2][0]);
                $out[2][0] = preg_replace('~<h3>'.$trans1.'[\t\n\r\s]+</h3>~', "<h3>".$trans2."</h3>", $out[2][0]);

            }

            $out[1][0] = preg_replace('/<a name="WTC_2"><\/a>[\t\n\r\s\w\W\S\d\D]+<a name="WTC_3"><\/a>/', "", $out[1][0]);
            $out[1][0] = preg_replace('/<a name="WTC_6"><\/a>[\t\n\r\s\w\W\S\d\D]+<a name="WTC_7"><\/a>/', "", $out[1][0]);
            $out[2][0] = preg_replace('/<a name="VOC_18"><\/a>[\t\n\r\s\w\W\S\d\D]+<a name="VOC_19"><\/a>/', "", $out[2][0]);

        preg_match_all('/<td>(.*?)<\/td>[\t\n\r\s]+<td>(\d{1,3})<\/td>[\t\n\r\s]+<td>(\d)<\/td>/', $out[2][0], $all_jobs);

        array_multisort($all_jobs[2], SORT_DESC, $all_jobs[1], $all_jobs[3]);

        $ar['all_jobs'] = $all_jobs;

        return  view('user.mapp.results_step3', $ar);
    }

    public function step4 ($date){

        $lang = "ru";

        $ar = array();
        $ar['title'] = trans('all.mapp_results_title');
        $ar['head_title'] = trans('all.mapp_results_title');
        $ar['username'] = Auth::user()->name." ".Auth::user()->surname;
        $ar['date'] = $date;

        $user_id = Auth::user()->id;

        $user_data = User::where('id', $user_id)->get();
        $mapp_login = $user_data['0']->mapp_login;
        $mapp_pass = $user_data['0']->mapp_pass;
        $name = $user_data['0']->name;
        $surname = $user_data['0']->surname;

        $result_in_base2 = MappResPercent::where("user_id", $user_id)
                                        ->where("type", "j")
                                        ->get();
        if($result_in_base2->count() > 0){
            $ar['jobs'] = $result_in_base2;
            $ar['not_loaded'] = 0;
            return  view('user.mapp.results_step4', $ar);
        }else{
            $ar['not_loaded'] = 1;
            return  view('user.mapp.results_step4', $ar);
        }
    }

    public function load_onenet (){

        $user_id = Auth::user()->id;

        get_job_auth:
        $result_in_base2 = MappResPercent::where("user_id", $user_id)
                                        ->where("type", "j")
                                        ->get();
        if($result_in_base2->count() > 0){
            return 0;
        }else{
            if(file_exists( public_path().'/mapp_cookies/'.$user_id)){

                $url = 'http://recruit.assessment.com/CareerCenter/MAPPMatching/SortedMAPPMatchToONET';

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_COOKIEFILE, public_path().'/mapp_cookies/'.$user_id);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                $response = curl_exec($ch);
                $err = curl_error($ch);
                curl_close($ch);

                preg_match_all('/<h3><a class="" href="\/CareerCenter\/MAPPMatching\/MAPPMatchToONET\/(.*?)">(.*?)<\/a><\/h3>[\t\n\r\s]+<p>(.*?)<\/p>/', $response, $out);
                preg_match_all('/<h3><a class="" href="\/CareerCenter\/MAPPMatching\/MAPPMatchToONET\/(.*?)">(.*?)%<\/a><\/h3>/', $response, $out2);

                $to_ins = array();

                for($i=0; $i<30; $i++){
                    //$to_ins[] = array("sys_id" => $out[1][$i], "job_name" => $out[2][$i], "job_desc" => $out[3][$i]);
                    $sys_id = $out[1][$i];

                    $job_data = MappJob::where("sys_id", $sys_id)->get();

                    $job_percent = $out2[2][$i];
                    $job_rel_id = $job_data[0]->id;

                    $to_ins[] = array(
                        "type" => "j",
                        "rel_id" => $job_rel_id,
                        "job_sys_id" => $sys_id,
                        "user_id" => $user_id,
                        "percent" => $job_percent
                    );
                    //$mapp = new MappAuto();
                    //$mapp->getJob($sys_id, $user_id);
                }
                MappResPercent::insert($to_ins);
            }else{
                $mapp = new MappAuto();
                $user_data = User::where('id', $user_id)->get();
                $mapp_login = $user_data[0]->mapp_login;
                $mapp_pass = $user_data[0]->mapp_pass;
                $mapp->mapp_auth($mapp_login, $mapp_pass, $user_id);
                goto get_job_auth;
            }
        }

    }

}
