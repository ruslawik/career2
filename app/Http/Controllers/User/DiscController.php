<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Questions_Disc;
use App\Questions_Disc_Kaz;
use App\Results_Disc;
use App\DiscPromocode;
use Carbon\Carbon;
use DateTimeZone;
use Response;
use App\User;
use App\Setting;
use App\DiscAdditionalImage;

class DiscController extends Controller
{
	//ADMIN METHODS

    public function uploadDiscAdditionalImg (Request $r){

        $disc_formula_id = $r->input('disc_formula_id');

        $fileName = "";

        if($r->file()) {
            $fileName = time().'_'.$r->img->getClientOriginalName();
            $filePath = $r->file('img')->storeAs('disc-additional-images', $fileName, 'public');
        }

        $discAdditionalImage = new DiscAdditionalImage();
        $discAdditionalImage->disc_formula_id = $disc_formula_id;
        $discAdditionalImage->image_url = $fileName;
        $discAdditionalImage->lang = $r->input('lang');

        $discAdditionalImage->save();

        return back();
    }

    public function deleteDiscAdditionalImg ($image_id){
        $img = DiscAdditionalImage::where('id', $image_id)->get();
        $image_name = $img[0]->image_url;
        DiscAdditionalImage::where('id', $image_id)->delete();
        unlink("storage/disc-additional-images/".$image_name);
        return back();
    }

	public function get_user_email ($user_id){

    	$user_data = User::where('id', $user_id)->get();
    	if($user_data->count() > 0){
    		return $user_data[0]->email;
    	}else{
    		return "-";
    	}
    }

    public function get_user_name ($user_id){

        $user_data = User::where('id', $user_id)->get();
        if($user_data->count() > 0){
            return $user_data[0]->name;
        }else{
            return "-";
        }
    }

    public function get_user_surname ($user_id){

        $user_data = User::where('id', $user_id)->get();
        if($user_data->count() > 0){
            return $user_data[0]->surname;
        }else{
            return "-";
        }
    }

	public function givePromocodes (Request $r){

		$amount = $r->input('amount');

		$now_time = Carbon::now(new DateTimeZone('Asia/Almaty'));
		$fp = fopen(public_path().'/disc_keycodes_'.$now_time.'.txt', 'w');
		for($i=0; $i<$amount; $i++){
			$code = str_random(8);
			$disc_promocode = new DiscPromocode();
			$disc_promocode->code = $code;
			$disc_promocode->given = 1;
			$disc_promocode->save();
			DiscPromocode::where('id', $disc_promocode->id)->update(["code" => $code.$disc_promocode->id]);
			fwrite($fp, $code.$disc_promocode->id."\r\n");
		}
		fclose($fp);
		return Response::download(public_path().'/disc_keycodes_'.$now_time.'.txt', "keycodes_".$now_time.".txt")->deleteFileAfterSend(true);
	}

	public function promocodesList (){

		$ar['title'] = "Управление промо-кодами DISC";
		$ar['given'] = DiscPromocode::where('given', 1)->count();
		$ar['used'] = DiscPromocode::where('given', 1)->where('used', 1)->count();

		return view("manage.disc.promocodes_list", $ar);
	}

	public function promocodesListAjax (){

		$keycodes = DiscPromocode::select('code', 'given', 'used', 'used_datetime', 'used_by_user_id')->get()->toArray();

    	$i=0;
    	foreach ($keycodes as $value) {
    		$keycodes[$i]['user_email'] = $this->get_user_email($keycodes[$i]['used_by_user_id']);
            $keycodes[$i]['user_name'] = $this->get_user_name($keycodes[$i]['used_by_user_id']);
            $keycodes[$i]['user_surname'] = $this->get_user_surname($keycodes[$i]['used_by_user_id']);
    		$i++;
    	}

        $output = array(
            'data' => $keycodes
        );
        return response()->json($output);
	}

	//USER METHODS

	public function openDisc (Request $r){

		User::where('id', $r->input('user_id'))->update(['disc' => 1]);
		return redirect('/user/tests/before-disc');
	}

	public function openDiscByPromocode (Request $r){

		$promocode = $r->input('promocode');

		$count_of_promocode = DiscPromocode::where('used', 0)->where('code', $promocode)->count();

		if($count_of_promocode <= 0){
			return back()->withErrors(['Ошибка! Промо-код введен неверно. Попробуйте еще раз.']);
		}else{
			DiscPromocode::where('code', $promocode)->update(["used"=>1, "used_by_user_id"=>Auth::user()->id, "used_datetime"=>Carbon::now(new DateTimeZone('Asia/Almaty'))]);
			User::where('id', Auth::user()->id)->update(['disc' => 1]);
			return redirect('/user/tests/before-disc');
		}
	}

    public function beforeDisc (){

    	$ar = array();
    	$ar['lang'] = app()->getLocale();
        $ar['title'] = trans('all.disc_title');
        $ar['head_title'] = trans('all.disc_title');
        $ar['username'] = Auth::user()->name." ".Auth::user()->surname;

        if(Auth::user()->disc >= 1){
            $ar['available'] = "1";
        }else{
            $ar['available'] = "0";
        }

        $disc_results = Results_Disc::where("user_id", Auth::user()->id)
                                    ->get()
                                    ->groupBy(function($pool) {
                                                return $pool->created_at->toDateTimeString();
                                             })
                                    ->toArray();
        if($disc_results != NULL){
            $ar['already_passed'] = count($disc_results);
        }else{
            $ar['already_passed'] = 0;
        }

        return view("user.disc.before_disc", $ar);
    }

    public function getBuyDisc (){

    	$ar = array();
    	$ar['lang'] = app()->getLocale();
        $ar['title'] = trans('all.disc_title');
        $ar['head_title'] = trans('all.disc_title');
        $ar['username'] = Auth::user()->name." ".Auth::user()->surname;

        $disc_results = Results_Disc::where("user_id", Auth::user()->id)
                                    ->get()
                                    ->groupBy(function($pool) {
                                                return $pool->created_at->toDateTimeString();
                                             })
                                    ->toArray();
        if($disc_results != NULL){
            $ar['already_passed'] = count($disc_results);
        }else{
            $ar['already_passed'] = 0;
        }

        $ar['disc_price'] = Setting::where('setting', 'disc_price')->get();

    	return view("user.disc.buy", $ar);
    }

    public function discPreview (){

    	$ar = array();
    	$ar['lang'] = app()->getLocale();
        $ar['title'] = trans('all.tests');
        $ar['head_title'] = trans('all.tests');
        $ar['username'] = Auth::user()->name." ".Auth::user()->surname;

        return view("user.disc.disc_preview", $ar);

    }

    public function passDisc (){

    	$ar = array();
    	$ar['lang'] = app()->getLocale();
        $ar['title'] = trans('all.disc_title');
        $ar['head_title'] = trans('all.disc_title');
        $ar['username'] = Auth::user()->name." ".Auth::user()->surname;
        $ar['action'] = action("User\UserController@saveResultDisc");
        if(app()->getLocale() == "ru"){
        	$ar['questions'] = Questions_Disc::all();
        }else{
        	$ar['questions'] = Questions_Disc_Kaz::all();
        }

        if(Auth::user()->disc >= 1){
            $ar['available'] = "1";
        }else{
            $ar['available'] = "0";
        }

        $disc_results = Results_Disc::where("user_id", Auth::user()->id)
                                    ->get()
                                    ->groupBy(function($pool) {
                                                return $pool->created_at->toDateTimeString();
                                             })
                                    ->toArray();
        if($disc_results != NULL){
            $ar['already_passed'] = count($disc_results);
        }else{
            $ar['already_passed'] = 0;
        }

        $group = 1;
        for($i = 1; $i <= 96; $i++){
            $group_ar[$i] = $group;
            if($i % 4 == 0){
                $group++;
            }
        }

        $ar['group_ar'] = $group_ar;

        return view("user.disc.disc_pass2", $ar);
    }
}
