<?php

namespace App\Http\Controllers;

use App\University;
use App\UniversityCategory;
use Illuminate\Http\Request;
use Auth;
use App\User;

class UniversityController extends Controller
{
	//USER METHODS

    public function university($university_id){

        $ar = array();

        $ar['title'] = "Вузы";
        $ar['head_title'] = "Вузы";
        $ar['username'] = Auth::user()->name." ".Auth::user()->surname;

        $university = University::where('id', $university_id)->get();
        $ar['university'] = $university[0];

        return view("user.universities.university", $ar);
    }

    public function search(Request $r){

    	$ar = array();

        $ar['title'] = "Вузы";
        $ar['head_title'] = "Вузы";
        $ar['username'] = Auth::user()->name." ".Auth::user()->surname;
        $ar['teklifler'] = UniversityCategory::where('on_promo', 1)->get();
        $ar['categories'] = UniversityCategory::all();
        $ar['universities'] = University::paginate(6);
        $ar['selected_cat'] = $r->input('category');
        $ar['q'] = "";

        if($r->has('category') && $r->input('category')!=-1){
            $ar['universities'] = University::where('category_id', $r->input('category'))
                                                ->paginate(6);
        }

        if($r->has('q')){
            $word = mb_strtolower($r->input('q'));
            $ar['universities'] = University::whereRaw('LOWER(name->"$.ru") like ?', '%'.$word.'%')
                                        ->orWhereRaw('LOWER(name->"$.kz") like ?', '%'.$word.'%')
                                        ->orWhereRaw('LOWER(description->"$.kz") like ?', '%'.$word.'%')
                                        ->orWhereRaw('LOWER(description->"$.ru") like ?', '%'.$word.'%')
                                        ->paginate(6);
            $ar['q'] = $r->input('q');
        }

        if($r->has('q') && $r->has('category')){
            $q = $r->input('q');
            $cat = $r->input('category');
            if($cat==-1){
                return redirect('user/university/search?q='.$q);
            }
            $ar['universities'] = University::where(function ($query){
                                    global $q;
                                    $query->where('name->ru', 'LIKE', "%".$q."%")
                                        ->orWhere('name->kz', 'LIKE', "%".$q."%")
                                        ->orWhere('description->ru', 'LIKE', "%".$q."%")
                                        ->orWhere('description->kz', 'LIKE', "%".$q."%");
                                    })
                                    ->where('category_id', $cat)
                                    ->paginate(6);
            $ar['q'] = $r->input('q');
        }

    	return view('user.universities.search', $ar);
    }



    //ADMIN METHODS

    public function university_list (){

    	$ar['title'] = "Все Вузы";

    	return view('manage.universities.university_list', $ar);
    }

    public function categories_list (){

    	$ar['title'] = "Все категории вузов";

    	$ar['categories'] = UniversityCategory::all();

    	return view('manage.universities.university_categories', $ar);
    }

    public function addUniversity (){

    	$ar['title'] = "Добавить Вуз";
    	$ar['categories'] = UniversityCategory::all();

    	return view('manage.universities.add_university', $ar);
    }

    public function postAddUniversity (Request $r){

        $r->validate([
            'name_ru' => 'required',
            'name_kz' => 'required',
            'description_ru' => 'required',
            'description_kz' => 'required',
        ]);
        if($r->file()) {
            $fileName = time().'_'.$r->img->getClientOriginalName();
            $filePath = $r->file('img')->storeAs('universities-images', $fileName, 'public');
        }

    	$university = new University();
        $university->img = $fileName;
    	$university->socials = $r->input('socials');
    	$university->category_id = $r->input('category_id');
    	$university->setTranslation('name', 'ru', $r->input('name_ru'))
    				->setTranslation('name', 'kz', $r->input('name_kz'))
    				->setTranslation('description', 'ru', $r->input('description_ru'))
    				->setTranslation('description', 'kz', $r->input('description_kz'))
    				->save();

    	return redirect('/manage/universities')->with('status', 'Новая Вуз успешно добавлена!');
    }

    public function editUniversity($university_id){

    	$ar['title'] = "Редактировать Вуз";

    	$ar['university'] = University::where('id', $university_id)->get();
    	$ar['categories'] = UniversityCategory::all();

    	return view("manage.universities.edit_university", $ar);

    }

    public function postEditUniversity (Request $r){

    	University::where('id', $r->input('university_id'))
    				->update(["socials" => $r->input('socials'),
    						  "category_id" => $r->input('category_id')]);

        if($r->file()) {
            $fileName = time().'_'.$r->img->getClientOriginalName();
            $filePath = $r->file('img')->storeAs('universities-images', $fileName, 'public');
            $now_file = University::where('id', $r->input('university_id'))->get();
            if(strlen($now_file[0]->img) > 0){
                $this->deleteFile("storage/universities-images/".$now_file[0]->img);
            }
            University::where('id', $r->input('university_id'))
                ->update(["img" => $fileName]);
        }


    	$university = University::where('id', $r->input('university_id'))->get();
    	$university[0]->setTranslation('name', 'ru', $r->input('name_ru'))
    				->setTranslation('name', 'kz', $r->input('name_kz'))
    				->setTranslation('description', 'ru', $r->input('description_ru'))
    				->setTranslation('description', 'kz', $r->input('description_kz'))
    				->save();

    	return back()->with('status', 'Информация успешно обновлена!');
    }

    public function addUniversityCat (Request $r){

    	$university = new UniversityCategory();
    	$university->setTranslation('name', 'ru', $r->input('name_ru'))
    				->setTranslation('name', 'kz', $r->input('name_kz'));
    	$university->on_promo = $r->input('on_promo');
    	$university->save();

    	return back()->with('status', 'Успешно добавлено!');
    }

    public function editUniversityCat ($cat_id){

    	$ar['title'] = "Редактировать категорию";

    	$ar['cat'] = UniversityCategory::where('id', $cat_id)->get();

    	return view('manage.universities.edit_university_category', $ar);
    }

    public function postEditUniversityCat (Request $r){

    	UniversityCategory::where('id', $r->input('cat_id'))->update(["on_promo"=>$r->input('on_promo')]);

    	$university_cat = UniversityCategory::where('id', $r->input('cat_id'))->get();
    	$university_cat[0]->setTranslation('name', 'ru', $r->input('name_ru'))
    				->setTranslation('name', 'kz', $r->input('name_kz'))
    				->save();

    	return redirect('/manage/university-cats')->with('status', 'Успешно обновлено!');
    }

    public function allUniversitiesAjax (){

    	$universities = University::select('category_id', 'name', 'description', 'socials', 'id')->with('category')
                        ->get()
                        ->toArray();

        $output = array(
            'data' => $universities
        );
        return response()->json($output);
    }

    public function deleteImg ($university_id){

        $now_file = University::where('id', $university_id)->get();
        $this->deleteFile("storage/universities-images/".$now_file[0]->img);

        University::where('id', $university_id)->update(['img' => null]);

        return back()->with('status', 'Картинка успешно удалена!');
    }

    public function deleteFile($fullPath){
        if(file_exists($fullPath)){
            unlink($fullPath);
        }
    }

    public function deleteUniversity ($university_id){

        University::where('id', $university_id)->delete();

        return back()->with('status', 'Вуз успешно удалена!');
    }
}
