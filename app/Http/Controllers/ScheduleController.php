<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Schedule;
use Auth;

class ScheduleController extends Controller
{
 	//USER METHODS
	public function index (){

		$ar = array();

        $ar['title'] = "Расписание курсов";
        $ar['head_title'] = "Расписание курсов";
        $ar['username'] = Auth::user()->name." ".Auth::user()->surname;
        $ar['posts'] = Schedule::paginate(6);

		return view('user.schedule.index', $ar);
	}

	public function schedule_single ($schedule_id){

		$ar = array();

		$ar['post'] = Schedule::where('id', $schedule_id)->get();

        $ar['title'] = $ar['post'][0]->name;
        $ar['head_title'] = $ar['post'][0]->name;
        $ar['username'] = Auth::user()->name." ".Auth::user()->surname;

		return view('user.schedule.single', $ar);
	}


	//ADMIN METHODS

	public function schedule_list(){

		$ar['title'] = "Все расписание курсов";

    	return view('manage.schedule.schedule_list', $ar);
	}

	public function allScheduleAjax (){

    	$schedule = Schedule::select('name', 'text', 'created_at', 'id')->get()->toArray();

    	$i=0;
    	foreach ($schedule as $value) {
    		$schedule[$i]['text'] = mb_substr($schedule[$i]['text'], 0, 100)."...";
    		$i++;
    	}

        $output = array(
            'data' => $schedule
        );
        return response()->json($output);
    }

    public function addSchedule (){

    	$ar['title'] = "Добавить расписание курсов";

    	return view('manage.schedule.add_schedule', $ar);
    }

    public function addPostSchedule(Request $r){

    	$r->validate([
            'name_ru' => 'required',
            'name_kz' => 'required',
            'text_ru' => 'required',
            'text_kz' => 'required',
        ]);

        $fileName = "";

        if($r->file()) {
            $fileName = time().'_'.$r->img->getClientOriginalName();
            $filePath = $r->file('img')->storeAs('schedule-images', $fileName, 'public');
        }

    	$schedule = new Schedule();
        $schedule->img = $fileName;
    	$schedule->setTranslation('name', 'ru', $r->input('name_ru'))
    				->setTranslation('name', 'kz', $r->input('name_kz'))
    				->setTranslation('text', 'ru', $r->input('text_ru'))
    				->setTranslation('text', 'kz', $r->input('text_kz'))
    				->save();
    	return redirect('/manage/schedule')->with('status', 'Расписание курсов успешно добавлено!');
    }

    public function uploadImage (Request $request){
        if($request->hasFile('upload')) {
            $originName = $request->file('upload')->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_FILENAME);
            $extension = $request->file('upload')->getClientOriginalExtension();
            $fileName = $fileName.'_'.time().'.'.$extension;

            $request->file('upload')->move(public_path('schedule/images'), $fileName);

            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
            $url = asset('schedule/images/'.$fileName);
            $msg = 'Image uploaded successfully';
            $response = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";

            @header('Content-type: text/html; charset=utf-8');
            echo $response;
        }
    }

    public function editSchedule ($schedule_id){

    	$ar['title'] = "Редактировать блог";

    	$ar['schedule'] = Schedule::where('id', $schedule_id)->get();

    	return view("manage.schedule.edit_schedule", $ar);

    }

    public function editPostSchedule(Request $r){

    	$r->validate([
            'name_ru' => 'required',
            'name_kz' => 'required',
            'text_ru' => 'required',
            'text_kz' => 'required',
        ]);

        if($r->file()) {
            $fileName = time().'_'.$r->img->getClientOriginalName();
            $filePath = $r->file('img')->storeAs('schedule-images', $fileName, 'public');
            $now_file = Schedule::where('id', $r->input('schedule_id'))->get();
            if(strlen($now_file[0]->img) > 0){
                unlink("storage/schedule-images/".$now_file[0]->img);
            }
            Schedule::where('id', $r->input('schedule_id'))
                    ->update(["img" => $fileName]);
        }

    	$schedule = Schedule::where('id', $r->input('schedule_id'))->get();

    	$schedule[0]->setTranslation('name', 'ru', $r->input('name_ru'))
    				->setTranslation('name', 'kz', $r->input('name_kz'))
    				->setTranslation('text', 'ru', $r->input('text_ru'))
    				->setTranslation('text', 'kz', $r->input('text_kz'))
    				->save();
    	return back()->with('status', 'Расписание курсов успешно обновлено!');
    }

    public function deleteSchedule ($schedule_id){

        $now_file = Schedule::where('id', $schedule_id)->get();
        if($now_file[0]->img != NULL){
            unlink("storage/schedule-images/".$now_file[0]->img);
        }

    	Schedule::where('id', $schedule_id)->delete();
    	return redirect('/manage/schedule')->with('status', 'Расписание успешно удалено!');
    }

    public function deleteImg ($schedule_id){

        $now_file = Schedule::where('id', $schedule_id)->get();
        unlink("storage/schedule-images/".$now_file[0]->img);

        Schedule::where('id', $schedule_id)->update(['img' => null]);

        return back()->with('status', 'Картинка успешно удалена!');
    }
}
