<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Questions_Tomas;
use Carbon\Carbon;
use DateTimeZone;
use App\Results_Tomas;
use Auth;
use App\User;
use Validator;

class TomasController extends Controller
{
    public function getTomasEdit (){

		$ar['title'] = "Редактировать тест `Поведение в конфликте`";
		$ar['action'] = action("TomasController@Save");
		$ar['questions'] = Questions_Tomas::all();

		return view('manage.tomas.edit', $ar);
	}

    public function getTomasKazEdit (){

        $ar['title'] = "`Поведение в конфликте` өзгерту";
        $ar['action'] = action("TomasController@SaveKaz");
        $ar['questions'] = Questions_Tomas::all();

        return view('manage.tomas.kazedit', $ar);
    }

    public function SaveKaz(Request $r){
        for($i = 1; $i <= 15; $i++){
            Questions_Tomas::where("id", "=", $i)->update(['q_kaz'=>$r->input('q'.$i)]);
        }
        return back();
    }

	public function Save (Request $r){
        for($i = 1; $i <= 15; $i++){
            Questions_Tomas::where("id", "=", $i)->update(['question'=>$r->input('q'.$i)]);
   		}
        return back();
    }

    public function getTomas ($lang){
        $ar['lang'] = $lang;
    	$ar['title'] = "Пройти тест «Поведение в конфликте»";
		$ar['action'] = action("TomasController@Send");
		$ar['questions'] = Questions_Tomas::all();
		$col = array( "1" => "#7BD38E");
        $ar['col'] = $col;

        $tomas_results = Results_Tomas::where("user_id", Auth::id())
                                    ->get()
                                    ->groupBy(function($pool) {
                                                return $pool->created_at->toDateTimeString();
                                             })
                                    ->toArray();

        if($tomas_results != NULL){
            $ar['passed'] = '1';
        }else{
            $ar['passed'] = '0';
        }

        if(Auth::user()->tomas == 1){
            $ar['available'] = "1";
        }else{
            $ar['available'] = "0";
        }

    	return view('user.tomas.pass', $ar);
    }

    public function Send (Request $r){

        $to_ins = array();
        $current = Carbon::now(new DateTimeZone('Asia/Almaty'));

        $messages = [
            'required' => 'Необходимо заполнить все вопросы',
        ];

        for($i=1; $i<=15; $i++){

            $validator = Validator::make($r->all(), [
                'check'.$i => 'required',
            ], $messages);

            if ($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput($r->input());
            }

             $to_ins[] = array("quest_id"=>$i, "answer" => $r->input('check'.$i), "user_id" => Auth::user()->id, "created_at" => $current);
        }

        Results_Tomas::insert($to_ins);
        User::where('id', Auth::user()->id)->update(["tomas_finished_date"=>$current]);
        return redirect('user/tomas/finish')->with('status', 'Вы успешно завершили тестирование «Поведение в конфликте»! Наши сотрудники свяжутся с Вами в ближайшее время.');

    }

    public function finish (){

        $ar['title'] = "«Поведение в конфликте»";

        return view("user.tomas.finish", $ar);
    }

    public function getTomasResultsByDate ($id, $date){
        $ar['title'] = "Детализация результатов теста «Поведение в конфликте»";
        $ar['name'] = User::find($id)->name;
        $ar['surname'] = User::find($id)->surname;
        $ar['date'] = $date;

        $results_by_date = Results_Tomas::where("user_id", $id)
                                            ->where("created_at", $date)
                                            ->get();
        $ar['results'] = $results_by_date;

        $a = array();
        foreach($results_by_date as $result){
            $a[$result->quest_id] = $result->answer;
        }
        $ar['sotrud'] = $a[4]+$a[9]+$a[12];
        $ar['prispo'] = $a[3]+$a[11]+$a[14];
        $ar['soper'] = $a[1]+$a[5]+$a[7];
        $ar['izbeg'] = $a[6]+$a[10]+$a[15];
        $ar['compro'] = $a[2]+$a[8]+$a[13];

        return view("manage.tomas.resultsbydate", $ar);
    }

     public function getTomasResultsByDateForSubadmin ($id, $date){
        $ar['title'] = "Детализация результатов теста «Поведение в конфликте»";
        $ar['name'] = User::find($id)->name;
        $ar['surname'] = User::find($id)->surname;
        $ar['date'] = $date;

        $results_by_date = Results_Tomas::where("user_id", $id)
                                            ->where("created_at", $date)
                                            ->get();
        $ar['results'] = $results_by_date;

        $a = array();
        foreach($results_by_date as $result){
            $a[$result->quest_id] = $result->answer;
        }
        $ar['sotrud'] = $a[4]+$a[9]+$a[12];
        $ar['prispo'] = $a[3]+$a[11]+$a[14];
        $ar['soper'] = $a[1]+$a[5]+$a[7];
        $ar['izbeg'] = $a[6]+$a[10]+$a[15];
        $ar['compro'] = $a[2]+$a[8]+$a[13];

        return view("subadmin.tomas.resultsbydate", $ar);
    }

    public function getTomasResultsByDateForUser ($date, $lang){
        $ar['title'] = "Детализация результатов теста «Поведение в конфликте»";
        $ar['name'] = User::find(Auth::id())->name;
        $ar['surname'] = User::find(Auth::id())->surname;
        $ar['date'] = $date;
        $ar['lang'] = $lang;
        
        $results_by_date = Results_Tomas::where("user_id", Auth::id())
                                            ->where("created_at", $date)
                                            ->get();
        $ar['results'] = $results_by_date;

        $a = array();
        foreach($results_by_date as $result){
            $a[$result->quest_id] = $result->answer;
        }
        $ar['sotrud'] = $a[4]+$a[9]+$a[12];
        $ar['prispo'] = $a[3]+$a[11]+$a[14];
        $ar['soper'] = $a[1]+$a[5]+$a[7];
        $ar['izbeg'] = $a[6]+$a[10]+$a[15];
        $ar['compro'] = $a[2]+$a[8]+$a[13];

        return view("user.tomas.resultsbydate", $ar);
    }

    public function results (){

         $tomas_results = Results_Tomas::where("user_id", Auth::id())
                                    ->get()
                                    ->groupBy(function($pool) {
                                                return $pool->created_at->toDateTimeString();
                                             })
                                    ->toArray();
        $ar['tomas_results'] = $tomas_results;
        $ar['title'] = "Результаты теста";

        return view("user.tomas.all_results", $ar);
    }

}
