<?php
namespace App\Http\Controllers\Manage;

use App\Http\Controllers\Controller;
use Intervention\Image\ImageManager;
use Image;

class Graphs_Disc_DrawController extends Controller{

	public static function draw_graph ($D_numbers, $I_numbers, $S_numbers, $C_numbers, $D, $I, $S, $C){
		$points = array();
		$img = Image::canvas(200, 700, '#ddd');
		$img->text("D", 15, 20, function($font) {
                //$font->file(public_path("fonts/Montserrat-Bold.ttf"));
                $font->size(20);
        });
        for($i=0; $i<count($D_numbers); $i++){
            $img->text($D_numbers[$i]['digit'], 15, ($D_numbers[$i]['pos']*20)+30, function($font) {
                //$font->file(public_path("fonts/Montserrat-Bold.ttf"));
                $font->size(15);
            });
            $points["D"]['x'] = 22;
            if($D_numbers[$i]['digit']>$D && isset($D_numbers[$i+1]['digit']) && $D_numbers[$i+1]['digit']<$D){
            		$points["D"]['y'] = (((($D_numbers[$i]['pos']*20)+25) + (($D_numbers[$i+1]['pos']*20)+25))/2);
                    $img->ellipse(15, 15, 22, $points["D"]['y'], function ($draw) {
                        $draw->background('#0000ff');
                    });
            }
            if($D_numbers[$i]['digit']<$D && isset($D_numbers[$i+1]['digit']) && $D_numbers[$i+1]['digit']>$D){
                    $points["D"]['y'] = (((($D_numbers[$i]['pos']*20)+25) + (($D_numbers[$i+1]['pos']*20)+25))/2);
                    $img->ellipse(15, 15, 22, $points["D"]['y'], function ($draw) {
                        $draw->background('#0000ff');
                    });
            }
            if($D_numbers[$i]['digit']==$D){
            		$points["D"]['y'] = (($D_numbers[$i]['pos']*20)+25);
                    $img->ellipse(15, 15, 22, ($D_numbers[$i]['pos']*20)+25, function ($draw) {
                        $draw->background('#0000ff');
                    });
            }
        }
         $img->text("I", 65, 20, function($font) {
                //$font->file(public_path("fonts/Montserrat-Bold.ttf"));
                $font->size(20);
        });
        for($i=0; $i<count($I_numbers); $i++){
            $img->text($I_numbers[$i]['digit'], 65, ($I_numbers[$i]['pos']*20)+30, function($font) {
               // $font->file(public_path("fonts/Montserrat-Bold.ttf"));
                $font->size(15);
            });
            $points["I"]['x'] = 67;
            if($I_numbers[$i]['digit']>$I && isset($I_numbers[$i+1]['digit']) && $I_numbers[$i+1]['digit']<$I){
            		$points["I"]['y'] = (((($I_numbers[$i]['pos']*20)+25) + (($I_numbers[$i+1]['pos']*20)+25))/2);
                    $img->ellipse(15, 15, 67, $points["I"]['y'], function ($draw) {
                        $draw->background('#0000ff');
                    });
            }
            if($I_numbers[$i]['digit']<$I && isset($I_numbers[$i+1]['digit']) && $I_numbers[$i+1]['digit']>$I){
                    $points["I"]['y'] = (((($I_numbers[$i]['pos']*20)+25) + (($I_numbers[$i+1]['pos']*20)+25))/2);
                    $img->ellipse(15, 15, 67, $points["I"]['y'], function ($draw) {
                        $draw->background('#0000ff');
                    });
            }
            if($I_numbers[$i]['digit']==$I){
            		$points["I"]['y'] = (($I_numbers[$i]['pos']*20)+25);
                    $img->ellipse(15, 15, 67, $points["I"]['y'], function ($draw) {
                        $draw->background('#0000ff');
                    });
            }
        }
         $img->text("S", 115, 20, function($font) {
                //$font->file(public_path("fonts/Montserrat-Bold.ttf"));
                $font->size(20);
        });
        for($i=0; $i<count($S_numbers); $i++){
            $img->text($S_numbers[$i]['digit'], 115, ($S_numbers[$i]['pos']*20)+30, function($font) {
               // $font->file(public_path("fonts/Montserrat-Bold.ttf"));
                $font->size(15);
            });
            $points["S"]['x'] = 117;
            if($S_numbers[$i]['digit']>$S && isset($S_numbers[$i+1]['digit']) && $S_numbers[$i+1]['digit']<$S){
            		$points["S"]['y'] = (((($S_numbers[$i]['pos']*20)+25) + (($S_numbers[$i+1]['pos']*20)+25))/2);
                    $img->ellipse(15, 15, 117, $points["S"]['y'], function ($draw) {
                        $draw->background('#0000ff');
                    });
            }
            if($S_numbers[$i]['digit']<$S && isset($S_numbers[$i+1]['digit']) && $S_numbers[$i+1]['digit']>$S){
                    $points["S"]['y'] = (((($S_numbers[$i]['pos']*20)+25) + (($S_numbers[$i+1]['pos']*20)+25))/2);
                    $img->ellipse(15, 15, 117, $points["S"]['y'], function ($draw) {
                        $draw->background('#0000ff');
                    });
            }
            if($S_numbers[$i]['digit']==$S){
            		$points["S"]['y'] = (($S_numbers[$i]['pos']*20)+25);
                    $img->ellipse(15, 15, 117, ($S_numbers[$i]['pos']*20)+25, function ($draw) {
                        $draw->background('#0000ff');
                    });
            }
        }
         $img->text("C", 165, 20, function($font) {
                //$font->file(public_path("fonts/Montserrat-Bold.ttf"));
                $font->size(20);
        });
        for($i=0; $i<count($C_numbers); $i++){
            $img->text($C_numbers[$i]['digit'], 165, ($C_numbers[$i]['pos']*20)+30, function($font) {
               // $font->file(public_path("fonts/Montserrat-Bold.ttf"));
                $font->size(15);
            });
            $points["C"]['x'] = 167;
            if($C_numbers[$i]['digit']>$C && isset($C_numbers[$i+1]['digit']) && $C_numbers[$i+1]['digit']<$C){
            		$points["C"]['y'] = (((($C_numbers[$i]['pos']*20)+25) + (($C_numbers[$i+1]['pos']*20)+25))/2);
                    $img->ellipse(15, 15, 167, $points["S"]['y'] , function ($draw) {
                        $draw->background('#0000ff');
                    });
            }
            if($C_numbers[$i]['digit']<$C && isset($C_numbers[$i+1]['digit']) && $C_numbers[$i+1]['digit']>$C){
                    $points["C"]['y'] = (((($C_numbers[$i]['pos']*20)+25) + (($C_numbers[$i+1]['pos']*20)+25))/2);
                    $img->ellipse(15, 15, 167, $points["S"]['y'] , function ($draw) {
                        $draw->background('#0000ff');
                    });
            }
            if($C_numbers[$i]['digit']==$C){
            		$points["C"]['y'] = (($C_numbers[$i]['pos']*20)+25);
                    $img->ellipse(15, 15, 167, ($C_numbers[$i]['pos']*20)+25, function ($draw) {
                        $draw->background('#0000ff');
                    });
            }
        }

        $img->line($points["D"]['x'], $points["D"]['y'], $points["I"]['x'], $points["I"]['y'], function ($draw) {
    		$draw->color('#0000ff');
		});
		$img->line($points["I"]['x'], $points["I"]['y'], $points["S"]['x'], $points["S"]['y'], function ($draw) {
    		$draw->color('#0000ff');
		});
		$img->line($points["S"]['x'], $points["S"]['y'], $points["C"]['x'], $points["C"]['y'], function ($draw) {
    		$draw->color('#0000ff');
		});
        $img->line(0, 354, 200, 354, function ($draw) {
            $draw->color('#000000');
        });

        $POINTS_TO_FIND_MAX = array("D" => $points["D"]['y'], "I" => $points["I"]['y'], "S" => $points["S"]['y'], "C" => $points["C"]['y']);

        $points_to_return = array();

        foreach ($POINTS_TO_FIND_MAX as $key => $value) {
            if($value<354){
                $points_to_return[$key] = $value;
            }
        }

        asort($points_to_return);


        return array("image"=>$img, "top_letters" => $points_to_return);

	}

}

?>