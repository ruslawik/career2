<?php
namespace App\Http\Controllers\Manage;

use App\Http\Controllers\Controller;
use App\Video;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class VideoController extends Controller{

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $videos = Video::latest()->paginate(10);

        return view("manage.videos.index", compact('videos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        return view('manage.videos.create')->with('types', Video::types());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'link' => 'required|max:500',
            'type_id' => 'required',
        ]);

        Video::create($request->all());

        return redirect()->route('videos.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Video $video
     * @return Application|Factory|View
     */
    public function edit(Video $video)
    {
        return view('manage.videos.edit', compact('video'))->with('types', Video::types());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Video $video
     * @return RedirectResponse
     */
    public function update(Request $request, Video $video)
    {
        $this->validate($request, [
            'link' => 'required|max:500',
            'type_id' => 'required',
        ]);

        $video->update($request->all());

        return redirect()->route('videos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Video $video
     * @return RedirectResponse
     * @throws Exception
     */
    public function destroy(Video $video): RedirectResponse
    {
        $video->delete();

        return redirect()->route('videos.index');
    }
}
