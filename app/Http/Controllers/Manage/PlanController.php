<?php
namespace App\Http\Controllers\Manage;

use App\Http\Controllers\Controller;
use App\Plan;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class PlanController extends Controller{

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $plans = Plan::paginate(100);

        return view("manage.plans.index", compact('plans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        return view('manage.plans.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'price' => 'required|integer',
            'link' => 'required|max:1000',
        ]);

        Plan::create($request->all());

        return redirect()->route('plans.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Plan $plan
     * @return Application|Factory|View
     */
    public function edit(Plan $plan)
    {
        return view('manage.plans.edit', compact('plan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Plan $plan
     * @return RedirectResponse
     */
    public function update(Request $request, Plan $plan)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'price' => 'required|integer',
            'link' => 'required|max:1000',
        ]);

        $plan->update($request->all());

        return redirect()->route('plans.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Plan $plan
     * @return RedirectResponse
     * @throws Exception
     */
    public function destroy(Plan $plan): RedirectResponse
    {
        $plan->delete();

        return redirect()->route('plans.index');
    }
}
