<?php
namespace App\Http\Controllers\Manage;

use App\Http\Controllers\Controller;
use App\Plan;
use App\PlanItem;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class PlanItemController extends Controller{

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        if($planId = request('plan_id')){
            $planItems = PlanItem::where('plan_id', $planId)->paginate(100);

            return view("manage.plan-items.index", compact('planItems'));
        }
        abort(404);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        return view('manage.plan-items.create')->with('plans', Plan::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'plan_id' => 'required',
        ]);
        $attributes = $request->all();
        $attributes['is_active'] = $attributes['is_active'] ?? 0;
        $planItem = PlanItem::create($attributes);

        return redirect()->route('plan-items.index', ['plan_id' => $planItem->plan_id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param PlanItem $planItem
     * @return Application|Factory|View
     */
    public function edit(PlanItem $planItem)
    {
        return view('manage.plan-items.edit', compact('planItem'))->with('plans', Plan::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param PlanItem $planItem
     * @return RedirectResponse
     */
    public function update(Request $request, PlanItem $planItem)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'plan_id' => 'required',
        ]);
        $attributes = $request->all();
        $attributes['is_active'] = $attributes['is_active'] ?? 0;
        $planItem->update($attributes);

        return redirect()->route('plan-items.index', ['plan_id' => $planItem->plan_id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param PlanItem $planItem
     * @return RedirectResponse
     * @throws Exception
     */
    public function destroy(PlanItem $planItem): RedirectResponse
    {
        $planItem->delete();

        return redirect()->route('plans.index');
    }
}
