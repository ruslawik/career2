<?php
namespace App\Http\Controllers;

use App\Plan;
use App\Video;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function about(){
        return view('frontend.about');
    }
    public function services(){
        return view('frontend.services')
            ->with('videos', Video::query()->where('type_id', Video::TYPE_SITE)->get())
            ->with('plans', Plan::query()->limit(3)->get());
    }
    public function contacts(){
        return view('frontend.contacts');
    }
    public function sendRequest(Request $request): JsonResponse
    {
        $validator = $this->validate($request, [
            'name' => 'required|max:20',
            'question' => 'max:255',
            'phone' => 'required|max:20',
        ]);
        if ($validator) {
            $token = "5694207576:AAGr-c8pXT8kgTva38l_hCoJnJRiSXiHNUo";
            $chat_id = "-1001606155550";

            $arr = [
                "Имя" => $request->name,
                "Телефон" => $request->phone,
                "Вопрос" => $request->question,
            ];
            $text = '';
            foreach($arr as $key => $value) {
                $text .= "<b>".$key."</b>: ".$value."%0A";
            }

            $fp=fopen("https://api.telegram.org/bot{$token}/sendMessage?chat_id={$chat_id}&parse_mode=html&text={$text}","r");
            return response()->json(['success' => true]);
        }

        return response()->json(['errors' => $validator->errors()->all()]);
    }
}
