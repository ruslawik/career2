<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Goal;

class GoalsController extends Controller
{
    public function index (){

    	$ar = array();

        $ar['title'] = "Мои цели";
        $ar['head_title'] = "Мои цели";
        $ar['username'] = Auth::user()->name." ".Auth::user()->surname;
        $ar['goals'] = Goal::where('user_id', Auth::user()->id)->orderBy('created_at', 'DESC')->paginate(5);
        $ar['statuses'] = array("no", "proccess", "yes");
        $ar['status_texts'] = array("Не сделано", "В процессе", "Сделано");

    	return view("user.goals.goals", $ar);
    }

    public function addGoal (Request $r){

    	$r->validate([
    		'date_of_exec' => 'required',
    		'goal' => 'required',
    		'description' => 'required'
		]);

        $last_goal_max_local_num = Goal::where('user_id', Auth::user()->id)->max('local_num');

    	$goal = new Goal();
    	$goal->user_id = Auth::user()->id;
        $goal->local_num = $last_goal_max_local_num+1;
    	$goal->date_of_exec = $r->input('date_of_exec');
    	$goal->goal = $r->input('goal');
    	$goal->description = $r->input('description');
    	$goal->status = 1;
    	$goal->save();

    	return back()->with('success', 'Цель успешно поставлена!');
    }

    public function updateGoalStatus ($goal_id, $status){

    	$goal = Goal::where('id', $goal_id)->get();
    	if(Auth::user()->id != $goal[0]->user_id){
    		return back()->withErrors(['Ошибка', 'Это не Ваша цель.']);
    	}

    	Goal::where('id', $goal_id)->update(['status'=>$status]);
    	return back()->with('success', 'Цель успешно обновлена!');
    }
}
