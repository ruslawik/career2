<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function index (){

    	$ar = array();
        $ar['title'] = "Ваш профиль";
        $ar['head_title'] = "Ваш профиль";
        $ar['username'] = Auth::user()->name." ".Auth::user()->surname;

        $ar['name'] = Auth::user()->name;
        $ar['surname'] = Auth::user()->surname;
        $ar['email'] = Auth::user()->email;
        $user_img_q = User::where('id', Auth::user()->id)->get();
        $ar['img'] = $user_img_q[0]->img;

    	return view("user.profile.index", $ar);
    }

    public function updateProfile (Request $r){

    	$r->validate([
            'name' => 'required',
            'surname' => 'required',
            'img' => 'image|mimes:jpg,png,jpeg,gif,svg|max:5000'
        ]);

        $fileName = NULL;

        if($r->file()) {
            $fileName = time().'_'.$r->img->getClientOriginalName();
            $filePath = $r->file('img')->storeAs('user-avatars', $fileName, 'public');
            User::where('id', Auth::user()->id)->update(['img' => $fileName]);
        }

        if(strlen($r->input('password')) >= 1){
        	$new_pass = Hash::make($r->input('password'));
        	User::where('id', Auth::user()->id)->update(["password" => $new_pass]);
        }

        User::where('id', Auth::user()->id)->update(["name" => htmlentities(strip_tags($r->input('name'))), "surname" => htmlentities(strip_tags($r->input('surname')))]);

        return back()->with('status', 'Профиль успешно обновлен!');
    }
}
