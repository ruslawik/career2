<?php

namespace App\Http\Controllers;

use App\NewsCategory;
use Illuminate\Http\Request;
use App\News;
use Auth;

class NewsController extends Controller
{
 	//USER METHODS
	public function index (){

		$ar = array();

        $ar['title'] = "Блог";
        $ar['head_title'] = "Блог";
        $ar['username'] = Auth::user()->name." ".Auth::user()->surname;
        $ar['posts'] = News::paginate(6);

		return view('user.news.index', $ar);
	}

	public function news_single ($news_id){

		$ar = array();

		$ar['post'] = News::where('id', $news_id)->get();

        $ar['title'] = $ar['post'][0]->name;
        $ar['head_title'] = $ar['post'][0]->name;
        $ar['username'] = Auth::user()->name." ".Auth::user()->surname;

		return view('user.news.single', $ar);
	}

    public function search(Request $r){

        $ar = array();

        $ar['title'] = "Блог";
        $ar['head_title'] = "Блог";
        $ar['username'] = Auth::user()->name." ".Auth::user()->surname;
        $ar['teklifler'] = NewsCategory::all();
        $ar['categories'] = NewsCategory::all();
        $ar['news'] = News::paginate(6);
        $ar['selected_cat'] = $r->input('category');
        $ar['q'] = "";

        if($r->has('category') && $r->input('category')!=-1){
            $ar['news'] = News::where('category_id', $r->input('category'))
                ->paginate(6);
        }

        if($r->has('q')){
            $word = mb_strtolower($r->input('q'));
            $ar['news'] = News::whereRaw('LOWER(name->"$.ru") like ?', '%'.$word.'%')
                ->orWhereRaw('LOWER(name->"$.kz") like ?', '%'.$word.'%')
                ->orWhereRaw('LOWER(text->"$.kz") like ?', '%'.$word.'%')
                ->orWhereRaw('LOWER(text->"$.ru") like ?', '%'.$word.'%')
                ->paginate(6);
            $ar['q'] = $r->input('q');
        }

        if($r->has('q') && $r->has('category')){
            $q = $r->input('q');
            $cat = $r->input('category');
            if($cat==-1){
                return redirect('user/news?q='.$q);
            }
            $ar['news'] = News::where(function ($query){
                global $q;
                $query->where('name->ru', 'LIKE', "%".$q."%")
                    ->orWhere('name->kz', 'LIKE', "%".$q."%")
                    ->orWhere('text->ru', 'LIKE', "%".$q."%")
                    ->orWhere('text->kz', 'LIKE', "%".$q."%");
            })
                ->where('category_id', $cat)
                ->paginate(6);
            $ar['q'] = $r->input('q');
        }

        return view('user.news.search', $ar);
    }

	//ADMIN METHODS

	public function news_list(){

		$ar['title'] = "Все блоги";

    	return view('manage.news.news_list', $ar);
	}

	public function allNewsAjax (){

    	$news = News::select('name', 'text', 'created_at', 'id')->get()->toArray();

    	$i=0;
    	foreach ($news as $value) {
    		$news[$i]['text'] = mb_substr($news[$i]['text'], 0, 100)."...";
    		$i++;
    	}

        $output = array(
            'data' => $news
        );
        return response()->json($output);
    }

    public function addNews (){

    	$ar['title'] = "Добавить новость";
        $ar['categories'] = NewsCategory::all();
    	return view('manage.news.add_news', $ar);
    }

    public function addPostNews(Request $r){

    	$r->validate([
            'name_ru' => 'required',
            'name_kz' => 'required',
            'text_ru' => 'required',
            'text_kz' => 'required',
            'category_id' => 'required',
        ]);

        $fileName = "";

        if($r->file()) {
            $fileName = time().'_'.$r->img->getClientOriginalName();
            $filePath = $r->file('img')->storeAs('news-images', $fileName, 'public');
        }

    	$news = new News();
        $news->img = $fileName;
        $news->category_id = $r->input('category_id');
    	$news->setTranslation('name', 'ru', $r->input('name_ru'))
    				->setTranslation('name', 'kz', $r->input('name_kz'))
    				->setTranslation('text', 'ru', $r->input('text_ru'))
    				->setTranslation('text', 'kz', $r->input('text_kz'))
    				->save();
    	return redirect('/manage/news')->with('status', 'Блог успешно добавлен!');
    }

    public function uploadImage (Request $request){
        if($request->hasFile('upload')) {
            $originName = $request->file('upload')->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_FILENAME);
            $extension = $request->file('upload')->getClientOriginalExtension();
            $fileName = $fileName.'_'.time().'.'.$extension;

            $request->file('upload')->move(public_path('news/images'), $fileName);

            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
            $url = asset('news/images/'.$fileName);
            $msg = 'Image uploaded successfully';
            $response = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";

            @header('Content-type: text/html; charset=utf-8');
            echo $response;
        }
    }

    public function editNews ($news_id){

    	$ar['title'] = "Редактировать блог";

    	$ar['news'] = News::where('id', $news_id)->get();
        $ar['categories'] = NewsCategory::all();
    	return view("manage.news.edit_news", $ar);

    }

    public function editPostNews(Request $r){

    	$r->validate([
            'name_ru' => 'required',
            'name_kz' => 'required',
            'text_ru' => 'required',
            'text_kz' => 'required',
            'category_id' => 'required',
        ]);

        if($r->file()) {
            $fileName = time().'_'.$r->img->getClientOriginalName();
            $filePath = $r->file('img')->storeAs('news-images', $fileName, 'public');
            $now_file = News::where('id', $r->input('news_id'))->get();
            if(strlen($now_file[0]->img) > 0){
                $this->deleteFile("storage/news-images/".$now_file[0]->img);
            }
            News::where('id', $r->input('news_id'))
                    ->update(["img" => $fileName]);
        }

        News::where('id', $r->input('news_id'))->update(["category_id" => $r->input('category_id')]);
        $news = News::where('id', $r->input('news_id'))->get();
    	$news[0]->setTranslation('name', 'ru', $r->input('name_ru'))
    				->setTranslation('name', 'kz', $r->input('name_kz'))
    				->setTranslation('text', 'ru', $r->input('text_ru'))
    				->setTranslation('text', 'kz', $r->input('text_kz'))
    				->save();
    	return back()->with('status', 'Блог успешно обновлен!');
    }

    public function deleteNews ($news_id){

        $now_file = News::where('id', $news_id)->get();
        $this->deleteFile("storage/news-images/".$now_file[0]->img);

    	News::where('id', $news_id)->delete();
    	return redirect('/manage/news')->with('status', 'Блог успешно удален!');
    }

    public function deleteImg ($news_id){

        $now_file = News::where('id', $news_id)->get();
        $this->deleteFile("storage/news-images/".$now_file[0]->img);

        News::where('id', $news_id)->update(['img' => null]);

        return back()->with('status', 'Картинка успешно удалена!');
    }

    public function deleteFile($fullPath){
        if(file_exists($fullPath)){
            unlink($fullPath);
        }
    }

    public function categories_list (){

        $ar['title'] = "Все категории блога";

        $ar['categories'] = NewsCategory::all();

        return view('manage.news.news_categories', $ar);
    }

    public function addNewsCat (Request $r){

        $news = new NewsCategory();

        $news->setTranslation('name', 'ru', $r->input('name_ru'))
            ->setTranslation('name', 'kz', $r->input('name_kz'));
        $news->save();

        return back()->with('status', 'Успешно добавлено!');
    }

    public function editNewsCat ($cat_id){

        $ar['title'] = "Редактировать категорию";

        $ar['cat'] = NewsCategory::where('id', $cat_id)->get();

        return view('manage.news.edit_news_category', $ar);
    }

    public function postEditNewsCat (Request $r){

        $news_cat = NewsCategory::where('id', $r->input('cat_id'))->get();
        $news_cat[0]->setTranslation('name', 'ru', $r->input('name_ru'))
            ->setTranslation('name', 'kz', $r->input('name_kz'))
            ->save();

        return redirect('/manage/news-cats')->with('status', 'Успешно обновлено!');
    }
}
