<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Setting;

class SettingsController extends Controller
{
    public function index (){
    	$ar['title'] = "Настройки";

    	$ar['mapp_price'] = Setting::where('setting', 'mapp_price')->get();
    	$ar['disc_price'] = Setting::where('setting', 'disc_price')->get();
    	return view('manage.settings', $ar);
    }

    public function save (Request $r){

    	Setting::where('setting', 'mapp_price')->update(['value' => $r->input('mapp_price')]);
    	Setting::where('setting', 'disc_price')->update(['value' => $r->input('disc_price')]);

    	return back()->with('status', 'Настройки успешно обновлены!');
    }
}
