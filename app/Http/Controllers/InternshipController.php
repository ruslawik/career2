<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Internship;
use Auth;

class InternshipController extends Controller
{
    //USER METHODS
    public function userIndex (){

        $ar = array();
        $ar['title'] = 'Все стажировки';
        $ar['head_title'] = 'Все стажировки';
        $ar['username'] = Auth::user()->name." ".Auth::user()->surname;
        $ar['posts'] = Internship::paginate(20);

        return view('user.internship.index', $ar);
    }

    public function internshipSingle ($internship_id){

        $ar = array();

        $ar['post'] = Internship::where('id', $internship_id)->get();

        $ar['title'] = $ar['post'][0]->name;
        $ar['head_title'] = $ar['post'][0]->name;
        $ar['username'] = Auth::user()->name." ".Auth::user()->surname;

        return view('user.internship.single', $ar);
    }

    //ADMIN METHODS

	public function internshipList(){

		$ar['title'] = "Все стажировки";

    	return view('manage.internship.internship_list', $ar);
	}

	public function internshipListAjax (){

    	$internship = Internship::select('name', 'location', 'descrip', 'img', 'id')->get()->toArray();

    	$i=0;
    	foreach ($internship as $value) {
    		$internship[$i]['descrip']['ru'] = mb_substr($internship[$i]['descrip']['ru'], 0, 50)."...";
    		$i++;
    	}

        $output = array(
            'data' => $internship
        );
        return response()->json($output);
    }

    public function addInternship (){

    	$ar['title'] = "Добавить стажировку";

    	return view('manage.internship.add_internship', $ar);
    }

    public function addPostInternship(Request $r){

    	$r->validate([
            'name_ru' => 'required',
            'name_kz' => 'required',
            'location_ru' => 'required',
            'location_kz' => 'required',
            'descrip_ru' => 'required',
            'descrip_kz' => 'required',
        ]);

        $fileName = "";

        if($r->file()) {
            $fileName = time().'_'.$r->img->getClientOriginalName();
            $filePath = $r->file('img')->storeAs('internship-images', $fileName, 'public');
        }

    	$internship = new Internship();
        $internship->img = $fileName;
    	$internship->setTranslation('name', 'ru', $r->input('name_ru'))
    				->setTranslation('name', 'kz', $r->input('name_kz'))
    				->setTranslation('descrip', 'ru', $r->input('descrip_ru'))
    				->setTranslation('descrip', 'kz', $r->input('descrip_kz'))
    				->setTranslation('location', 'ru', $r->input('location_ru'))
    				->setTranslation('location', 'kz', $r->input('location_kz'))
    				->save();

    	return redirect('/manage/internship')->with('status', 'Стажировка успешно добавлена!');
    }

    public function uploadImage (Request $request){
        if($request->hasFile('upload')) {
            $originName = $request->file('upload')->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_FILENAME);
            $extension = $request->file('upload')->getClientOriginalExtension();
            $fileName = $fileName.'_'.time().'.'.$extension;
        
            $request->file('upload')->move(public_path('internship/images'), $fileName);
   
            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
            $url = asset('internship/images/'.$fileName); 
            $msg = 'Image uploaded successfully'; 
            $response = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";
               
            @header('Content-type: text/html; charset=utf-8'); 
            echo $response;
        }
    }

    public function editInternship ($internship_id){

    	$ar['title'] = "Редактировать стажировку";

    	$ar['internship'] = Internship::where('id', $internship_id)->get();

    	return view("manage.internship.edit_internship", $ar);

    }

    public function editPostInternship(Request $r){

    	$r->validate([
            'name_ru' => 'required',
            'name_kz' => 'required',
            'location_ru' => 'required',
            'location_kz' => 'required',
            'descrip_ru' => 'required',
            'descrip_kz' => 'required',
        ]);

        if($r->file()) {
            $fileName = time().'_'.$r->img->getClientOriginalName();
            $filePath = $r->file('img')->storeAs('internship-images', $fileName, 'public');
            $now_file = Internship::where('id', $r->input('internship_id'))->get();
            if(strlen($now_file[0]->img) > 0){
                unlink("storage/internship-images/".$now_file[0]->img);
            }
            Internship::where('id', $r->input('internship_id'))
                    ->update(["img" => $fileName]);
        }

    	$internship = Internship::where('id', $r->input('internship_id'))->get();

    	$internship[0]->setTranslation('name', 'ru', $r->input('name_ru'))
    				->setTranslation('name', 'kz', $r->input('name_kz'))
    				->setTranslation('descrip', 'ru', $r->input('descrip_ru'))
    				->setTranslation('descrip', 'kz', $r->input('descrip_kz'))
    				->setTranslation('location', 'ru', $r->input('location_ru'))
    				->setTranslation('location', 'kz', $r->input('location_kz'))
    				->save();
    	return back()->with('status', 'Стажировка успешно обновлена!');
    }

    public function deleteInternship ($internship_id){

        $now_file = Internship::where('id', $internship_id)->get();
        if($now_file[0]->img != NULL){
        	unlink("storage/internship-images/".$now_file[0]->img);
        }

    	Internship::where('id', $internship_id)->delete();
    	return redirect('/manage/internship')->with('status', 'Стажировка успешно удалена!');
    }

    public function deleteImg ($internship_id){

        $now_file = Internship::where('id', $internship_id)->get();
        unlink("storage/internship-images/".$now_file[0]->img);

        Internship::where('id', $internship_id)->update(['img' => null]);

        return back()->with('status', 'Картинка успешно удалена!');
    }
}
