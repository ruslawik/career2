<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Questions_Holl;
use Carbon\Carbon;
use DateTimeZone;
use App\Results_Holl;
use Auth;
use App\User;
use Validator;

class HollController extends Controller
{
    
	public function getHollEdit (){

		$ar['title'] = "Редактировать опросник «Социальный интеллект»";
		$ar['action'] = action("HollController@Save");
		$ar['questions'] = Questions_Holl::all();

		return view('manage.holl.edit', $ar);
	}

    public function getHollKazEdit(){

        $ar['title'] = "«Социальный интеллект» өзгерту";
        $ar['action'] = action("HollController@SaveKaz");
        $ar['questions'] = Questions_Holl::all();

        return view('manage.holl.kaz_edit', $ar);
    }

	public function Save (Request $r){
        for($i = 1; $i <= 30; $i++){
            Questions_Holl::where("id", "=", $i)->update(['question'=>$r->input('q'.$i)]);
   		}
        return back();
    }

    public function SaveKaz (Request $r){
        for($i = 1; $i <= 30; $i++){
            Questions_Holl::where("id", "=", $i)->update(['q_kaz'=>$r->input('q'.$i)]);
        }
        return back();
    }

    public function getHoll ($lang){
        $ar['lang'] = $lang;
    	$ar['title'] = "Пройти тест «Социальный интеллект»";
		$ar['action'] = action("HollController@Send");
		$ar['questions'] = Questions_Holl::all();
		$col = array( "1" => "#7BD38E", "2" => "#74B7C4", "3" => "#EEBB8A");
        $ar['col'] = $col;

        $holl_results = Results_Holl::where("user_id", Auth::id())
                                    ->get()
                                    ->groupBy(function($pool) {
                                                return $pool->created_at->toDateTimeString();
                                             })
                                    ->toArray();
        if($holl_results != NULL){
            $ar['passed'] = '1'; 
        }else{
            $ar['passed'] = '0';
        }

        if(Auth::user()->holl == 1){
            $ar['available'] = "1";
        }else{
            $ar['available'] = "0";
        }

    	return view('user.holl.pass', $ar);
    }

    public function Send (Request $r){

        $to_ins = array();
        $current = Carbon::now(new DateTimeZone('Asia/Almaty'));

        $messages = [
            'required' => 'Необходимо заполнить все вопросы',
        ];

        for($i=1; $i<=30; $i++){

            $validator = Validator::make($r->all(), [
                'check'.$i => 'required',
            ], $messages);

            if ($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput($r->input());
            }

             $to_ins[] = array("quest_id"=>$i, "answer" => $r->input('check'.$i), "user_id" => Auth::user()->id, "created_at" => $current);
        }

        Results_Holl::insert($to_ins);
        User::where('id', Auth::user()->id)->update(["holl_finished_date"=>$current]);
        return redirect('user/holl/finish')->with('status', 'Вы успешно завершили тестирование «Социальный интеллект»!Наши сотрудники свяжутся с Вами в ближайшее время.');
    }

    public function finish (){

        $ar['title'] = "«Социальный интеллект»";

        return view("user.holl.finish", $ar);
    }

    public function getHollResultsByDate ($id, $date){
        $ar['title'] = "Детализация результатов теста «Социальный интеллект»";
        $ar['name'] = User::find($id)->name;
        $ar['surname'] = User::find($id)->surname;
        $ar['date'] = $date;

        $results_by_date = Results_Holl::where("user_id", $id)
                                            ->where("created_at", $date)
                                            ->get();
        $ar['results'] = $results_by_date;

        $a = array();
        foreach($results_by_date as $result){
            $a[$result->quest_id] = $result->answer;
        }
        $ar['samosoz'] = $a[1]+$a[6]+$a[11]+$a[16]+$a[21]+$a[26];
        $ar['samoreg'] = $a[2]+$a[7]+$a[12]+$a[17]+$a[22]+$a[27];
        $ar['empathy'] = $a[3]+$a[8]+$a[13]+$a[18]+$a[23]+$a[28];
        $ar['vzaimo'] = $a[4]+$a[9]+$a[14]+$a[19]+$a[24]+$a[29];
        $ar['samomot'] = $a[5]+$a[10]+$a[15]+$a[20]+$a[25]+$a[30];

        return view("manage.holl.resultsbydate", $ar);
    }
    public function getHollResultsByDateForSubadmin ($id, $date){
        $ar['title'] = "Детализация результатов теста «Социальный интеллект»";
        $ar['name'] = User::find($id)->name;
        $ar['surname'] = User::find($id)->surname;
        $ar['date'] = $date;

        $results_by_date = Results_Holl::where("user_id", $id)
                                            ->where("created_at", $date)
                                            ->get();
        $ar['results'] = $results_by_date;

        $a = array();
        foreach($results_by_date as $result){
            $a[$result->quest_id] = $result->answer;
        }
        $ar['samosoz'] = $a[1]+$a[6]+$a[11]+$a[16]+$a[21]+$a[26];
        $ar['samoreg'] = $a[2]+$a[7]+$a[12]+$a[17]+$a[22]+$a[27];
        $ar['empathy'] = $a[3]+$a[8]+$a[13]+$a[18]+$a[23]+$a[28];
        $ar['vzaimo'] = $a[4]+$a[9]+$a[14]+$a[19]+$a[24]+$a[29];
        $ar['samomot'] = $a[5]+$a[10]+$a[15]+$a[20]+$a[25]+$a[30];

        return view("subadmin.holl.resultsbydate", $ar);
    }

    public function getHollResultsByDateForUser ($date, $lang){
        $ar['title'] = "Детализация результатов теста «Социальный интеллект»";
        $ar['name'] = User::find(Auth::id())->name;
        $ar['surname'] = User::find(Auth::id())->surname;
        $ar['date'] = $date;
        $ar['lang'] = $lang;
        
        $results_by_date = Results_Holl::where("user_id", Auth::id())
                                            ->where("created_at", $date)
                                            ->get();
        $ar['results'] = $results_by_date;

        $a = array();
        foreach($results_by_date as $result){
            $a[$result->quest_id] = $result->answer;
        }
        $ar['samosoz'] = $a[1]+$a[6]+$a[11]+$a[16]+$a[21]+$a[26];
        $ar['samoreg'] = $a[2]+$a[7]+$a[12]+$a[17]+$a[22]+$a[27];
        $ar['empathy'] = $a[3]+$a[8]+$a[13]+$a[18]+$a[23]+$a[28];
        $ar['vzaimo'] = $a[4]+$a[9]+$a[14]+$a[19]+$a[24]+$a[29];
        $ar['samomot'] = $a[5]+$a[10]+$a[15]+$a[20]+$a[25]+$a[30];

        return view("user.holl.resultsbydate", $ar);
    }

    public function results (){

        $holl_results = Results_Holl::where("user_id", Auth::id())
                                    ->get()
                                    ->groupBy(function($pool) {
                                                return $pool->created_at->toDateTimeString();
                                             })
                                    ->toArray();
        $ar['holl_results'] = $holl_results;
        $ar['title'] = "Результаты";

        return view("user.holl.all_results", $ar);
    }

}
