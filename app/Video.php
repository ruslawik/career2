<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $guarded = ['id'];

    const TYPE_SITE = 1;
    const TYPE_CAB = 2;

    /**
     * @return string[]
     */
    public static function types()
    {
        return [
            self::TYPE_SITE => 'Для сайта',
            self::TYPE_CAB => 'Для кабинета',
        ];
    }

    public function getUrlAttribute()
    {
        return str_replace('https://youtu.be/', '', $this->link);
    }

    /**
     * @return string|null
     */
    public function getType()
    {
        return self::types()[$this->type_id];
    }
}
