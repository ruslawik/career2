<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Results_Solomin1 extends Model
{
    protected $table = "results_solomin1";

    function quest_text (){

    	return $this->hasOne("App\Solomin1", 'id', 'quest_id');
    }

}
