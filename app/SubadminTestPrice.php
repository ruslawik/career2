<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubadminTestPrice extends Model
{
    protected $table = "subadmin_test_prices";
}
