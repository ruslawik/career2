<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations as BaseHasTranslations;

trait HasTranslations
{
    use BaseHasTranslations;
    /**
     * Convert the model instance to an array.
     *
     * @return array
     */
    public function toArray()
    {
        $attributes = parent::toArray();
        foreach ($this->getTranslatableAttributes() as $field) {
            $attributes[$field] = $this->getTranslation($field, \App::getLocale());
        }
        return $attributes;
    }
}

class University extends Model
{
    use HasTranslations;

    protected $table = "universities";

    public $translatable = ['name', 'description'];

    public function category(){

        return $this->belongsTo('App\UniversityCategory', 'category_id', 'id');
    }

    public function intern_text($intern){
        if($intern == 0){
            return "Нет стажировки";
        }
        if($intern==1){
            return "Есть стажировка";
        }
    }

    public function small_desc($desc){
        return mb_substr(strip_tags($desc), 0, 120)."...";
    }
}
