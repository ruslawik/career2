<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    protected $guarded = ['id'];

    public function items(){
        return $this->hasMany(PlanItem::class)->orderBy('is_active', 'desc');
    }
}
