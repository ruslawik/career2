<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations as BaseHasTranslations;

trait HasTranslations
{
    use BaseHasTranslations;
    /**
     * Convert the model instance to an array.
     *
     * @return array
     */
    public function toArray()
    {
        $attributes = parent::toArray();
        foreach ($this->getTranslatableAttributes() as $field) {
            $attributes[$field] = $this->getTranslation($field, \App::getLocale());
        }
        return $attributes;
    }
}

class Schedule extends Model
{
    use HasTranslations;
    protected $table = 'schedule';

    public $translatable = ['name', 'text'];

    public function small_desc($desc){
        $desc = strip_tags(html_entity_decode($desc));
        return mb_substr($desc, 0, 120)."...";
    }
}
