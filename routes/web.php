<?php


/*
NEW ROUTES FOR CAREER 2
*/

use App\Http\Controllers\HomeController;

Route::get('/local/{locale}', 'Front\FrontController@setLang');
Route::post('/auto-register-post', 'MappApiController@reg_in_mapp_system');

Route::get('/is-mapp-passed/{email}', 'MappApiController@isMappPassed');
/*
END NEW ROUTES
*/
Route::get('/', "LoginController@getLogin");
Route::get('/about', "HomeController@about");
Route::get('/services', "HomeController@services");
Route::get('/contacts', "HomeController@contacts");
Route::post('/send-request', [HomeController::class, 'sendRequest'])->name('send.request');;
Route::get('/cloudpayments', "LoginController@getCloud");
Route::get('/is_exists', "LoginController@check_login");

Route::get('login', 'LoginController@getLogin');
Route::post('login', 'LoginController@postLogin');

Route::get('regpage', 'LoginController@getRegpage');
Route::post('reg', 'LoginController@postReg');

Route::any('logout', 'LoginController@getLogout');
Route::any('job/{id}', 'MappAuto@getJobUnivers');

Route::get('application', 'LoginController@getApplication');
Route::post('/application_save', 'LoginController@saveApplication');

//Защита админки посредником через роль аккаунта из БД (админ - 1, юзер - 2)
Route::group(['middleware' => ['auth.admin'], 'prefix' => 'manage'], function () {
	//Main Route
	Route::any("/admin", "Manage\ManageController@getIndex");
	Route::any("show_stat/{test_name}/{from_date}/{to_date}", "Manage\ManageController@getStatByTest");
	Route::any("show_stat_by_pockets/{test_name}/{from_date}/{to_date}", "Manage\ManageController@getStatByPockets");
	Route::any("/subadmin_info/{subadmin_id}", "Manage\ManageController@getSubadminInfo");

	//Internship
	Route::get('/internship', 'InternshipController@internshipList');
	Route::get('/all-internships', 'InternshipController@internshipListAjax');
	Route::get('/add-internship', 'InternshipController@addInternship');
	Route::get('/edit-internship/{internship_id}', 'InternshipController@editInternship');
	Route::post('/edit-internship', 'InternshipController@editPostInternship');
	Route::post('/add-internship', 'InternshipController@addPostInternship');
	Route::post('/upload_image_internship','InternshipController@uploadImage')->name('internship_upload_image');
	Route::get('/delete-internship/{internship_id}', 'InternshipController@deleteInternship');
	Route::get('/internship/delete-image/{internship_id}','InternshipController@deleteImg');

	//Univers
	Route::get('/univers', 'UniverController@univerList');


	//Settings routes
	Route::get('/settings', 'SettingsController@index');
	Route::post('/settings-save', 'SettingsController@save');

	//MAPP Keycodes management
	Route::get('/mapp-keycodes', 'MappKeycodeManage@index');
	Route::get('/all-keycodes', 'MappKeycodeManage@allKeycodesAjax');
	Route::get('/load-keycodes', 'MappKeycodeManage@loadKeycodes');
	Route::post('/give-keycodes', 'MappKeycodeManage@giveKeycodes');
	Route::get('/edit-mapp-keycode/{keycode_id}', 'MappKeycodeManage@editKeycode');
	Route::post('/mappkeycode-save', 'MappKeycodeManage@saveKeycode');

	//DISC Promocodes management
	Route::get('/disc-promocodes', 'User\DiscController@promocodesList');
	Route::get('/disc-promocodes-ajax', 'User\DiscController@promocodesListAjax');
	Route::post('/give-disc-keycodes', 'User\DiscController@givePromocodes');

	//News
	Route::get('/news', "NewsController@news_list");
    Route::get('/news-cats', 'NewsController@categories_list');
    Route::post('/add-news-category', 'NewsController@addNewsCat');
    Route::get('/edit-news-category/{cat_id}', 'NewsController@editNewsCat');
    Route::post('/edit-news-category', 'NewsController@postEditNewsCat');
	Route::get('/all-news', 'NewsController@allNewsAjax');
	Route::get('/add-news', 'NewsController@addNews');
	Route::post('/add-news', 'NewsController@addPostNews');
	Route::get('/edit-news/{news_id}', 'NewsController@editNews');
	Route::post('/edit-news', 'NewsController@editPostNews');
	Route::get('/delete-news/{news_id}', 'NewsController@deleteNews');
	Route::post('/upload_image_news','NewsController@uploadImage')->name('news_upload_image');
	Route::get('/news/delete-image/{news_id}','NewsController@deleteImg');

	//Schedule
	Route::get('/schedule', "ScheduleController@schedule_list");
	Route::get('/all-schedule', 'ScheduleController@allScheduleAjax');
	Route::get('/add-schedule', 'ScheduleController@addSchedule');
	Route::post('/add-schedule', 'ScheduleController@addPostSchedule');
	Route::get('/edit-schedule/{schedule_id}', 'ScheduleController@editSchedule');
	Route::post('/edit-schedule', 'ScheduleController@editPostSchedule');
	Route::get('/delete-schedule/{schedule_id}', 'ScheduleController@deleteSchedule');
	Route::post('/upload_image_schedule','ScheduleController@uploadImage')->name('schedule_upload_image');
	Route::get('/schedule/delete-image/{schedule_id}','ScheduleController@deleteImg');

	//Professions
	Route::get('/professions', 'ProfessionController@profession_list');
	Route::get('/profession-cats', 'ProfessionController@categories_list');
	Route::post('/add-profession-category', 'ProfessionController@addProfessionCat');
	Route::get('/add-profession', 'ProfessionController@addProfession');
	Route::post('/add-profession', 'ProfessionController@postAddProfession');

	Route::get('/edit-profession/{prof_id}', 'ProfessionController@editProfession');
	Route::post('/edit-profession', 'ProfessionController@postEditProfession');

	Route::get('/delete-profession/{prof_id}', 'ProfessionController@deleteProfession');

	Route::get('/edit-profession-category/{cat_id}', 'ProfessionController@editProfessionCat');
	Route::post('/edit-profession-category', 'ProfessionController@postEditProfessionCat');

	Route::get('/all-professions', 'ProfessionController@allProfessionsAjax');
    Route::get('/professions/delete-image/{cat_id}','ProfessionController@deleteImg');

    //universities
    Route::get('/universities', 'UniversityController@university_list');
    Route::get('/university-cats', 'UniversityController@categories_list');
    Route::post('/add-university-category', 'UniversityController@addUniversityCat');
    Route::get('/add-university', 'UniversityController@addUniversity');
    Route::post('/add-university', 'UniversityController@postAddUniversity');

    Route::get('/edit-university/{university_id}', 'UniversityController@editUniversity');
    Route::post('/edit-university', 'UniversityController@postEditUniversity');

    Route::get('/delete-university/{university_id}', 'UniversityController@deleteUniversity');

    Route::get('/edit-university-category/{cat_id}', 'UniversityController@editUniversityCat');
    Route::post('/edit-university-category', 'UniversityController@postEditUniversityCat');

    Route::get('/all-universities', 'UniversityController@allUniversitiesAjax');
    Route::get('/universities/delete-image/{university_id}','UniversityController@deleteImg');

	//Courses
	Route::get('/courses-cats', 'CourseController@categories_list');
	Route::post('/add-course-category', 'CourseController@addCourseCat');
	Route::get('/edit-course-category/{cat_id}', 'CourseController@editCourseCat');
	Route::post('/edit-course-category', 'CourseController@postEditCourseCat');

	Route::get('/courses', 'CourseController@courses_list');
	Route::get('/all-courses', 'CourseController@allCoursesAjax');

	Route::get('/add-course', 'CourseController@addCourse');
	Route::post('/add-course', 'CourseController@postAddCourse');

	Route::get('/edit-course/{course_id}', 'CourseController@editCourse');
	Route::post('/edit-course', 'CourseController@postEditCourse');

	Route::get('/delete-course/{course_id}', 'CourseController@deleteCourse');

	//Tests routes
	//KEIRSI
	Route::any("keirsi_edit", "Manage\ManageController@getKeirsiEdit");
	Route::any("keirsi_edit_kaz", "Manage\ManageController@getKeirsiEditKaz");
	Route::post("save_keirsi", "Manage\ManageController@savePostKeirsi");
	Route::post("save_keirsi_kaz", "Manage\ManageController@savePostKeirsiKaz");
	Route::get("user/{id}/keirsi/{date}", "Manage\ManageController@getKeirsiResultsByDate");
	//MAPP
	Route::any("mapp_edit", "Manage\ManageController@getMappEdit");
	Route::any("mapp_edit_kaz", "Manage\ManageController@getMappEditKaz");
	Route::post("save_mapp", "Manage\ManageController@savePostMapp");
	Route::post("save_mapp_kaz", "Manage\ManageController@savePostMappKaz");
	Route::any("user/{id}/mapp/{date}", "Manage\ManageController@getMappResults");
	Route::post("mapp_edit/save_edited", "Manage\ManageController@saveEditedMapp");
	Route::post("mapp_edit/save_editedkaz", "Manage\ManageController@saveEditedMappKAZ");
	Route::any("mapp_tercume", "Manage\ManageController@getMappTraslatePage");
	Route::any("mapp_tercume_all", "Manage\ManageController@getMappAllTraslatePage");
	Route::any("mapp_tercume_jobdesc", "Manage\ManageController@getMappTraslateJobDesc");
	Route::any("mapp_tercume_details", "Manage\ManageController@getMappTraslateJobDetails");
	Route::any("save_mapp_translate", "Manage\ManageController@saveMappTranslate");
	Route::any("save_mapp_translate_all", "Manage\ManageController@saveMappAllTranslate");
	Route::any("save_mapp_translate_jobdesc", "Manage\ManageController@saveMappTranslateJobDesc");
	Route::any("save_mapp_translate_jobdetail", "Manage\ManageController@saveMappTranslateJobDetails");

	Route::any("add_one_translate", "Manage\ManageController@add_one_translate");

	//DISC
	Route::any("disc_edit", "Manage\ManageController@getDiscEdit");
	Route::any("disc_edit_kaz", "Manage\ManageController@getDiscEditKaz");
	Route::post("save_disc", "Manage\ManageController@savePostDisc");
	Route::post("save_disc_kaz", "Manage\ManageController@savePostDiscKaz");
	Route::any("disc_edit_en", "Manage\ManageController@getDiscEditEn");
	Route::post("save_disc_en", "Manage\ManageController@savePostDiscEn");
	Route::any("user/{id}/disc/{date}/{lang}", "Manage\ManageController@getDiscResults");
	Route::any("disc_formulas", "Manage\ManageController@getDiscFormulasEdit");
	Route::any("disc/add_formula", 'Manage\ManageController@discAddFormula');
	Route::post("disc/save_formulas", 'Manage\ManageController@saveDiscFormulas');
	Route::get("disc_text_edit/{text_id}/{lang}", "Manage\ManageController@discTextEdit");
	Route::post("save_disc_text", "Manage\ManageController@saveDiscText");
	Route::post("upload-disc-additional-img", "User\DiscController@uploadDiscAdditionalImg");
	Route::get("disc-additional-images/delete/{image_id}", "User\DiscController@deleteDiscAdditionalImg");

	//HOLL
	Route::any("holl_edit", "HollController@getHollEdit");
	Route::any("holl_edit_kaz", "HollController@getHollKazEdit");
	Route::any("save_holl", "HollController@Save");
	Route::any("save_holl_kaz", "HollController@SaveKaz");
	Route::get("user/{id}/holl/{date}", "HollController@getHollResultsByDate");

	//TOMAS
	Route::any("tomas_edit", "TomasController@getTomasEdit");
	Route::any("tomas_edit_kaz", "TomasController@getTomasKazEdit");
	Route::any("save_tomas", "TomasController@Save");
	Route::any("save_tomas_kaz", "TomasController@SaveKaz");
	Route::get("user/{id}/tomas/{date}", "TomasController@getTomasResultsByDate");

	//Solomin
	Route::any("solomin_edit", "SolominController@getSolominEdit");
	Route::any("solomin_editkaz", "SolominController@getSolominKazEdit");
	Route::any("solomin2_edit", "SolominController@getSolomin2Edit");
	Route::any("solomin2_editkaz", "SolominController@getSolomin2KazEdit");
	Route::any("save_solomin", "SolominController@Save");
	Route::any("save_solominkaz", "SolominController@SaveKaz");
	Route::any("save_solomin2", "SolominController@Save2");
	Route::any("save_solomin2kaz", "SolominController@Save2Kaz");
	Route::get("user/{id}/solomin/{date}", "SolominController@getSolominResultsByDate");

	//Users Routes
	Route::any("add_user", "Manage\ManageController@getAddUser");
	Route::any("add_subadmin", "Manage\ManageController@getAddSubadmin");
	Route::any("all_users", "Manage\ManageController@getAllUsers");
	Route::post("save_new_user", "Manage\ManageController@savePostNewUser");
	Route::post('save_new_subadmin', "Manage\ManageController@savePostNewSubadmin");
	Route::get("user/{id}", "Manage\ManageController@getUserInfo");
	Route::get("ajax_all_users/{type}", "Manage\ManageController@ajax_all_users");
	Route::get("ajax_all_users_subadmin/{type}/{parent_id}", "Manage\ManageController@ajax_all_subadmin_users");
	Route::get("edit_user/{user_id}", "Manage\ManageController@getEditUser");
	Route::get("add_one_more_disc/{user_id}", "Manage\ManageController@add_one_more_disc");
	Route::get("edit_subadmin/{user_id}", "Manage\ManageController@getEditSubadmin");
	Route::post('save_edited_user', "Manage\ManageController@saveEditedUser");
	Route::post('save_edited_subadmin', "Manage\ManageController@saveEditedSubadmin");
	Route::any('download_mapp_results/{user_id}/{lang}', "Manage\ManageController@download_mapp_results");

	//All applications
	Route::any("applications", "Manage\ManageController@allApplications");
	Route::any("app/{id}", "Manage\ManageController@applicationById");

    Route::resource('videos', "Manage\VideoController");
    Route::resource('plans', "Manage\PlanController");
    Route::resource('plan-items', "Manage\PlanItemController");
});

Route::group(['middleware' => ['auth.subadmin'], 'prefix' => 'subadmin'], function () {
	Route::any("/main", "Subadmin\SubadminController@getIndex");
	Route::any("show_stat/{test_name}/{from_date}/{to_date}", "Subadmin\SubadminController@getStatByTest");
	Route::any("/subadmin_info/{subadmin_id}", "Subadmin\SubadminController@getSubadminInfo");

	Route::any("/test_amount", "Subadmin\SubadminController@testAmount");
	Route::any("/profile", "Subadmin\SubadminController@getEditProfile");
	Route::post("save_profile", "Subadmin\SubadminController@saveProfile");

	Route::any("add_user", "Subadmin\SubadminController@getAddUser");
	Route::any("add_subadmin", "Subadmin\SubadminController@getAddSubadmin");
	Route::post("save_new_user", "Subadmin\SubadminController@savePostNewUser");
	Route::post('save_new_subadmin', "Subadmin\SubadminController@savePostNewSubadmin");
	Route::get("edit_subadmin/{user_id}", "Subadmin\SubadminController@getEditSubadmin");
	Route::post('save_edited_subadmin', "Subadmin\SubadminController@saveEditedSubadmin");
	Route::any("all_users", "Subadmin\SubadminController@getAllUsers");
	Route::get("ajax_all_users/{type}", "Subadmin\SubadminController@ajax_all_users");
	Route::get("edit_user/{user_id}", "Subadmin\SubadminController@getEditUser");
	Route::post('save_edited_user', "Subadmin\SubadminController@saveEditedUser");
	Route::get("user/{id}", "Subadmin\SubadminController@getUserInfo");
	Route::get("ajax_all_users_subadmin/{type}/{parent_id}", "Subadmin\SubadminController@ajax_all_subadmin_users");

	Route::get("user/{id}/keirsi/{date}", "Subadmin\SubadminController@getKeirsiResultsByDate");
	Route::any("user/{id}/disc/{date}/{lang}", "Subadmin\SubadminController@getDiscResults");
	Route::any("user/{id}/mapp/{date}", "Subadmin\SubadminController@getMappResults");
	Route::get("user/{id}/holl/{date}", "HollController@getHollResultsByDateForSubadmin");
	Route::get("user/{id}/tomas/{date}", "TomasController@getTomasResultsByDateForSubadmin");
	Route::get("user/{id}/solomin/{date}", "SolominController@getSolominResultsByDateForSubadmin");
});

//Для обычного юзера роуты через простой посредник, проверяющий только авторизацию
Route::group(['middleware' => ['auth.user'], 'prefix' => 'user'], function () {

	//PROFILE
	Route::get('/profile', 'ProfileController@index');
	Route::post('/update-profile', 'ProfileController@updateProfile');

	//INTERNSHIPS
	Route::get('/internship', 'InternshipController@userIndex');
	Route::get('/internship/{internship_id}', "InternshipController@internshipSingle");

	//NEWS
	Route::get('/news', 'NewsController@search');
	Route::get('/news/{news_id}', "NewsController@news_single");

	//SCHEDULE
	Route::get('/schedule', 'ScheduleController@index');
	Route::get('/schedule/{schedule_id}', "ScheduleController@schedule_single");

	//COURSES
	Route::get('/course/search', "CourseController@search");

	//PROFESSIONS
	Route::get('/profession/search', "ProfessionController@search");
	Route::get('/profession/{prof_id}', "ProfessionController@profession");

    //PROFESSIONS
    Route::get('/university/search', "UniversityController@search");
    Route::get('/university/{university_id}', "UniversityController@university");

	//GOALS
	Route::get('/goals', "GoalsController@index");
	Route::post('/goals/add', "GoalsController@addGoal");
	Route::get('/goals/{goal_id}/update-status/{status}', "GoalsController@updateGoalStatus");

	//AUTO MAPP
	Route::any("/send_to_mapp_server", "MappAuto@mapp_send");
	Route::any("/download_mapp_results", "MappAuto@getResults");
	Route::any("/show_mapp_results_online/{lang}", "User\UserController@show_mapp_results_online");
	Route::any("/show_mapp_results_small/{lang}", "User\MappResults@show_mapp_results_small");
	Route::any('/mapp/joblist', "MappAuto@getJobList");
	Route::any('/mapp_job/{sys_id}', "MappAuto@showJob");

	//MAPP RESULTS
	Route::get('/mapp/results/{date}/step1', "User\MappResultsByStep@step1");
	Route::get('/mapp/results/{date}/step2', "User\MappResultsByStep@step2");
	Route::get('/mapp/results/{date}/step3', "User\MappResultsByStep@step3");
	Route::get('/mapp/results/{date}/step4', "User\MappResultsByStep@step4");
	Route::post('/mapp/results/getRuDescForMappStep1', "MappApiController@getRuDescForMappStep1");

	Route::get('/mapp/results/download/{date}', "MappApiController@download");

	Route::get('/mapp/buy', 'User\UserController@getBuyMapp');
	Route::get('/mapp/how-buy', 'User\UserController@getHowBuyMapp');
	Route::post('/mapp/promo', 'User\UserController@getPromoMapp');

	Route::post('/mapp/get-buy-form', 'User\UserController@getBuyMappForm');
	Route::post('/mapp/get-promo-form', 'User\UserController@getPromoMappForm');

	Route::any("/download_mapp_results_rus", "MappApiController@getResultsRus");
	Route::any("/mapp/load_onenet", "User\MappResultsByStep@load_onenet");

	//Main Route
	Route::get("/main", "User\UserController@getIndex");
	Route::get("/tests", "User\UserController@getTestsList");
	Route::get("/tests/mapp", "User\UserController@getTests");

	//Mapp pass page
	Route::get("/before-mapp", "User\UserController@beforeMappStart");
	Route::get("/mapp", "User\UserController@getMapp");
	Route::get("/mappkaz", "User\UserController@getMappKaz");
	Route::post("/mapp/save", "User\UserController@saveResultMapp");
	Route::get("mapp/results", "User\UserController@resultsMapp");

	//KEIRSI pass page
	Route::get("/keirsi", "User\UserController@getKeirsi");
	Route::get("/keirsikaz", "User\UserController@getKeirsiKaz");
	Route::post("/keirsi/save", "User\UserController@saveResultKeirsi");
	Route::get("/keirsi/results", "User\UserController@resultsKeirsi");
	Route::get("/keirsi/{date}", "User\UserController@resultsKeirsiByDate");
	//DISC
	Route::get("/disc", "User\UserController@getDisc");
	Route::get("/disckaz", "User\UserController@getDiscKaz");
	Route::get("/discen", "User\UserController@getDiscEn");
	Route::post("/disc/save", "User\UserController@saveResultDisc");
	Route::any("/disc/results", "User\UserController@resultsDisc");
	Route::get("/disc/results/{date}", "User\UserController@resultsDiscByDate");

	//DISC NEW
	Route::get("/tests/disc-preview", "User\DiscController@discPreview");
	Route::get('/tests/before-disc', "User\DiscController@beforeDisc");
	Route::get('/tests/disc', "User\DiscController@passDisc");
	Route::get('/disc/buy', 'User\DiscController@getBuyDisc');
	Route::post('/disc/open', 'User\DiscController@openDisc');
	Route::post('/disc/open-promo', 'User\DiscController@openDiscByPromocode');

	//HOLL
	Route::any("/holl/{lang}", "HollController@getHoll");
	Route::post("/send_holl", "HollController@Send");
	Route::any("/holl/finish", "HollController@finish");
	Route::any("/holl_res/results", "HollController@results");
	Route::get("/holl_indres/{date}/{lang}", "HollController@getHollResultsByDateForUser");
	//TOMAS
	Route::any("/tomas/{lang}", "TomasController@getTomas");
	Route::any("/send_tomas", "TomasController@Send");
	Route::any("/tomas/finish", "TomasController@finish");
	Route::any("/tomas_res/results", "TomasController@results");
	Route::get("/tomas_indres/{date}/{lang}", "TomasController@getTomasResultsByDateForUser");
	//SOLOMIN
	Route::any("/tests/solomin", "SolominController@getSolomin");
	Route::any("/send_solomin", "SolominController@Send");
	Route::any("/solomin/results", "SolominController@results");
	Route::get("/solomin/results/single-res/{date}", "SolominController@getSolominResultsByDateForUser");
	Route::get("/tests/solomin-preview", "SolominController@getSolominPreview");

});
